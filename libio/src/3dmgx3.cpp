//  created:    2011/08/19
//  filename:   3dmgx3.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    objet integrant la centrale 3dmgx3
//
//
/*********************************************************************/

#include "3dmgx3.h"
#include <stdio.h>
#include "rtdk.h"
#include <math.h>

using namespace std;

//gx3::gx3(string sensor_name,string fd_name,unsigned char command_type):rt_thread(sensor_name)
gx3::gx3(string sensor_name,string fd_name):rt_thread(sensor_name)
{
    struct rtser_config write_config;
    int err = 0;
    is_started=false;
   unsigned char command_type=EULER_ANGULAR;

	write_config.config_mask       = RTSER_SET_BAUD | RTSER_SET_TIMESTAMP_HISTORY,
	write_config.baud_rate         = 921600,
	write_config.timestamp_history = RTSER_DEF_TIMESTAMP_HISTORY,
	// the rest implicitely remains default

    //uart imu
	uart_fd = rt_dev_open(fd_name.c_str(), 0);
	if (uart_fd < 0)
	{
		perror("3DMGX3: open uart");
	}

	// config
	err = rt_dev_ioctl(uart_fd, RTSER_RTIOC_SET_CONFIG, &write_config );
	if (err)
	{
		printf("3DMGX3: error while RTSER_RTIOC_SET_CONFIG, %s\n",strerror(-err));
	}


	int status=rt_mutex_create(&gx3_mutex,"gx3_mutex");
    if(status!=0) printf("3DMGX3:  erreur creation gx3_mutex (%i)\n",status);

    data_rate=500;
    gyro_acc_size=15;
    mag_size=17;
    up_comp=10;
    north_comp=10;
    coning=true;
    disable_magn=true;
    disable_north_comp=false;
    disable_grav_comp=false;

    command=command_type;
    //printf("imu commande %x\n",command);
}

gx3::~gx3()
{
    //printf("destruction 3DMGX3\n");
    is_started=false;

    int status=rt_mutex_delete(&gx3_mutex);
    if(status!=0) printf("3DMGX3: erreur destruction mutex %i\n",status);
}

bool gx3::IsStarted()
{
    return is_started;
}
void gx3::run()
{
    //rt_printf("run 3DMGX3\n");

    //reset IMU to be sure it is at 115200
    rt_printf("gx3:Reset IMU at 921600\n");

    DeviceReset();
    SleepMS(100);
    FlushInput();
    SetBaudrate(115200);

    DeviceBaudrate(921600);

    sampling_settings();
    gyros_bias();

    //rt_printf("firmware version: %i\n",get_firmware_number());

    is_started=true;

    //periode a passer an argument (reglable)
    //ou plutot laisser la periode geree par le cantrale (polling)
    rt_task_set_periodic(NULL, TM_NOW, 2* 1000000);

    while(is_started==true)
    {
        rt_task_wait_period(NULL);
        update_imu();
    }

    DeviceBaudrate(115200);

    rt_dev_close(uart_fd);

}

void gx3::update_imu(void)
{
	uint8 response[67] = {0};
	ssize_t read = 0;
	ssize_t written = 0;

	union float_4uint8 {
            float f;
            uint32 u;
            uint8 b[4];
        }
        float_value;

    rt_mutex_acquire(&gx3_mutex,TM_INFINITE);
    written = rt_dev_write(uart_fd, &command, 1);
    if (written < 0 )
    {
        rt_printf("3DMGX3: error on rt_dev_write update_imu\n");
    } else if (written !=1)
    {
        rt_printf( "3DMGX3: only %d / %d byte transmitted update_imu\n",written, 1);
    }
    imu_time=rt_timer_read();

    if(command==EULER_ANGULAR)
    {
        read = rt_dev_read(uart_fd, &response[0],31);

        if(calc_checksum(response,31)==false)
        {
            rt_printf("3DMGX3: wrong checksum\n");
            return;
        }

        float_value.b[0]=response[4];
        float_value.b[1]=response[3];
        float_value.b[2]=response[2];
        float_value.b[3]=response[1];
        roll_f=float_value.f;
        float_value.b[0]=response[8];
        float_value.b[1]=response[7];
        float_value.b[2]=response[6];
        float_value.b[3]=response[5];
        pitch_f=float_value.f;
        float_value.b[0]=response[12];
        float_value.b[1]=response[11];
        float_value.b[2]=response[10];
        float_value.b[3]=response[9];
        yaw_f=float_value.f;

        float_value.b[0]=response[16];
        float_value.b[1]=response[15];
        float_value.b[2]=response[14];
        float_value.b[3]=response[13];
        droll_f=float_value.f;
        float_value.b[0]=response[20];
        float_value.b[1]=response[19];
        float_value.b[2]=response[18];
        float_value.b[3]=response[17];
        dpitch_f=float_value.f;
        float_value.b[0]=response[24];
        float_value.b[1]=response[23];
        float_value.b[2]=response[22];
        float_value.b[3]=response[21];
        dyaw_f=float_value.f;
    }/*
    if(command==ACC_ANGULAR_MATRIX)
    {
        float M[3][3];
        read = rt_dev_read(uart_fd, &response[0],67);

        if(calc_checksum(response,67)==false)
        {
            rt_printf("3DMGX3: wrong checksum\n");
            return;
        }

        float_value.b[0]=response[4];
        float_value.b[1]=response[3];
        float_value.b[2]=response[2];
        float_value.b[3]=response[1];
        ay_f=9.80665*float_value.f;

        float_value.b[0]=response[8];
        float_value.b[1]=response[7];
        float_value.b[2]=response[6];
        float_value.b[3]=response[5];
        ax_f=-9.80665*float_value.f;
//rt_printf("%f\n",ax_f);
        float_value.b[0]=response[12];
        float_value.b[1]=response[11];
        float_value.b[2]=response[10];
        float_value.b[3]=response[9];
        float_value.f=-float_value.f;
        //float_value.f-=1;
        az_f=9.80665*float_value.f;

        float_value.b[0]=response[16];
        float_value.b[1]=response[15];
        float_value.b[2]=response[14];
        float_value.b[3]=response[13];
        dpitch_f=float_value.f;//centrale tournée de 90°

        float_value.b[0]=response[20];
        float_value.b[1]=response[19];
        float_value.b[2]=response[18];
        float_value.b[3]=response[17];
        droll_f=-float_value.f;//centrale tournée de 90°

        float_value.b[0]=response[24];
        float_value.b[1]=response[23];
        float_value.b[2]=response[22];
        float_value.b[3]=response[21];
        dyaw_f=float_value.f;

        float_value.b[0]=response[28];
        float_value.b[1]=response[27];
        float_value.b[2]=response[26];
        float_value.b[3]=response[25];
        M[0][0]=float_value.f;

        float_value.b[0]=response[32];
        float_value.b[1]=response[31];
        float_value.b[2]=response[30];
        float_value.b[3]=response[29];
        M[0][1]=float_value.f;

        float_value.b[0]=response[36];
        float_value.b[1]=response[35];
        float_value.b[2]=response[34];
        float_value.b[3]=response[33];
        M[0][2]=float_value.f;

        float_value.b[0]=response[40];
        float_value.b[1]=response[39];
        float_value.b[2]=response[38];
        float_value.b[3]=response[37];
        M[1][0]=float_value.f;

        float_value.b[0]=response[44];
        float_value.b[1]=response[43];
        float_value.b[2]=response[42];
        float_value.b[3]=response[41];
        M[1][1]=float_value.f;

        float_value.b[0]=response[48];
        float_value.b[1]=response[47];
        float_value.b[2]=response[46];
        float_value.b[3]=response[45];
        M[1][2]=float_value.f;

        float_value.b[0]=response[52];
        float_value.b[1]=response[51];
        float_value.b[2]=response[50];
        float_value.b[3]=response[49];
        M[2][0]=float_value.f;

        float_value.b[0]=response[56];
        float_value.b[1]=response[55];
        float_value.b[2]=response[54];
        float_value.b[3]=response[53];
        M[2][1]=float_value.f;

        float_value.b[0]=response[60];
        float_value.b[1]=response[59];
        float_value.b[2]=response[58];
        float_value.b[3]=response[57];
        M[2][2]=float_value.f;

        pitch_f=atanf(M[1][2]/M[2][2]);//centrale tournée de 90° pitch_drone=roll_imu
        roll_f=-asinf(-M[0][2]);//centrale tournée de 90° roll_drone=-pitch_imu
        yaw_f=atan2f(M[0][1],M[0][0]);

    }*/


    rt_mutex_release(&gx3_mutex);
}

void gx3::gyros_bias(void)
{
    uint8 response[19] = {0};
    uint8 command[5];
    ssize_t read = 0;
    ssize_t written = 0;

    rt_printf("3DMGX3: calib gyros, do not move the UAV\n");
    command[0]=0xcd;//entete
    command[1]=0xc1;//confirm
    command[2]=0x29;//confirm
    command[3]=0x27;//time MSB
    command[4]=0x10;//time LSB

    rt_mutex_acquire(&gx3_mutex,TM_INFINITE);
    written = rt_dev_write(uart_fd, &command, 5);
    if (written < 0 )
    {
        rt_printf("3DMGX3: error on rt_dev_write gyro biais\n");
    } else if (written !=5)
    {
        rt_printf( "3DMGX3: only %d / %d byte transmitted gyro biais\n",written, 1);
    }

    read = rt_dev_read(uart_fd, &response[0],19);
    rt_mutex_release(&gx3_mutex);

    if(calc_checksum(response,19)==false)
    {
        rt_printf("3DMGX3: wrong checksum\n");
        //return -1;
    }
    rt_printf("3DMGX3: calib gyros done\n");
}

void gx3::sampling_settings(void)
{
    uint8 response[19] = {0};
    uint8 command[20];
    uint8 result;
    ssize_t read = 0;
	ssize_t written = 0;

//rt_printf("reglage IMU\n");

    uint16 rate=1000/data_rate;
    //if(gyro_acc_size>32) gyro_acc_size=32;
    //if(mag_size>32) mag_size=32;
    //if(up_comp>1000) up_comp=1000;
    //if(north_comp>1000) north_comp=1000;

//rt_printf("rate %i\n",rate);

    command[0]=0xdb;//entete
    command[1]=0xa8;//confirm
    command[2]=0xb9;//confirm
    command[3]=1;//change values
    command[4]=(rate>>8)&0xff;//data rate MSB
    command[5]=rate&0xff;//data rate LSB
    result=0;
    if(disable_magn==true) result|=0x01;
    if(disable_north_comp==true) result|=0x04;
    if(disable_grav_comp==true) result|=0x08;
    //rt_printf("byte 6 %x\n",result);
    command[6]=result;
    result=0x01;//Calculate orientation
    if(coning==true) result|=0x02;
    //rt_printf("byte 7 %x\n",result);
    command[7]=result;
    command[8]=gyro_acc_size;//gyro acc filter window
    command[9]=mag_size;//mag filter window
    command[10]=(up_comp>>8)&0xff;//up comp MSB
    command[11]=up_comp&0xff;//up comp LSB
    command[12]=(north_comp>>8)&0xff;//north comp MSB
    command[13]=north_comp&0xff;//north comp LSB
    command[14]=0;//reserved
    command[15]=0;//reserved
    command[16]=0;//reserved
    command[17]=0;//reserved
    command[18]=0;//reserved
    command[19]=0;//reserved

    rt_mutex_acquire(&gx3_mutex,TM_INFINITE);
    written = rt_dev_write(uart_fd, &command, 20);
    if (written < 0 )
    {
        rt_printf("3DMGX3: error on rt_dev_write sampling setting\n");
    } else if (written !=20)
    {
        rt_printf( "3DMGX3: only %d / %d byte transmitted sampling setting\n",written, 20);
    }

    read = rt_dev_read(uart_fd, &response[0],19);
    rt_mutex_release(&gx3_mutex);

    if(calc_checksum(response,19)==false)
    {
        rt_printf("3DMGX3: wrong checksum\n");
    }

    //rt_printf("recu %i\n",read);
    //for(int i=0;i<read;i++) rt_printf("%x ",response[i]);
    //rt_printf("\n");

}

void gx3::DeviceBaudrate(int value)
{
    uint8 response[10] = {0};
    uint8 command[11];
    ssize_t read = 0;
	ssize_t written = 0;

	union int32_4uint8 {
            int32 i;
            uint8 b[4];
    }
    baudrate_value;

    baudrate_value.i=value;
    rt_printf("3DMGX3: baudrate -> %i\n",baudrate_value.i);

    command[0]=0xd9;//entete
    command[1]=0xc3;//confirm
    command[2]=0x55;//confirm
    command[3]=1;//primary uart
    command[4]=1;//chgt temporaire
    command[5]=baudrate_value.b[3];
    command[6]=baudrate_value.b[2];
    command[7]=baudrate_value.b[1];
    command[8]=baudrate_value.b[0];
    command[9]=2;//uart enabled
    command[10]=0;//reserved

    rt_mutex_acquire(&gx3_mutex,TM_INFINITE);
    written = rt_dev_write(uart_fd, command, sizeof(command));
    if (written < 0 )
    {
        rt_printf("3DMGX3: error on rt_dev_write baudrate %s\n",strerror(-written));
    } else if (written !=sizeof(command))
    {
        rt_printf( "3DMGX3: only %d / %d byte transmitted baudrate\n",written, sizeof(command));
    }
    read = rt_dev_read(uart_fd, response,sizeof(response));
    if (read < 0 )
    {
        rt_printf("3DMGX3: error on rt_dev_read baudrate %s\n",strerror(-read));
    } else if (read !=sizeof(response))
    {
        rt_printf( "3DMGX3: only %d / %d byte read baudrate\n",read, sizeof(response));
    }
    rt_mutex_release(&gx3_mutex);

    if(calc_checksum(response,10)==false)
    {
        rt_printf("3DMGX3: wrong checksum\n");
        return ;
    }
    //printf("recu %i\n",read);
    //for(int i=0;i<read;i++) printf("%x ",response[i]);
    //printf("\n");

    SetBaudrate(value);
}

void gx3::SetBaudrate(int baudrate)
{
    struct rtser_config write_config;

	write_config.config_mask       = RTSER_SET_BAUD;
	write_config.baud_rate         = baudrate;
	// the rest implicitely remains default

	// config
	int err = rt_dev_ioctl(uart_fd, RTSER_RTIOC_SET_CONFIG, &write_config );
	if (err)
	{
	    rt_printf("erreur rt_dev_ioctl RTSER_RTIOC_SET_CONFIG (%s)\n",strerror(-err));
	}
}


void gx3::DeviceReset(void) {
    uint8_t command[3];
	ssize_t written = 0;

    command[0]=0xfe;//entete
    command[1]=0x9e;//confirm
    command[2]=0x3a;//confirm

    rt_mutex_acquire(&gx3_mutex,TM_INFINITE);
    written=rt_dev_write(uart_fd,command, sizeof(command));
    rt_mutex_release(&gx3_mutex);
    if(written<0)
    {
        rt_printf("Write error (%s)\n",strerror(-written));
    }
    else if (written != sizeof(command))
    {
        rt_printf("Write error %i/%i\n",written,sizeof(command));
    }
}

void gx3::SetRxTimeout(RTIME timeout_ns)
{
    struct rtser_config write_config;

	write_config.config_mask       = RTSER_SET_TIMEOUT_RX;
	write_config.rx_timeout         = timeout_ns;
	// the rest implicitely remains default

	// config
	int err = rt_dev_ioctl(uart_fd, RTSER_RTIOC_SET_CONFIG, &write_config );
	if (err)
	{
	    rt_printf("erreur rt_dev_ioctl RTSER_RTIOC_SET_CONFIG (%s)\n",strerror(-err));
	}
}

void gx3::FlushInput(void)
{
    char tmp;
    SetRxTimeout(1000000);
    while(rt_dev_read(uart_fd,&tmp,1)==1);
    SetRxTimeout(RTDM_TIMEOUT_INFINITE);
}

int gx3::get_firmware_number(void)
{
    uint8 response[7] = {0};
    uint8 command;
    ssize_t read = 0;
	ssize_t written = 0;
	union int32_4uint8 {
            int32 i;
            uint8 b[4];
    }
    value;


    command=0xe9;//entete

    rt_mutex_acquire(&gx3_mutex,TM_INFINITE);
    written = rt_dev_write(uart_fd, &command, 1);
    if (written < 0 )
    {
        rt_printf("3DMGX3: error on rt_dev_write gyro biais\n");
    } else if (written !=1)
    {
        rt_printf( "3DMGX3: only %d / %d byte transmitted get_firmware_number\n",written, 1);
    }

    read = rt_dev_read(uart_fd, &response[0],7);
    rt_mutex_release(&gx3_mutex);

    if(calc_checksum(response,7)==false)
    {
        rt_printf("3DMGX3: wrong checksum\n");
        return -1;
    }

    value.b[3]=response[1];
    value.b[2]=response[2];
    value.b[1]=response[3];
    value.b[0]=response[4];

    return value.i;

}


bool gx3::calc_checksum(uint8 *buf,int size)
{
    uint16 tChksum,tResponseChksum;

    tChksum = 0;
    for (int i = 0; i < size - 2; i++) tChksum += buf[i];
    // Extract the big-endian checksum from reply
    tResponseChksum = 0;
    tResponseChksum = buf[size - 2] << 8;
    tResponseChksum += buf[size - 1];

    if(tChksum!=tResponseChksum)
        return false;
    else
        return true;
}

float gx3::roll(void)
{
    rt_mutex_acquire(&gx3_mutex,TM_INFINITE);
    float tmp=roll_f;
    rt_mutex_release(&gx3_mutex);
    return tmp;
}

float gx3::pitch(void)
{
    rt_mutex_acquire(&gx3_mutex,TM_INFINITE);
    float tmp=pitch_f;
    rt_mutex_release(&gx3_mutex);
    return tmp;
}

float gx3::yaw(void)
{
    rt_mutex_acquire(&gx3_mutex,TM_INFINITE);
    float tmp=yaw_f;
    rt_mutex_release(&gx3_mutex);
    return tmp;
}

float gx3::droll(void)
{
    rt_mutex_acquire(&gx3_mutex,TM_INFINITE);
    float tmp=droll_f;
    rt_mutex_release(&gx3_mutex);
    return tmp;
}

float gx3::dpitch(void)
{
    rt_mutex_acquire(&gx3_mutex,TM_INFINITE);
    float tmp=dpitch_f;
    rt_mutex_release(&gx3_mutex);
    return tmp;
}

float gx3::dyaw(void)
{
    rt_mutex_acquire(&gx3_mutex,TM_INFINITE);
    float tmp=dyaw_f;
    rt_mutex_release(&gx3_mutex);
    return tmp;
}

float gx3::ax(void)
{
    rt_mutex_acquire(&gx3_mutex,TM_INFINITE);
    float tmp=ax_f;
    rt_mutex_release(&gx3_mutex);
    return tmp;
}

float gx3::ay(void)
{
    rt_mutex_acquire(&gx3_mutex,TM_INFINITE);
    float tmp=ay_f;
    rt_mutex_release(&gx3_mutex);
    return tmp;
}


float gx3::az(void)
{
    rt_mutex_acquire(&gx3_mutex,TM_INFINITE);
    float tmp=az_f;
    rt_mutex_release(&gx3_mutex);
    return tmp;
}


RTIME gx3::time(void)
{
    rt_mutex_acquire(&gx3_mutex,TM_INFINITE);
    RTIME tmp=imu_time;
    rt_mutex_release(&gx3_mutex);
    return tmp;
}
