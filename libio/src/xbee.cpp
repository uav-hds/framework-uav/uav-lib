//  created:    2013/06/14
//  filename:   xbee.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    objet integrant le xbee
//
//
/*********************************************************************/

#include "xbee.h"
#include <stdio.h>
#include "rtdk.h"
#include <stdlib.h>
#include <nucleus/synch.h>

//constantes XBee
#define START_DELIMITER 0x7e
#define TX_REQUEST 0x01
#define TX_STATUS 0x89
#define RX_PACKET 0x81
#define DESTINATION_MSB 0x00
#define DESTINATION_LSB 0x01
#define OPTION 0x00
#define XBEE_MAX_SIZE 99

#define LAST_TRAME 0x80

using namespace std;

xbee::xbee(string fd_name)
{
    struct rtser_config write_config;
    int err = 0;
    is_started=true;
    send_state=SendNewMessage;
    tmp_message=(char*)malloc(65535);
    offset_tmp_message=0;

	write_config.config_mask       = RTSER_SET_BAUD | RTSER_SET_TIMESTAMP_HISTORY ,
	write_config.baud_rate         = 111111,
	write_config.timestamp_history = RTSER_DEF_TIMESTAMP_HISTORY,
	// the rest implicitely remains default

    //uart xbee
	xbee_fd = rt_dev_open(fd_name.c_str(), 0);
	if (xbee_fd < 0)
	{
		perror("open uart");
	}

	// config
	err = rt_dev_ioctl(xbee_fd, RTSER_RTIOC_SET_CONFIG, &write_config );
	if (err)
	{
		printf("error while RTSER_RTIOC_SET_CONFIG, %s\n",strerror(-err));
	}

	//buffers
	int status=rt_buffer_create(&send_buffer,"xbee_send_buf",10*1024,B_FIFO);
    if(status!=0)
    {
        printf("xbee::xbee: rt_buffer_create failed (%s)\n",strerror(-status));
    }
    status=rt_buffer_create(&recv_buffer,"xbee_recv_buf",10*1024,B_FIFO);
    if(status!=0)
    {
        printf("xbee::xbee: rt_buffer_create failed (%s)\n",strerror(-status));
    }

    //reception
    status=rt_task_create(&recv_task, "xbee_rcv", 0, 89, T_JOINABLE);
    if(status!=0) printf("erreur rt_task_create (%s)\n",strerror(-status));

    status=rt_task_start(&recv_task, &recv, (void*)this);
    if(status!=0) printf("erreur rt_task_start (%s)\n",strerror(-status));

    // Initialise the rt_print buffer for this task explicitly
    rt_print_init(512, "xbee_rcv");

    //envoi
    status=rt_task_create(&send_task, "xbee_send", 0, 90, T_JOINABLE);
    if(status!=0) printf("erreur rt_task_create (%s)\n",strerror(-status));

    status=rt_task_start(&send_task, &send, (void*)this);
    if(status!=0) printf("erreur rt_task_start (%s)\n",strerror(-status));

    // Initialise the rt_print buffer for this task explicitly
    rt_print_init(512, "xbee_send");
}

xbee::~xbee()
{
    printf("destruction xbee\n");

    is_started=false;
    int status;

    status=rt_task_join(&recv_task);
    if(status!=0) printf("erreur rt_task_join %s\n",strerror(-status));

    status=rt_task_join(&send_task);
    if(status!=0) printf("erreur rt_task_join %s\n",strerror(-status));

    status=rt_buffer_delete(&send_buffer);
    if(status!=0) printf("xbee::~xbee: rt_buffer_delete failed (%s)\n",strerror(-status));

    status=rt_buffer_delete(&recv_buffer);
    if(status!=0) printf("xbee::~xbee: rt_buffer_delete failed (%s)\n",strerror(-status));

    rt_dev_close(xbee_fd);
    free(tmp_message);
}

void xbee::send(void * arg)
{
    xbee *caller = (xbee*)arg;
    caller->send_impl();
}

void xbee::send_impl()
{
    while(is_started==true)
    {
        if(send_state==SendNewMessage)
        {
            get_message_from_buf();
        }
        if(send_state==SendNewMessage || send_state==ReSendMessage)
        {
            if(send_message_size!=0)
            {/*
                printf("lu buf\n");
                for(size_t i=0;i<6;i++)//send_message_size;i++)
                {
                    printf("%x ",send_message[i]);
                }
                printf("\n");
*/
                ssize_t written = rt_dev_write(xbee_fd, send_message, send_message_size);
                if (written < 0 )
                {
                    printf("error on rt_dev_write xbee %s\n",strerror(-written));
                } else if (written !=(ssize_t)send_message_size)
                {
                    printf( "only %d / %d byte transmitted xbee\n",written, send_message_size);
                }
                else
                {
                    //si id trame !=0, on attend un ack
                    if(send_message[4]!=0) send_state=WaitACK;
                }
            }
        }
        if(send_state==WaitACK)
        {
            //on attend l'ack
            rt_task_sleep(100000000);
        }
    }
}

void xbee::get_message_from_buf()
{
    ssize_t read;
    size_t toread;
    send_message_size=0;

    //read size
    read=rt_buffer_read(&send_buffer,send_message,3,100000000);
    if(read<0)
    {
        if(read!=-ETIMEDOUT)
        {
            printf("xbee::get_message_from_buf: erreur rt_buffer_read (%s)\n",strerror(-read));
        }
        else
        {
            return;
        }
    }
    else if (read != 3)
    {
        printf("xbee::get_message_from_buf: erreur rt_buffer_read %i/3\n",read);
        return;
    }

    //read message
    toread=send_message[1]*256+send_message[2]+1;//+1 pour checksum
    read=rt_buffer_read(&send_buffer,&send_message[3],toread,TM_INFINITE);
    if(read<0)
    {
        printf("xbee::get_message_from_buf: erreur rt_buffer_read (%s)\n",strerror(-read));
        return;
    }
    else if (read != (ssize_t)toread)
    {
        printf("xbee::get_message_from_buf: erreur rt_buffer_read %i/1\n",read);
        return;
    }

    send_message_size=toread+3;
}

void xbee::write(const char* data,ssize_t size,uint8_t id_trame)
{
    char buf[255];
    char* data_ptr=(char*)data;
    ssize_t frame_size;

    if(id_trame>127)
    {
        printf("erreur trame incorrect %i\n",id_trame);
    }

    while(size!=0)
    {
        if(size>XBEE_MAX_SIZE)
        {
            frame_size=XBEE_MAX_SIZE;
        }
        else
        {
            frame_size=size;
        }
        size-=frame_size;

        buf[0]=START_DELIMITER;
        buf[1]=0;//taille MSB, 0 à priori
        buf[2]=frame_size+10-4;//taille LSB
        //le checksum commence a partir de la
        buf[3]=TX_REQUEST;
        buf[4]=id_trame;
        buf[5]=DESTINATION_MSB;
        buf[6]=DESTINATION_LSB;
        buf[7]=OPTION;
        buf[8]=id_trame;
        if(size==0) buf[8]|=LAST_TRAME;

        memcpy(&buf[9],data_ptr,frame_size);
        data_ptr+=frame_size;

        buf[9+frame_size]=calc_checksum(&buf[3],frame_size+6);
/*
        for(int i=0;i<frame_size+10;i++)
        {
            printf("%x ",buf[i]);
        }
        printf("\n");
*/
        ssize_t written=rt_buffer_write(&send_buffer,buf,frame_size+10,TM_NONBLOCK);
        if(written<0)
        {
            printf("xbee::write: erreur rt_buffer_write (%s)\n",strerror(-written));
        }
        else if (written != frame_size+10)
        {
            printf("xbee::write: erreur rt_buffer_write %i/1\n",written);
        }
    }
}

void xbee::recv(void * arg)
{
    xbee *caller = (xbee*)arg;
    caller->recv_impl();
}

void xbee::recv_impl()
{
    while(is_started==true)
    {
        //recpetion xbee
        recv_message_from_xbee();
        if(recv_message_size!=0)
        {/*
            printf("lu xbee\n");
            for(size_t i=0;i<recv_message_size;i++)
            {
                printf("%x ",recv_message[i]);
            }
            printf("\n");
*/
            switch(recv_message[3])
            {
                case TX_STATUS:
                    if(recv_message[4]!=send_message[4]) printf("xbee::recv, probleme TX_STATUS, trame inattendue!\n");
                    if(recv_message[5]==0)
                    {
                        send_state=SendNewMessage;
                    }
                    else
                    {
                        send_state=ReSendMessage;
                    }
                    break;
                case RX_PACKET:
                {
                    memcpy(&tmp_message[3]+offset_tmp_message,&recv_message[9],recv_message_size-10);
                    offset_tmp_message+=recv_message_size-10;
                    if((recv_message[8]&LAST_TRAME)==LAST_TRAME)
                    {
                        tmp_message[0]=((offset_tmp_message+2)>>8)&0xff;//id et checksum
                        tmp_message[1]=(offset_tmp_message+2)&0xff;
                        tmp_message[2]=recv_message[8]&(~LAST_TRAME);
                        tmp_message[offset_tmp_message+3]=calc_checksum(&tmp_message[2],offset_tmp_message+1);

                        ssize_t written=rt_buffer_write(&recv_buffer,tmp_message,offset_tmp_message+4,TM_NONBLOCK);
                        if(written<0)
                        {
                            printf("xbee::recv: erreur rt_buffer_write (%s)\n",strerror(-written));
                        }
                        else if (written != (ssize_t)offset_tmp_message+4)
                        {
                            printf("xbee::recv: erreur rt_buffer_write %i/1\n",written);
                        }
/*
                        printf("lu xbee->buf\n");
                        for(size_t i=0;i<offset_tmp_message+4;i++)
                        {
                            printf("%x ",tmp_message[i]);
                        }
                        printf("\n");*/
                        offset_tmp_message=0;
                    }
                    break;
                }
                default:
                    printf("xbee::recv, id %x non gérée\n",recv_message[3]);
                    break;
            }
        }
    }
}

void xbee::recv_message_from_xbee()
{
    ssize_t read;
    size_t toread;
    recv_message_size=0;

    //read header
    set_timeout(100000000);

    read = rt_dev_read(xbee_fd, recv_message,1);
    if (read < 0 )
    {
        if(read!=-ETIMEDOUT)
        {
            printf("xbee::recv_message_from_xbee: error on rt_dev_read %i %s\n",read,strerror(-read));
            return;
        }
        else
        {
            return;
        }
    }
    else if (read != 1)
    {
        printf("xbee::recv_message_from_xbee: erreur rt_dev_read %i/1\n",read);
        return;
    }

    //read size
    set_timeout(RTSER_TIMEOUT_INFINITE);
    read = rt_dev_read(xbee_fd, &recv_message[1],2);
    if(read<0)
    {
        printf("xbee::recv_message_from_xbee: erreur rt_dev_read (%s)\n",strerror(-read));
    }
    else if (read != 2)
    {
        printf("xbee::recv_message_from_xbee: erreur rt_dev_read %i/1\n",read);
        return;
    }
    toread=recv_message[1]*256+recv_message[2]+1;//+1 pour checksum

    //read message
    read=rt_dev_read(xbee_fd,&recv_message[3],toread);
    if(read<0)
    {
        printf("xbee::recv_message_from_xbee: erreur rt_dev_read (%s)\n",strerror(-read));
    }
    else if (read != (ssize_t)toread)
    {
        printf("xbee::recv_message_from_xbee: erreur rt_dev_read %i/1\n",read);
    }

    if(calc_checksum(&recv_message[3],toread)!=0)
    {
        printf("xbee::recv_message_from_xbee: erreur checksum\n");
        return;
    }

    recv_message_size=toread+3;
}

ssize_t xbee::read(char* data,ssize_t size)
{
    ssize_t read;
    size_t toread;
    char tmp[2];

    //read size
    read=rt_buffer_read(&recv_buffer,tmp,2,TM_NONBLOCK);
    if(read<0)
    {
        if(read!=-EWOULDBLOCK)
        {
            printf("xbee::read: erreur rt_buffer_read (%s)\n",strerror(-read));
            return -1;
        }
        else
        {
            return 0;
        }
    }
    else if (read != 2)
    {
        printf("xbee::read: erreur rt_buffer_read %i/2\n",read);
        return -1;
    }
    toread=tmp[0]*256+tmp[1];

    //read message
    read=rt_buffer_read(&recv_buffer,data,toread,TM_NONBLOCK);
    if(read<0)
    {
        printf("xbee::read: erreur rt_buffer_read (%s)\n",strerror(-read));
        return -1;
    }
    else if (read != (ssize_t)toread)
    {
        printf("xbee::read: erreur rt_buffer_read %i/1\n",read);
        return -1;
    }

    return toread;
}

uint8_t xbee::calc_checksum(char* buf,ssize_t size)
{
    uint8_t result=0;
    for(int i=0;i<size;i++) result+=buf[i];
    return 0xff-result;
}

void xbee::set_timeout(nanosecs_rel_t rx_timeout)
{
    struct rtser_config write_config;

    write_config.config_mask       = RTSER_SET_TIMEOUT_RX;
	write_config.rx_timeout         = rx_timeout;
	// the rest implicitely remains default

	// config
	int err = rt_dev_ioctl(xbee_fd, RTSER_RTIOC_SET_CONFIG, &write_config );
	if (err)
	{
		printf("error while RTSER_RTIOC_SET_CONFIG, %s\n",strerror(-err));
	}
}
