//  created:    2013/06/14
//  filename:   xbee.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    objet integrant le xbee
//
//
/*********************************************************************/

#ifndef XBEE_H
#define XBEE_H

#include "typedef.h"
#include <string>
#include <native/buffer.h>
#include <rtdm/rtserial.h>
#include <native/task.h>

class xbee
{
    public:
        xbee(std::string fd_name);
        ~xbee();
        void write(const char* data,ssize_t size,uint8_t id_trame);
        ssize_t read(char* data,ssize_t size);

    private:
        int xbee_fd;
        bool is_started;
        RT_BUFFER send_buffer,recv_buffer;
        char send_message[255];
        char recv_message[255];
        char *tmp_message;
        size_t offset_tmp_message;
        size_t send_message_size,recv_message_size;
        RT_TASK recv_task,send_task;
        typedef enum {SendNewMessage,ReSendMessage, WaitACK} state;
        state send_state;

        void get_message_from_buf();
        void recv_message_from_xbee();
        uint8_t calc_checksum(char* buf,ssize_t size);
        static void recv(void * arg);
        void recv_impl();
        static void send(void * arg);
        void send_impl();
        void set_timeout(nanosecs_rel_t rx_timeout);
};

#endif // XBEE_H
