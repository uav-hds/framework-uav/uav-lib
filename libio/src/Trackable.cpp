//  created:    2013/03/21
//  filename:   Trackable.cpp
//
//  author:     C�sar Richard
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    objet trackable
//
//
/*********************************************************************/

#include "Trackable.h"
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <native/timer.h>
#include <vrpn_Connection.h>
#include <math.h>
#include <cmath>

using namespace std;

void Trackable::initialize(string name, vrpn_Connection *connection)
/// Methode interne commune a tous les contructeurs
{
    //init pipe
    int status=rt_pipe_create(&rt_pipe, name.c_str(), P_MINOR_AUTO,256);
    if(status!=0) printf("Trackable::initialize, %s: rt_pipe_create failed (%s)\n", name.c_str(),strerror(status));

    nrt_pipe=-1;
    string devname= "/proc/xenomai/registry/native/pipes/"  + name;
    while(nrt_pipe<0)
    {
        nrt_pipe = open(devname.c_str(), O_RDWR);
        if (nrt_pipe < 0 && errno!=ENOENT) printf("Trackable::initialize, %s: err open nrt_pipe (%s)\n",name.c_str(),strerror(-errno));
        usleep(1000);
    }

    last_rt_time=rt_timer_read();

	// Inititalisation des tableaux internes avec 0
	for(int i=0;i<=2;i++){
		for(int j=0;j<=2;j++)
			matrix_rot[i][j] = 0;
	}

	for(int i=0;i<=3;i++){
		quaternions_tab[i] = 0;
	}

	for(int i=0;i<=2;i++){
		euler_tab[i] = 0;
	}

	for(int i=0;i<=2;i++){
		position_tab[i] = 0;
	}

	// Initialisations des variables internes
	this->name = name;
	this->lastPacketTime.tv_sec = 0;
	this->lastPacketTime.tv_usec = 0;
	this->tracker = new vrpn_Tracker_Remote(name.c_str(), connection);
	this->tracker->register_change_handler(this,changedCallback);

	init=false;

	printf("Tracker created for %s\n", this->getName().c_str());
}

Trackable::Trackable(string name, vrpn_Connection *connection, vrpn_TRACKERCHANGEHANDLER changedCallback)
/// Constructeur permettant de fournir sa propre methode a appeller pour le ChangeHandler
{
	initialize(name,connection);
	if(changedCallback != NULL){
		this->changedCallback = changedCallback;
	}

}

Trackable::Trackable(string name, vrpn_Connection *connection)
/// Constructeur "normal"
{
	this->changedCallback = Trackable::handle_pos;
	initialize(name,connection);
}

Trackable::~Trackable(void)
{
    printf("delete tracker\n");
	this->tracker->unregister_change_handler(this,this->changedCallback);
	delete tracker;

	int status=rt_pipe_delete(&rt_pipe);
    if(status!=0) printf("erreur destruction pipe %i\n",status);
    printf("delete tracker ok\n");
}

string Trackable::getName(void)
{
	return this->name;
}

void Trackable::mainloop(){
	this->tracker->mainloop();
}

void Trackable::setShutup(bool flag){
	this->tracker->shutup=flag;
}

bool Trackable::getShutup(){
	return this->tracker->shutup;
}

struct timeval Trackable::getLastPacketTime(){
	return lastPacketTime;
}

void Trackable::setLastPacketTime(struct timeval time){
	lastPacketTime= time;
}

void Trackable::setQuaternions(vrpn_float64 quaternions[4]){
	for(int i=0;i<=3;i++){
		quaternions_tab[i] = quaternions[i];
	}
	*quaternions_tab = *quaternions;
}

vrpn_float64* Trackable::getQuaternions(){
	return quaternions_tab;
}

void Trackable::setMatrix(vrpn_float64 matrix[3][3]){
	for(int i=0;i<=2;i++){
		for(int j=0;j<=2;j++)
			matrix_rot[i][j] = matrix[i][j];
	}
}

vrpn_float64*** Trackable::getMatrix(){
	return (vrpn_float64***)*matrix_rot;
}

void Trackable::getMatrix(vrpn_float64 matrix[3][3]){
	for(int i=0;i<=2;i++){
		for(int j=0;j<=2;j++)
			matrix[i][j] = matrix_rot[i][j];
	}
}

void Trackable::setEuler(vrpn_float64 euler[3]){
	for(int i=0;i<=2;i++){
		euler_tab[i] = euler[i];
	}
}

vrpn_float64* Trackable::getEuler(){
	return euler_tab;
}

void Trackable::setPosition(vrpn_float64 position[3]){
	for(int i=0;i<=2;i++){
		position_tab[i] = position[i];
	}
}

void Trackable::setVitesse(vrpn_float64 position[3],struct timeval time){

    struct timeval tvd;
    timersub(&time,&last_time,&tvd);

    if(tvd.tv_sec!=0 || tvd.tv_usec!=0)
    {
        if(init==true)
        {
            for(int i=0;i<=2;i++){
                vitesse_tab[i] = (position[i]-old_position[i])/((float)tvd.tv_usec/1000000.+(float)tvd.tv_sec);
            }
        }
        else
        {
             for(int i=0;i<=2;i++){
                vitesse_tab[i] = 0;
            }
        }
    }
    else
    {
        printf("prob diff temps nulle\n");
    }

    init=true;
	memcpy(&old_position[0],&position[0],3*sizeof(vrpn_float64));
	last_time=time;
}

void Trackable::getEuler(vrpn_float64* yaw,vrpn_float64* pitch,vrpn_float64* roll){
	*yaw = euler_tab[0];
	*pitch = euler_tab[1];
	*roll = euler_tab[2];
}

void Trackable::getEuler(float* yaw,float* pitch,float* roll){
	*yaw = (float)euler_tab[0];
	*pitch =(float)euler_tab[1];
	*roll = (float)euler_tab[2];
}

void Trackable::getEuler(vrpn_float64* yaw,vrpn_float64* pitch,vrpn_float64* roll,struct timeval* time){
	*yaw = euler_tab[0];
	*pitch = euler_tab[1];
	*roll = euler_tab[2];
	*time = lastPacketTime;
}

vrpn_float64* Trackable::getPosition(){
	return position_tab;
}

void Trackable::getPosition(vrpn_float64* x,vrpn_float64* y,vrpn_float64* z){
	*x = position_tab[0];
	*y = position_tab[1];
	*z = position_tab[2];
}

void Trackable::getPosition(float* x,float* y,float* z){
    *x = (float)position_tab[0];
	*y = (float)position_tab[1];
	*z = (float)position_tab[2];
}

void Trackable::getPosition(vrpn_float64* x,vrpn_float64* y,vrpn_float64* z,struct timeval* time){
	*x = position_tab[0];
	*y = position_tab[1];
	*z = position_tab[2];
	*time = lastPacketTime;
}

void Trackable::getVitesse(vrpn_float64* x,vrpn_float64* y,vrpn_float64* z){
	*x = vitesse_tab[0];
	*y = vitesse_tab[1];
	*z = vitesse_tab[2];
}

void Trackable::getVitesse(float* x,float* y,float* z){
	*x = (float)vitesse_tab[0];
	*y = (float)vitesse_tab[1];
	*z = (float)vitesse_tab[2];
}

void Trackable::QtoM(){
	/// Methode de conversion des quaternions vers la matrice de rotation

	//matrix_rot[colonne][ligne]

	vrpn_float64* q=this->quaternions_tab;
	vrpn_float64 x2,y2,z2,w2;
	x2=pow(q[0],2);
	y2=pow(q[1],2);
	z2=pow(q[2],2);
	w2=pow(q[3],2);

	this->matrix_rot[0][0]= x2 + y2 - z2 - w2;
	this->matrix_rot[0][1]= 2 * (q[1] * q[2] + q[0] * q[3]);
	this->matrix_rot[0][2]= 2 * (q[1] * q[3] - q[0] * q[2]);

	this->matrix_rot[1][0]= 2 * (q[1] * q[2] - q[0] * q[3]);
	this->matrix_rot[1][1]= x2 - y2 + z2 - w2;
	this->matrix_rot[1][2]= 2 * (q[2] * q[3] + q[0] * q[1]);

	this->matrix_rot[2][0]= 2 * (q[1] * q[3] + q[0] * q[2]);
	this->matrix_rot[2][1]= 2 * (q[2] * q[3] - q[0] * q[1]);
	this->matrix_rot[2][2]= x2 - y2 - z2 + w2;

}

void Trackable::QtoE(){
	/// Methode de conversion des quaternions vers les angles d'Euler
	//0=>Yaw
	//1=>Pitch
	//2=>Roll
	this->euler_tab[0]=atan2(2*(this->quaternions_tab[1]*this->quaternions_tab[3] + this->quaternions_tab[0]*this->quaternions_tab[2]),pow(this->quaternions_tab[0],2) - pow(this->quaternions_tab[1],2) - pow(this->quaternions_tab[2],2) + pow(this->quaternions_tab[3],2));//*180/M_PI;//yaw
	this->euler_tab[1]=-asin(-2*(this->quaternions_tab[2]*this->quaternions_tab[3] - this->quaternions_tab[0]*this->quaternions_tab[1]));//*180/M_PI;//pitch
	this->euler_tab[2]=(M_PI-abs(atan2(2*(this->quaternions_tab[1]*this->quaternions_tab[2] + this->quaternions_tab[0]*this->quaternions_tab[3]),pow(this->quaternions_tab[0],2) - pow(this->quaternions_tab[1],2) + pow(this->quaternions_tab[2],2) - pow(this->quaternions_tab[3],2))));//*180/M_PI;//roll

    //remet dans le rep�re du drone:
    this->euler_tab[0]=-this->euler_tab[0];
}

void VRPN_CALLBACK Trackable::handle_pos (void *userdata, const vrpn_TRACKERCB t)
{
	Trackable* trackable= reinterpret_cast<Trackable*>(userdata);
	//if(t.msg_time.tv_usec > trackable->getLastPacketTime()){
//printf("track nrt %s\n",trackable->getName().c_str());
        write(trackable->nrt_pipe,&t,sizeof(vrpn_TRACKERCB));

	//}else{
	//	printf("%s //Packet retardataire !\n",trackable->getName());
	//}
}

RTIME Trackable::Update(){

    vrpn_TRACKERCB t;
//printf("track rt %s\n",getName().c_str());
    ssize_t nb_read;

    nb_read=rt_pipe_read(&rt_pipe,&t,sizeof(vrpn_TRACKERCB),TM_NONBLOCK);
    if(nb_read==sizeof(vrpn_TRACKERCB))
    {
        while(nb_read>0) nb_read=rt_pipe_read(&rt_pipe,&t,sizeof(vrpn_TRACKERCB),TM_NONBLOCK);

  //      printf("track rt %s ok\n",getName().c_str());
        last_rt_time=rt_timer_read();

        //Mise en m�moire de la date du paquet
		setLastPacketTime(t.msg_time);

        //Mise en m�moire des quaternions
		vrpn_float64 quat[4];
		quat[0]=t.quat[0];
		quat[1]=t.quat[1];
		quat[2]=t.quat[2];
		quat[3]=t.quat[3];
		setQuaternions(quat);

		//Mise en m�moire de la position (en m)
		vrpn_float64 pos[3];
		pos[0]=-t.pos[2];
		pos[1]=-t.pos[0];
		pos[2]=t.pos[1];
		setPosition(pos);
		setVitesse(pos,t.msg_time);

		//calcul et mise en m�moire de la matrice de rotation
		QtoM();

		//calcul et mise en m�moire de la des angles d'Euler
		QtoE();

    }

    return last_rt_time;
}
