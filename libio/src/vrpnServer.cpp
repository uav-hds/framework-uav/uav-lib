//  created:    2012/11/08
//  filename:   vrpnServer.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    objet se connectant au serveur vrpn
//
//
/*********************************************************************/

#include "vrpnServer.h"
#include <stdio.h>
#include <unistd.h>
#include <limits.h>

using namespace std;

vrpnServer::vrpnServer(string address,int us_period)
{
    continuer=true;
    timeout=us_period;

    connection = vrpn_get_connection_by_name(address.c_str());

}

vrpnServer::~vrpnServer()
{
    //printf("delete vrpnServer\n");
    continuer=false;
    pthread_join(thread, NULL);

    for(unsigned int i=0;i<trackables.size();i++) delete trackables.at(i);
    //trackables.clear();

    delete connection;
    //printf("delete vrpnServer ok\n");
}

Trackable* vrpnServer::AddTrackable(string name)
{
    Trackable* tmp;
	tmp = new Trackable(name.c_str(),connection);
	tmp->setShutup(true);

    trackables.push_back(tmp);

    return tmp;
}

void vrpnServer::Start(void)
{
    /*
    // Initialize thread creation attributes
    pthread_attr_t attr;
    if(pthread_attr_init(&attr) != 0)
    {
        printf("vrpnServer erreur pthread_attr_init\n");
    }

    if(pthread_attr_setstacksize(&attr, PTHREAD_STACK_MIN) != 0)
    {
       printf("vrpnServer erreur pthread_attr_setstacksize\n");
    }
*/
    //start thread
    if(pthread_create (&thread, NULL, loop,  (void*)this) < 0)
    {
        printf("vrpnServer: pthread_create failed\n");
    }
}

void* vrpnServer::loop(void * arg)
{
    vrpnServer *caller = (vrpnServer*)arg;

    while(caller->continuer==true)
    {
        //printf("vrpn main\n");
        caller->connection->mainloop();
        // printf("vrpn trackable\n");
        //for(unsigned int i=0;i<caller->trackables.size();i++) caller->trackables.at(i)->mainloop();

        // printf("vrpn ok\n");

        usleep(caller->timeout);
    }
}
