#include "i2c.h"
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string.h>
#include <fstream>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <rtdm/rti2c.h>
#include <rtdk.h>
#include <native/timer.h>

using namespace std;

int i2c_fd;
bool motors_started;
int time_sec; // total flight time in seconds

void init_i2c(char* filename)
{
    //init I2C
    struct rti2c_config write_config;

    printf("Openning i2c: %s\n",filename);
    i2c_fd = rt_dev_open( filename, 0);
    if(i2c_fd < 0)
	{
		perror("open i2c");
	}

	write_config.config_mask       = RTI2C_SET_BAUD|RTI2C_SET_TIMEOUT_RX|RTI2C_SET_TIMEOUT_TX;
	write_config.baud_rate         = 400000;
	write_config.rx_timeout        = 500000;//1000000000;
	write_config.tx_timeout        = 10000000;//1000000000;
	// the rest implicitely remains default
    //printf("I2C baudrate %i\n",write_config.baud_rate);

	// config
	int err = rt_dev_ioctl(i2c_fd, RTI2C_RTIOC_SET_CONFIG, &write_config );
	if (err)
	{
		printf("error while RTI2C_RTIOC_SET_CONFIG, %s\n",strerror(-err));
	}

    //flight time
	motors_started=false;
	FILE *file;

    file=fopen("/etc/flight_time","r");
    if (file == NULL)
    {
        printf ("fichier d'info de vol vide\n");
        time_sec=0;
    }
    else
    {
        char ligne[32];
        fgets(ligne, 32, file);
        time_sec=atoi(ligne);
        printf ("temps de vol total: %is = %imin = %ih\n",time_sec,time_sec/60,time_sec/3600);
        fclose(file);
    }
}

void close_i2c(void)
{
    nb_motors();//pour afficher voltage
    rt_dev_close(i2c_fd);
}


/*
** ===================================================================
**    r�gle les PWMs par I2C
**
** ===================================================================
*/

int nb_motors()
{
    int nb=0;
    ssize_t read,nb_write;
    uint8_t value[3];
    uint8_t tx[2];

    for(int i=0;i<8;i++)
    {
        //printf("test %i\n",i);
        set_slave(0x29+i);
        tx[0]=0;
        tx[1]=16+8;//16+8 pour recuperer la vitesse

        nb_write = rt_dev_write(i2c_fd,tx, 2);

        if (nb_write != sizeof(tx))
        {
            //printf("ecrit %i moteur %i\n",nb_write,i);
            continue;
        }
        nb++;
    }

    for(int i=0;i<8;i++)
    {
        set_slave(0x29+i);
        read = rt_dev_read(i2c_fd, value, 3);

        if (read != sizeof(value))
        {
            //printf("lu %i moteur %i\n",read,i);
            continue;
        }
    }
    // voltage value should be the same for all motor drivers. We pick up the last one.
    printf("Voltage %f\n",value[2]/10.);

    return nb;
}

void set_motor(uint16 value,uint8 id)
{
    int nb_write;
    unsigned char tx[2];

    set_slave(0x29+id);
    tx[0]=(unsigned char)(value>>3);//msb
    tx[1]=(value&0x07);//lsb
//rt_printf("%i %i %i\n",id,tx[0],tx[1]);
    nb_write = rt_dev_write(i2c_fd,&tx, 2);

    if (nb_write != 2)
    {
        rt_printf("ecrit %i moteur %i\n",nb_write,id);
    }
}

void set_motors(uint16* moteurs,int nb_moteurs)
{
    static RTIME start_time;
    int cpt=0;

    // How many motors are required to work?
    for(int i=0;i<nb_moteurs;i++)
    {
        if(moteurs[i]!=0) cpt++;
    }
    // We start all the motors together, that is if they have all non zero values.
    if(cpt==nb_moteurs && motors_started==false)
    {
        motors_started=true;
        start_time = rt_timer_read();
        rt_printf("start moteurs\n");
    }

    // How many motors are required to stop
    cpt=0;
    for(int i=0;i<nb_moteurs;i++)
    {
        if(moteurs[i]==0) cpt++;
    }
    // We stop all the motors together, that is if they have all zero values.
    if(cpt==nb_moteurs && motors_started==true)
    {
        RTIME now= rt_timer_read();
        int t_sec;
        FILE *file;
        char ligne[32];

        motors_started=false;
        rt_printf("stop moteurs\n");

        t_sec=(now - start_time)/1000000000;
        time_sec+=t_sec;

        rt_printf ("temps de vol: %is = %imin\n",t_sec,t_sec/60);
        rt_printf ("temps de vol total: %is = %imin = %ih\n",time_sec,time_sec/60,time_sec/3600);

        file=fopen("/etc/flight_time","w");
        if (file == NULL)
        {
            printf ("Erreur a l'ouverture du fichier d'info vol\n");
        }
        else
        {
            sprintf(ligne,"%i",time_sec);
            fputs(ligne,file);
            fclose(file);
        }
    }

    for(int i=0;i<nb_moteurs;i++) set_motor(moteurs[i],i);
}

void stop_pwms(void)
{


}

/*
** ===================================================================
**     ultra son
**
**
** ===================================================================
*/

void init_us(uint8 range,uint8 max_gain)
{
    int  nb_write;
    uint8 tx[2];

    set_slave(0x70);

    tx[0]=2;//range register
    tx[1]=range;//range*43+43=dist max en mm
    nb_write = rt_dev_write(i2c_fd, &tx, 2);

    if (nb_write != 2)
    {
        rt_printf("SFR08: ecrit %i/2 (range)\n",nb_write);
    }

    tx[0]=1;//max gain register
    tx[1]=max_gain;
    nb_write = rt_dev_write(i2c_fd, &tx, 2);

    if (nb_write != 2)
    {
        rt_printf("SFR08: ecrit %i/2 (max_gain)\n",nb_write);
    }
    rt_printf("SFR08: init ok\n");
}

// sends an ultrasound echo
int start_capture_us(void)
{
    int  nb_write;
    uint8 tx[2];
//printf("start capture\n");
    set_slave(0x70);

    tx[0]=0;//command register
    tx[1]=82;//ranging mode in usec
    nb_write = rt_dev_write(i2c_fd, &tx, 2);

    if (nb_write != 2)
    {
        rt_printf("SFR08: ecrit %i/2 (start)\n",nb_write);
        return -1;
    }
    return 0;
}

// get ultrasound value in meter (must follow a call to start_capture_us)
float get_value_us()
{
    int  nb_write,nb_read;
    uint8 tx[2],rx[2];
    //static float old=0;

    set_slave(0x70);

    tx[0]=2;
    nb_write = rt_dev_write(i2c_fd, &tx, 1);
//rt_printf("get wr %i\n",nb_write);
    if (nb_write != 1)
    {
        rt_printf("SFR08: write %i/1 (get) \n",nb_write);
        return -1;
    }

    nb_read = rt_dev_read(i2c_fd, rx, 2);
//rt_printf("get rd %i %x %x\n",nb_read,rx[0],rx[1]);
    if (nb_read != 2)
    {
        rt_printf("SFR08: read %i/2 (get) \n",nb_read);
        return -1;
    }
//rt_printf("%x %x\n",rx[0],rx[1]);
//if(rx[0]==255 && rx[1]==255) rt_printf("read ffff\n");
//rt_printf("%f\n",0.000001*(float)(rx[0]*256+rx[1])*344/2);
    return 0.000001*(float)(rx[0]*256+rx[1])*344/2;//d=v*t; v=344m/s, t=t usec/2 (aller retour)

}


void set_slave(unsigned short address)
{
	int err = rt_dev_ioctl(i2c_fd, RTI2C_RTIOC_SET_SLAVE, &address);
    if (err)
        rt_printf("error on RTI2C_RTIOC_SET_SLAVE\n");
}

