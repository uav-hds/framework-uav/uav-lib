//  created:    2012/05/26
//  filename:   dbt.h
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:   objet interface pour l'ecriture dbt
//
//
/*********************************************************************/

#include "dbt.h"
#include <stdio.h>
#include <cstdlib>
#include <unistd.h>
#include "rtdk.h"
#include "kernel/io_hdfile.h"
#include "kernel/err.h"

using namespace std;

dbt::dbt(string name,int type, int size)
{
    file_name=name;
    id=type;
    taille=size;

    int status;

    //creation pipe
    status=rt_pipe_create(&dbt_pipe, name.c_str(), P_MINOR_AUTO,size*100);
    if(status!=0) printf("dbt::%s: rt_pipe_create failed (%i:%s)\n",name.c_str(),status,strerror(status));

    continuer_dbt=true;

    // Initialize thread creation attributes
    pthread_attr_t attr;
    if(pthread_attr_init(&attr) != 0)
    {
        printf("dbt::%s: erreur pthread_attr_init\n",name.c_str());
    }

    if(pthread_attr_setstacksize(&attr, PTHREAD_STACK_MIN) != 0)
    {
       printf("dbt::%s: erreur pthread_attr_setstacksize\n",name.c_str());
    }

    //start receive thread (user side)
    if(pthread_create (&write_thread,  &attr, write_dbt,  (void*)this) < 0)
    {
        printf("dbt::%s: pthread_create failed\n",name.c_str());
    }

}

dbt::~dbt()
{
    continuer_dbt=false;
    pthread_join(write_thread, NULL);

    int status=rt_pipe_delete(&dbt_pipe);
    if(status!=0) printf("erreur destruction pipe %i\n",status);

}

void dbt::write(char* buf,RTIME time)
{
    ssize_t written;
    //printf("dbt write %s %i\n",file_name.c_str(),taille);

    road_time_t temps=(road_time_t)(time/1000);
    road_timerange_t tr=(road_timerange_t)(time%1000);

    written=rt_pipe_write(&dbt_pipe,buf,taille,P_NORMAL);
    if(written!=taille) printf("ecrit %i/%i\n",written,taille);
     written=rt_pipe_write(&dbt_pipe,(char*)&temps,sizeof(road_time_t),P_NORMAL);
    if(written!=sizeof(road_time_t)) printf("ecrit %i/%i\n",written,sizeof(road_time_t));
     written=rt_pipe_write(&dbt_pipe,(char*)&tr,sizeof(road_timerange_t),P_NORMAL);
     if(written!=sizeof(road_timerange_t)) printf("ecrit %i/%i\n",written,sizeof(road_timerange_t));
    //printf("dbt write ok\n");
}

void* dbt::write_dbt(void * arg)
{
    dbt *dbt_caller = (dbt*)arg;
    int pipe_fd=-1;
    fd_set set;
    struct timeval timeout;
    char* buf=NULL;
    int rv;
    ssize_t nb_read;
    string devname;
    hdfile_t *dbtFile;
    road_time_t time;
    road_timerange_t tr;


    buf=(char*)malloc(dbt_caller->taille+2*sizeof(road_time_t));
    if(buf==NULL)
    {
        printf("erreur malloc send_user\n");
    }

    //devname= "/media/ram/" +dbt_caller->file_name +".dbt";
    devname= "/tmp/" +dbt_caller->file_name +".dbt";
    dbtFile = inithdFile((char*)devname.c_str(),dbt_caller->id,dbt_caller->taille);

    devname= "/proc/xenomai/registry/native/pipes/" +dbt_caller->file_name;
    while(pipe_fd<0)
    {
        pipe_fd = open(devname.c_str(), O_RDONLY);
        if (pipe_fd < 0 && errno!=ENOENT) printf("err open read pipe_fd: %i\n",errno);
        usleep(1000);
    }

    while(dbt_caller->continuer_dbt==true)
    {


        FD_ZERO(&set); // clear the set
        FD_SET(pipe_fd, &set); // add our file descriptor to the set
        timeout.tv_sec = 0;
        timeout.tv_usec = 10000;
        rv = select(pipe_fd + 1, &set, NULL, NULL, &timeout);
        if(rv == -1)
            printf("select\n"); // an error accured
        else if(rv == 0)
        {
            //printf("timeout ui %s\n",ui_com_caller->name); // a timeout occured
        }
        else
        {

            //printf("ecrit\n");
            nb_read=read(pipe_fd,buf,dbt_caller->taille);
            if(nb_read!=dbt_caller->taille) printf("erreur taille %s %i/%i\n",dbt_caller->file_name.c_str() ,nb_read,dbt_caller->taille);

            nb_read=read(pipe_fd,&time,sizeof(road_time_t));
            if(nb_read!=sizeof(road_time_t)) printf("erreur taille %s %i/%i\n",dbt_caller->file_name.c_str(),nb_read,sizeof(road_time_t));

            nb_read=read(pipe_fd,&tr,sizeof(road_timerange_t));
            if(nb_read!=sizeof(road_timerange_t)) printf("erreur taille %s %i/%i\n",dbt_caller->file_name.c_str(),nb_read,sizeof(road_timerange_t));

            write_hdfile(dbtFile,buf,time,tr,dbt_caller->taille);

        }

    }

    close(pipe_fd);
    free(buf);
    close_hdfile(dbtFile);
    pthread_exit(0);


}
