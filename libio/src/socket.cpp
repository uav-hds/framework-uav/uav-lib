//  created:    2013/03/20
//  filename:   socket.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    classe implementant un socket udt
//
//
/*********************************************************************/

#include "socket.h"
#include <unistd.h>
#include <stdio.h>
#include <iostream>
#include <string>
#include <fstream>
#include <fcntl.h>
#include <cstdlib>
#include <cstring>
#include <rtdk.h>
#include <udt.h>
#include <stdarg.h>
#include <native/task.h>
#include <native/pipe.h>
#include <pthread.h>

using namespace std;

class udt_socket_impl
{

    public:
        udt_socket_impl(std::string name,int port);
        ~udt_socket_impl();
        void send_sock(uint8* buf,int32 size);
        int32 read_sock(uint8* buf,int32 buf_size);//buf must be of buf_size size

    private:
        UDTSOCKET socket_fd;
        UDTSOCKET serv;
        RT_PIPE socket_pipe_rd,socket_pipe_wr;

        bool socket_ready;
        bool continuer_socket;
        pthread_t read_thread,write_thread;
        int Port;
        std::string Name;

        static void* read_socket(void * arg);
        static void* write_socket(void * arg);
        void init_sock(void);
        void send_sock_nrt(char* buf,int32 size);
        int32 read_sock_nrt(uint8* buf,int32 buf_size);
        void connect_sock(void);
        uint8 calc_checksum(uint8* buf,int32 size);

    protected:

};

udt_socket::udt_socket(string name,int port)
{
     pimpl_=new udt_socket_impl(name,port);
}

udt_socket_impl::udt_socket_impl(string name,int port)
{
    int status;
    Port=port;
    Name=name;

    //creation pipe
    string dev_name=name+ "-rd";
    status=rt_pipe_create(&socket_pipe_rd,dev_name.c_str(), P_MINOR_AUTO,0);
    if(status!=0) printf("rt_pipe_create failed (%i:%s)\n",status,strerror(status));
    dev_name=name+ "-wr";
    status=rt_pipe_create(&socket_pipe_wr,dev_name.c_str() ,P_MINOR_AUTO,0);
    if(status!=0) printf("rt_pipe_create failed (%i:%s)\n",status,strerror(status));

    continuer_socket=true;

    // Initialize thread creation attributes
    pthread_attr_t attr;
    if(pthread_attr_init(&attr) != 0)
    {
        printf("InitSocket: erreur pthread_attr_init\n");
    }

    if(pthread_attr_setstacksize(&attr, 1024*1000)!=0)//PTHREAD_STACK_MIN ) != 0)
    {
       printf("InitSocket: erreur pthread_attr_setstacksize\n");
    }

    //start receive thread (user side)
    if(pthread_create (&read_thread, &attr, read_socket, (void*)this) < 0)
    {
        printf("read_socket: pthread_create failed\n");
    }

    //start send thread (user side)
    if(pthread_create (&write_thread, &attr, write_socket, (void*)this) < 0)
    {
        printf("write_socket: pthread_create failed\n");
    }
}

udt_socket::~udt_socket()
{
    delete pimpl_;
}

udt_socket_impl::~udt_socket_impl()
{
    continuer_socket=false;

	pthread_join(read_thread, NULL);
	pthread_join(write_thread, NULL);

    int status=rt_pipe_delete(&socket_pipe_rd);
    if(status!=0) printf("erreur destruction pipe %i\n",status);

    status=rt_pipe_delete(&socket_pipe_wr);
    if(status!=0) printf("erreur destruction pipe %i\n",status);

    UDT::close(socket_fd);

    // use this function to release the UDT library
    UDT::cleanup();
}

void* udt_socket_impl::read_socket(void *arg)
{
    int pipe_fd=-1;
    uint8* buf;
    int bytesRead;
    udt_socket_impl *caller = (udt_socket_impl*)arg;

    buf=(uint8*)malloc(320*240);

    // use this function to initialize the UDT library
    UDT::startup();
    caller->init_sock();


    while(pipe_fd<0)
    {
        string devname= "/proc/xenomai/registry/native/pipes/"+  caller->Name + "-wr";
        pipe_fd = open(devname.c_str(), O_WRONLY);
        if (pipe_fd < 0 && errno!=ENOENT) printf("err open write pipe_fd: %i\n",errno);
        usleep(1000);
    }

    while(caller->continuer_socket==true)
    {
        if(caller->socket_ready==true)
        {
            bytesRead= caller->read_sock_nrt(buf,320*240);

            if(bytesRead>0)
            {
                //for(int i=0;i<bytesRead;i++) printf("%x ",buf[i]);
                //printf("\n");
                write(pipe_fd,buf,bytesRead);
            }
        }
        else
        {
            usleep(100000);
        }

    }
    //printf("fin read user\n");
    close(pipe_fd);
    free(buf);
    pthread_exit(0);
}

void* udt_socket_impl::write_socket(void * arg)
{
    int pipe_fd=-1;
    fd_set set;
    struct timeval timeout;
    char* buf=NULL;
    int rv;
    ssize_t nb_read;//,nb_write;
    udt_socket_impl *caller = (udt_socket_impl*)arg;

    buf=(char*)malloc(320*240);
    if(buf==NULL)
    {
        printf("erreur malloc send_user\n");
    }

    while(pipe_fd<0)
    {
        string devname= "/proc/xenomai/registry/native/pipes/"+  caller->Name + "-rd";
        pipe_fd = open(devname.c_str(), O_RDONLY);
        if (pipe_fd < 0 && errno!=ENOENT) printf("err open read pipe_fd: %i\n",errno);
        usleep(1000);
    }

    while(caller->continuer_socket==true)
    {

        FD_ZERO(&set); /* clear the set */
        FD_SET(pipe_fd, &set); /* add our file descriptor to the set */
        timeout.tv_sec = 0;
        timeout.tv_usec = 10000;
        rv = select(pipe_fd + 1, &set, NULL, NULL, &timeout);
        if(rv == -1)
            printf("select\n"); // an error accured
        else if(rv == 0)
        {
            //printf("timeout ui %s\n",ui_com_caller->name); // a timeout occured
        }
        else
        {
            nb_read=read(pipe_fd,buf,320*240);

            caller->send_sock_nrt(buf,nb_read);
        }
    }

    close(pipe_fd);
    free(buf);
    pthread_exit(0);
}


void udt_socket_impl::init_sock(void)
{

    serv = UDT::socket(AF_INET, SOCK_DGRAM, 0);
    bool block = false;
    UDT::setsockopt(serv, 0, UDT_SNDSYN, &block, sizeof(bool));
    block = true;
    UDT::setsockopt(serv, 0, UDT_RCVSYN, &block, sizeof(bool));
    int timeout=100;//ms
    UDT::setsockopt(serv, 0, UDT_RCVTIMEO, &timeout, sizeof(int));
    int buf_len=1024*1000;
    UDT::setsockopt(serv, 0, UDT_SNDBUF, &buf_len, sizeof(int));
    UDT::setsockopt(serv, 0, UDT_RCVBUF, &buf_len, sizeof(int));

    socket_ready=false;

    sockaddr_in my_addr;
    my_addr.sin_family = AF_INET;
    my_addr.sin_port = htons(Port);
    my_addr.sin_addr.s_addr = INADDR_ANY;
    memset(&(my_addr.sin_zero), '\0', 8);

    if (UDT::ERROR == UDT::bind(serv, (sockaddr*)&my_addr, sizeof(my_addr)))
    {
      cout << "bind: " << UDT::getlasterror().getErrorMessage() << endl;
      socket_ready=false;
      //return 0;
    }

    //cout << "server is ready at port: " << port << endl;

    UDT::listen(serv, 1);
}

void udt_socket_impl::connect_sock(void)
{
    sockaddr_in their_addr;
    int namelen = sizeof(their_addr);

    if (UDT::INVALID_SOCK == (socket_fd = UDT::accept(serv, (sockaddr*)&their_addr, &namelen)))
    {
      //cout << "accept: " << UDT::getlasterror().getErrorMessage() << endl;
      socket_ready=false;
    }
    else
    {
        UDT::close(serv);
        printf("socket %s connected\n",Name.c_str());
        socket_ready=true;
    }
}


int32 udt_socket_impl::read_sock_nrt(uint8* buf,int32 buf_size)
{
    int32 numRead;

    numRead = UDT::recvmsg(socket_fd, (char*)(buf),buf_size);
    //printf("%i\n",numRead);
    if(numRead<0)
    {
        //if not errtimeout
        if(UDT::getlasterror().getErrorCode()!=6003) printf("read_sock_nrt: %s %i\n",UDT::getlasterror().getErrorMessage(),UDT::getlasterror().getErrorCode());
    }
    if(numRead==-1) numRead=0;
    return numRead;

}

int32 udt_socket::read_sock(uint8* buf,int32 buf_size)
{
    return pimpl_->read_sock(buf,buf_size);
}

int32 udt_socket_impl::read_sock(uint8* buf,int32 buf_size)
{
    return rt_pipe_read(&socket_pipe_wr,buf,buf_size,TM_NONBLOCK);
}

void udt_socket::send_sock(uint8* buf,int32 size)
{
    pimpl_->send_sock(buf,size);
}

void udt_socket_impl::send_sock(uint8* buf,int32 size)
{
    ssize_t written=rt_pipe_write(&socket_pipe_rd,(char*)buf,size,P_NORMAL);

    if(written<0)
    {
        rt_printf("udt_socket::send_sock: %s, erreur write pipe (%s)\n",Name.c_str(),strerror(-written));
    }
    else if (written != (ssize_t)size)
    {
        rt_printf("udt_socket::send_sock: %s, erreur write pipe %ld/%ld\n",Name.c_str(),written,size);
    }
}

void udt_socket_impl::send_sock_nrt(char* buf,int32 size)
{
    if(size<0)
    {
        printf("erreur taille trame UDT\n");
        return;
    }
    if(socket_ready==true)
    {
        uint8* buf_tosend;

        buf_tosend=(uint8*)malloc(size+1);//datas+checksum

        memcpy(buf_tosend,buf,size);
        buf_tosend[size]=calc_checksum(buf_tosend,size);//checksum

        int32 ssize = UDT::sendmsg(socket_fd, (char*)buf_tosend,size+1, 100, false);
        if(ssize!=size+1)
        {
            cout << "erreur envoi: " << UDT::getlasterror().getErrorMessage() << "code " << UDT::getlasterror().getErrorCode() << endl;
        }
        if( UDT::getlasterror().getErrorCode()==2001)
        {
           init_sock();
           UDT::close(socket_fd);
           printf("diconnected\n");
        }
        free(buf_tosend);

    }
    else
        connect_sock();

}


uint8 udt_socket_impl::calc_checksum(uint8* buf,int32 size)
{
    if(size>0)
    {
        uint8 checksum=0;
        for(int32 i=0;i<size;i++) checksum+=buf[i];

        return (uint8)(255-checksum);
    }
    else
        return 0;
}

void udt_socket::debug(const char * format, ...)
{
    int n;
    char printf_buffer[512];

    va_list args;
    va_start (args, format);
    n = vsprintf(&printf_buffer[1],format, args);
    va_end (args);
    if (n<=0) return;
    printf_buffer[0]=0xff;//ID_TRAME_DEBUG;

    pimpl_->send_sock((uint8*)(&printf_buffer),n+1);
}
