//  created:    2012/05/26
//  filename:   dbt.h
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:   objet interface pour l'ecriture dbt
//
//
/*********************************************************************/

#ifndef DBT_H
#define DBT_H

#include <native/pipe.h>
#include <string>

class dbt
{

    public:
        dbt(std::string name,int type, int size);
        ~dbt();
        void write(char* buf,RTIME time);



    private:
        std::string file_name;
        RT_PIPE dbt_pipe;
        bool continuer_dbt;
        pthread_t write_thread;
        static void* write_dbt(void * arg);
        int id;
        int taille;

};

#endif // DBT_H
