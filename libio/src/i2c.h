#include <typedef.h>
#include <string>

#define BLCTRV1_BROKEN 1
#define BLCTRV2 2
#define BLCTRV2_TEMPO 3

void init_i2c(char* filename);
void close_i2c(void);
void set_motors(uint16* moteurs,int nb_moteurs);
void set_motor(uint16 value,uint8 id);
int nb_motors();
void stop_pwms(void);
int start_capture_us(void);
void init_us(uint8 range,uint8 max_gain);
float get_value_us();
void set_slave(unsigned short address);

