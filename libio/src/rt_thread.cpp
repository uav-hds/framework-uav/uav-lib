//  created:    2011/09/13
//  filename:   rt_thread.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    classe implementant une RT_TASK
//
//
/*********************************************************************/

#include "rt_thread.h"
#include <stdio.h>
#include <stdlib.h>
#include "rtdk.h"

using namespace std;

rt_thread::rt_thread(string name)
{
    isRunning=false;

    th_name=name;
    //printf("creation rt_thread %s\n",th_name.c_str());
}

rt_thread::~rt_thread()
{
    //printf("destruction rt_thread %s\n",th_name.c_str());

    if(isRunning==true)
    {
        isRunning=false;

        int status=rt_task_join(&task);
        if(status!=0) printf("erreur join main_task %i\n",status);
    }
}


void rt_thread::start()
{
    int status;

    isRunning=true;

    status=rt_task_create(&task, th_name.c_str(), 0, 99, T_JOINABLE);
    if(status!=0) rt_printf("erreur creation main_task %s (%i)\n",th_name.c_str(),status);

    status=rt_task_start(&task, &main_task, (void*)this);
    if(status!=0) rt_printf("erreur start main_task %s (%i)\n",th_name.c_str(),status);

    // Initialise the rt_print buffer for this task explicitly
    rt_print_init(512, th_name.c_str());
}

void rt_thread::main_task(void * arg)
{
    rt_thread *caller = (rt_thread*)arg;

    // Perform auto-init of rt_print buffers if the task doesn't do so
    rt_print_auto_init(1);

    // Ask Xenomai to warn us upon switches to secondary mode.
    //rt_task_set_mode(0, T_WARNSW, NULL);

    caller->run();
}

void rt_thread::SleepMS(uint32_t time)
{
    int status=rt_task_sleep(time*1000*1000);
    if(status!=0) rt_printf("erreur rt_task_sleep (%s)\n",strerror(-status));
}
