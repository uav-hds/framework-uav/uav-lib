#ifndef HEADER_INTERFACE
#define HEADER_INTERFACE

#include <QStringList>
#include <QPushButton>
#include <QLineEdit>
#include <QSpinBox>
#include <QSettings>
#include <QSignalMapper>
#include <QGridLayout>
#include <QLabel>
#include <QComboBox>
#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>
#include <stdint.h>

#include "Trackable.h"
#include <vrpn_Connection.h>
#include <vrpn_Tracker.h>

class Interface : public QWidget
{
    Q_OBJECT

    public:
        Interface(QString name);
        ~Interface();

    private:
        QSignalMapper *signalMapper;
        QGridLayout *obj_layout;
        QLineEdit *object_text,*ip_text;
        QSpinBox *object_id,*port_box;
        QList<QPushButton*> del_buttons;
        QList<QLabel*> labels;
        QStringList object_liste;
        QList<uint8_t> id_liste;
        QSettings *settings;
        QPushButton *connect_vrpn,*disconnect_vrpn,*connect_xbee,*disconnect_xbee;
        QComboBox *port_serie,*baud_serie;
        vrpn_Connection *vrpn_connection;
        QList<Trackable*> trackables;
        QSerialPort serial;
        QList<QSerialPortInfo> ports;
        int timer_id;

        void add_item(QString objectTrackable,uint8_t id,bool update_settings);
        void write_settings(void);
        bool vrpn_connected(void);

     private slots:
        void add();
        void del(int index);
        void check_id(int index);
        void connection();
        void disconnection();
        void open_xbee();
        void close_xbee();

     protected:
        virtual void timerEvent(QTimerEvent *e);

};


#endif

