#include "Interface.h"
#include <QtCore/QDebug>
#include <QGroupBox>
#include <QMessageBox>

Interface::Interface(QString name):QWidget()
{
    setWindowTitle(name);
    setWindowFlags( (windowFlags() | Qt::CustomizeWindowHint) & ~Qt::WindowMaximizeButtonHint);
    vrpn_connection=NULL;

    signalMapper = new QSignalMapper(this);

    //layout principal,
    QGridLayout *group_layout = new QGridLayout();
    group_layout->setSizeConstraint(QLayout::SetFixedSize);
    setLayout(group_layout);

    //groupbox serveur vrpn
    QGroupBox *serveur_box=new QGroupBox("Server VRPN",this);
    group_layout->addWidget(serveur_box,0,0);
    QGridLayout *server_layout = new QGridLayout();
    serveur_box->setLayout(server_layout);

    //groupbox xbee
    QGroupBox *xbee_box=new QGroupBox("Modem Xbee",this);
    group_layout->addWidget(xbee_box,1,0);
    QGridLayout *xbee_layout = new QGridLayout();
    xbee_box->setLayout(xbee_layout);

    //groupbox objets vrpn
    QGroupBox *object_box=new QGroupBox("Objects",this);
    group_layout->addWidget(object_box,2,0);
    QGridLayout *main_layout = new QGridLayout();
    object_box->setLayout(main_layout);

    //remplissage groupbox server
    QLabel *server=new QLabel("ip");
    QLabel *port=new QLabel("port");
    ip_text=new QLineEdit(this);
    ip_text->setText("127.0.0.1");
    port_box=new QSpinBox(this);
    port_box->setMaximum(65535);
    port_box->setMinimum(0);
    port_box->setValue(3883);
    connect_vrpn= new QPushButton("Connect");
    disconnect_vrpn= new QPushButton("Disconnect");
    disconnect_vrpn->setEnabled(false);
    server_layout->addWidget(server,0,0);
    server_layout->addWidget(port,0,1);
    server_layout->addWidget(ip_text,1,0);
    server_layout->addWidget(port_box,1,1);
    server_layout->addWidget(connect_vrpn,2,0);
    server_layout->addWidget(disconnect_vrpn,2,1);

    //remplissage groupbox xbee, detection des ports
    QLabel *port_xbee=new QLabel("port");
    QLabel *baud=new QLabel("baudrate");
    port_serie=new QComboBox(this);
    baud_serie=new QComboBox(this);
    xbee_layout->addWidget(port_xbee,0,0);
    xbee_layout->addWidget(baud,0,1);
    xbee_layout->addWidget(port_serie,1,0);
    xbee_layout->addWidget(baud_serie,1,1);
    foreach (const QSerialPortInfo &info, QSerialPortInfo::availablePorts())
    {
        ports.append(info);
        port_serie->addItem(info.portName());
    }
    connect_xbee= new QPushButton("Connect");
    disconnect_xbee= new QPushButton("Disconnect");
    disconnect_xbee->setEnabled(false);
    if(ports.count()==0) connect_xbee->setEnabled(false);
    xbee_layout->addWidget(connect_xbee,2,0);
    xbee_layout->addWidget(disconnect_xbee,2,1);


    //remplissage groupbox objets vrpn, partie fixe
    QGridLayout *add_layout= new QGridLayout();
    main_layout->addLayout(add_layout,0,0);
    QLabel *obj=new QLabel("vrpn object");
    QLabel *id=new QLabel("xbee id");
    object_text=new QLineEdit(this);
    object_text->setText("Drone");
    add_layout->addWidget(obj,0,0);
    add_layout->addWidget(id,0,1);
    add_layout->addWidget(object_text,1,0);
    object_id=new QSpinBox(this);
    object_id->setMaximum(255);
    object_id->setMinimum(0);
    QPushButton *add_button= new QPushButton("Add");
    add_layout->addWidget(object_id,1,1);
    add_layout->addWidget(add_button,1,2);

    //remplissage groupbox objets vrpn, partie variable a partir des settings
    obj_layout= new QGridLayout();
    main_layout->addLayout(obj_layout,1,0);
    settings=new QSettings("Heudiasyc", "vrpn2xbee");
    int array_size = settings->beginReadArray("objects");
    for (int i = 0; i < array_size; i++)
    {
        settings->setArrayIndex(i);
        add_item(settings->value("object").toString(),settings->value("id").toInt(),false);
    }
    settings->endArray();

    //signaux
    connect(add_button, SIGNAL(clicked(bool)),this, SLOT(add()));
    connect(connect_vrpn, SIGNAL(clicked(bool)),this, SLOT(connection()));
    connect(disconnect_vrpn, SIGNAL(clicked(bool)),this, SLOT(disconnection()));
    connect(connect_xbee, SIGNAL(clicked(bool)),this, SLOT(open_xbee()));
    connect(disconnect_xbee, SIGNAL(clicked(bool)),this, SLOT(close_xbee()));
    connect(object_id, SIGNAL(valueChanged(int)),this, SLOT(check_id(int)));
    connect(signalMapper, SIGNAL(mapped(int)),this, SLOT(del(int)));

}

Interface::~Interface()
{
    disconnection();
    close_xbee();
}

void Interface::timerEvent(QTimerEvent *)
{/*
uint64_t temps,pf;
QueryPerformanceFrequency((LARGE_INTEGER*)&pf);
QueryPerformanceCounter((LARGE_INTEGER*)&temps);
double tps=temps*1.0/(double)pf;
printf("%f %i\n",tps*1000,(int)tps);

*/
TODO: utiliser le timeout mainloop, cf flair

    vrpn_connection->mainloop();
    for(int i=0;i<trackables.count();i++)
    {
        trackables.at(i)->mainloop();
        trackables.at(i)->send();
    }
}

void Interface::add()
{
   add_item(object_text->text(),object_id->value(),true);
}

void Interface::add_item(QString object,uint8_t id,bool update_settings)
{
    QLabel* new_label;
    new_label=new QLabel(QString("%1 -> %2").arg(object).arg(id));
    labels.append(new_label);

    QPushButton* new_button;
    new_button= new QPushButton("Del");
    del_buttons.append(new_button);

    obj_layout->addWidget(new_label,obj_layout->rowCount () +1,0);
    obj_layout->addWidget(new_button,obj_layout->rowCount ()-1,1);

    connect(new_button ,SIGNAL(clicked()), signalMapper, SLOT(map()));
    signalMapper->setMapping(new_button, del_buttons.count()-1);

    object_liste.append(object);
    id_liste.append(id);

    object_id->setValue((uint8_t)(id+1));

    if(vrpn_connected()==true)
    {
        Trackable *tracker = new Trackable(object,id,vrpn_connection);
        trackables.append(tracker);
    }

    if(update_settings==true)
    {
        write_settings();
    }
}

void Interface::del(int index)
{
    for(int i=0;i<del_buttons.count();i++)
    {
        signalMapper->removeMappings(del_buttons.at(i));
    }
    delete del_buttons.at(index);
    del_buttons.removeAt(index);
    for(int i=0;i<del_buttons.count();i++)
    {
        signalMapper->setMapping(del_buttons.at(i), i);
    }

    delete labels.at(index);
    labels.removeAt(index);

    if(vrpn_connected()==true)
    {
        delete trackables.at(index);
        trackables.removeAt(index);
    }

    object_liste.removeAt(index);
    id_liste.removeAt(index);

    write_settings();
}

void Interface::write_settings(void)
{
    settings->beginWriteArray("objects");
    for(int i=0;i<labels.count();i++)
    {
        settings->setArrayIndex(i);
        settings->setValue("object",object_liste.at(i));
        settings->setValue("id", id_liste.at(i));
    }
    settings->endArray();
    settings->sync();
}

void Interface::check_id(int index)
{
    bool match=false;
    for(int i=0;i<labels.count();i++)
    {
        if(id_liste.at(i)==index)
        {
            match=true;
            break;
        }
    }
    if(match==true)
    {
        check_id((uint8_t)(index+1));
        return;
    }
    object_id->setValue(index);
}

void Interface::connection()
{
    disconnect_vrpn->setEnabled(true);
    connect_vrpn->setEnabled(false);

    QString connectionName=QString("%1:%2").arg(ip_text->text()).arg(port_box->value());
    vrpn_connection = vrpn_get_connection_by_name(connectionName.toLocal8Bit().data());

    for(int i=0;i<object_liste.count();i++)
    {
        Trackable *tracker = new Trackable(object_liste.at(i),id_liste.at(i),vrpn_connection);
        trackables.append(tracker);
    }

    timer_id=startTimer(1);
}

void Interface::disconnection()
{
    disconnect_vrpn->setEnabled(false);
    connect_vrpn->setEnabled(true);

    killTimer(timer_id);
    for(int i=0;i<trackables.count();i++)
    {
        delete trackables.at(i);
    }
    trackables.clear();

    if(vrpn_connection!=NULL) delete vrpn_connection;
    vrpn_connection=NULL;
}

bool Interface::vrpn_connected(void)
{
    return disconnect_vrpn->isEnabled();
}

void Interface::open_xbee()
{
    serial.setPort(ports.at(port_serie->currentIndex()));
    //serial.setPortName("ttyS1");
    if (serial.open(QIODevice::ReadWrite))
    {
        disconnect_xbee->setEnabled(true);
        connect_xbee->setEnabled(false);
        serial.setBaudRate(111111);

        for(int i=0;i<trackables.count();i++)
        {
            trackables.at(i)->setSerialPort(&serial);
        }
    }
    else
    {
        QMessageBox msgBox;
        msgBox.critical( this, "Error", "Unable to open port!");
    }
}

void Interface::close_xbee()
{
    disconnect_xbee->setEnabled(false);
    connect_xbee->setEnabled(true);
    for(int i=0;i<trackables.count();i++)
    {
        trackables.at(i)->setSerialPort(NULL);
    }

    serial.close();
}
