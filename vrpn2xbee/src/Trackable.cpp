#include "Trackable.h"
//#include <windows.h>
#include <math.h>

//constantes XBee
#define START_DELIMITER 0x7e
#define TX_REQUEST 0x01
#define DESTINATION_MSB 0xff
#define DESTINATION_LSB 0xff
#define OPTION 0x01

Trackable::Trackable(QString name,uint8_t id,vrpn_Connection *connection)
{
	this->changedCallback = Trackable::handle_pos;

	// Initialisations des variables internes
	this->lastPacketTime = -1;
	this->tracker = new vrpn_Tracker_Remote(name.toStdString().c_str(), connection);
	this->tracker->register_change_handler(this,changedCallback);
	this->tracker->shutup=true;
	this->tracker->set_update_rate(200);
	this->serial=NULL;
	this->id=id;
	to_send=false;

	printf("Tracker started for %s\n", name.toStdString().c_str());
}

Trackable::~Trackable(void)
{
    this->tracker->unregister_change_handler(this,this->changedCallback);
    delete this->tracker;
}

void Trackable::mainloop(){
	this->tracker->mainloop();
}

long Trackable::getLastPacketTime(){
	return lastPacketTime;
}

void Trackable::setLastPacketTime(long time){
	lastPacketTime= time;
}

void VRPN_CALLBACK Trackable::handle_pos (void *userdata, const vrpn_TRACKERCB t)
{
	Trackable* trackable= reinterpret_cast<Trackable*>(userdata);
/*
uint64_t temps,pf;
QueryPerformanceFrequency((LARGE_INTEGER*)&pf);
QueryPerformanceCounter((LARGE_INTEGER*)&temps);
double tps=temps*1.0/(double)pf;
printf("%f %i %u %u\n",tps*1000,(int)tps,t.msg_time.tv_sec,t.msg_time.tv_usec);*/
    //Mise en m�moire de la date du paQTimequet
    trackable->setLastPacketTime(t.msg_time.tv_usec);

    trackable->pos[0]=t.pos[0];
    trackable->pos[1]=t.pos[1];
    trackable->pos[2]=t.pos[2];
    trackable->quat[0]=t.quat[0];
    trackable->quat[1]=t.quat[1];
    trackable->quat[2]=t.quat[2];
    trackable->quat[3]=t.quat[3];
    if(fabs(t.pos[0])>10 ||fabs(t.pos[1])>10 ||fabs(t.pos[2])>10 )
        printf("Depassement pos %f / %f / %f\n",t.pos[0],t.pos[1],t.pos[2]);
    trackable->to_send=true;

}

void Trackable::send(void)
{
	if(serial!=NULL && to_send==true)
    {/*
        uint64_t temps,pf;
QueryPerformanceFrequency((LARGE_INTEGER*)&pf);
QueryPerformanceCounter((LARGE_INTEGER*)&temps);
double tps=temps*1.0/(double)pf;
printf("%f %i send\n",tps*1000,(int)tps);
*/
        unsigned char* buf=(unsigned char*)malloc(255);
        buf[0]=START_DELIMITER;
        buf[1]=0;//taille MSB, 0 � priori
        buf[2]=sizeof(pos)+sizeof(quat)+6;//taille LSB
        //le checksum commence a partir de la
        buf[3]=TX_REQUEST;
        buf[4]=0;//trackable->id;
        buf[5]=DESTINATION_MSB;
        buf[6]=DESTINATION_LSB;
        buf[7]=OPTION;
        buf[8]=id;
        memcpy(&buf[9],pos,sizeof(pos));
        memcpy(&buf[9]+sizeof(pos),quat,sizeof(quat));
        buf[9+sizeof(pos)+sizeof(quat)]=calc_checksum(&buf[3],buf[2]);
        int sent=serial->write((char*)buf,8+sizeof(pos)+sizeof(quat)+2);
        //trackable->serial->flush();
        serial->waitForBytesWritten(10);
        //printf("%i/%i\n",sent,8+sizeof(pos)+sizeof(quat)+2);
        //for(int i=0;i<8+sizeof(pos)+sizeof(quat)+2;i++) printf("%x ",buf[i]);
        //printf("\n");
        free(buf);
        to_send=false;
    }
}

void Trackable::setSerialPort(QSerialPort *serial)
{
    this->serial=serial;
}

uint8_t Trackable::calc_checksum(uint8_t* buf,int32_t size)
{
    uint8_t checksum=0;
    for(int32_t i=0;i<size;i++) checksum+=buf[i];

    return (uint8_t)(255-checksum);
}
