#pragma once
#include <stdint.h>
#include <vrpn_Connection.h>
#include <vrpn_Tracker.h>
#include <QString>
#include <QtSerialPort/QSerialPort>

class Trackable
{
public:
	Trackable(QString name,uint8_t id,vrpn_Connection *connection);
	~Trackable(void);
	void setSerialPort(QSerialPort *serial);
    void send(void);
    void mainloop();
	long getLastPacketTime();
	void setLastPacketTime(long time);

private:
	vrpn_Tracker_Remote* tracker;
	vrpn_TRACKERCHANGEHANDLER changedCallback;
	long lastPacketTime;
	QSerialPort *serial;
	uint8_t id;
	uint8_t calc_checksum(uint8_t* buf,int32_t size);
	static void	VRPN_CALLBACK handle_pos (void *userdata, const vrpn_TRACKERCB t);
	float pos[3];
	float quat[4];
	bool to_send;
};

