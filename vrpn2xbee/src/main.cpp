#include <QApplication>
#include <QCleanlooksStyle>
#include <Interface.h>


int main(int argc, char* argv[])
{
    QApplication app(argc, argv);
    app.setStyle(new QCleanlooksStyle);

    Interface fenetre("vrpn2xbee");
    fenetre.show();

    app.exec();
}
