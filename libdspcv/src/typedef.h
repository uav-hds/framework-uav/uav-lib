#ifndef TYPEDEF_H
#define TYPEDEF_H

#define L1DSRAM 1
#define DDR2 2
#define BUF_ALIGN 0

#define BCACHE_MAR_ENABLE 1
#define BCACHE_MAR_DISABLE 0

#define const
#define restrict
/*
typedef char Int8;
typedef unsigned char Uint8;
typedef unsigned char UInt8;
typedef short Int16;
typedef unsigned short Uint16;
typedef unsigned short UInt16;
typedef int Int32;
typedef unsigned int Uint32;
typedef unsigned int UInt32;
*/

/*
typedef long Word40;
typedef unsigned long UWord40;
typedef unsigned long long UWord64;
typedef long long Word64;
*/


/**
 *  @brief  ACPY3 16-bit param field structure. These values are passed to
 *          ACPY3_fastConfigure16b() to indicate the field of the
 *          ACPY3_Params structure to be changed.
 */
typedef enum ACPY3_ParamField16b {

    ACPY3_PARAMFIELD_ELEMENTSIZE =  8,
    ACPY3_PARAMFIELD_NUMELEMENTS = 10 ,
    ACPY3_PARAMFIELD_ELEMENTINDEX_SRC = 16,
    ACPY3_PARAMFIELD_ELEMENTINDEX_DST = 18,
    ACPY3_PARAMFIELD_FRAMEINDEX_SRC =  24,
    ACPY3_PARAMFIELD_FRAMEINDEX_DST =  26,
    ACPY3_PARAMFIELD_NUMFRAMES = 28

} ACPY3_ParamField16b;

/**
 *  @brief      ACPY3 32-bit param field structure. These values are passed to
 *          ACPY3_fastConfigure32b() to indicate the field of the
 *          ACPY3_Params structure to be changed.
 */
typedef enum ACPY3_ParamField32b {

    ACPY3_PARAMFIELD_SRCADDR = 4,
    ACPY3_PARAMFIELD_DSTADDR = 12,
    ACPY3_PARAMFIELD_ELEMENTINDEXES = 16,
    ACPY3_PARAMFIELD_FRAMEINDEXES = 24

} ACPY3_ParamField32b;

/**
 *  @brief      ACPY3 DMA Transfer Types:
 *
 *  Note that all transfers can be specified using the ACPY3_2D2D transfer
 *  type, and providing element and frame indices for both source and
 *  destination. However, the other transfer types are provided to simplify
 *  configuration when applicable. For example, when using the ACPY3_1D2D
 *  transfer type, you do not need to set the source element and frame
 *  indices in the ACPY3_Params structure.
 */
typedef enum ACPY3_TransferType {

    /**
     *  Only one element is copied from source to destination.
     *  The size of the element is specified in the elementSize field of
     *  ACPY3_Params
     */
    ACPY3_1D1D,

    /**
     *  After each element is copied to destination, the source
     *  and destination of the next element to copy is updated
     *  as follows:
     *  - src = src + element size
     *  - dst = dst + destination element index
     *
     *  After an entire frame is copied (the frame size specified by the
     *  numElements field of ACPY3_Params), the source and destination of
     *  the next frame to copy are given by:
     *  - src = src + element size
     *  - dst = start of frame + destination frame index
     */
    ACPY3_1D2D,

    /**
     *  This is similar to ACPY3_1D2D, except that source and
     *  destination are updated after an element is copied,
     *  as follows:
     *  - src = src + source element index
     *  - dst = dst + element size
     *
     *  After an entire frame is copied by:
     *  - src = start of frame + source frame index
     *  - dst = dst + element size
     */
    ACPY3_2D1D,

    /**
     *  This transfer type combines ACPY3_1D2D and ACPY3_2D1D,
     *  so that source and destination are updated after an
     *  element is copied by:
     *  - src = src + source element index
     *  - dst = dst + destination element index
     *
     *  After a frame is copied by:
     *  - src = start of frame + source frame index
     *  - dst = start of frame + destination frame index
     */
    ACPY3_2D2D
} ACPY3_TransferType;


/**
 *  @brief  DMA transfer specific parameters.  Defines the configuration
 *          of a logical channel.
 */
typedef struct ACPY3_Params {

    /** 1D1D, 1D2D, 2D1D or 2D2D */
    ACPY3_TransferType transferType;

    /** Source Address of the DMA transfer */
    void *         srcAddr;

    /** Destination Address of the DMA transfer */
    void *         dstAddr;

    /** Number of consecutive bytes in each 1D transfer vector (ACNT)   */
    unsigned short elementSize;

    /** Number of 1D vectors in 2D transfers (BCNT) */
    unsigned short numElements;

    /** Number of 2D frames in 3D transfers (CCNT) */
    unsigned short numFrames ;

    /**
     *  Offset in number of bytes from beginning of each 1D vector to the
     *  beginning of the next 1D vector.  (SBIDX)
     */
    short srcElementIndex;

    /**
     *  Offset in number of bytes from beginning of each 1D vector to the
     *  beginning of the next 1D vector.  (DBIDX)
     */
    short dstElementIndex;

    /**
     *  Offset in number of bytes from beginning of 1D vector of current
     *  (source) frame to the beginning of next frame's first 1D vector.
     *  (SCIDX): signed value between -32768 and 32767.
     */
    short srcFrameIndex;

    /**
     *  Offset in number of bytes from beginning of 1D vector of current
     *  (destination) frame to the beginning of next frame's first 1D vector.
     *  (DCIDX): signed value between -32768 and 32767.
     */
    short dstFrameIndex;

    /**
     *  For a linked transfer entry:
     *  -  waitId = -1           : no intermediate wait on this transfer
     *  -  0 <= waitId < numWaits : this transfer can be waited on or polled
     *                            for completion
     *  - Ignored for single-transfers and for the last transfer in a
     *    sequence of linked transfers which are always synchronized with
     *    waitId == (numWaits - 1)
     */
    short waitId;

} ACPY3_Params;


typedef ACPY3_Params* IDMA3_Handle;

#endif // TYPEDEF_H
