#include "dspcv_gpp.h"
#include <cmem.h>
#include <stdio.h>
#include "opencv_common.h"
#include "message.h"
#include "message_os.h"

bool initDsp(const char* dspExecutable,int warningTimeoutMs) {
	return init_dsp(dspExecutable,warningTimeoutMs);
}
bool closeDsp(void) {
	return close_dsp();
}

/** ============================================================================
 *  fonctions OpenCV
 *
 *  ============================================================================
 */
void prepare_image(IplImage* img,char **buf,struct _IplROI **roi)
{
    if(img->roi!=NULL) CMEM_cacheWb(img->roi,sizeof(_IplROI));

    *buf=img->imageData;//sauvegarde adresse virtuelle
    if(roi!=NULL) *roi=img->roi;//sauvegarde adresse virtuelle
    img->imageData = (char*)CMEM_getPhys(img->imageData);//remplace par adresse physique
    if(img->roi!=NULL) img->roi = (struct _IplROI*)CMEM_getPhys(img->roi);//remplace par adresse physique
    CMEM_cacheWb(img,sizeof(IplImage));
}

void restore_image(IplImage* img,char *buf,struct _IplROI *roi)
{
    img->imageData = buf;//restaure adresse virtuelle
    img->roi = roi;//restaure adresse virtuelle
}

char* shMemAlloc(size_t size) {
	CMEM_AllocParams params;

	params = CMEM_DEFAULTPARAMS;
	params.flags = CMEM_CACHED;
	params.type=CMEM_HEAP;
  
  char* ptr=(char*)CMEM_alloc(size,&params);
  if(ptr==NULL) fprintf(stderr,"error allocating %i bytes\n",size);
	return ptr;
}

char* shMemFree(char* buf) {
	CMEM_AllocParams params;

	params = CMEM_DEFAULTPARAMS;
	params.flags = CMEM_CACHED;
	params.type=CMEM_HEAP;
	
	if (CMEM_free(buf, &params) < 0) printf("Failed to free buffer at %#x\n",buf);
}

void dspSaveToJpeg(IplImage* src_img,const char* filename,unsigned char input_format,unsigned char output_format,unsigned char compression_level) {
	char* jpg_buf=shMemAlloc(src_img->imageSize);
  
  uint32_t compressed_size=dspConvertToJpeg(src_img,jpg_buf,src_img->imageSize,input_format,output_format,compression_level);
  FILE* fOutFile = fopen (filename, "wb");;
  fwrite(jpg_buf,1,compressed_size,fOutFile);
  fclose(fOutFile);
  shMemFree(jpg_buf);
}

uint32_t dspConvertToJpeg(IplImage* src_img,char* out_buf,unsigned int out_buf_size,unsigned char input_format,unsigned char output_format,unsigned char compression_level) {
	uint32_t *output_size;
	unsigned int return_ouput_size;
	struct Jpeg_msg *message;
	CMEM_AllocParams params;
	DSPCV_query* outMsg;
  char *data_img_src_virt;

	params = CMEM_DEFAULTPARAMS;
	params.flags = CMEM_NONCACHED;
	params.type=CMEM_HEAP;

	MESSAGE_0Print ("Entered dspConvertToJpeg()\n") ;

	if(src_img->nChannels!=1 && input_format==GRAY) {
		printf("dspConvertToJpeg: gray input image must be 1 channel\n");
		return 0;
	}
	if(src_img->nChannels==1 && input_format!=GRAY) {
		printf("dspConvertToJpeg: 1 channel input image must be gray\n");
		return 0;
	}
	if(src_img->nChannels>2) {
		printf("dspConvertToJpeg: non gray input image must be 2 channels\n");
		return 0;
	}
	
	if(compression_level>100) compression_level=100;

	output_size=(uint32_t*)CMEM_alloc(sizeof(uint32_t),&params);

	CMEM_cacheWb((char*)src_img->imageData,src_img->imageSize);

	// allocate processing request
	MESSAGE_Alloc(&outMsg);

  data_img_src_virt=src_img->imageData;//sauvegarde adresse virtuelle
  src_img->imageData = (char*)CMEM_getPhys(src_img->imageData);//remplace par adresse physique
  CMEM_cacheWb(src_img,sizeof(IplImage));
	message =(Jpeg_msg *)CMEM_alloc(sizeof(Jpeg_msg),&params);
	message->img_src=(IplImage*)CMEM_getPhys(src_img);
	message->data_img_dst=(uint8_t*)CMEM_getPhys(out_buf);
	message->compression_level=compression_level;
	message->input_format=input_format;
	message->output_format=output_format;
	message->output_size=(uint32_t*)CMEM_getPhys(output_size);

	outMsg->openCVFxnIndex=CV_JPEG;
	outMsg->msg_ptr=CMEM_getPhys(message);

	MESSAGE_Execute(outMsg) ;

  src_img->imageData = data_img_src_virt;//restaure adresse virtuelle
	CMEM_cacheInv(out_buf,*output_size);

	return_ouput_size=*output_size;
	if (CMEM_free(message, &params) < 0) printf("Failed to free buffer at %#x\n",(unsigned int) message);
	if (CMEM_free(output_size, &params) < 0) printf("Failed to free buffer at %#x\n",(unsigned int) output_size);

	MESSAGE_0Print ("Leaving dspConvertToJpeg()\n") ;

	return return_ouput_size;
}

Void
dspGoodFeaturesToTrack( IplImage* img_src, CvPoint* features, unsigned int* count,float quality_level, float min_distance)
{
    //struct timeval tvs, tve, tvd;
    unsigned int* total_count;
    struct GoodFeaturesToTrack_msg *message;
    CMEM_AllocParams params;
    DSPCV_query* outMsg;
    params = CMEM_DEFAULTPARAMS;
    params.flags = CMEM_NONCACHED;
    params.type=CMEM_HEAP;
    char *data_img_src_virt;
    struct _IplROI *roi;

    MESSAGE_0Print ("Entered dspGoodFeaturesToTrack()\n") ;

    total_count=( unsigned int*)CMEM_alloc(sizeof(Int32),&params);
    *total_count=*count;

    // allocate processing request
    MESSAGE_Alloc(&outMsg);

    CMEM_cacheWb((char*)img_src->imageData,img_src->imageSize);
    prepare_image(img_src,&data_img_src_virt,&roi);

    message =(GoodFeaturesToTrack_msg *)CMEM_alloc(sizeof(GoodFeaturesToTrack_msg),&params);
    message->img_src=(IplImage*)CMEM_getPhys(img_src);
    message->features=(CvPoint*)CMEM_getPhys(features);
    message->quality_level=(float)quality_level;
    message->min_distance=(float)min_distance;
    message->total_count=(uint32_t*)CMEM_getPhys(total_count);

    outMsg->openCVFxnIndex=CV_GOOD_FEATURES_TO_TRACK;
    outMsg->msg_ptr=CMEM_getPhys(message);

		MESSAGE_Execute(outMsg) ;

    restore_image(img_src,data_img_src_virt,roi);

    CMEM_cacheInv((void*)features,sizeof(CvPoint)*(*total_count));

    *count=*total_count;

    if (CMEM_free(total_count, &params) < 0) printf("Failed to free buffer at %#x\n",(unsigned int) total_count);
    if (CMEM_free(message, &params) < 0) printf("Failed to free buffer at %#x\n",(unsigned int) message);

    MESSAGE_0Print ("Leaving dspGoodFeaturesToTrack\n") ;

}



Void
dspPyrDown( IplImage* img_src, IplImage* img_dst, Uint8 level)
{
    DSPCV_query* outMsg;
    struct PyrDown_msg *message;
    CMEM_AllocParams params;
    params = CMEM_DEFAULTPARAMS;
    params.flags = CMEM_NONCACHED;
    params.type=CMEM_HEAP;
    char *data_img_src_virt,*data_img_dst_virt;
    struct _IplROI *src_roi;

    MESSAGE_0Print ("Entered dspPyrDown()\n") ;
    // allocate processing request
    MESSAGE_Alloc(&outMsg);


    CMEM_cacheWb((char*)img_src->imageData,img_src->imageSize);
    prepare_image(img_src,&data_img_src_virt,&src_roi);
    prepare_image(img_dst,&data_img_dst_virt,NULL);

    message =(PyrDown_msg*)CMEM_alloc(sizeof(PyrDown_msg),&params);
    message->img_src = (IplImage*)CMEM_getPhys(img_src);
    message->img_dst = (IplImage*)CMEM_getPhys(img_dst);
    message->level=level;
    outMsg->openCVFxnIndex=CV_PYR_DOWN;
    outMsg->msg_ptr=CMEM_getPhys(message);

		MESSAGE_Execute(outMsg);

    restore_image(img_src,data_img_src_virt,src_roi);
    restore_image(img_dst,data_img_dst_virt,NULL);

    CMEM_cacheInv((char*)img_dst->imageData,img_dst->imageSize);
    if (CMEM_free(message, &params) < 0) printf("Failed to free buffer at %#x\n",(unsigned int) message);

    MESSAGE_0Print ("Leaving dspPyrDown()\n") ;
}




Void
dspCalcOpticalFlowPyrLK( IplImage* img_A, IplImage* img_B, IplImage* pyr_A, IplImage* pyr_B,
         CvPoint* features_A, CvPoint* features_B, int count, CvSize winSize,
         int level, char *feat_status, unsigned int *error, CvTermCriteria criteria, int flags)
{
    DSPCV_query* outMsg;
    struct CalcOpticalFlowPyrLK_msg *message;
    CMEM_AllocParams params;

    params = CMEM_DEFAULTPARAMS;
    params.flags = CMEM_NONCACHED;
    params.type=CMEM_HEAP;
    char *data_img_A_virt,*data_img_B_virt;
    char *data_pyr_A_virt,*data_pyr_B_virt;
    struct _IplROI *img_A_roi,*img_B_roi;

    MESSAGE_0Print ("Entered dspLK()\n") ;

//mettre le flag assume_pyramide cf opencv
    //compute pyramides
    //dspPyrDown(img_A,pyr_A);
    //dspPyrDown(img_B,pyr_B);

    CMEM_cacheWb(features_A,count*sizeof(CvPoint));

    CMEM_cacheWb(img_A->imageData,img_A->imageSize);
    CMEM_cacheWb(img_B->imageData,img_B->imageSize);

    prepare_image(img_A,&data_img_A_virt,&img_A_roi);
    prepare_image(img_B,&data_img_B_virt,&img_B_roi);

    CMEM_cacheWb(pyr_A->imageData,pyr_A->imageSize);
    CMEM_cacheWb(pyr_B->imageData,pyr_B->imageSize);

    prepare_image(pyr_A,&data_pyr_A_virt,NULL);
    prepare_image(pyr_B,&data_pyr_B_virt,NULL);

    // allocate processing request
    MESSAGE_Alloc(&outMsg);

    message =(CalcOpticalFlowPyrLK_msg*)CMEM_alloc(sizeof(CalcOpticalFlowPyrLK_msg),&params);
    message->winSize=winSize;
    message->level=level;
    message->pyr_A=(IplImage*)CMEM_getPhys(pyr_A);
    message->pyr_B=(IplImage*)CMEM_getPhys(pyr_B);
    message->features_A=(CvPoint*)CMEM_getPhys(features_A);
    message->features_B=(CvPoint*)CMEM_getPhys(features_B);
    message->count=count;
    message->criteria=criteria;//(CvTermCriteria*)CMEM_getPhys(criteria_tmp);
    message->feat_status=(int8_t*)CMEM_getPhys(feat_status);
    message->error=(uint32_t*)CMEM_getPhys(error);
    message->img_A =(IplImage*)CMEM_getPhys(img_A);
    message->img_B =(IplImage*)CMEM_getPhys(img_B);
    outMsg->openCVFxnIndex=CV_LK;
    outMsg->msg_ptr=CMEM_getPhys(message);

    MESSAGE_Execute(outMsg);

    restore_image(img_A,data_img_A_virt,img_A_roi);
    restore_image(img_B,data_img_B_virt,img_B_roi);
    restore_image(pyr_A,data_pyr_A_virt,NULL);
    restore_image(pyr_B,data_pyr_B_virt,NULL);

    CMEM_cacheInv(feat_status,count*sizeof(char));
    CMEM_cacheInv(error,count*sizeof(unsigned int));
    CMEM_cacheInv(features_B,count*sizeof(CvPoint));

    if (CMEM_free(message, &params) < 0) printf("Failed to free buffer at %#x\n",(unsigned int) message);

    MESSAGE_0Print ("Leaving dspLK()\n") ;
}


Void
dspCvtColor( IplImage* img_src, IplImage* img_dst, int code )
{
    DSPCV_query* outMsg;
    struct CvtColor_msg *message;
    CMEM_AllocParams params;
    params = CMEM_DEFAULTPARAMS;
    params.flags = CMEM_NONCACHED;
    params.type=CMEM_HEAP;
    char *data_img_src_virt,*data_img_dst_virt;

    MESSAGE_0Print ("Entered dspCvtColor()\n") ;

    // allocate processing request
    MESSAGE_Alloc(&outMsg);

    CMEM_cacheWb((char*)img_src->imageData,img_src->imageSize);

    data_img_src_virt=img_src->imageData;//sauvegarde adresse virtuelle
    data_img_dst_virt=img_dst->imageData;
    img_src->imageData = (char*)CMEM_getPhys(img_src->imageData);//remplace par adresse physique
    img_dst->imageData = (char*)CMEM_getPhys(img_dst->imageData);
    CMEM_cacheWb(img_src,sizeof(IplImage));
    CMEM_cacheWb(img_dst,sizeof(IplImage));

    message =(CvtColor_msg*)CMEM_alloc(sizeof(CvtColor_msg),&params);
    message->img_src = (IplImage*)CMEM_getPhys(img_src);
    message->img_dst = (IplImage*)CMEM_getPhys(img_dst);
    message->code=(int32_t)code;
    outMsg->openCVFxnIndex=CV_CVT_COLOR;
    outMsg->msg_ptr=CMEM_getPhys(message);

    MESSAGE_Execute(outMsg) ;

    img_src->imageData = data_img_src_virt;//restaure adresse virtuelle
    img_dst->imageData = data_img_dst_virt;

    CMEM_cacheInv((char*)img_dst->imageData,img_dst->imageSize);
    if (CMEM_free(message, &params) < 0) printf("Failed to free buffer at %#x\n",(unsigned int) message);

    MESSAGE_0Print ("Leaving dspCvtColor()\n") ;
}



Void
dspCloneImage( IplImage* img_src, IplImage* img_dst)
{
    DSPCV_query* outMsg;
    struct CloneImage_msg *message;
    CMEM_AllocParams params;
    params = CMEM_DEFAULTPARAMS;
    params.flags = CMEM_NONCACHED;
    params.type=CMEM_HEAP;
    char *data_img_src_virt,*data_img_dst_virt;

    MESSAGE_0Print ("Entered dspCloneImage()\n") ;

    // allocate processing request
    MESSAGE_Alloc(&outMsg);

    CMEM_cacheWb((char*)img_src->imageData,img_src->imageSize);
    data_img_src_virt=img_src->imageData;//sauvegarde adresse virtuelle
    data_img_dst_virt=img_dst->imageData;
    img_src->imageData = (char*)CMEM_getPhys(img_src->imageData);//remplace par adresse physique
    img_dst->imageData = (char*)CMEM_getPhys(img_dst->imageData);
    CMEM_cacheWb(img_src,sizeof(IplImage));
    CMEM_cacheWb(img_dst,sizeof(IplImage));

    message =(CloneImage_msg*)CMEM_alloc(sizeof(CloneImage_msg),&params);
    message->img_src = (IplImage*)CMEM_getPhys(img_src);
    message->img_dst = (IplImage*)CMEM_getPhys(img_dst);

    outMsg->openCVFxnIndex=CV_CLONE_IMAGE;
    outMsg->msg_ptr=CMEM_getPhys(message);

    MESSAGE_Execute(outMsg) ;

    img_src->imageData = data_img_src_virt;//restaure adresse virtuelle
    img_dst->imageData = data_img_dst_virt;

    CMEM_cacheInv((char*)img_dst->imageData,img_dst->imageSize);
    if (CMEM_free(message, &params) < 0) MESSAGE_1Print("Failed to free buffer at %#x\n",(unsigned int) message);

    MESSAGE_0Print ("Leaving dspCloneImage()\n") ;
}


Void
dspThreshold( IplImage* img_src, IplImage* img_dst, float threshold,float max_value,int threshold_type )
{
    DSPCV_query* outMsg;
    struct Threshold_msg *message;
    CMEM_AllocParams params;
    params = CMEM_DEFAULTPARAMS;
    params.flags = CMEM_NONCACHED;
    params.type=CMEM_HEAP;
    char *data_img_src_virt,*data_img_dst_virt;

    MESSAGE_0Print ("Entered dspThreshold()\n") ;

    // allocate processing request
    MESSAGE_Alloc(&outMsg);

    CMEM_cacheWb((char*)img_src->imageData,img_src->imageSize);
    data_img_src_virt=img_src->imageData;//sauvegarde adresse virtuelle
    data_img_dst_virt=img_dst->imageData;
    img_src->imageData = (char*)CMEM_getPhys(img_src->imageData);//remplace par adresse physique
    if(img_dst!= img_src) img_dst->imageData = (char*)CMEM_getPhys(img_dst->imageData);
    CMEM_cacheWb(img_src,sizeof(IplImage));
    if(img_dst!= img_src) CMEM_cacheWb(img_dst,sizeof(IplImage));

    message =(Threshold_msg*)CMEM_alloc(sizeof(Threshold_msg),&params);
    message->img_src = (IplImage*)CMEM_getPhys(img_src);
    message->img_dst = (IplImage*)CMEM_getPhys(img_dst);
    message->max_value=max_value;
    message->threshold=threshold;
    message->threshold_type=threshold_type;

    outMsg->openCVFxnIndex=CV_THRESHOLD;
    outMsg->msg_ptr=CMEM_getPhys(message);

    MESSAGE_Execute(outMsg) ;

    img_src->imageData = data_img_src_virt;//restaure adresse virtuelle
    if(img_dst!= img_src) img_dst->imageData = data_img_dst_virt;

    CMEM_cacheInv((char*)img_dst->imageData,img_dst->imageSize);
    if (CMEM_free(message, &params) < 0) MESSAGE_1Print("Failed to free buffer at %#x\n",(unsigned int) message);

    MESSAGE_0Print ("Leaving dspThreshold()\n") ;
}


Void
dspSobel( IplImage* img_src, IplImage* img_dst,int dx,int dy)
{
    DSPCV_query* outMsg;
    struct Sobel_msg *message;
    CMEM_AllocParams params;
    params = CMEM_DEFAULTPARAMS;
    params.flags = CMEM_NONCACHED;
    params.type=CMEM_HEAP;
    char *data_img_src_virt,*data_img_dst_virt;

    MESSAGE_0Print ("Entered dspSobel()\n") ;

    // allocate processing request
    MESSAGE_Alloc(&outMsg);

    CMEM_cacheWb((char*)img_src->imageData,img_src->imageSize);
    data_img_src_virt=img_src->imageData;//sauvegarde adresse virtuelle
    data_img_dst_virt=img_dst->imageData;
    img_src->imageData = (char*)CMEM_getPhys(img_src->imageData);//remplace par adresse physique
    img_dst->imageData = (char*)CMEM_getPhys(img_dst->imageData);
    CMEM_cacheWb(img_src,sizeof(IplImage));
    CMEM_cacheWb(img_dst,sizeof(IplImage));

    message =(Sobel_msg*)CMEM_alloc(sizeof(Sobel_msg),&params);
    message->img_src = (IplImage*)CMEM_getPhys(img_src);
    message->img_dst = (IplImage*)CMEM_getPhys(img_dst);
    message->dx=dx;
    message->dy=dy;
    outMsg->openCVFxnIndex=CV_SOBEL;
    outMsg->msg_ptr=CMEM_getPhys(message);

    MESSAGE_Execute(outMsg);

    img_src->imageData = data_img_src_virt;//restaure adresse virtuelle
    img_dst->imageData = data_img_dst_virt;

    CMEM_cacheInv((char*)img_dst->imageData,img_dst->imageSize);
    if (CMEM_free(message, &params) < 0) printf("Failed to free buffer at %#x\n",(unsigned int) message);

    MESSAGE_0Print ("Leaving dspSobel()\n") ;
}



int
dspHoughLines2( IplImage* img_src, CvMat* line_storage, int method,
                              float rho_step, float theta_step, int threshold,
                              float param1, float param2)
{
    DSPCV_query* outMsg;
    struct Houghlines2_msg *message;
    CMEM_AllocParams params;
    params = CMEM_DEFAULTPARAMS;
    params.flags = CMEM_NONCACHED;
    params.type=CMEM_HEAP;
    char *data_img_src_virt;
    float *data_dst_virt;
    int* nb_line;
    int result;

    MESSAGE_0Print ("Entered dspHoughLines2()\n") ;

    // allocate processing request
    MESSAGE_Alloc(&outMsg);

    CMEM_cacheWb((char*)img_src->imageData,img_src->imageSize);

    nb_line=(int*)CMEM_alloc(sizeof(int),&params);

    data_img_src_virt=img_src->imageData;//sauvegarde adresse virtuelle
    data_dst_virt=line_storage->data.fl;
    img_src->imageData = (char*)CMEM_getPhys(img_src->imageData);//remplace par adresse physique
    line_storage->data.fl = (float*)CMEM_getPhys(line_storage->data.fl);

    CMEM_cacheWb(img_src,sizeof(IplImage));
    CMEM_cacheWb(line_storage,sizeof(CvMat));

    message =(Houghlines2_msg*)CMEM_alloc(sizeof(Houghlines2_msg),&params);
    message->img_src = (IplImage*)CMEM_getPhys(img_src);
    message->line_storage=(CvMat*)CMEM_getPhys(line_storage);
    message->method=method;
    message->rho_step=rho_step;
    message->theta_step=theta_step;
    message->threshold=threshold;
    message->param1=param1;
    message->param2=param2;
    message->nb_line=(int*)CMEM_getPhys(nb_line);

    outMsg->openCVFxnIndex=CV_HOUGHLINES2;
    outMsg->msg_ptr=CMEM_getPhys(message);

    MESSAGE_Execute(outMsg) ;

    img_src->imageData = data_img_src_virt;//restaure adresse virtuelle
    line_storage->data.fl=data_dst_virt;
    result=*nb_line;

    CMEM_cacheInv((char*)line_storage->data.fl,line_storage->step*line_storage->rows*line_storage->cols);
    if (CMEM_free(message, &params) < 0) MESSAGE_1Print("Failed to free buffer at %#x\n",(unsigned int) message);
    if (CMEM_free(nb_line, &params) < 0) MESSAGE_1Print("Failed to free buffer at %#x\n",(unsigned int) nb_line);

    MESSAGE_0Print ("Leaving dspHoughLines2()\n") ;

    return result;
}

int
dspHoughLinesTracking( IplImage* img_src, CvMat* line_storage, int method,
                               float rho_step, float theta_moy,float delta_theta,float theta_step, int32_t threshold)
{
    DSPCV_query* outMsg;
    struct HoughlinesTracking_msg *message;
    CMEM_AllocParams params;
    params = CMEM_DEFAULTPARAMS;
    params.flags = CMEM_NONCACHED;
    params.type=CMEM_HEAP;
    char *data_img_src_virt;
    float *data_dst_virt;
    int* nb_line;
    int result;

    MESSAGE_0Print ("Entered dspHoughLinesTracking()\n") ;

    // allocate processing request
    MESSAGE_Alloc(&outMsg);

    CMEM_cacheWb((char*)img_src->imageData,img_src->imageSize);

    nb_line=(int*)CMEM_alloc(sizeof(int),&params);

    data_img_src_virt=img_src->imageData;//sauvegarde adresse virtuelle
    data_dst_virt=line_storage->data.fl;
    img_src->imageData = (char*)CMEM_getPhys(img_src->imageData);//remplace par adresse physique
    line_storage->data.fl = (float*)CMEM_getPhys(line_storage->data.fl);

    CMEM_cacheWb(img_src,sizeof(IplImage));
    CMEM_cacheWb(line_storage,sizeof(CvMat));

    message =(HoughlinesTracking_msg*)CMEM_alloc(sizeof(HoughlinesTracking_msg),&params);
    message->img_src = (IplImage*)CMEM_getPhys(img_src);
    message->line_storage=(CvMat*)CMEM_getPhys(line_storage);
    message->method=method;
    message->rho_step=rho_step;
    message->theta_moy=theta_moy;
    message->delta_theta=delta_theta;
    message->theta_step=theta_step;
    message->threshold=threshold;
    message->nb_line=(int*)CMEM_getPhys(nb_line);

    outMsg->openCVFxnIndex=CV_HOUGHLINES_TRACKING;
    outMsg->msg_ptr=CMEM_getPhys(message);

    MESSAGE_Execute(outMsg) ;

    img_src->imageData = data_img_src_virt;//restaure adresse virtuelle
    line_storage->data.fl=data_dst_virt;
    result=*nb_line;

    CMEM_cacheInv((char*)line_storage->data.fl,line_storage->step*line_storage->rows*line_storage->cols);
    if (CMEM_free(message, &params) < 0) MESSAGE_1Print("Failed to free buffer at %#x\n",(unsigned int) message);
    if (CMEM_free(nb_line, &params) < 0) MESSAGE_1Print("Failed to free buffer at %#x\n",(unsigned int) nb_line);

    MESSAGE_0Print ("Leaving dspHoughLinesTracking()\n") ;

    return result;
}

int
dspHoughLines2_test( IplImage* img_src, CvMat* line_storage, int method,
                               float rho, float theta_min,float theta_max,float theta_step, int32_t threshold)
{
    DSPCV_query* outMsg;
    struct HoughlinesTracking2_msg *message;
    CMEM_AllocParams params;
    params = CMEM_DEFAULTPARAMS;
    params.flags = CMEM_NONCACHED;
    params.type=CMEM_HEAP;
    char *data_img_src_virt;
    float *data_dst_virt;
    int* nb_line;
    int result;

    MESSAGE_0Print ("Entered dspHoughLines2_test()\n") ;

    if(rho<=1)//car tabSinCos sur 16bits, si rho plus grand il y aura debordement
    {
        printf("erreur dspHoughLines2 rho<=1 (%f)\n",rho);
        return 0;
    }
    if(theta_min<=-CV_PI/2)//algo fonctionne que sur ]-PI/2..PI/2]
    {
        printf("erreur dspHoughLines2 theta_min<-CV_PI/2 (%f)\n",theta_min);
        return 0;
    }
    if(theta_max>CV_PI/2)//algo fonctionne que sur -PI/2..PI/2
    {
        printf("erreur dspHoughLines2 theta_max>CV_PI/2 (%f>%f)\n",theta_max,CV_PI/2);
        return 0;
    }

    // allocate processing request
    MESSAGE_Alloc(&outMsg);

    CMEM_cacheWb((char*)img_src->imageData,img_src->imageSize);

    nb_line=(int*)CMEM_alloc(sizeof(int),&params);

    data_img_src_virt=img_src->imageData;//sauvegarde adresse virtuelle
    data_dst_virt=line_storage->data.fl;
    img_src->imageData = (char*)CMEM_getPhys(img_src->imageData);//remplace par adresse physique
    line_storage->data.fl = (float*)CMEM_getPhys(line_storage->data.fl);

    CMEM_cacheWb(img_src,sizeof(IplImage));
    CMEM_cacheWb(line_storage,sizeof(CvMat));

    message =(HoughlinesTracking2_msg*)CMEM_alloc(sizeof(HoughlinesTracking2_msg),&params);
    message->img_src = (IplImage*)CMEM_getPhys(img_src);
    message->line_storage=(CvMat*)CMEM_getPhys(line_storage);
    message->method=method;
    message->rho=rho;
    message->theta_min=theta_min;
    message->theta_max=theta_max;
    message->theta_step=theta_step;
    message->threshold=threshold;
    message->nb_line=(int*)CMEM_getPhys(nb_line);

    outMsg->openCVFxnIndex=CV_HOUGHLINES2_TEST;
    outMsg->msg_ptr=CMEM_getPhys(message);

    MESSAGE_Execute(outMsg) ;

    img_src->imageData = data_img_src_virt;//restaure adresse virtuelle
    line_storage->data.fl=data_dst_virt;
    result=*nb_line;

    CMEM_cacheInv((char*)line_storage->data.fl,line_storage->step*line_storage->rows*line_storage->cols);
    if (CMEM_free(message, &params) < 0) MESSAGE_1Print("Failed to free buffer at %#x\n",(unsigned int) message);
    if (CMEM_free(nb_line, &params) < 0) MESSAGE_1Print("Failed to free buffer at %#x\n",(unsigned int) nb_line);

    MESSAGE_0Print ("Leaving dspHoughLines2_test()\n") ;

    return result;
}