#include "wrappers.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "C64intrins.h"
#include <cxtypes.h>

IDMA3_Handle h_in,h_out;
pthread_mutex_t mutex;

bool initDsp(char* dspExecutable,int warningTimeoutMs)
{
    printf("initializing fake dsp\n");
    h_in=(IDMA3_Handle)malloc(sizeof(ACPY3_Params));
    h_out=(IDMA3_Handle)malloc(sizeof(ACPY3_Params));

    //mutex to protect acces to "dsp"
    //dsp functions are not reentrant because h_in and h_out are globals
    if(pthread_mutex_init(&mutex, NULL)!=0) {
        fprintf(stderr,"pthread_mutex_init error\n");
				return false;
    }
		return true;
}

bool closeDsp(void) {
    printf("closing fake dsp\n");

    //destroy mutex
    pthread_mutex_destroy(&mutex);
		return true;
}

void* MEM_alloc(int seg_id, int size,int seg_align)
{
    return malloc(size);
}

void* MEM_valloc(int seg_id, int size,int seg_align,char value)
{
    void* buf= malloc(size);
    memset( buf, value, size);

    return buf;

}

void MEM_free(int seg_id,void* buf, int size)
{
    free(buf);
}


void ACPY3_configure(IDMA3_Handle handle, ACPY3_Params *params,short transferNo)
{
    //printf("ACPY3_configure %x %x\n",handle,params);
    memcpy(handle,params,sizeof(ACPY3_Params));
    //printf("ACPY3_configure fin\n");
}

void ACPY3_fastConfigure16b(IDMA3_Handle handle, ACPY3_ParamField16b fieldId,
    unsigned short value, short transferNo)
{
    switch(fieldId)
    {
        case ACPY3_PARAMFIELD_ELEMENTSIZE:
            handle->elementSize=value;
            break;
        case ACPY3_PARAMFIELD_ELEMENTINDEX_SRC:
            handle->srcElementIndex=value;
            break;
        case ACPY3_PARAMFIELD_NUMELEMENTS:
            handle->numElements=value;
            break;
        default:
            printf("ACPY3_fastConfigure16b params %i non géré\n",fieldId);

    }

}

void ACPY3_fastConfigure32b(IDMA3_Handle handle, ACPY3_ParamField32b fieldId,
    unsigned int value, short transferNo)
{
    switch(fieldId)
    {
        case ACPY3_PARAMFIELD_SRCADDR:
            printf("ACPY3_ParamField32b: utiliser ACPY3_fastConfigure_ptr\n");
            break;
        case ACPY3_PARAMFIELD_DSTADDR:
            printf("ACPY3_ParamField32b: utiliser ACPY3_fastConfigure_ptr\n");
            break;
        default:
            printf("ACPY3_ParamField32b params %i non géré\n",fieldId);

    }
}

void ACPY3_fastConfigure_ptr(IDMA3_Handle handle, ACPY3_ParamField32b fieldId,
    void* ptr, short transferNo)
{
    switch(fieldId)
    {
        case ACPY3_PARAMFIELD_SRCADDR:
            handle->srcAddr=ptr;
            break;
        case ACPY3_PARAMFIELD_DSTADDR:
            handle->dstAddr=ptr;
            break;
        default:
            printf("ACPY3_ParamField32b params %i non géré\n",fieldId);

    }
}

void ACPY3_start(IDMA3_Handle handle)
{
    //plutot a mettre dans le wait
    if(handle->transferType == ACPY3_1D1D)
    {
        //printf("copie 1D1D src %x dst %x taille %i\n",handle->srcAddr,handle->dstAddr,handle->elementSize);
        memcpy(handle->dstAddr,handle->srcAddr,(int)handle->elementSize);
        return;
    }

    if(handle->transferType == ACPY3_2D1D)
    {
        //printf("copie 2D1D src %x dst %x taille %i\n",handle->srcAddr,handle->dstAddr,handle->elementSize);
        void* src=handle->srcAddr;
        void* dst=handle->dstAddr;
        int i;
        for(i=0;i<handle->numElements;i++)
        {
            //printf("copie 2D1D src %x dst %x taille %i, %i/%i\n",src,dst,handle->elementSize,i,handle->numElements);
            memcpy(dst,src,handle->elementSize);
            dst+=handle->elementSize;
            src+=handle->srcElementIndex;
        }
        return;
    }

    printf("transferType non géré %i\n",handle->transferType);
}

void ACPY3_wait(IDMA3_Handle handle)
{

}

void BCACHE_setMar(void* baseAddr, size_t byteSize,int value)
{

}

void BCACHE_wb(void* blockPtr, size_t byteCnt, int wait)
{

}

void dspSaveToJpeg(IplImage* src_img,const char* filename,unsigned char input_format,unsigned char output_format,unsigned char compression_level) {
	if(!cvSaveImage(filename,src_img)) printf("Could not save.\n");
}