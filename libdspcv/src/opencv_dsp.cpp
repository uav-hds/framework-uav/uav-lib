#include "opencv_dsp.h"
#include "stdio.h"
#include "highgui.h"
#include <cmem.h>


// CMEM <malloc>
void* icvCMEMAlloc( size_t size, void* )
{
    char *ptr;
    CMEM_AllocParams params;

    params = CMEM_DEFAULTPARAMS;
    params.flags = CMEM_CACHED;
    params.type=CMEM_HEAP;

//fprintf(stderr,"alloc %i\n",size);
    ptr = (char*)CMEM_alloc((size_t)(size + CMEM_MALLOC_ALIGN*((size >= 4096) + 1) + sizeof(char*)), &params);//code opencv
    //fprintf(stderr,"custom alloc CMEM virt:0x%x(phy:0x%x), size:%x , verifier l'alignement!!!\n",ptr,CMEM_getPhys(ptr),size);
//printf("alloc %x\n",ptr);
    if( !ptr ) return 0;

    return ptr;
}


// CMEM <free>
int icvCMEMFree( void* ptr, void*)
{
    CMEM_AllocParams params;

    params = CMEM_DEFAULTPARAMS;
    params.flags = CMEM_CACHED;
    params.type=CMEM_HEAP;

    //fprintf(stderr,"custom free CMEM %x(%x), verifier l'alignement!!!\n",ptr,CMEM_getPhys(ptr));
    if (CMEM_free(ptr, &params) < 0)
    {
        //fprintf(stderr, "Failed to free buffer at %#x\n",(unsigned int) ptr);
        free(ptr);
    }

    return 0;
}

