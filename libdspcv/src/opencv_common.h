//  created:    2012/01/18
//  filename:   opencv_common.h
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 6599
//
//  version:    $Id: $
//
//  purpose:    definition des echanges DSP/ARM
//
//
/*********************************************************************/

#include <stdint.h>

//a reorganiser
//1 fichier pour echange entre dsp et lib gpp
//le reste pour les definitions style code conversion couleur

#if defined OMAPL138 || defined OMAP3530
#include <msgq.h>
#endif

#include <cxtypes.h>

//liste des fonctions
#define CV_PYR_DOWN   0x0001
#define CV_GOOD_FEATURES_TO_TRACK 0x0002
#define CV_SOBEL 0x0003
#define CV_CVT_COLOR 0x0004
#define CV_CLONE_IMAGE 0x0005
#define CV_LK 0x0006
#define CV_JPEG 0x0007
#define CV_THRESHOLD 0x0008
#define CV_HOUGHLINES2 0x0009
#define CV_HOUGHLINES2_TEST 0x000a
#define CV_HOUGHLINES_TRACKING 0x000b

#define TEST 0xfffe
#define CV_QUIT 0xffff


//status renvoyés par le DSP
#define DSP_OK  1
#define DSP_KO 0


//reglages
#define MAX_POINTS_TO_TRACK 256

//codes conversions couleurs
#define DSP_BGR2GRAY 6
#define DSP_YUYV2GRAY 62
#define DSP_UYVY2GRAY 63

//formats d'images pour compression JPG
#define CHROMA_NA -1     /**< Chroma format not applicable. */
#define YUV_420P 1       /**< YUV 4:2:0 planer. */
#define YUV_422P 2       /**< YUV 4:2:2 planer. */
#define YUV_422IBE 3     /**< YUV 4:2:2 interleaved (big endian). */
#define YUV_422ILE 4     /**< YUV 4:2:2 interleaved (little endian). */
#define YUV_444P 5       /**< YUV 4:4:4 planer. */
#define YUV_411P 6       /**< YUV 4:1:1 planer. */
#define GRAY 7           /**< Gray format. */
//#define RGB 8            /**< RGB color format. */
#define YUV_420SP 9      /**< YUV 420 semi_planar format.(Luma 1st plane,
                             *   CbCr interleaved 2nd plane)
                             */
#define ARGB8888 10      /**< Alpha plane. */
#define RGB555 11        /**< RGB 555 color format. */
#define RGB565 12        /**< RGB 565 color format. */
#define YUV_444ILE 13    /**< YUV 4:4:4 interleaved (little endian). */
    /** Default setting. */
#define CHROMAFORMAT_DEFAULT YUV_422ILE


// messages from ARM to DSP
#if defined OMAPL138 || defined OMAP3530
typedef struct DSPCV_query {
    MSGQ_MsgHeader header;
    uint16_t openCVFxnIndex;
    uint32_t msg_ptr;
} DSPCV_query;
#endif

typedef struct CalcOpticalFlowPyrLK_msg {
    IplImage* img_A;
    IplImage* img_B;
    IplImage* pyr_A;
    IplImage* pyr_B;
    CvPoint* features_A;
    CvPoint* features_B;
    uint16_t count;
    CvSize winSize;
    uint8_t level;
    int8_t* feat_status;
    uint32_t* error;
    CvTermCriteria criteria;
    int32_t flags;
} CalcOpticalFlowPyrLK_msg;

typedef struct GoodFeaturesToTrack_msg {
    IplImage* img_src;
    CvPoint* features;
    float quality_level;
    float min_distance;
    uint32_t* total_count;
} GoodFeaturesToTrack_msg;


typedef struct PyrDown_msg {
    IplImage* img_src;
    IplImage* img_dst;
    uint8_t level;
} PyrDown_msg;

typedef struct CloneImage_msg {
    IplImage* img_src;
    IplImage* img_dst;
} CloneImage_msg;

typedef struct CvtColor_msg {
    IplImage* img_src;
    IplImage* img_dst;
    int32_t code;
} CvtColor_msg;

typedef struct Threshold_msg {
    IplImage* img_src;
    IplImage* img_dst;
    float threshold;
    float max_value;
    int32_t threshold_type;
} Threshold_msg;

typedef struct Sobel_msg {
    IplImage* img_src;
    IplImage* img_dst;
    int32_t dx;
    int32_t dy;
} Sobel_msg;

typedef struct Jpeg_msg {
    IplImage* img_src;
    uint8_t* data_img_dst;
    uint8_t compression_level;
    uint8_t input_format;
    uint8_t output_format;
    uint32_t* output_size;
} Jpeg_msg;

typedef struct Houghlines2_msg {
    IplImage* img_src;
    CvMat* line_storage;
    int32_t method;
    float rho_step;
    float theta_step;
    int32_t threshold;
    float param1;
    float param2;
    int32_t* nb_line;
} Houghlines2_msg;

typedef struct HoughlinesTracking_msg {
    IplImage* img_src;
    CvMat* line_storage;
    int32_t method;
    float rho_step;
    float theta_moy;
    float delta_theta;
    float theta_step;
    int32_t threshold;
    int32_t* nb_line;
} HoughlinesTracking_msg;

typedef struct HoughlinesTracking2_msg {
    IplImage* img_src;
    CvMat* line_storage;
    int32_t method;
    float rho;
    float theta_min;
    float theta_max;
    float theta_step;
    int32_t threshold;
    int32_t* nb_line;
} HoughlinesTracking2_msg;

