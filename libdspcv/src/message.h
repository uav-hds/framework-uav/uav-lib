/** ============================================================================
 *  @file   message.h
 *
 *  @path   $(DSPLINK)/gpp/src/samples/message/
 *
 *  @desc   Defines the configurable parameters for the message test which
 *          sends a message across the DSP processor and receives it back
 *          using DSP/BIOS LINK.
 *          It also does the data verification on the received message.
 *
 *  @ver    1.63
 *  ============================================================================
 *  Copyright (C) 2002-2009, Texas Instruments Incorporated -
 *  http://www.ti.com/
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *  *  Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *  *  Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *  *  Neither the name of Texas Instruments Incorporated nor the names of
 *     its contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 *  THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 *  PURPOSE ARE DISCLAIMED.  NO EVENT SHALL THE COPYRIGHT OWNER OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 *  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 *  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *  WHETHER  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 *  OTHERWISE) ARISING  ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *  ============================================================================
 */

#if !defined (MESSAGE_H)
#define MESSAGE_H

#if defined (__cplusplus)
extern "C" {
#endif /* defined (__cplusplus) */



// Configuration Macros

// Messaging buffer used by the application.
// Note: This buffer must be aligned according to the alignment expected
// by the device/platform.
#define APP_OUT_BUFFER_SIZE DSPLINK_ALIGN (sizeof (DSPCV_query), DSPLINK_BUF_ALIGN)
//#define APP_IN_BUFFER_SIZE DSPLINK_ALIGN (sizeof (DSPLIB_result), DSPLINK_BUF_ALIGN)

//void init_dsp( Char8 * dspExecutable, int warningTimeoutMs) ;


/** ============================================================================
 *  @func   MESSAGE_Execute
 *
 *  @desc   This function implements the execute phase for this application.
 *
 *  @arg    numIterations
 *              Number of times to send the message to the DSP.
 *
 *  @arg    processorId
 *             Id of the DSP Processor.
 *
 *  @ret    DSP_SOK
 *              Operation successfully completed.
 *          DSP_EFAIL
 *              MESSAGE execution failed.
 *
 *  @enter  None
 *
 *  @leave  None
 *
 *  @see    MESSAGE_Delete , MESSAGE_Create
 *  ============================================================================
 */


DSP_STATUS
MESSAGE_Execute( DSPCV_query* outMsg);

DSP_STATUS
MESSAGE_Alloc( DSPCV_query** outMsg);

DSP_STATUS
receive_message(void);


DSP_STATUS
send_message( DSPCV_query* outMsg);

bool init_dsp(const char* dspExecutable,int warningTimeoutMs=-1) ;
bool close_dsp(void) ;


/** ============================================================================
 *  affichage du debug DSP
 *
 *  ============================================================================
 */

void LogCallback (Uint32 event, Pvoid arg, Pvoid info);


#if defined (__cplusplus)
}
#endif /* defined (__cplusplus) */


#endif /* !defined (MESSAGE_H) */
