/* ***********************************************************
* THIS PROGRAM IS PROVIDED "AS IS". TI MAKES NO WARRANTIES OR
* REPRESENTATIONS, EITHER EXPRESS, IMPLIED OR STATUTORY,
* INCLUDING ANY IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
* FOR A PARTICULAR PURPOSE, LACK OF VIRUSES, ACCURACY OR
* COMPLETENESS OF RESPONSES, RESULTS AND LACK OF NEGLIGENCE.
* TI DISCLAIMS ANY WARRANTY OF TITLE, QUIET ENJOYMENT, QUIET
* POSSESSION, AND NON-INFRINGEMENT OF ANY THIRD PARTY
* INTELLECTUAL PROPERTY RIGHTS WITH REGARD TO THE PROGRAM OR
* YOUR USE OF THE PROGRAM.
*
* IN NO EVENT SHALL TI BE LIABLE FOR ANY SPECIAL, INCIDENTAL,
* CONSEQUENTIAL OR INDIRECT DAMAGES, HOWEVER CAUSED, ON ANY
* THEORY OF LIABILITY AND WHETHER OR NOT TI HAS BEEN ADVISED
* OF THE POSSIBILITY OF SUCH DAMAGES, ARISING IN ANY WAY OUT
* OF THIS AGREEMENT, THE PROGRAM, OR YOUR USE OF THE PROGRAM.
* EXCLUDED DAMAGES INCLUDE, BUT ARE NOT LIMITED TO, COST OF
* REMOVAL OR REINSTALLATION, COMPUTER TIME, LABOR COSTS, LOSS
* OF GOODWILL, LOSS OF PROFITS, LOSS OF SAVINGS, OR LOSS OF
* USE OR INTERRUPTION OF BUSINESS. IN NO EVENT WILL TI'S
* AGGREGATE LIABILITY UNDER THIS AGREEMENT OR ARISING OUT OF
* YOUR USE OF THE PROGRAM EXCEED FIVE HUNDRED DOLLARS
* (U.S.$500).
*
* Unless otherwise stated, the Program written and copyrighted
* by Texas Instruments is distributed as "freeware".  You may,
* only under TI's copyright in the Program, use and modify the
* Program without any charge or restriction.  You may
* distribute to third parties, provided that you transfer a
* copy of this license to the third party and the third party
* agrees to these terms by its first use of the Program. You
* must reproduce the copyright notice and any other legend of
* ownership on each copy or partial copy, of the Program.
*
* You acknowledge and agree that the Program contains
* copyrighted material, trade secrets and other TI proprietary
* information and is protected by copyright laws,
* international copyright treaties, and trade secret laws, as
* well as other intellectual property laws.  To protect TI's
* rights in the Program, you agree not to decompile, reverse
* engineer, disassemble or otherwise translate any object code
* versions of the Program to a human-readable form.  You agree
* that in no event will you alter, remove or destroy any
* copyright notice included in the Program.  TI reserves all
* rights not specifically granted under this license. Except
* as specifically provided herein, nothing in this agreement
* shall be construed as conferring by implication, estoppel,
* or otherwise, upon you, any license or other right under any
* TI patents, copyrights or trade secrets.
*
* You may not use the Program in non-TI devices.
* ********************************************************* */
/***************************************************************************/
/* Texas Instruments, Stafford, TX                                         */
/*									   */
/* NAME			C64intrins.c					   */
/*									   */
/* REVISION		C64intrins.c					   */
/*									   */
/* DESCRIPTION	C interpretation of the `C64xx intrinsics		   */
/*                                                                         */
/* AUTHOR       Gayathri Krishnamurthy                                     */
/***************************************************************************/

#if !defined (_TMS320C6400)

#include "C64intrins.h"
#include <stdio.h>



Word32 SAT_bit = 0;
volatile UWord32 GFPGFR = 0x0700001D; /* Default - change if needed */

/***************************************************************************/
/* _abs2 C function						           */
/***************************************************************************/
Word32 abs2_c(Word32 src)
{
    Word32 lo = (src << 16) >> 16;    /* Signed extract of lower half */
    Word32 hi = src >> 16;

    if (lo == MIN_16)
       lo = MAX_16;
    else
       lo = (lo < 0)? -lo:lo;

    if (hi == MIN_16)
       hi = MAX_16;
    else if (hi < 0)
       hi = -hi;

    /* Now pack these into one 32-bit word */
    lo &= 0x0ffff;
    hi = hi << 16;

    return (hi | lo);
}

/***************************************************************************/
/* _add2 C function						           */
/***************************************************************************/
Word32 add2_c(Word32 src1, Word32 src2)
{
   return (( ((src1 & 0xffff)+(src2 & 0xffff)) & 0xffff ) |
           ( ((src1 & 0xffff0000)+(src2 & 0xffff0000)) & 0xffff0000 )  );
}

/***************************************************************************/
/* _add4 C function						           */
/***************************************************************************/
Word32 add4_c(Word32 src1, Word32 src2)
{
   return (( ((src1 & 0xff)+(src2 & 0xff)) & 0xff ) |
           ( ((src1 & 0x0000ff00)+(src2 & 0x0000ff00)) & 0x0000ff00 ) |
           ( ((src1 & 0x00ff0000)+(src2 & 0x00ff0000)) & 0x00ff0000 ) |
           ( ((src1 & 0xff000000)+(src2 & 0xff000000)) & 0xff000000 )  );
}

/***************************************************************************/
/* _amem4_const C function					           	   */
/***************************************************************************/
UWord32 amem4_const_c(void* ptr)
{
    UWord32* pt=(UWord32*)ptr;
    return *pt;
}

/***************************************************************************/
/* _avg2 C function						           */
/***************************************************************************/
Word32 avg2_c(Word32 src1, Word32 src2)
{
   Word16 hi_1 = (src1 >> 16);
   Word16 hi_2 = (src2 >> 16);
   Word16 lo_1 = (Word16) src1;
   Word16 lo_2 = (Word16) src2;

   UWord16 lo_avg = (((Word32)lo_1 + lo_2 + 1) >> 1);
   Word16  hi_avg = (((Word32)hi_1 + hi_2 + 1) >> 1);

   return ( ((Word32)hi_avg) << 16 | lo_avg);
}

/***************************************************************************/
/* _avgu4 C function						           */
/***************************************************************************/
Word32 avgu4_c(UWord32 src1, UWord32 src2)
{
   UWord32 src1_1   = (src1 & 0x0000ff00) >> 8;
   UWord32 src2_1   = (src2 & 0x0000ff00) >> 8;

   UWord32 src1_2   = (src1 & 0x00ff0000) >> 16;
   UWord32 src2_2   = (src2 & 0x00ff0000) >> 16;

   UWord32 src1_3   = src1 >> 24;
   UWord32 src2_3   = src2 >> 24;

   UWord32 byte0_avg = ((src1 & 0xff) + (src2 & 0xff) + 1 >> 1);
   UWord32 byte1_avg = ((src1_1 + src2_1 + 1) >> 1) << 8;
   UWord32 byte2_avg = ((src1_2 + src2_2 + 1) >> 1) << 16;
   UWord32 byte3_avg = ((src1_3 + src2_3 + 1) >> 1) << 24;

   return (byte3_avg | byte2_avg | byte1_avg | byte0_avg);
}


/***************************************************************************/
/* _bitc4 C function						           */
/***************************************************************************/
UWord32 bitc4_c(UWord32 src)
{
   UWord32 result = 0;
   int i, j;

   for (j=0; j<4; j++)
   {
      UWord32 count = 0;
      for (i=0; i<8; i++)
      {
         count += (src & 0x01);
         src >>= 1;
      }
      result |= (count << (8*j));
   }

   return result;
}

/***************************************************************************/
/* _bitr C function	                                                   */
/***************************************************************************/
UWord32 bitr_c(UWord32 src)
{
   UWord32 i, ith_bit;
   UWord32 result = 0;

   /* Bits 1 through 15 occupy bits 31 through 16 of result */
   UWord32 shift_amount = 31;
   for (i=0; i<16; i++)
   {
	 ith_bit       = (src & (1 << i));
	 result       |= (ith_bit << shift_amount);
         shift_amount -= 2;
   }

   /* Bits 16 through 31 occupy bits 15 through 0 of result */
   shift_amount = 1;
   for (i=16; i<32; i++)
   {
	 ith_bit       = (src & (1 << i));
	 result       |= (ith_bit >> shift_amount);
         shift_amount += 2;
   }

   return (result);
}

/***************************************************************************/
/* _cmpeq2 C function						           */
/***************************************************************************/
Word32 cmpeq2_c(Word32 src1, Word32 src2)
{
   Word32 result = 0;

   if ((src1 & 0xffff) == (src2 & 0xffff))
       result |= 1;

   if ((src1 & 0xffff0000) == (src2 & 0xffff0000))
       result |= 2;

   return (result);
}

/***************************************************************************/
/* _cmpeq4 C function						           */
/***************************************************************************/
Word32 cmpeq4_c(Word32 src1, Word32 src2)
{
   Word32 result = 0;

   if ((src1 & 0xff) == (src2 & 0xff))
      result |= 1;
   if ((src1 & 0xff00) == (src2 & 0xff00))
      result |= 2;
   if ((src1 & 0xff0000) == (src2 & 0xff0000))
      result |= 4;
   if ((src1 & 0xff000000) == (src2 & 0xff000000))
      result |= 8;

   return (result);
}

/***************************************************************************/
/* _cmpgt2 C function						           */
/***************************************************************************/
Word32 cmpgt2_c(Word32 src1, Word32 src2)
{
   Word32 result = 0;

   if ((Word16)src1 > (Word16)src2)
       result |= 1;

   if ((Word16)(src1 >> 16) > (Word16)(src2 >> 16))
       result |= 2;

   return (result);
}

/***************************************************************************/
/* _cmpgtu4 C function						           */
/***************************************************************************/
UWord32 cmpgtu4_c(UWord32 src1, UWord32 src2)
{
   UWord32 result = 0;

   if ((src1 & 0xff) > (src2 & 0xff))
      result |= 1;
   if ((src1 & 0xff00) > (src2 & 0xff00))
      result |= 2;
   if ((src1 & 0xff0000) > (src2 & 0xff0000))
      result |= 4;
   if ((src1 & 0xff000000) > (src2 & 0xff000000))
      result |= 8;

   return (result);
}

/***************************************************************************/
/* _deal C function						           */
/***************************************************************************/
UWord32 deal_c(UWord32 src)
{
   UWord32 result = 0;

   /* Top half of result = odd bits */
   result |= (src & 0x80000000)          | ((src & 0x20000000) << 1)  |
	     ((src & 0x08000000) << 2)   | ((src & 0x02000000) << 3)  |
	     ((src & 0x00800000) << 4)   | ((src & 0x00200000) << 5)  |
	     ((src & 0x00080000) << 6)   | ((src & 0x00020000) << 7)  |
	     ((src & 0x00008000) << 8)   | ((src & 0x00002000) << 9)  |
	     ((src & 0x00000800) << 10)  | ((src & 0x00000200) << 11) |
	     ((src & 0x00000080) << 12)  | ((src & 0x00000020) << 13) |
	     ((src & 0x00000008) << 14)  | ((src & 0x00000002) << 15);

   /* Bottom half of result = even bits */
   result |= (src & 0x00000001) | ((src & 0x00000004) >> 1) |
	     ((src & 0x00000010) >> 2) | ((src & 0x00000040) >> 3) |
	     ((src & 0x00000100) >> 4) | ((src & 0x00000400) >> 5) |
	     ((src & 0x00001000) >> 6) | ((src & 0x00004000) >> 7) |
	     ((src & 0x00010000) >> 8) | ((src & 0x00040000) >> 9) |
	     ((src & 0x00100000) >> 10) | ((src & 0x00400000) >> 11) |
	     ((src & 0x01000000) >> 12) | ((src & 0x04000000) >> 13) |
	     ((src & 0x10000000) >> 14) | ((src & 0x40000000) >> 15);

   return (result);
}

/***************************************************************************/
/* _dotp2 C function						           */
/***************************************************************************/
Word32 dotp2_c(Word32 src1, Word32 src2)
{
   Word16 hi_1 = (src1 >> 16);
   Word16 hi_2 = (src2 >> 16);
   Word16 lo_1 = (Word16) src1;
   Word16 lo_2 = (Word16) src2;

   return (hi_1*hi_2 + lo_1*lo_2);
}

/***************************************************************************/
/* _ldotp2 C function						           */
/***************************************************************************/
Word40 ldotp2_c(Word32 src1, Word32 src2)
{
   Word16 hi_1 = (src1 >> 16);
   Word16 hi_2 = (src2 >> 16);
   Word16 lo_1 = (Word16) src1;
   Word16 lo_2 = (Word16) src2;

   return (hi_1*hi_2 + lo_1*lo_2);
}

/***************************************************************************/
/* _dotpn2 C function						           */
/***************************************************************************/
Word32 dotpn2_c(Word32 src1, Word32 src2)
{
   Word16 hi_1 = (src1 >> 16);
   Word16 hi_2 = (src2 >> 16);
   Word16 lo_1 = (Word16) src1;
   Word16 lo_2 = (Word16) src2;

   return (hi_1*hi_2 - lo_1*lo_2);
}

/***************************************************************************/
/* _dotpnrsu2 C function					           */
/***************************************************************************/
Word32 dotpnrsu2_c(Word32 src1, UWord32 src2)
{
   Word16  hi_1 = (src1 >> 16);
   UWord16 hi_2 = (src2 >> 16);
   Word16  lo_1 = (Word16)  src1;
   UWord16 lo_2 = (UWord16) src2;

   Word32 result = (hi_1*hi_2 - lo_1*lo_2 + 0x8000);

   return (result >> 16);
}

/***************************************************************************/
/* _dotprsu2 C function					           	   */
/***************************************************************************/
Word32 dotprsu2_c(Word32 src1, UWord32 src2)
{
   Word16  hi_1 = (src1 >> 16);
   UWord16 hi_2 = (src2 >> 16);
   Word16  lo_1 = (Word16)  src1;
   UWord16 lo_2 = (UWord16) src2;

   Word32 result = (hi_1*hi_2 + lo_1*lo_2 + 0x8000);

   return (result >> 16);
}

/***************************************************************************/
/* _dotpsu4 C function					           	   */
/***************************************************************************/
Word32 dotpsu4_c(Word32 src1, UWord32 src2)
{
   Word8 src1_3 = (src1 >> 24);
   Word8 src1_2 = (src1 >> 16);
   Word8 src1_1 = (src1 >> 8);
   Word8 src1_0 = (Word8) src1;

   UWord8 src2_3 = (src2 >> 24);
   UWord8 src2_2 = (src2 << 8) >> 24;
   UWord8 src2_1 = (src2 << 16) >> 24;
   UWord8 src2_0 = (UWord8) src2;

   return (src1_3*src2_3 + src1_2*src2_2 + src1_1*src2_1 + src1_0*src2_0);
}

/***************************************************************************/
/* _dotpu4 C function					           	   */
/***************************************************************************/
UWord32 dotpu4_c(UWord32 src1, UWord32 src2)
{
   UWord8 src1_3 = (src1 >> 24);
   UWord8 src1_2 = (src1 << 8) >> 24;
   UWord8 src1_1 = (src1 << 16) >> 24;
   UWord8 src1_0 = (UWord8) src1;

   UWord8 src2_3 = (src2 >> 24);
   UWord8 src2_2 = (src2 << 8) >> 24;
   UWord8 src2_1 = (src2 << 16) >> 24;
   UWord8 src2_0 = (UWord8) src2;

   return (src1_3*src2_3 + src1_2*src2_2 + src1_1*src2_1 + src1_0*src2_0);
}

/***************************************************************************/
/* ddotpl2_c C function					           	   */
/***************************************************************************/
Word64 ddotpl2_c(Word64 src1, UWord32 src2)
{
    UWord32 tmp=_packlh2(_hill(src1),_loll(src1));
    UWord32 tmp1=_dotp2(src2,tmp);
    UWord32 tmp2=_dotp2(src2,_loll(src1));

    return _itoll(tmp1,tmp2);

}

/***************************************************************************/
/* ext_c C function					           	   */
/***************************************************************************/
Word32 ext_c(Word32 src2,UWord32 csta,UWord32 cstb)
{
    int i;
    src2=(src2<<csta);
    for(i=0;i<cstb;i++) src2=src2/2;

    return src2;
}


/***************************************************************************/
/* extu_c C function					           	   */
/***************************************************************************/
UWord32 extu_c(UWord32 src2,UWord32 csta,UWord32 cstb)
{
    return (UWord32)((src2<<csta)>>cstb);
}

/***************************************************************************/
/* ftoi_c C function					           	   */
/***************************************************************************/
UWord32 ftoi_c(float value)
{
    return (UWord32)value;
}

/***************************************************************************/
/* _gmpy helper function	                                           */
/*                                                                         */
/* (Borrowed with permission from David Hoyle's code in Tor Jeremiassen's  */
/*  C6x simulator and modified to suit this document. With grateful thanks */
/*  to author).                                                            */
/***************************************************************************/
UWord32 gmpy (UWord8 op1, UWord8 op2, UWord32 size, UWord32 polynomial)
{
  unsigned int pp[8];
  int prod,bd,mask,lbd;
  unsigned int i;


  op2 = (op2 >> (7-size)) << (7-size);
  bd = 0x00000001;
  for ( i=0; i<8; i++ ) {
    if ( op2 & bd )
      pp[i] = op1 << i;
    else
      pp[i] = 0;
    bd <<= 1;
  }

  prod = pp[0] ^ pp[1] ^ pp[2] ^ pp[3] ^ pp[4] ^ pp[5] ^ pp[6] ^ pp[7];

  mask = ( 0x00000100 | polynomial ) << 6;
  lbd  = 0x00004000;
  for ( i=0; i<size; i++ ) {
    if ( prod & lbd )
      prod ^= mask;
    mask >>= 1;
    lbd  >>= 1;
  }

  prod >>= (7-size);
  prod &= (0xffffffff << (7-size));
  return (0xff & prod) ;

} /* gmpy */


/***************************************************************************/
/* _gmpy4 C function							   */
/*								           */
/* (Borrowed with permission from Tor Jeremiassen's C6x simulator and      */
/*  modified to suit this document. Many thanks to author).                */
/***************************************************************************/
UWord32 gmpy4_c(UWord32 src1, UWord32 src2)
{
    UWord8 src1_0 = (UWord8) (0xff & src1);
    UWord8 src2_0 = (UWord8) (0xff & src2);
    UWord8 src1_1 = (UWord8) (0xff & (src1 >> 8));
    UWord8 src2_1 = (UWord8) (0xff & (src2 >> 8));
    UWord8 src1_2 = (UWord8) (0xff & (src1 >> 16));
    UWord8 src2_2 = (UWord8) (0xff & (src2 >> 16));
    UWord8 src1_3 = (UWord8) (0xff & (src1 >> 24));
    UWord8 src2_3 = (UWord8) (0xff & (src2 >> 24));

    UWord32 gpoly = GFPGFR & 0xff;
    UWord32 gsize = (GFPGFR >> 24) & 0x7;
    UWord32 r0, r1, r2, r3;

    r0 = gmpy(src1_0,src2_0,gsize,gpoly);
    r1 = gmpy(src1_1,src2_1,gsize,gpoly) << 8;
    r2 = gmpy(src1_2,src2_2,gsize,gpoly) << 16;
    r3 = gmpy(src1_3,src2_3,gsize,gpoly) << 24;

    return(r0 | r1 | r2 | r3);
}

/***************************************************************************/
/* _hi C function					           	   */
/***************************************************************************/
UWord32 hi_c(double value)
{
    printf("hi deprecated\n");
    typedef union Cv64suf
    {
        UWord64 u;
        double f;
    }
    Cv64suf;

    Cv64suf tmp;
    tmp.f=value;

    return (UWord32)((tmp.u>>32)&0xffffffff);
}

/***************************************************************************/
/* _lo C function					           	   */
/***************************************************************************/
UWord32 lo_c(double value)
{
    printf("lo deprecated\n");
    typedef union Cv64suf
    {
        UWord64 u;
        double f;
    }
    Cv64suf;

    Cv64suf tmp;
    tmp.f=value;

    return (UWord32)(tmp.u&0xffffffff);
}

/***************************************************************************/
/* _hill C function					           	   */
/***************************************************************************/
UWord32 hill_c(UWord64 value)
{
    return (UWord32)((value>>32)&0xffffffff);
}

/***************************************************************************/
/* _loll C function					           	   */
/***************************************************************************/
UWord32 loll_c(UWord64 value)
{
    return (UWord32)(value&0xffffffff);
}

/***************************************************************************/
/* itod_c C function					           	   */
/***************************************************************************/
double itod_c(UWord32 src2, UWord32 src1)
{
    typedef union Cv64suf
    {
        UWord64 u;
        double f;
    }
    Cv64suf;

    Cv64suf tmp;
    tmp.u=(UWord64)( (((UWord64)src2)<<32)+((UWord64)src1) );

    return tmp.f;
}

/***************************************************************************/
/* itoll_c C function					           	   */
/***************************************************************************/
UWord64 itoll_c(UWord32 src2, UWord32 src1)
{
    return (UWord64)( (((UWord64)src2)<<32)+((UWord64)src1) );
}


/***************************************************************************/
/* _max2 C function					           	   */
/***************************************************************************/
Word32 max2_c(Word32 src1, Word32 src2)
{
   Word16 hi_1 = (src1 >> 16);
   Word16 hi_2 = (src2 >> 16);
   Word16 lo_1 = (Word16) src1;
   Word16 lo_2 = (Word16) src2;
   Word32 result = 0;

   result = (hi_1 >= hi_2)? hi_1:hi_2;
   result <<= 16;
   result |= (UWord16)((lo_1 >= lo_2)? lo_1:lo_2);
   return (result);
}

/***************************************************************************/
/* _maxu4 C function					           	   */
/***************************************************************************/
UWord32 maxu4_c(UWord32 src1, UWord32 src2)
{
   UWord32 src1_3 = (src1 >> 24);
   UWord32 src1_2 = (src1 << 8) >> 24;
   UWord32 src1_1 = (src1 << 16) >> 24;
   UWord32 src1_0 = (src1 & 0xff);

   UWord32 src2_3 = (src2 >> 24);
   UWord32 src2_2 = (src2 << 8) >> 24;
   UWord32 src2_1 = (src2 << 16) >> 24;
   UWord32 src2_0 = (src2 & 0xff);

   src1_3 = (src1_3 >= src2_3)? src1_3: src2_3;
   src1_2 = (src1_2 >= src2_2)? src1_2: src2_2;
   src1_1 = (src1_1 >= src2_1)? src1_1: src2_1;
   src1_0 = (src1_0 >= src2_0)? src1_0: src2_0;

   return(src1_3 << 24 | src1_2 << 16 | src1_1 << 8 | src1_0 );
}

/***************************************************************************/
/* mem4_const_c C function					           	   */
/***************************************************************************/
UWord32 mem4_const_c(void* ptr)
{
    UWord32* pt=(UWord32*)ptr;
    return *pt;
}

/***************************************************************************/
/* mem8_const_c C function					           	   */
/***************************************************************************/
UWord64 mem8_const_c(void* ptr)
{
    UWord64* pt=(UWord64*)ptr;
    return *pt;
}

/***************************************************************************/
/* memd8_const_c C function					           	   */
/***************************************************************************/
double memd8_const_c(void* ptr)
{
    printf("memd8_const deprecated\n");
    double* pt=(double*)ptr;
    return *pt;
}

/***************************************************************************/
/* _min2 C function					           	   */
/***************************************************************************/
Word32 min2_c(Word32 src1, Word32 src2)
{
   Word16 hi_1 = (src1 >> 16);
   Word16 hi_2 = (src2 >> 16);
   Word16 lo_1 = (Word16) src1;
   Word16 lo_2 = (Word16) src2;
   Word32 result = 0;

   result = (hi_1 <= hi_2)? hi_1:hi_2;
   result <<= 16;
   result |= (UWord16)((lo_1 <= lo_2)? lo_1:lo_2);
   return (result);
}

/***************************************************************************/
/* _minu4 C function					           	   */
/***************************************************************************/
UWord32 minu4_c(UWord32 src1, UWord32 src2)
{
   UWord32 src1_3 = (src1 >> 24);
   UWord32 src1_2 = (src1 << 8) >> 24;
   UWord32 src1_1 = (src1 << 16) >> 24;
   UWord32 src1_0 = src1 & 0xff;

   UWord32 src2_3 = (src2 >> 24);
   UWord32 src2_2 = (src2 << 8) >> 24;
   UWord32 src2_1 = (src2 << 16) >> 24;
   UWord32 src2_0 = src2 & 0xff;

   src1_3 = (src1_3 <= src2_3)? src1_3: src2_3;
   src1_2 = (src1_2 <= src2_2)? src1_2: src2_2;
   src1_1 = (src1_1 <= src2_1)? src1_1: src2_1;
   src1_0 = (src1_0 <= src2_0)? src1_0: src2_0;

   return (src1_3 << 24 | src1_2 << 16 | src1_1 << 8 | src1_0 );
}

/***************************************************************************/
/* _mpy32 C function					           	   */
/***************************************************************************/
Word64 mpy32ll_c(Word32 src1, Word32 src2)
{
    return (Word64)src1*(Word64)src2;
}

/***************************************************************************/
/* _mpy2 C function					           	   */
/***************************************************************************/
Word64 mpy2_c(Word32 src1, Word32 src2)
{
   Word16 hi_1 = (src1 >> 16);
   Word16 hi_2 = (src2 >> 16);
   Word16 lo_1 = (Word16) src1;
   Word16 lo_2 = (Word16) src2;

   Word32 res_hi  = (Word32)hi_1 * hi_2;
   UWord32 res_lo = (Word32)lo_1 * lo_2;

   return(((Word64)res_hi << 32) | (UWord64)res_lo);
}

double mpy2_wrapper(Word32 x, Word32 y)
{
    printf("Deprecated: use _mpy2ll instead\n");
  Word64 temp = mpy2_c(x, y);
  return (_itod((Word32)(temp >> 32), (Word32)temp));
}


/***************************************************************************/
/* _mpyhi C function					           	   */
/***************************************************************************/
Word64 mpyhi_c(Word32 src1, Word32 src2)
{
   Word16 hi_1 = (Word16)(src1 >> 16);

   return( ((Word64)hi_1 * src2));
}

double mpyhi_wrapper(Word32 x, Word32 y)
{
    printf("mpyhideprecated\n");
  Word64 temp = mpyhi_c(x, y);
  return (_itod((Word32)(temp >> 32), (Word32)temp));
}

/***************************************************************************/
/* _mpyli C function					           	   */
/***************************************************************************/
Word64 mpyli_c(Word32 src1, Word32 src2)
{
   Word16 lo_1 = (Word16)src1;

   return( (Word64)(lo_1 * src2));
}

double mpyli_wrapper(Word32 x, Word32 y)
{
    printf("mpyli deprecated\n");
  Word64 temp = mpyli_c(x, y);
  return (_itod((Word32)(temp >> 32), (Word32)temp));
}

/***************************************************************************/
/* _mpyhir C function					           	   */
/***************************************************************************/
Word32 mpyhir_c(Word32 src1, Word32 src2)
{
   Word16 hi_1   = (Word16)(src1 >> 16);
   Word64 result = ((hi_1*(Word64)src2)+0x4000) >> 15;

   return ((Word32)result);
   //return( ((hi_1 * src2) + 0x4000) >> 15);
}

/***************************************************************************/
/* _mpylir C function					           	   */
/***************************************************************************/
Word32 mpylir_c(Word32 src1, Word32 src2)
{
   Word16 lo_1   = (Word16)src1;
   Word64 result = ((lo_1*(Word64)src2)+0x4000) >> 15;

   return ((Word32)result);
}


/***************************************************************************/
/* _mpysu4 C function					           	   */
/***************************************************************************/
Word64 mpysu4_c(Word32 src1, UWord32 src2)
{
   Word8 src1_3 = (src1 >> 24);
   Word8 src1_2 = (src1 << 8) >> 24;
   Word8 src1_1 = (src1 << 16) >> 24;
   Word8 src1_0 = (Word8) src1;

   UWord8 src2_3 = (src2 >> 24);
   UWord8 src2_2 = (src2 << 8) >> 24;
   UWord8 src2_1 = (src2 << 16) >> 24;
   UWord8 src2_0 = (UWord8) src2 ;

   UWord16 result_0 = (UWord16)src2_0*src1_0;
   UWord16 result_1 = (UWord16)src2_1*src1_1;
   UWord16 result_2 = (UWord16)src2_2*src1_2;
   UWord16 result_3 = (UWord16)src2_3*src1_3;

   UWord32 result_lo = (((UWord32)result_1) << 16) | result_0;
   UWord32 result_hi = (((UWord32)result_3) << 16) | result_2;

   return(( ((Word64) result_hi) << 32) | (UWord64)result_lo);
}


double mpysu4_wrapper(Word32 x, UWord32 y)
{
    printf("mpysu4 deprecated\n");
  Word64 temp = mpysu4_c(x, y);
  return (_itod((Word32)(temp >> 32), (Word32)temp));
}

/***************************************************************************/
/* _mpyu4 C function					           	   */
/***************************************************************************/
UWord64 mpyu4_c(UWord32 src1, UWord32 src2)
{
   UWord8 src1_3 = (src1 >> 24);
   UWord8 src1_2 = (src1 << 8) >> 24;
   UWord8 src1_1 = (src1 << 16) >> 24;
   UWord8 src1_0 = (UWord8) src1;

   UWord8 src2_3 = (src2 >> 24);
   UWord8 src2_2 = (src2 << 8) >> 24;
   UWord8 src2_1 = (src2 << 16) >> 24;
   UWord8 src2_0 = (UWord8) src2;

   UWord32 result_lo = 0;
   UWord32 result_hi = 0;

   result_lo = ((UWord32)src1_0*src2_0) | ((UWord32)src1_1*src2_1) << 16;
   result_hi = ((UWord32)src1_2*src2_2) | ((UWord32)src1_3*src2_3) << 16;

   return(( ((UWord64) result_hi) << 32) | (UWord64)(result_lo));
}

double mpyu4_wrapper(UWord32 x, UWord32 y)
{
  Word64 temp = mpyu4_c(x, y);
  return (_itod((Word32)(temp >> 32), (Word32)temp));
}

/***************************************************************************/
/* _mvd C function					           	   */
/***************************************************************************/
Word32 mvd_c(Word32 src)
{
   return(src);
}

/***************************************************************************/
/* nround_c C function					           	   */
/***************************************************************************/
Word32 nround_c(double value)
{
    typedef union Cv64suf
    {
        UWord64 u;
        double f;
    }
    Cv64suf;

    /*
     the algorithm was taken from Agner Fog's optimization guide
     at http://www.agner.org/assem
     */
    Cv64suf temp;
    temp.f = value + 6755399441055744.0;
    return (Word32)temp.u;
}
/***************************************************************************/
/* _pack2 C function						           */
/***************************************************************************/
UWord32 pack2_c(UWord32 src1, UWord32 src2)
{
   UWord16 lo_1 = (UWord16) src1;
   UWord16 lo_2 = (UWord16) src2;

   return ( ( ((UWord32) lo_1) << 16) | (UWord32) lo_2);
}

/***************************************************************************/
/* _packh2 C function						           */
/***************************************************************************/
UWord32 packh2_c(UWord32 src1, UWord32 src2)
{
   UWord32 hi_1 = src1 >> 16;
   UWord32 hi_2 = src2 >> 16;

   return (hi_1 << 16 | hi_2);
}

/***************************************************************************/
/* _packh4 C function						           */
/***************************************************************************/
UWord32 packh4_c(UWord32 src1, UWord32 src2)
{
   UWord32 src1_3 = (src1 >> 24);
   UWord32 src1_1 = (src1 << 16) >> 24;

   UWord32 src2_3 = (src2 >> 24);
   UWord32 src2_1 = (src2 << 16) >> 24;

   return (src1_3 << 24 | src1_1 << 16 | src2_3 << 8 | src2_1);
}

/***************************************************************************/
/* _packl4 C function						           */
/***************************************************************************/
UWord32 packl4_c(UWord32 src1, UWord32 src2)
{
   UWord32 src1_2 = (src1 << 8) >> 24;
   UWord32 src1_0 = src1 & 0xff;

   UWord32 src2_2 = (src2 << 8) >> 24;
   UWord32 src2_0 = src2 & 0xff;

   return (src1_2 << 24 | src1_0 << 16 | src2_2 << 8 | src2_0);
}

/***************************************************************************/
/* _packhl2 C function						           */
/***************************************************************************/
UWord32 packhl2_c(UWord32 src1, UWord32 src2)
{
   UWord32 hi_1 = src1 >> 16;
   UWord32 lo_2 = src2 & 0xffff;

   return (hi_1 << 16 | lo_2);
}

/***************************************************************************/
/* _packlh2 C function						           */
/***************************************************************************/
UWord32 packlh2_c(UWord32 src1, UWord32 src2)
{
   UWord32 lo_1 = src1 & 0xffff;
   UWord32 hi_2 = src2 >> 16;

   return (lo_1 << 16 | hi_2);
}

/***************************************************************************/
/* _rotl C function						           */
/***************************************************************************/
UWord32 rotl_c(UWord32 src1, UWord32 src2)
{
   src2 &= 0x1f;    /* Extract 5 lsbs - rest are ignored */

   return ( (src1 << src2) | (src1 >> (32-src2)) );
}

/***************************************************************************/
/* _sadd C function                                                       */
/***************************************************************************/
Word32 sadd_c(Word32 src1, Word32 src2)
{
   Word64 src_1 = (Word64) src1;
   Word64 src_2 = (Word64) src2;

   Word64 result = src_1 + src_2;

   if (result > MAX_INT)
       result = MAX_INT;

   else if (result < MIN_INT)
       result = MIN_INT;


   return (Word32)result;
}

/***************************************************************************/
/* _sadd2 C function                                                       */
/***************************************************************************/
Word32 sadd2_c(Word32 src1, Word32 src2)
{
   Word16 hi_1 = (Word16) (src1 >> 16);
   Word16 lo_1 = (Word16) src1;
   Word16 hi_2 = (Word16) (src2 >> 16);
   Word16 lo_2 = (Word16) src2;

   Word32 result_lo = (Word32) lo_1 + lo_2;
   Word32 result_hi = (Word32) hi_1 + hi_2;

   if (result_lo > MAX_16)
       result_lo = MAX_16;

   else if (result_lo < MIN_16)
       result_lo = MIN_16;

   if (result_hi > MAX_16)
       result_hi = MAX_16;

   else if (result_hi < MIN_16)
       result_hi = MIN_16;

   return( (result_hi << 16) | (result_lo & 0xffff) );
}

/***************************************************************************/
/* _saddus2 C function						           */
/***************************************************************************/
Word32 saddus2_c(UWord32 src1, Word32 src2)
{
   UWord16 hi_1 = (UWord16) (src1 >> 16);
   UWord16 lo_1 = (UWord16) src1;
   Word16 hi_2  = (Word16) (src2 >> 16);
   Word16 lo_2  = (Word16) src2;

   Word32 result_lo = (Word32) lo_1 + lo_2;
   Word32 result_hi = (Word32) hi_1 + hi_2;

   if (result_lo > 65535)
       result_lo = 65535;

   if (result_lo < 0)
       result_lo = 0;

   if (result_hi > 65535)
       result_hi = 65535;

   if (result_hi < 0)
       result_hi = 0;

   return( (result_hi << 16) | (UWord16)result_lo );
}

/***************************************************************************/
/* _saddu4 C function						           */
/***************************************************************************/
UWord32 saddu4_c(UWord32 src1, UWord32 src2)
{
   UWord8 src1_3 = (src1 >> 24);
   UWord8 src1_2 = (src1 << 8) >> 24;
   UWord8 src1_1 = (src1 << 16) >> 24;
   UWord8 src1_0 = (UWord8) src1;

   UWord8 src2_3 = (src2 >> 24);
   UWord8 src2_2 = (src2 << 8) >> 24;
   UWord8 src2_1 = (src2 << 16) >> 24;
   UWord8 src2_0 = (UWord8) src2;

   UWord32 result_3 = (UWord32) src1_3 + src2_3;
   UWord32 result_2 = (UWord32) src1_2 + src2_2;
   UWord32 result_1 = (UWord32) src1_1 + src2_1;
   UWord32 result_0 = (UWord32) src1_0 + src2_0;

   if (result_3 > 255)
       result_3 = 255;

   if (result_2 > 255)
       result_2 = 255;

   if (result_1 > 255)
       result_1 = 255;

   if (result_0 > 255)
       result_0 = 255;

   return( result_3 << 24 | result_2 << 16 | result_1 << 8 | result_0 );
}

/***************************************************************************/
/* _shfl C function						           */
/***************************************************************************/
UWord32 shfl_c(UWord32 src)
{
   UWord32 result = 0;

   /* Bottom half of src becomes even bits of dst */
   result |= (src & 0x00000001)         | ((src & 0x00000002) << 1)  |
	     ((src & 0x00000004) << 2)  | ((src & 0x00000008) << 3)  |
	     ((src & 0x00000010) << 4)  | ((src & 0x00000020) << 5)  |
	     ((src & 0x00000040) << 6)  | ((src & 0x00000080) << 7)  |
	     ((src & 0x00000100) << 8)  | ((src & 0x00000200) << 9)  |
	     ((src & 0x00000400) << 10) | ((src & 0x00000800) << 11) |
	     ((src & 0x00001000) << 12) | ((src & 0x00002000) << 13) |
	     ((src & 0x00004000) << 14) | ((src & 0x00008000) << 15);

   /* Top half of src becomes odd bits of dst */
   result |= (src & 0x80000000)         | ((src & 0x40000000) >> 1)  |
	     ((src & 0x20000000) >> 2)  | ((src & 0x10000000) >> 3)  |
	     ((src & 0x08000000) >> 4)  | ((src & 0x04000000) >> 5)  |
	     ((src & 0x02000000) >> 6)  | ((src & 0x01000000) >> 7)  |
	     ((src & 0x00800000) >> 8)  | ((src & 0x00400000) >> 9)  |
	     ((src & 0x00200000) >> 10) | ((src & 0x00100000) >> 11) |
	     ((src & 0x00080000) >> 12) | ((src & 0x00040000) >> 13) |
	     ((src & 0x00020000) >> 14) | ((src & 0x00010000) >> 15);

   return (result);
}

/***************************************************************************/
/* _shlmb C function						           */
/***************************************************************************/
UWord32 shlmb_c(UWord32 src1, UWord32 src2)
{
   UWord32 src1_3 = (src1 >> 24);
   UWord32 src2_2 = (src2 << 8) >> 24;
   UWord32 src2_1 = (src2 << 16) >> 24;
   UWord32 src2_0 = src2 & 0xff;

   return (src2_2 << 24 | src2_1 << 16 | src2_0 << 8 | src1_3);
}

/***************************************************************************/
/* _shrmb C function						           */
/***************************************************************************/
UWord32 shrmb_c(UWord32 src1, UWord32 src2)
{
   UWord32 src1_0 = src1 & 0xff;
   UWord32 src2_3 = (src2 >> 24);
   UWord32 src2_2 = (src2 << 8) >> 24;
   UWord32 src2_1 = (src2 << 16) >> 24;

   return (src1_0 << 24 | src2_3 << 16 | src2_2 << 8 | src2_1);
}

/***************************************************************************/
/* _shr2 C function						           */
/***************************************************************************/
Word32 shr2_c(Word32 src1, UWord32 src2)
{
    Word16 hi = (Word16) (src1 >> 16);
    Word16 lo = (Word16) src1;

    src2 &= 0x001f; /* Ignore bits 5 through 31 */

    if (src2 >= 16)
       src2 = 15;

    return((hi >> src2) << 16 | (UWord16)(lo >> src2));
}

/***************************************************************************/
/* _shru2 C function						           */
/***************************************************************************/
UWord32 shru2_c(UWord32 src1, UWord32 src2)
{
    UWord32 hi = src1 >> 16;
    UWord32 lo = src1 & 0xffff;

    src2 &= 0x001f; /* Ignore bits 5 through 31 */

    if (src2 >= 16)
       return(0);

    return ((hi >> src2) << 16 | lo >> src2);
}

/***************************************************************************/
/* _smpy2 C function						           */
/***************************************************************************/
Word64 smpy2_c(Word32 src1, Word32 src2)
{
    Word16 hi_1 = (Word16) (src1 >> 16);
    Word16 lo_1 = (Word16) src1;

    Word16 hi_2 = (Word16) (src2 >> 16);
    Word16 lo_2 = (Word16) src2;

    Word32  result_hi = ((Word32)hi_1 * hi_2) << 1;
    UWord32  result_lo = ((UWord32)lo_1 * lo_2) << 1;

    if (result_hi == 0x80000000)
    {
        result_hi = 0x7fffffff;
        SAT_bit   = 1;
    }

    if (result_lo == 0x80000000)
    {
        result_lo = 0x7fffffff;
        SAT_bit   = 1;
    }

    return( ((Word64)result_hi << 32) | (UWord64)result_lo);
}

double smpy2_wrapper(Word32 x, Word32 y)
{
     printf("smpy2 deprecated\n");
  Word64 temp = smpy2_c(x, y);
  return (_itod((Word32)(temp >> 32), (Word32)temp));
}

/***************************************************************************/
/* _spack2 C function						           */
/***************************************************************************/
Word32 spack2_c(Word32 src1, Word32 src2)
{
    Word16 result_hi = (src1 > MAX_16)? MAX_16: (src1 < MIN_16)?MIN_16:src1;
    Word16 result_lo = (src2 > MAX_16)? MAX_16: (src2 < MIN_16)?MIN_16:src2;

    return( ((UWord32)result_hi << 16) | (UWord16)result_lo);
}

/***************************************************************************/
/* _spacku4 C function						           */
/***************************************************************************/
UWord32 spacku4_c(Word32 src1, Word32 src2)
{
    Word16 hi_1 = (Word16) (src1 >> 16);
    Word16 lo_1 = (Word16) src1;

    Word16 hi_2 = (Word16) (src2 >> 16);
    Word16 lo_2 = (Word16) src2;

    UWord8 result_3 = (hi_1 < 0)?0: (hi_1 > 0xff)?0xff:hi_1;
    UWord8 result_2 = (lo_1 < 0)?0: (lo_1 > 0xff)?0xff:lo_1;
    UWord8 result_1 = (hi_2 < 0)?0: (hi_2 > 0xff)?0xff:hi_2;
    UWord8 result_0 = (lo_2 < 0)?0: (lo_2 > 0xff)?0xff:lo_2;

    return (((UWord32)result_3) << 24 | ((UWord32)result_2) << 16 |
   	    ((UWord32)result_1) <<  8 | (UWord32)result_0);
}

/***************************************************************************/
/* _sshvl C function						           */
/***************************************************************************/
Word32 sshvl_c (Word32 src1, Word32 src2)
{
    Word32 result = 0;

    src2   = (src2 > 31)? 31:((src2 < -31)? -31: src2);
    result = (src2 > 0)? (src1 << src2):(src1 >> -src2);

    if ( result > MAX_INT)
    {
        result  = MAX_INT;
        SAT_bit = 1;
    }
    else if (result < MIN_INT)
    {
        result = MIN_INT;
        SAT_bit = 1;
    }

    return(result);
}

/***************************************************************************/
/* _sshvr C function						           */
/***************************************************************************/
Word32 sshvr_c(Word32 src1, Word32 src2)
{
    Word32 result;

    src2   = (src2 > 31)? 31:(src2 < -31)? -31:src2;
    result = (src2 > 0)? (src1 >> src2): (src1 << (-src2));

    if (result > MAX_INT)
    {
        result  = MAX_INT;
        SAT_bit = 1;
    }
    else if (result < MIN_INT)
    {
        result = MIN_INT;
        SAT_bit = 1;
    }

    return(result);
}

/***************************************************************************/
/* _sub2 C function						           */
/***************************************************************************/
Word32 sub2_c(Word32 src1, Word32 src2)
{
   return (( ((src1 & 0xffff)-(src2 & 0xffff)) & 0xffff ) |
           ( ((src1 & 0xffff0000)-(src2 & 0xffff0000)) & 0xffff0000 )  );
}


/***************************************************************************/
/* _sub4 C function						           */
/***************************************************************************/
Word32 sub4_c(Word32 src1, Word32 src2)
{
   return (( ((src1 & 0xff)-(src2 & 0xff)) & 0xff ) |
           ( ((src1 & 0x0000ff00)-(src2 & 0x0000ff00)) & 0x0000ff00 ) |
           ( ((src1 & 0x00ff0000)-(src2 & 0x00ff0000)) & 0x00ff0000 ) |
           ( ((src1 & 0xff000000)-(src2 & 0xff000000)) & 0xff000000 )  );
}


/***************************************************************************/
/* _subabs4 C function						           */
/***************************************************************************/
Word32 subabs4_c(Word32 src1, Word32 src2)
{
   UWord32 result = 0;

   UWord8 src1_3 = (src1 >> 24);
   UWord8 src1_2 = (src1 <<  8) >> 24;
   UWord8 src1_1 = (src1 << 16) >> 24;
   UWord8 src1_0 = (UWord8)src1;

   UWord8 src2_3 = (src2 >> 24);
   UWord8 src2_2 = (src2 <<  8) >> 24;
   UWord8 src2_1 = (src2 << 16) >> 24;
   UWord8 src2_0 = (UWord8) src2;

   result |= ((UWord32)((src1_3 > src2_3)?
                       (src1_3 - src2_3):(src2_3 - src1_3))) << 24;
   result |= ((UWord32)((src1_2 > src2_2)?
                       (src1_2 - src2_2):(src2_2 - src1_2))) << 16;
   result |= ((UWord32)((src1_1 > src2_1)?
                       (src1_1 - src2_1):(src2_1 - src1_1))) << 8;
   result |= ((UWord32)((src1_0 > src2_0)?
                       (src1_0 - src2_0):(src2_0 - src1_0)));

   return result;
}

/***************************************************************************/
/* _swap4 C function						           */
/***************************************************************************/
UWord32 swap4_c(UWord32 src)
{
   UWord32 src_3 = (src >> 24);
   UWord32 src_2 = (src <<  8) >> 24;
   UWord32 src_1 = (src << 16) >> 24;
   UWord32 src_0 = (src & 0xff);

   return (src_2 << 24 | src_3 << 16 | src_0 << 8 | src_1);
}

/***************************************************************************/
/* _unpkhu4 C function						           */
/***************************************************************************/
UWord32 unpkhu4_c(UWord32 src)
{
   UWord32 src_3 = (src >> 24);
   UWord32 src_2 = (src <<  8) >> 24;

   return (src_3 << 16 | src_2);
}

/***************************************************************************/
/* _unpklu4 C function						           */
/***************************************************************************/
UWord32 unpklu4_c(UWord32 src)
{
   UWord32 src_1 = (src << 16) >> 24;
   UWord32 src_0 = (src & 0xff);

   return (src_1 << 16 | src_0);
}

/***************************************************************************/
/* _xpnd2 C function						           */
/***************************************************************************/
UWord32 xpnd2_c(UWord32 src)
{
   UWord32 bit_0 = src & 0x01;
   UWord32 bit_1 = src & 0x02;

   UWord32 result;

   result = bit_0;

   if (result)
      result |= bit_0 << 1 | bit_0 << 2  | bit_0 << 3  | bit_0 << 4 |
                bit_0 << 5 | bit_0 << 6  | bit_0 << 7  | bit_0 << 8 |
                bit_0 << 9 | bit_0 << 10 | bit_0 << 11 | bit_0 << 12 |
                bit_0 << 13 | bit_0 << 14 | bit_0 << 15;

   if (bit_1)   /* This is bit #1, so shift amounts are correct */
      result |= bit_1 << 15 | bit_1 << 16 | bit_1 << 17 | bit_1 << 18 |
                bit_1 << 19 | bit_1 << 20 | bit_1 << 21 | bit_1 << 22 |
                bit_1 << 23 | bit_1 << 24 | bit_1 << 25 | bit_1 << 26 |
                bit_1 << 27 | bit_1 << 28 | bit_1 << 29 | bit_1 << 30;

   return (result);
}

/***************************************************************************/
/* _xpnd4 C function						           */
/***************************************************************************/
UWord32 xpnd4_c(UWord32 src)
{
   UWord32 bit_0 = src & 0x01;
   UWord32 bit_1 = src & 0x02;
   UWord32 bit_2 = src & 0x04;
   UWord32 bit_3 = src & 0x08;

   UWord32 result;

   result = bit_0;

   if (result)
      result |= bit_0 << 1 | bit_0 << 2  | bit_0 << 3  | bit_0 << 4 |
                bit_0 << 5 | bit_0 << 6  | bit_0 << 7;

   if (bit_1)   /* This is bit #1, so shift amounts are correct */
      result |=  bit_1 << 7  | bit_1 << 8  | bit_1 << 9 | bit_1 << 10 |
                 bit_1 << 11 | bit_1 << 12 | bit_1 << 13 | bit_1 << 14;

   if (bit_2)   /* This is bit #2, so shift amounts are correct */
      result |= bit_2 << 14 | bit_2 << 15 | bit_2 << 16 | bit_2 << 17 |
                bit_2 << 18 | bit_2 << 19 | bit_2 << 20 | bit_2 << 21 ;

   if (bit_3)   /* This is bit #2, so shift amounts are correct */
     result |=  bit_3 << 21 | bit_3 << 22 | bit_3 << 23 | bit_3 << 24 |
                bit_3 << 25 | bit_3 << 26 | bit_3 << 27 | bit_3 << 28 ;

   return (result);
}

#endif
