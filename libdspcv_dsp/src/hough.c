//  created:    2012/01/18
//  filename:   hough.c
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 6599
//
//  version:    $Id: $
//
//  purpose:    transformee de Hough
//
//
/*********************************************************************/

#include "hough.h"
#include <math.h>
#include <stdlib.h>

#if defined OMAPL138 || defined OMAP3530
//#include <xdc/std.h>
#include <std.h>
#include <mem.h>
#include <bcache.h>
#include <ti/xdais/idma3.h>
#include <ti/sdo/fc/acpy3/acpy3.h>
#define _ACPY3_fastConfigure32b(handle,type,addr,id) ACPY3_fastConfigure32b(handle,type,(uint32_t)addr,id)
//faire un header
extern void _printf(const char * format, ...);
#else
#include <string.h>
#include "C64intrins.h"
#include "wrappers.h"
#define _ACPY3_fastConfigure32b(handle,type,addr,id) ACPY3_fastConfigure_ptr(handle,type,(void*)addr,id)
extern pthread_mutex_t mutex;
#endif


#ifdef OMAPL138
extern int32_t DDR;
extern int32_t IRAM;
extern int32_t L1DSRAM;
extern int32_t CACHE_L1D;
#define BUF_SEG_ID IRAM
//#define BUF_SEG_ID L1DSRAM
#define BUF_ALIGN 128
#endif


#ifdef OMAP3530
extern int32_t DDR2;
extern int32_t L1DSRAM;
#define BUF_ALIGN 128
#endif

#if defined OMAPL138 || defined OMAP3530
//inline void pc_printf(const char * format, ...) {}
#else
//#define pc_printf printf
//inline void pc_printf(const char * format, ...) {}
#endif



int32_t dspHoughLines2(IplImage* img_src, CvMat* line_storage, int32_t method,
                              float rho_step, float theta_step, int32_t threshold,
                              float param1, float param2)
{

	int16_t* restrict accum = 0;
	int16_t* restrict accum_offset1 = 0;
	int16_t* restrict accum_offset2 = 0;
	int32_t* restrict sort_buf=0;
	uint16_t* tabSinCos=0;
	uint32_t* restrict tabSinCos32=0;
	int8_t* restrict image;
	int32_t width, height;
	int16_t numangle,numrho;
	int32_t total = 0;
	int32_t r, n;
	uint16_t i, j;
	float ang;
	int32_t idx;
	int32_t linesMax;
	uint32_t tmp;

#if !defined(OMAPL138) && !defined(OMAP3530)
	pthread_mutex_lock(&mutex);
#endif

	image = (int8_t*)img_src->imageData;
	width = img_src->width;
	height = img_src->height;

	//verifications pour que l'algo tourne avec les parametres
	if(rho_step<1) {//car tabSinCos sur 16bits, si rho_step plus grand il y aura debordement
			rho_step=1;
	}
	float irho = 1 / rho_step;

	numangle = _nround((float)CV_PI / theta_step);
	//nb de rho
	//si theta [0,pi/2], rho max vaut la diagonale
	//si theta [pi/2,pi], rho max vaut la largeur (en rho negatif)
	numrho = _nround((width+sqrtf(width*width+height*height)+1) / rho_step);
	linesMax=line_storage->rows;
	
	tabSinCos = MEM_alloc( L1DSRAM,sizeof(tabSinCos[0]) * numangle*2 , BUF_ALIGN);
	tabSinCos32=(uint32_t*)tabSinCos;
	if(tabSinCos==NULL) {
		_printf("erreur allocation dspHoughLines2 tabSinCos (%i bytes)\n",sizeof(tabSinCos[0]) * numangle*2);
#if !defined(OMAPL138) && !defined(OMAP3530)
		pthread_mutex_unlock(&mutex);
#endif
		return -1;
	}
	
	accum=MEM_valloc(L1DSRAM, sizeof(accum[0]) * (numangle+2) * (numrho+2), BUF_ALIGN,0);
	if(accum==NULL) {
		MEM_free( L1DSRAM,tabSinCos,sizeof(tabSinCos[0]) * numangle*2);
		_printf("erreur allocation dspHoughLines2 accum (%i bytes)\n",sizeof(accum[0]) * (numangle+2) * (numrho+2));
#if !defined(OMAPL138) && !defined(OMAP3530)
		pthread_mutex_unlock(&mutex);
#endif
		return -1;
	}
	
	sort_buf = MEM_alloc( DDR2,sizeof(sort_buf[0]) * numangle * numrho,BUF_ALIGN );
	if(sort_buf==NULL){
		MEM_free( L1DSRAM,tabSinCos,sizeof(tabSinCos[0]) * numangle*2);
		MEM_free(L1DSRAM, accum,sizeof(accum[0]) * (numangle+2) * (numrho+2));
		_printf("erreur allocation dspHoughLines2 sort_buf (%i bytes)\n",sizeof(accum[0]) * numangle * numrho);
#if !defined(OMAPL138) && !defined(OMAP3530)
		pthread_mutex_unlock(&mutex);
#endif
		return -1;
	}

	//pc_printf("num angle %i, num rho %i\n",numangle,numrho);

	//cas theta=0
	tmp=irho*32768*2+.5f;
	if(tmp>0xffff) tmp=0xffff;
	tabSinCos[0]=tmp;
	tabSinCos[1] =0;
	
  //pc_printf("%f %i %i\n",0.,tabSinCos[0],tabSinCos[1]);
  float tmpf= irho*32768*2;
	for(ang = theta_step, n = 1; n <= numangle/2; ang += theta_step, n++ ) {
		tmp=cosf(ang) *tmpf+.5f;//+.5 pour faire l'arrondi, on est entre 0 et Pi/2 donc resultat positif
		if(tmp>0xffff) tmp=0xffff;
		tabSinCos[2*n] =tmp;
		tmp=sinf(ang) * tmpf+.5f;//idem
		if(tmp>0xffff) tmp=0xffff;
		tabSinCos[2*n+1] =tmp;
		//pc_printf("%f %i %i\n",ang,tabSinCos[2*n],tabSinCos[2*n+1]);
	}

	// stage 1. fill accumulator
	accum_offset1=&accum[(numrho - 1)/2+1+(numrho+2)];
	accum_offset2=&accum[(numrho - 1)/2+1+(numangle+1)*(numrho+2)];
	BCACHE_setMar((void *)image,img_src->imageSize,BCACHE_MAR_ENABLE);

	uint32_t k=0;
	for(i = 0; i < height; i++ ) {
		for( j = 0; j < width; j+=8 ) {
			int64_t pix=_amem8_const((void*)&image[k]);
			k+=8;
			int32_t pack=_pack2(i,j);
			if(_loll(pix)!=0) {//evite de faire les 4 test si pix pas bon
				if( (_loll(pix)&0xff) != 0 )
					feedAccum(pack,numangle,numrho,tabSinCos32,accum_offset1,accum_offset2);
				if( (_loll(pix)&0xff00) != 0 )
					feedAccum(pack+1,numangle,numrho,tabSinCos32,accum_offset1,accum_offset2);
				if( (_loll(pix)&0xff0000) != 0 )
					feedAccum(pack+2,numangle,numrho,tabSinCos32,accum_offset1,accum_offset2);
				if( (_loll(pix)&0xff000000) != 0 )
					feedAccum(pack+3,numangle,numrho,tabSinCos32,accum_offset1,accum_offset2);
			}
			if(_hill(pix)!=0) {
				if( (_hill(pix)&0xff) != 0 )
					feedAccum(pack+4,numangle,numrho,tabSinCos32,accum_offset1,accum_offset2);
				if( (_hill(pix)&0xff00) != 0 )
					feedAccum(pack+5,numangle,numrho,tabSinCos32,accum_offset1,accum_offset2);
				if( (_hill(pix)&0xff0000) != 0 )
					feedAccum(pack+6,numangle,numrho,tabSinCos32,accum_offset1,accum_offset2);
				if( (_hill(pix)&0xff000000) != 0 )
					feedAccum(pack+7,numangle,numrho,tabSinCos32,accum_offset1,accum_offset2);
			}
		}
	}
	
	BCACHE_setMar((void *)image,img_src->imageSize,BCACHE_MAR_DISABLE);
	BCACHE_inv((void *)image,img_src->imageSize,FALSE);//wait is done at end of function

	// stage 2. find local maximums
	for( n = 0; n < numangle; n++ ) {
		for( r = 0; r < numrho; r++ ) {
			int32_t base = (n+1) * (numrho+2) + r+1;
			int64_t test=_mem8_const((void*)&accum[base-1]);
			int16_t ref=_loll(test)>>16;
			int16_t ref_prec=_loll(test)&0xffff;
			int16_t ref_suiv=_hill(test)&0xffff;
			if( ref > threshold &&
					ref > ref_prec && ref >= ref_suiv &&
					ref > accum[base - numrho - 2] && ref >= accum[base + numrho + 2] ) {
						sort_buf[total++] = base;
						//pc_printf("%i\n",base);
					}
					
		}
	}

	// stage 3. sort the detected lines by accumulator value
	icvHoughSortDescent32s( sort_buf, total, accum );

	// stage 4. store the first min(total,linesMax) lines to the output buffer
	linesMax = MIN(linesMax, total);

	float scale = 1.f/(numrho+2);
	for( i = 0; i < linesMax; i++ ) {
		idx = sort_buf[i];
		//pc_printf("%i %i\n",accum[sort_buf[i]],sort_buf[i]);
		n = (idx*scale) - 1;//cvFloor(idx*scale) - 1;
		r = idx - (n+1)*(numrho+2) - 1;
		(line_storage->data.fl)[2*i] = (r - (numrho - 1)*0.5f) * rho_step;
		(line_storage->data.fl)[2*i+1]= n * theta_step;
	}

	MEM_free(L1DSRAM, accum,sizeof(accum[0]) * (numangle+2) * (numrho+2));
	MEM_free(DDR2, sort_buf,sizeof(sort_buf[0]) * numangle * numrho);
	MEM_free( L1DSRAM,tabSinCos,sizeof(tabSinCos[0]) * numangle*2);

#if !defined(OMAPL138) && !defined(OMAP3530)
	pthread_mutex_unlock(&mutex);
#endif

	BCACHE_wait();

	return linesMax;

}

int32_t dspHoughLines2_test(IplImage* img_src, CvMat* line_storage, int32_t method,
                              float rho, float theta_min,float theta_max,float theta_step, int32_t threshold)
{

    int16_t* restrict accum = 0;
    int16_t* restrict accum_offset = 0;
    int32_t* restrict sort_buf=0;
    int16_t* tabSinCos=0;
    uint32_t* restrict tabSinCos32=0;
    int8_t* restrict image;
    int32_t width, height;
    int16_t numangle,numrho;
    int32_t total = 0;
    float ang;
    int32_t r, n;
    uint16_t i, j;
    float irho = 1 / rho;
    float scale;
    int32_t idx;
    int32_t linesMax;

    image = (int8_t*)img_src->imageData;
    width = img_src->width;
    height = img_src->height;
		
		//verifications pour que l'algo tourne avec les parametres
    if(rho<=1)//car tabSinCos sur 16bits, si rho plus grand il y aura debordement
    {
        rho=2;
    }
    if(theta_min<=-CV_PI/2)//algo fonctionne que sur ]-PI/2..PI/2]
    {
       theta_min=-CV_PI/2+theta_step;
    }
    if(theta_max>CV_PI/2)//algo fonctionne que sur -PI/2..PI/2
    {
        theta_max=CV_PI/2;
    }

    numangle = _nround((theta_max-theta_min) / theta_step)+1;
    //numrho = _nround(((width + height) * 2 + 1) / rho);

    //nb de rho
    //si theta negatif, rho max vaut la hauteur
    //si theta positif, rho max vaut la diagonale
    numrho = _nround((height+sqrt(width*width+height*height)) / rho);
    //pc_printf("num angle %i, num rho %i offset %i\n",numangle,numrho,_nround(height / rho));

#if !defined(OMAPL138) && !defined(OMAP3530)
    pthread_mutex_lock(&mutex);
#endif

    linesMax=line_storage->rows;
    
    tabSinCos = MEM_alloc( L1DSRAM,sizeof(tabSinCos[0]) * numangle*2 , BUF_ALIGN);
    if(tabSinCos==NULL)
    {
        _printf("erreur allocation dspHoughLines2 tabSinCos (%i bytes)\n",sizeof(tabSinCos[0]) * numangle*2);
#if !defined(OMAPL138) && !defined(OMAP3530)
        pthread_mutex_unlock(&mutex);
#endif
        return -1;
    }
		
	accum=MEM_valloc(L1DSRAM, sizeof(accum[0]) * (numangle+2) * (numrho+2), BUF_ALIGN,0);
    if(accum==NULL)
    {
		MEM_free( L1DSRAM,tabSinCos,sizeof(tabSinCos[0]) * numangle*2);
		_printf("erreur allocation dspHoughLines2 accum (%i bytes)\n",sizeof(accum[0]) * (numangle+2) * (numrho+2));
#if !defined(OMAPL138) && !defined(OMAP3530)
		pthread_mutex_unlock(&mutex);
#endif
		return -1;
    }
		
	sort_buf = MEM_alloc( DDR2,sizeof(sort_buf[0]) * numangle * numrho,BUF_ALIGN );
    if(sort_buf==NULL)
    {
		 MEM_free(L1DSRAM, accum,sizeof(accum[0]) * (numangle+2) * (numrho+2));
		MEM_free( L1DSRAM,tabSinCos,sizeof(tabSinCos[0]) * numangle*2);
		_printf("erreur allocation dspHoughLines2 sort_buf (%i bytes)\n",sizeof(accum[0]) * numangle * numrho);
#if !defined(OMAPL138) && !defined(OMAP3530)
		pthread_mutex_unlock(&mutex);
#endif
		return -1;
    }

    for( ang = theta_min, n = 0; n < numangle; ang += theta_step, n++ )
    {
        tabSinCos[2*n] =(int16_t)(cosf(ang) * irho*32768*2+.5);
        //printf("%i %i\n",tabSinCos[2*n],(uint16_t)(cos(ang) * irho*32768*2+.5));
        if(tabSinCos[2*n]==-32768) tabSinCos[2*n]=0x7FFF;//ce cas arrive qd rho=2 et theta=0, on arrondi au dessous pour eviter les debordements
        tabSinCos[2*n+1] =(int16_t)(sinf(ang) * irho*32768*2+.5);
        //printf("%x %x\n",tabSinCos[2*n],tabSinCos[2*n+1]);
        //printf("%i: %f %x %x %x %x\n",n,ang,(uint16_t)(cos(ang) * irho*32768*2+.5),(uint32_t)(cos(ang) * irho*32768*2+.5),(uint16_t)(sin(ang) * irho*32768*2+.5),(uint32_t)(sin(ang) * irho*32768*2+.5));
    }


    tabSinCos32=(uint32_t*)tabSinCos;


    // stage 1. fill accumulator
    //accum_offset=&accum[(numrho - 1)/2+1+(numrho+2)];
    accum_offset=&accum[(int)_nround(height / rho)];
    BCACHE_setMar((void *)image,img_src->imageSize,BCACHE_MAR_ENABLE);

    for( i = 0; i < height; i++ )
        {
            for( j = 0; j < width; j+=8 )
            {
                int64_t pix=_amem8_const((void*)&image[i * width + j]);
                int32_t pack=_pack2(i,j);
                if(_loll(pix)!=0)//evite de faire les 4 test si pix pas bon
                {
                    if( (_loll(pix)&0xff) != 0 )
                    	feedAccumTracking(pack,numangle,numrho,tabSinCos32,accum_offset);
                    if( (_loll(pix)&0xff00) != 0 )
                    	feedAccumTracking(pack+1,numangle,numrho,tabSinCos32,accum_offset);
                    if( (_loll(pix)&0xff0000) != 0 )
                    	feedAccumTracking(pack+2,numangle,numrho,tabSinCos32,accum_offset);
                    if( (_loll(pix)&0xff000000) != 0 )
                    	feedAccumTracking(pack+3,numangle,numrho,tabSinCos32,accum_offset);
                }
                if(_hill(pix)!=0)
                {
                    if( (_hill(pix)&0xff) != 0 )
                    	feedAccumTracking(pack+4,numangle,numrho,tabSinCos32,accum_offset);
                    if( (_hill(pix)&0xff00) != 0 )
                    	feedAccumTracking(pack+5,numangle,numrho,tabSinCos32,accum_offset);
                    if( (_hill(pix)&0xff0000) != 0 )
                    	feedAccumTracking(pack+6,numangle,numrho,tabSinCos32,accum_offset);
                    if( (_hill(pix)&0xff000000) != 0 )
                    	feedAccumTracking(pack+7,numangle,numrho,tabSinCos32,accum_offset);
                }
            }
        }

    BCACHE_setMar((void *)image,img_src->imageSize,BCACHE_MAR_DISABLE);
    BCACHE_inv((void *)image,img_src->imageSize,FALSE);//wait is done at end of function

    // stage 2. find local maximums
    for( n = 0; n < numangle; n++ )
    {
        for( r = 0; r < numrho; r++ )
        {
            int32_t base = (n+1) * (numrho+2) + r+1;
            int64_t test=_mem8_const((void*)&accum[base-1]);
            int16_t ref=_loll(test)>>16;
            int16_t ref_prec=_loll(test)&0xffff;
            int16_t ref_suiv=_hill(test)&0xffff;

            if( ref > threshold &&
                ref > ref_prec && ref >= ref_suiv &&
                ref > accum[base - numrho - 2] && ref >= accum[base + numrho + 2] )
									sort_buf[total++] = base;
        }
    }
//pc_printf("\n");
    // stage 3. sort the detected lines by accumulator value
    icvHoughSortDescent32s( sort_buf, total, accum );

    // stage 4. store the first min(total,linesMax) lines to the output buffer
    linesMax = MIN(linesMax, total);

    scale = 1./(numrho+2);
    for( i = 0; i < linesMax; i++ )
    {
        idx = sort_buf[i];
//pc_printf("%i\n",accum[sort_buf[i]]);
        n = (idx*scale) - 1;
        r = idx - (n+1)*(numrho+2) - 1;
 n++;//il y a un souci d'index qque part, ce ++ semble resoudre... a verifier
        //(line_storage->data.fl)[2*i] = (r - (numrho - 1)*0.5f) * rho;
        (line_storage->data.fl)[2*i] = (r - _nround(height / rho)) * rho;
        (line_storage->data.fl)[2*i+1]= theta_min+n * theta_step;
/*
        if(theta_min+n * theta_step==0 && (r - (numrho - 1)*0.5f) * rho<0)
            (line_storage->data.fl)[2*i] = -(r - (numrho - 1)*0.5f) * rho;
            */

    }

    MEM_free(L1DSRAM, accum,sizeof(accum[0]) * (numangle+2) * (numrho+2));
    MEM_free(DDR2, sort_buf,sizeof(sort_buf[0]) * numangle * numrho);
    MEM_free( L1DSRAM,tabSinCos,sizeof(tabSinCos[0]) * numangle*2);

#if !defined(OMAPL138) && !defined(OMAP3530)
    pthread_mutex_unlock(&mutex);
#endif

    BCACHE_wait();

    return linesMax;
}

int32_t dspHoughLinesTracking(IplImage* img_src, CvMat* line_storage, int32_t method,
                              float rho_step, float theta_moy,float delta_theta,float theta_step, int32_t threshold)
{

	int16_t* restrict accum = 0;
	int16_t* restrict accum_offset = 0;
	int32_t* restrict sort_buf=0;
	int16_t* tabSinCos=0;
	uint32_t* restrict tabSinCos32=0;
	int8_t* restrict image;
	int32_t width, height;
	int16_t numangle,numrho;
	int32_t total = 0;
	int32_t r, n;
	uint16_t i, j;
	float ang,theta_min,theta_max;
	int32_t idx;
	int32_t linesMax;

#if !defined(OMAPL138) && !defined(OMAP3530)
	pthread_mutex_lock(&mutex);
#endif

	image = (int8_t*)img_src->imageData;
	width = img_src->width;
	height = img_src->height;
    
	//verifications pour que l'algo tourne avec les parametres
	if(rho_step<2) {//car tabSinCos sur 16bits, si rho_step plus grand il y aura debordement
			rho_step=2;
	}
	float irho = 1 / rho_step;
  
  if(theta_moy<0 || theta_moy>CV_PI) {
    _printf("error theta_moy (%f) outside bounds [0,PI[\n",theta_moy);
#if !defined(OMAPL138) && !defined(OMAP3530)
	pthread_mutex_unlock(&mutex);
#endif
	return -1;
  }
  
  if(delta_theta>CV_PI/2) delta_theta=CV_PI/2;
  
  //determination des bornes
  theta_min=theta_moy;
  while(theta_min-theta_step>theta_moy-delta_theta) theta_min-=theta_step;
  theta_max=theta_moy;
  while(theta_max+theta_step<theta_moy+delta_theta) theta_max+=theta_step;
	
	numangle = _nround((theta_max-theta_min) / theta_step)+1;

	//nb de rho
	//si theta [0,pi/2], rho max vaut la diagonale
	//si theta [pi/2,pi], rho max vaut la largeur (en rho negatif)
	numrho = _nround((width+sqrtf(width*width+height*height)+1) / rho_step);
	
//	pc_printf("num angle %i, num rho %i\n",numangle,numrho);
//pc_printf("theta min %f theta max %f\n",theta_min*180.f/CV_PI,theta_max*180.f/CV_PI);
	linesMax=line_storage->rows;
	
	tabSinCos = MEM_alloc( L1DSRAM,sizeof(tabSinCos[0]) * numangle*2 , BUF_ALIGN);
	if(tabSinCos==NULL) {
		_printf("erreur allocation dspHoughLines2 tabSinCos (%i bytes)\n",sizeof(tabSinCos[0]) * numangle*2);
#if !defined(OMAPL138) && !defined(OMAP3530)
		pthread_mutex_unlock(&mutex);
#endif
		return -1;
	}
	
	accum=MEM_valloc(L1DSRAM, sizeof(accum[0]) * (numangle+2) * (numrho+2), BUF_ALIGN,0);
	if(accum==NULL) {
		MEM_free( L1DSRAM,tabSinCos,sizeof(tabSinCos[0]) * numangle*2);
		_printf("erreur allocation dspHoughLines2 accum (%i bytes)\n",sizeof(accum[0]) * (numangle+2) * (numrho+2));
#if !defined(OMAPL138) && !defined(OMAP3530)
		pthread_mutex_unlock(&mutex);
#endif
		return -1;
	}
	
	sort_buf = MEM_alloc( DDR2,sizeof(sort_buf[0]) * numangle * numrho,BUF_ALIGN );
	if(sort_buf==NULL) {
		MEM_free(L1DSRAM, accum,sizeof(accum[0]) * (numangle+2) * (numrho+2));
		MEM_free( L1DSRAM,tabSinCos,sizeof(tabSinCos[0]) * numangle*2);
		_printf("erreur allocation dspHoughLines2 sort_buf (%i bytes)\n",sizeof(accum[0]) * numangle * numrho);
#if !defined(OMAPL138) && !defined(OMAP3530)
		pthread_mutex_unlock(&mutex);
#endif
		return -1;
	}

  //theta moy is between [0,pi[, delta<=pi/2, theta is between [-pi/2,3pi/2[
	for(ang = theta_min, n = 0; n < numangle; ang += theta_step, n++ ) {
    int32_t tmp;
    float tmpf=irho*32768*2;
    if(ang<(float)CV_PI/2) {// && ang>-(float)CV_PI/2 implicite vu les bornes de theta
      tmp=cosf(ang)*tmpf+.5f;//+.5 pour faire l'arrondi, car resultat positif
			if(tmp>0x7fff) tmp=0x7fff;
		} else {
			tmp=cosf(ang)*tmpf-.5f;//-.5 pour faire l'arrondi, car resultat negatif
      if(tmp<-32768) tmp=-32768;
		}
    tabSinCos[2*n] =tmp;
    
    if(ang>0 && ang<(float)CV_PI) {
      tmp=sinf(ang)*tmpf+.5f;//idem
      if(tmp>0x7fff) tmp=0x7fff;
    } else {
      tmp=sinf(ang)*tmpf-.5f;//idem
      if(tmp<-32768) tmp=-32768;
    }
    tabSinCos[2*n+1] =tmp;

		//pc_printf("%f %i %i\n",ang,tabSinCos[2*n],tabSinCos[2*n+1]);
	}
	tabSinCos32=(uint32_t*)tabSinCos;
	
	// stage 1. fill accumulator
	accum_offset=&accum[(numrho - 1)/2+1+(numrho+2)];
	BCACHE_setMar((void *)image,img_src->imageSize,BCACHE_MAR_ENABLE);

	uint32_t k=0;
	for( i = 0; i < height; i++ ) {
		for( j = 0; j < width; j+=8 ) {
			int64_t pix=_amem8_const((void*)&image[k]);
      k+=8;
			int32_t pack=_pack2(i,j);
			if(_loll(pix)!=0) {//evite de faire les 4 test si pix pas bon
				if( (_loll(pix)&0xff) != 0 )
					feedAccumTracking(pack,numangle,numrho,tabSinCos32,accum_offset);
				if( (_loll(pix)&0xff00) != 0 )
					feedAccumTracking(pack+1,numangle,numrho,tabSinCos32,accum_offset);
				if( (_loll(pix)&0xff0000) != 0 )
					feedAccumTracking(pack+2,numangle,numrho,tabSinCos32,accum_offset);
				if( (_loll(pix)&0xff000000) != 0 )
					feedAccumTracking(pack+3,numangle,numrho,tabSinCos32,accum_offset);
			}
			if(_hill(pix)!=0) {
				if( (_hill(pix)&0xff) != 0 )
					feedAccumTracking(pack+4,numangle,numrho,tabSinCos32,accum_offset);
				if( (_hill(pix)&0xff00) != 0 )
					feedAccumTracking(pack+5,numangle,numrho,tabSinCos32,accum_offset);
				if( (_hill(pix)&0xff0000) != 0 )
					feedAccumTracking(pack+6,numangle,numrho,tabSinCos32,accum_offset);
				if( (_hill(pix)&0xff000000) != 0 )
					feedAccumTracking(pack+7,numangle,numrho,tabSinCos32,accum_offset);
			}
		}
	}
	BCACHE_setMar((void *)image,img_src->imageSize,BCACHE_MAR_DISABLE);
	BCACHE_inv((void *)image,img_src->imageSize,FALSE);//wait is done at end of function

	// stage 2. find local maximums
	for( n = 0; n < numangle; n++ ) {
		for( r = 0; r < numrho; r++ ) {
			int32_t base = (n+1) * (numrho+2) + r+1;
			int64_t test=_mem8_const((void*)&accum[base-1]);
			int16_t ref=_loll(test)>>16;
			int16_t ref_prec=_loll(test)&0xffff;
			int16_t ref_suiv=_hill(test)&0xffff;
			if( ref > threshold &&
					ref > ref_prec && ref >= ref_suiv &&
					ref > accum[base - numrho - 2] && ref >= accum[base + numrho + 2] ) {
				sort_buf[total++] = base;
				//pc_printf("%i\n",base);
					}
		}
	}

	// stage 3. sort the detected lines by accumulator value
	icvHoughSortDescent32s( sort_buf, total, accum );

	// stage 4. store the first min(total,linesMax) lines to the output buffer
	linesMax = MIN(linesMax, total);

	float scale = 1.f/(numrho+2);
	for( i = 0; i < linesMax; i++ ) {
		idx = sort_buf[i];
		//pc_printf("%i %i\n",accum[sort_buf[i]],sort_buf[i]);
		n = (idx*scale) - 1;
		r= idx - (n+1)*(numrho+2) - 1;
		
    //todo revoir les index car probleme regle a la main ci dessous
    if(theta_min+n * theta_step>(float)CV_PI) {
      (line_storage->data.fl)[2*i] = -(r - (numrho -1)*0.5f) * rho_step-2;
      (line_storage->data.fl)[2*i+1]= theta_min+n * theta_step-CV_PI;
    } else if(theta_min+n * theta_step<0) {
      (line_storage->data.fl)[2*i] = -(r - (numrho -1)*0.5f) * rho_step-2;
      (line_storage->data.fl)[2*i+1]= theta_min+n * theta_step+CV_PI;
    } else if(theta_min+n*theta_step==0) {
      (line_storage->data.fl)[2*i] = (r - (numrho -1)*0.5f) * rho_step+2;
      (line_storage->data.fl)[2*i+1]= theta_min+n * theta_step;
    } else {
      (line_storage->data.fl)[2*i] = (r - (numrho -1)*0.5f) * rho_step;
      (line_storage->data.fl)[2*i+1]= theta_min+n * theta_step;
    }
		
	}

	MEM_free(L1DSRAM, accum,sizeof(accum[0]) * (numangle+2) * (numrho+2));
	MEM_free(DDR2, sort_buf,sizeof(sort_buf[0]) * numangle * numrho);
	MEM_free( L1DSRAM,tabSinCos,sizeof(tabSinCos[0]) * numangle*2);

#if !defined(OMAPL138) && !defined(OMAP3530)
	pthread_mutex_unlock(&mutex);
#endif

	BCACHE_wait();

	return linesMax;
}

void feedAccumTracking(int32_t pack,int32_t numangle,int32_t numrho,uint32_t* restrict tabSinCos32,int16_t* restrict accum) {
	int32_t r,n;

	for( n = 0; n < numangle; n++ ) {
		r=_dotprsu2(tabSinCos32[n],pack);
		accum[n*(numrho+2) + r]++;
	}
}

void feedAccum(int32_t pack,int32_t numangle,int32_t numrho,uint32_t* restrict tabSinCos32,int16_t* restrict accum_offset1,int16_t* restrict accum_offset2)
{
    int32_t r,n,nMax;
    uint32_t tmp;

    //traite l'angle 0
    tmp=tabSinCos32[0];
    r=_dotprsu2(pack,tmp);//_dotp2(tmp,pack)>>15;
    accum_offset1[r]++;

		//traite le 90 si appartient aux thetas
		if(numangle%2==0) {
			tmp=tabSinCos32[numangle/2];
			r=_dotprsu2(pack,tmp);//_dotp2(tmp,pack)>>15;
			accum_offset1[(numangle/2)*(numrho+2) + r]++;
			nMax=numangle/2;
		} else {
			nMax=numangle/2+1;
		}

    //symetrie des autres angles
    for( n = 1; n < nMax; n++ ) {
        tmp=tabSinCos32[n];
        r=_dotprsu2(pack,tmp);//_dotp2(tmp,pack)>>15;
        accum_offset1[(n)*(numrho+2) + r]++;
        r=_dotpnrsu2(pack,tmp);//_dotpn2(tmp,pack)>>15;
        accum_offset2[(-n)*(numrho+2) + r]++;
    }
}


//ci dessous code opencv
/*
typedef union Cv32suf
{
    int i;
    unsigned u;
    float f;
}
Cv32suf;

typedef union Cv64suf
{
    long long i;
    unsigned long long u;
    double f;
}
Cv64suf;

int  cvRound( double value )
{

    Cv64suf temp;
    temp.f = value + 6755399441055744.0;
    return (int)temp.u;

}


int  cvFloor( double value )
{

    int temp = cvRound(value);
    Cv32suf diff;
    diff.f = (float)(value - temp);
    return temp - (diff.i < 0);

}

int32_t dspHoughLines2_cv(IplImage* img, CvMat* line_storage, int32_t method,
                              float rho, float theta, int32_t threshold,
                              float param1, float param2)
{
    int16_t *accum = 0;
    int *sort_buf=0;
    float *tabSin = 0;
    float *tabCos = 0;
    const unsigned char* image;
    int step, width, height;
    int numangle, numrho;
    int total = 0;
    float ang;
    int r, n;
    int i, j;
    float irho = 1 / rho;
    double scale;

    int32_t linesMax;
    linesMax=line_storage->rows;

#if !defined(OMAPL138) && !defined(OMAP3530)
    pthread_mutex_lock(&mutex);
#endif

    image = (unsigned char*)img->imageData;
    step = img->width;
    width = img->width;
    height = img->height;

    numangle = cvRound(CV_PI / theta);
    numrho = cvRound(((width + height) * 2 + 1) / rho);

pc_printf("angle %i rho %i\n",numangle,numrho);

    accum=MEM_valloc(L1DSRAM, sizeof(accum[0]) * (numangle+2) * (numrho+2), BUF_ALIGN,0);
    sort_buf = MEM_alloc( DDR2,sizeof(sort_buf[0]) * numangle * numrho,BUF_ALIGN );
    tabSin = MEM_alloc( L1DSRAM,sizeof(tabSin[0]) * numangle , BUF_ALIGN);
		tabCos = MEM_alloc( L1DSRAM,sizeof(tabCos[0]) * numangle , BUF_ALIGN);



    for( ang = 0, n = 0; n < numangle; ang += theta, n++ )
    {
        tabSin[n] = (float)(sin(ang) * irho);
        tabCos[n] = (float)(cos(ang) * irho);
    }

    // stage 1. fill accumulator
    for( i = 0; i < height; i++ )
        for( j = 0; j < width; j++ )
        {
            if( image[i * step + j] != 0 ) {
                for( n = 0; n < numangle; n++ )
                {
                    r = cvRound( j * tabCos[n] + i * tabSin[n] );
                    r += (numrho - 1) / 2;
										if((n+1) * (numrho+2) + r+1==10281) pc_printf("%x\n",(i<<16)+j);
                    accum[(n+1) * (numrho+2) + r+1]++;
                }
							}
        }

    // stage 2. find local maximums
    for( r = 0; r < numrho; r++ )
        for( n = 0; n < numangle; n++ )
        {
            int base = (n+1) * (numrho+2) + r+1;
            if( accum[base] > threshold &&
                accum[base] > accum[base - 1] && accum[base] >= accum[base + 1] &&
                accum[base] > accum[base - numrho - 2] && accum[base] >= accum[base + numrho + 2] )
                sort_buf[total++] = base;
        }

    // stage 3. sort the detected lines by accumulator value
    icvHoughSortDescent32s( sort_buf, total, accum );

    // stage 4. store the first min(total,linesMax) lines to the output buffer
    linesMax = MIN(linesMax, total);
    scale = 1./(numrho+2);
    for( i = 0; i < linesMax; i++ )
    {
pc_printf("%i %i\n",accum[sort_buf[i]],sort_buf[i]);
        int idx = sort_buf[i];
        int n = cvFloor(idx*scale) - 1;
        int r = idx - (n+1)*(numrho+2) - 1;
        (line_storage->data.fl)[2*i] = (r - (numrho - 1)*0.5f) * rho;
        (line_storage->data.fl)[2*i+1]= n * theta;

    }

#if !defined(OMAPL138) && !defined(OMAP3530)
    pthread_mutex_unlock(&mutex);
#endif
    return linesMax;
}
*/
