//  created:    2012/01/18
//  filename:   basic_image.c
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 6599
//
//  version:    $Id: $
//
//  purpose:    copie et conversion de couleurs
//
//
/*********************************************************************/

#include "basic_image.h"
#include <stdlib.h>

#if defined OMAPL138 || defined OMAP3530
//#include <xdc/std.h>
#include <std.h>
//#include <stdarg.h>
//#include <stddef.h>
//#include <mem.h>
//#include <ti/xdais/idma3.h>
#include <ti/sdo/fc/acpy3/acpy3.h>
#define _ACPY3_fastConfigure32b(handle,type,addr,id) ACPY3_fastConfigure32b(handle,type,(uint32_t)addr,id)
//faire un header
extern void _printf(const char * format, ...);
#else
#include <string.h>
#include "C64intrins.h"
#include "wrappers.h"
#define _ACPY3_fastConfigure32b(handle,type,addr,id) ACPY3_fastConfigure_ptr(handle,type,(void*)addr,id)
extern pthread_mutex_t mutex;
#endif

#include "opencv_common.h"

#ifdef OMAPL138
extern int32_t DDR;
extern int32_t IRAM;
extern int32_t L1DSRAM;
extern int32_t CACHE_L1D;
#define BUF_SEG_ID IRAM
//#define BUF_SEG_ID L1DSRAM
#define BUF_ALIGN 128
#endif


#ifdef OMAP3530
extern int32_t DDR2;
extern int32_t L1DSRAM;
#define BUF_ALIGN 128
#endif

#define BUFF_SIZE (1024*39) //doit etre un multiple de 64

//les copies DMA sont plus rapide sur une taille multiple de 8
#define COPY_SIZE (65528) //doit etre un multiple de 8

extern IDMA3_Handle h_in,h_out;


void dspCloneImage(IplImage* img_src,IplImage* img_dst)
{
    ACPY3_Params p;
    uint32_t i,max_iterations;
    uint8_t *data_src=(uint8_t *)img_src->imageData;
    uint8_t *data_dst=(uint8_t *)img_dst->imageData;

#if !defined(OMAPL138) && !defined(OMAP3530)
    pthread_mutex_lock(&mutex);
#endif

    max_iterations=(uint32_t)((img_src->imageSize)/COPY_SIZE);

    p.transferType = ACPY3_1D1D;
    p.dstAddr = (void *)data_dst;
    p.srcAddr = (void *)data_src;
    p.elementSize = COPY_SIZE;
    p.srcElementIndex = 1;
    p.dstElementIndex = 1;
    p.numElements = 1;
    p.numFrames = 1;
    p.waitId = 0;// waitId of 0 implies wait after the first transfer
    ACPY3_configure(h_out, &p, 0);

    for(i=0;i<max_iterations;i++)
    {
        ACPY3_start(h_out);
        data_src+=COPY_SIZE;
        data_dst+=COPY_SIZE;

        ACPY3_wait(h_out);
        _ACPY3_fastConfigure32b(h_out,ACPY3_PARAMFIELD_SRCADDR,data_src,0);
        _ACPY3_fastConfigure32b(h_out,ACPY3_PARAMFIELD_DSTADDR,data_dst,0);
    }
    //traitement du rebus (ou si l'image est plus petite que COPY_SIZE)
    if(img_src->imageSize!=COPY_SIZE*max_iterations)
    {
        ACPY3_fastConfigure16b(h_out,ACPY3_PARAMFIELD_ELEMENTSIZE,(uint16_t)(img_src->imageSize-COPY_SIZE*max_iterations),0);
        ACPY3_start(h_out);
        ACPY3_wait(h_out);
    }

#if !defined(OMAPL138) && !defined(OMAP3530)
    pthread_mutex_unlock(&mutex);
#endif

}

void dspCvtColor(IplImage* img_src,IplImage* img_dst, int32_t code)
{

    ACPY3_Params p;
    uint32_t i;
    int8_t* restrict cache_in_old;
    int8_t* restrict cache_in;
    int8_t* restrict cache_out_old;
    int8_t* restrict cache_out;
    int8_t* data_src=(int8_t*)img_src->imageData;
    int8_t* data_dst=(int8_t*)img_dst->imageData;
    int8_t* cache_tmp;

#if !defined(OMAPL138) && !defined(OMAP3530)
    pthread_mutex_lock(&mutex);
#endif

    if(code==DSP_BGR2GRAY)
    {
        cache_in_old=MEM_alloc(L1DSRAM, 3*img_src->width, BUF_ALIGN);
        cache_in=MEM_alloc(L1DSRAM, 3*img_src->width, BUF_ALIGN);
        cache_out_old=MEM_alloc(L1DSRAM,img_src->width, BUF_ALIGN);
        cache_out=MEM_alloc(L1DSRAM, img_src->width, BUF_ALIGN);
        if(cache_in_old==NULL) {
            _printf("erreur allocation dspCvtColor %i\n",3*img_src->width);
#if !defined(OMAPL138) && !defined(OMAP3530)
			pthread_mutex_unlock(&mutex);
#endif
            return;
        }
        if(cache_in==NULL) {
             _printf("erreur allocation dspCvtColor %i\n",3*img_src->width);
 #if !defined(OMAPL138) && !defined(OMAP3530)
 			pthread_mutex_unlock(&mutex);
 #endif
 			MEM_free(L1DSRAM, cache_in_old,3*img_src->width);
            return;
        }
        if(cache_out_old==NULL) {
             _printf("erreur allocation dspCvtColor %i\n",img_src->width);
 #if !defined(OMAPL138) && !defined(OMAP3530)
 			pthread_mutex_unlock(&mutex);
 #endif
 			MEM_free(L1DSRAM, cache_in_old,3*img_src->width);
 			MEM_free(L1DSRAM, cache_in,3*img_src->width);
            return;
        }
        if(cache_out==NULL) {
			 _printf("erreur allocation dspCvtColor %i\n",img_src->width);
 #if !defined(OMAPL138) && !defined(OMAP3530)
			pthread_mutex_unlock(&mutex);
 #endif
			MEM_free(L1DSRAM, cache_in_old,3*img_src->width);
			MEM_free(L1DSRAM, cache_in,3*img_src->width);
			MEM_free(L1DSRAM, cache_out_old,img_src->width);
			return;
        }

        p.transferType = ACPY3_1D1D;
        p.elementSize = 3*img_src->width;
        p.srcElementIndex = 1;
        p.dstElementIndex = 1;
        p.numElements = 1;
        p.numFrames = 1;
        p.waitId = 0;// waitId of 0 implies wait after the first transfer
        ACPY3_configure(h_in, &p, 0);
        ACPY3_configure(h_out, &p, 0);

        ACPY3_fastConfigure16b(h_out,ACPY3_PARAMFIELD_ELEMENTSIZE,img_src->width,0);

        //les 2 premieres iterations ne sortent rien (+2)
        for(i=0;i<img_src->height+2;i++)
        {
            //cache_out_old > dst	trait cache_in > cache_out  src > cache_in_old
            if(i>1)
            {
                _ACPY3_fastConfigure32b(h_out,ACPY3_PARAMFIELD_SRCADDR,cache_out_old,0);
                _ACPY3_fastConfigure32b(h_out,ACPY3_PARAMFIELD_DSTADDR,data_dst,0);
                ACPY3_start(h_out);
                data_dst+=img_src->width;
            }

            _ACPY3_fastConfigure32b(h_in,ACPY3_PARAMFIELD_SRCADDR,data_src,0);
            _ACPY3_fastConfigure32b(h_in,ACPY3_PARAMFIELD_DSTADDR,cache_in_old,0);
            ACPY3_start(h_in);
            data_src+=3*img_src->width;

            //traitement non necessaire quand i=0 et à la fin, on le fait qd meme pour enlever un if...
            //car traitement + rapide que la copie
            convert_BGR2GRAY(cache_out,cache_in,img_src->width);

            ACPY3_wait(h_out);
            ACPY3_wait(h_in);

            //permutation circulaire
            cache_tmp=cache_in;
            cache_in=cache_in_old;
            cache_in_old=cache_tmp;

            cache_tmp=cache_out;
            cache_out=cache_out_old;
            cache_out_old=cache_tmp;
        }

        MEM_free(L1DSRAM, cache_in_old,3*img_src->width);
        MEM_free(L1DSRAM, cache_in,3*img_src->width);
        MEM_free(L1DSRAM, cache_out_old,img_src->width);
        MEM_free(L1DSRAM, cache_out,img_src->width);

    }

    if(code==DSP_YUYV2GRAY)
    {
        cache_in_old=(int8_t*)MEM_alloc(L1DSRAM, 2*img_src->width, BUF_ALIGN);
        cache_in=(int8_t*)MEM_alloc(L1DSRAM, 2*img_src->width, BUF_ALIGN);
        cache_out_old=(int8_t*)MEM_alloc(L1DSRAM,img_src->width, BUF_ALIGN);
        cache_out=(int8_t*)MEM_alloc(L1DSRAM, img_src->width, BUF_ALIGN);

        if(cache_in_old==NULL) {
             _printf("erreur allocation dspCvtColor %i\n",3*img_src->width);
 #if !defined(OMAPL138) && !defined(OMAP3530)
 			pthread_mutex_unlock(&mutex);
 #endif
             return;
         }
         if(cache_in==NULL) {
              _printf("erreur allocation dspCvtColor %i\n",3*img_src->width);
  #if !defined(OMAPL138) && !defined(OMAP3530)
  			pthread_mutex_unlock(&mutex);
  #endif
  			MEM_free(L1DSRAM, cache_in_old,3*img_src->width);
             return;
         }
         if(cache_out_old==NULL) {
              _printf("erreur allocation dspCvtColor %i\n",img_src->width);
  #if !defined(OMAPL138) && !defined(OMAP3530)
  			pthread_mutex_unlock(&mutex);
  #endif
  			MEM_free(L1DSRAM, cache_in_old,3*img_src->width);
  			MEM_free(L1DSRAM, cache_in,3*img_src->width);
             return;
         }
         if(cache_out==NULL) {
 			 _printf("erreur allocation dspCvtColor %i\n",img_src->width);
  #if !defined(OMAPL138) && !defined(OMAP3530)
 			pthread_mutex_unlock(&mutex);
  #endif
 			MEM_free(L1DSRAM, cache_in_old,3*img_src->width);
 			MEM_free(L1DSRAM, cache_in,3*img_src->width);
 			MEM_free(L1DSRAM, cache_out_old,img_src->width);
 			return;
         }

        p.transferType = ACPY3_1D1D;
        p.elementSize = 2*img_src->width;
        p.srcElementIndex = 1;
        p.dstElementIndex = 1;
        p.numElements = 1;
        p.numFrames = 1;
        p.waitId = 0;// waitId of 0 implies wait after the first transfer
        ACPY3_configure(h_in, &p, 0);
        ACPY3_configure(h_out, &p, 0);
        ACPY3_fastConfigure16b(h_out,ACPY3_PARAMFIELD_ELEMENTSIZE,img_src->width,0);

        //les 2 premieres iterations ne sortent rien (+2)
        for(i=0;i<img_src->height+2;i++)
        {
            //cache_out_old > dst	trait cache_in > cache_out  src > cache_in_old
            if(i>1)
            {
                _ACPY3_fastConfigure32b(h_out,ACPY3_PARAMFIELD_SRCADDR,cache_out_old,0);
                _ACPY3_fastConfigure32b(h_out,ACPY3_PARAMFIELD_DSTADDR,data_dst,0);
                ACPY3_start(h_out);
                data_dst+=img_src->width;
            }

            _ACPY3_fastConfigure32b(h_in,ACPY3_PARAMFIELD_SRCADDR,data_src,0);
            _ACPY3_fastConfigure32b(h_in,ACPY3_PARAMFIELD_DSTADDR,cache_in_old,0);
            ACPY3_start(h_in);
            data_src+=2*img_src->width;

            //traitement non necessaire quand i=0, on le fait qd meme pour enlever un if...
            //car traitement + rapide que la copie
            convert_YUYV2GRAY(cache_out,cache_in,img_src->width);

            ACPY3_wait(h_out);
            ACPY3_wait(h_in);

            //permutation circulaire
            cache_tmp=cache_in;
            cache_in=cache_in_old;
            cache_in_old=cache_tmp;

            cache_tmp=cache_out;
            cache_out=cache_out_old;
            cache_out_old=cache_tmp;
        }

        MEM_free(L1DSRAM, cache_in_old,2*img_src->width);
        MEM_free(L1DSRAM, cache_in,2*img_src->width);
        MEM_free(L1DSRAM, cache_out_old,img_src->width);
        MEM_free(L1DSRAM, cache_out,img_src->width);
    }

    if(code==DSP_UYVY2GRAY)
        {
            cache_in_old=(int8_t*)MEM_alloc(L1DSRAM, 2*img_src->width, BUF_ALIGN);
            cache_in=(int8_t*)MEM_alloc(L1DSRAM, 2*img_src->width, BUF_ALIGN);
            cache_out_old=(int8_t*)MEM_alloc(L1DSRAM,img_src->width, BUF_ALIGN);
            cache_out=(int8_t*)MEM_alloc(L1DSRAM, img_src->width, BUF_ALIGN);

            if(cache_in_old==NULL) {
                 _printf("erreur allocation dspCvtColor %i\n",3*img_src->width);
     #if !defined(OMAPL138) && !defined(OMAP3530)
     			pthread_mutex_unlock(&mutex);
     #endif
                 return;
             }
             if(cache_in==NULL) {
                  _printf("erreur allocation dspCvtColor %i\n",3*img_src->width);
      #if !defined(OMAPL138) && !defined(OMAP3530)
      			pthread_mutex_unlock(&mutex);
      #endif
      			MEM_free(L1DSRAM, cache_in_old,3*img_src->width);
                 return;
             }
             if(cache_out_old==NULL) {
                  _printf("erreur allocation dspCvtColor %i\n",img_src->width);
      #if !defined(OMAPL138) && !defined(OMAP3530)
      			pthread_mutex_unlock(&mutex);
      #endif
      			MEM_free(L1DSRAM, cache_in_old,3*img_src->width);
      			MEM_free(L1DSRAM, cache_in,3*img_src->width);
                 return;
             }
             if(cache_out==NULL) {
     			 _printf("erreur allocation dspCvtColor %i\n",img_src->width);
      #if !defined(OMAPL138) && !defined(OMAP3530)
     			pthread_mutex_unlock(&mutex);
      #endif
     			MEM_free(L1DSRAM, cache_in_old,3*img_src->width);
     			MEM_free(L1DSRAM, cache_in,3*img_src->width);
     			MEM_free(L1DSRAM, cache_out_old,img_src->width);
     			return;
             }

            p.transferType = ACPY3_1D1D;
            p.elementSize = 2*img_src->width;
            p.srcElementIndex = 1;
            p.dstElementIndex = 1;
            p.numElements = 1;
            p.numFrames = 1;
            p.waitId = 0;// waitId of 0 implies wait after the first transfer
            ACPY3_configure(h_in, &p, 0);
            ACPY3_configure(h_out, &p, 0);
            ACPY3_fastConfigure16b(h_out,ACPY3_PARAMFIELD_ELEMENTSIZE,img_src->width,0);

            //les 2 premieres iterations ne sortent rien (+2)
            for(i=0;i<img_src->height+2;i++)
            {
                //cache_out_old > dst	trait cache_in > cache_out  src > cache_in_old
                if(i>1)
                {
                    _ACPY3_fastConfigure32b(h_out,ACPY3_PARAMFIELD_SRCADDR,cache_out_old,0);
                    _ACPY3_fastConfigure32b(h_out,ACPY3_PARAMFIELD_DSTADDR,data_dst,0);
                    ACPY3_start(h_out);
                    data_dst+=img_src->width;
                }

                _ACPY3_fastConfigure32b(h_in,ACPY3_PARAMFIELD_SRCADDR,data_src,0);
                _ACPY3_fastConfigure32b(h_in,ACPY3_PARAMFIELD_DSTADDR,cache_in_old,0);
                ACPY3_start(h_in);
                data_src+=2*img_src->width;

                //traitement non necessaire quand i=0, on le fait qd meme pour enlever un if...
                //car traitement + rapide que la copie
                convert_UYVY2GRAY(cache_out,cache_in,img_src->width);

                ACPY3_wait(h_out);
                ACPY3_wait(h_in);

                //permutation circulaire
                cache_tmp=cache_in;
                cache_in=cache_in_old;
                cache_in_old=cache_tmp;

                cache_tmp=cache_out;
                cache_out=cache_out_old;
                cache_out_old=cache_tmp;
            }

            MEM_free(L1DSRAM, cache_in_old,2*img_src->width);
            MEM_free(L1DSRAM, cache_in,2*img_src->width);
            MEM_free(L1DSRAM, cache_out_old,img_src->width);
            MEM_free(L1DSRAM, cache_out,img_src->width);
        }
#if !defined(OMAPL138) && !defined(OMAP3530)
    pthread_mutex_unlock(&mutex);
#endif
}


inline void convert_BGR2GRAY(int8_t* restrict dest,const int8_t* restrict src, uint32_t n)
{
    uint32_t i,pix,y;

    for (i=0; i<n; i++)
    {
        pix=_mem4_const(src);
        src+=3;
        y=_dotpu4(0x004d971c,pix);
        // undo the scale by 256 and write to memory:
        *dest++ = (y>>8);
    }
}

inline void convert_YUYV2GRAY(int8_t* restrict dest,const int8_t* restrict src, uint32_t n)
{
    uint32_t i;
    for (i=0; i<n; i+=2)
    {
        *dest=*src;//Y
        *src++;//U
        *src++;//Y
        *dest++;

        *dest=*src;//Y
        *src++;//V
        *src++;//Y
        *dest++;
    }
}

inline void convert_UYVY2GRAY(int8_t* restrict dest,const int8_t* restrict src, uint32_t n)
{
    uint32_t i;
    for (i=0; i<n; i+=2)
    {
        *src++;//Y
        *dest=*src;//Y
        *src++;//V
        *dest++;

        *src++;//Y
        *dest=*src;//Y
        *src++;//U
        *dest++;
    }
}

void dspThreshold(IplImage* img_src,IplImage* img_dst, float threshold,float max_value,int32_t threshold_type )
{
    ACPY3_Params p;
    uint32_t i;
    uint32_t nb_lines;
    uint32_t max_iterations;
    int8_t* restrict cache_in_old;
    int8_t* restrict cache_in;
    int8_t* restrict cache_out_old;
    int8_t* restrict cache_out;
    int8_t* data_src=(int8_t*)img_src->imageData;
    int8_t* data_dst=(int8_t*)img_dst->imageData;
    int8_t* cache_tmp;
    uint8_t thresh=(uint8_t)threshold;

#if !defined(OMAPL138) && !defined(OMAP3530)
    pthread_mutex_lock(&mutex);
#endif

    if(max_value!=255) _printf("threshold fixe a 255 dspThreshold\n");

    //calcul de la taille des buffers
    nb_lines=0;
    while(4*img_dst->width*nb_lines<BUFF_SIZE) nb_lines++;
    nb_lines--;
    while(img_dst->height%nb_lines!=0) nb_lines--;

    cache_in_old=MEM_alloc(L1DSRAM, img_src->width*nb_lines, BUF_ALIGN);
    cache_in=MEM_alloc(L1DSRAM, img_src->width*nb_lines, BUF_ALIGN);
    cache_out_old=MEM_alloc(L1DSRAM,img_src->width*nb_lines, BUF_ALIGN);
    cache_out=MEM_alloc(L1DSRAM, img_src->width*nb_lines, BUF_ALIGN);

    if(cache_in_old==NULL || cache_in==NULL || cache_out_old==NULL || cache_out==NULL)
    {
        _printf("erreur allocation dspThreshold\n");
    }

    p.transferType = ACPY3_1D1D;
    p.elementSize = img_src->width*nb_lines;
    p.srcElementIndex = 1;
    p.dstElementIndex = 1;
    p.numElements = 1;
    p.numFrames = 1;
    p.waitId = 0;// waitId of 0 implies wait after the first transfer
    ACPY3_configure(h_in, &p, 0);
    ACPY3_configure(h_out, &p, 0);

    //les 2 premieres iterations ne sortent rien (+2)
    max_iterations=(img_src->height/nb_lines)+2;
    for(i=0;i<max_iterations;i++)
    {
        //cache_out_old > dst	trait cache_in > cache_out  src > cache_in_old
        if(i>1)
        {
            _ACPY3_fastConfigure32b(h_out,ACPY3_PARAMFIELD_SRCADDR,cache_out_old,0);
            _ACPY3_fastConfigure32b(h_out,ACPY3_PARAMFIELD_DSTADDR,data_dst,0);
            ACPY3_start(h_out);
            data_dst+=img_src->width*nb_lines;
        }

        if(i<max_iterations-2)
        {
            _ACPY3_fastConfigure32b(h_in,ACPY3_PARAMFIELD_SRCADDR,data_src,0);
            _ACPY3_fastConfigure32b(h_in,ACPY3_PARAMFIELD_DSTADDR,cache_in_old,0);
            ACPY3_start(h_in);
            data_src+=img_src->width*nb_lines;
        }

        //traitement non necessaire quand i=0 et à la fin, on le fait qd meme pour enlever un if...
        //car traitement + rapide que la copie ?
        //voir la diff si on met le if...
        if(i<max_iterations-1) IMG_thr_8((uint8_t *)cache_in,(uint8_t *)cache_out,img_src->width,nb_lines,thresh);

        ACPY3_wait(h_out);
        ACPY3_wait(h_in);

        //permutation circulaire
        cache_tmp=cache_in;
        cache_in=cache_in_old;
        cache_in_old=cache_tmp;

        cache_tmp=cache_out;
        cache_out=cache_out_old;
        cache_out_old=cache_tmp;
    }

    MEM_free(L1DSRAM, cache_in_old,img_src->width*nb_lines);
    MEM_free(L1DSRAM, cache_in,img_src->width*nb_lines);
    MEM_free(L1DSRAM, cache_out_old,img_src->width*nb_lines);
    MEM_free(L1DSRAM, cache_out,img_src->width*nb_lines);
#if !defined(OMAPL138) && !defined(OMAP3530)
    pthread_mutex_unlock(&mutex);
#endif
}

void IMG_thr_8
(
    const uint8_t *in_data,                /*  Input image data    */
    uint8_t       *restrict out_data,      /*  Output image data   */
    int16_t cols, int16_t rows,                      /*  Image dimensions    */
    uint8_t       threshold                /*  Threshold value     */
)
{
    int32_t i, pixels = rows * cols;
    uint32_t thththth;

    _nassert((int32_t)in_data % 8 == 0);
    _nassert((int32_t)out_data % 8 == 0);
    _nassert(pixels % 16 == 0);
    _nassert(pixels      >= 16);


    thththth = _pack2(threshold, threshold);
    thththth = _packl4(thththth, thththth);

    /* -------------------------------------------------------------------- */
    /*  Step through input image copying pixels to the output.  If the      */
    /*  pixels are above our threshold, set them to 255.                    */
    /* -------------------------------------------------------------------- */
    #pragma MUST_ITERATE(4,,4);
    #pragma UNROLL(4);
    for (i = 0; i < pixels; i += 4)
    {
        uint32_t p3p2p1p0, x3x2x1x0;

        p3p2p1p0 = _amem4_const(&in_data[i]);
        x3x2x1x0 = _xpnd4(_cmpgtu4(p3p2p1p0, thththth));
#if defined OMAPL138 || defined OMAP3530
        _amem4(&out_data[i]) =  x3x2x1x0;
#else
        memcpy(&out_data[i],&x3x2x1x0,4);
#endif
    }
}
