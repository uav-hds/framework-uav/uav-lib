/** ============================================================================
 *  @file   tskMessage.c
 *
 *  @path   $(DSPLINK)/dsp/src/samples/message/
 *
 *  @desc   This is simple TSK based application that uses MSGQ.  It receives a
 *          messages from the GPP, verifies its content and sends it back to
 *          the GPP.
 *
 *  @ver    1.63
 *  ============================================================================
 *  Copyright (C) 2002-2009, Texas Instruments Incorporated -
 *  http://www.ti.com/
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *  *  Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *  *  Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *  *  Neither the name of Texas Instruments Incorporated nor the names of
 *     its contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 *  THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 *  PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 *  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 *  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 *  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *  ============================================================================
 */

/*
 * Guillaume Sanahuja: ajout des appels vers fonctions traitement vidéo
 */

/*  ----------------------------------- DSP/BIOS Headers            */

#include <std.h>
#include <gbl.h>
#include <sys.h>
#include <sem.h>
#include <tsk.h>
#include <msgq.h>
//#include <pool.h>


/*  ----------------------------------- DSP/BIOS LINK Headers       */
#include <dsplink.h>
#include <platform.h>

/*  ----------------------------------- Sample Headers              */
#include "message_config.h"
#include "tskMessage.h"

/*  ----------------------------------- OpenCV       */
#include "sobel.h"
#include "basic_image.h"
#include "featureselect.h"
#include "lkpyramid.h"
#include "pyramids.h"
#include "hough.h"
#include "dsp_opencv.h"
#include "opencv_common.h"
#include "dman3_config.h"

#ifdef __cplusplus
extern "C" {
#endif

//extern far Int INTERNALHEAP;
//extern far Int EXTMEMHEAP;
//extern int L1DHEAP; // Heap Label for L1DRAM memory allocation
//extern int EXTMEMHEAP; //Heap La

//faire un header
extern void _printf(const char * format, ...);

IDMA3_Handle h_in,h_out;


/** ============================================================================
 *  @func   TSKMESSAGE_create
 *
 *  @desc   Create phase function for the TSKMESSAGE application. Initializes
 *          the TSKMESSAGE_TransferInfo structure with the information that will
 *          be used by the other phases of the application.
 *
 *  @modif  None.
 *  ============================================================================
 */
Int TSKMESSAGE_create (TSKMESSAGE_TransferInfo ** infoPtr)
{
    Int                       status     = SYS_OK ;
    MSGQ_Attrs                msgqAttrs  = MSGQ_ATTRS ;
    TSKMESSAGE_TransferInfo * info       = NULL ;
    MSGQ_LocateAttrs          syncLocateAttrs ;

    /* Allocate TSKMESSAGE_TransferInfo structure that will be initialized
     * and passed to other phases of the application */
    *infoPtr = MEM_calloc (DSPLINK_SEGID,
                           sizeof (TSKMESSAGE_TransferInfo),
                           DSPLINK_BUF_ALIGN) ;
    if (*infoPtr == NULL)
    {
        status = SYS_EALLOC ;
        _printf("MEM_calloc: Failed.\n");
    }
    else {
        info = *infoPtr ;
        info->localMsgq    = MSGQ_INVALIDMSGQ ;
        info->locatedMsgq  = MSGQ_INVALIDMSGQ ;
    }

    if (status == SYS_OK) {
        /* Set the semaphore to a known state. */
        SEM_new (&(info->notifySemObj), 0) ;

        /* Fill in the attributes for this message queue. */
        msgqAttrs.notifyHandle = &(info->notifySemObj) ;
        msgqAttrs.pend         = (MSGQ_Pend) SEM_pendBinary ;
        msgqAttrs.post         = (MSGQ_Post) SEM_postBinary ;

        /* Creating message queue */
        status = MSGQ_open (DSP_MSGQNAME, &info->localMsgq, &msgqAttrs) ;
        if (status != SYS_OK)
        {
            _printf("MSGQ_open: Failed.\n");
        }
        else {
            /* Set the message queue that will receive any async. errors. */
            MSGQ_setErrorHandler (info->localMsgq, SAMPLE_POOL_ID) ;

            /* Synchronous locate. */
            status = SYS_ENOTFOUND ;
            while ((status == SYS_ENOTFOUND) || (status == SYS_ENODEV))
            {
                syncLocateAttrs.timeout = SYS_FOREVER ;
                status = MSGQ_locate (GPP_MSGQNAME,&info->locatedMsgq,&syncLocateAttrs) ;
                if ((status == SYS_ENOTFOUND) || (status == SYS_ENODEV))
                {
                    TSK_sleep (1000) ;
                }
                else if (status != SYS_OK) {
                    _printf("MSGQ_locate (msgqOut) failed.Status = 0x%x\n", status) ;
                }
            }
        }

    }

    return status ;
}


/** ============================================================================
 *  @func   TSKMESSAGE_execute
 *
 *  @desc   Execute phase function for the TSKMESSAGE application. Application
 *          receives a message, verifies the id and sends it back.
 *
 *  @modif  None.
 *  ============================================================================
 */

Int TSKMESSAGE_execute(TSKMESSAGE_TransferInfo * info)
{
    Int         status = SYS_OK;
    MSGQ_Msg    msg ;
    DSPCV_query *inMsg;
    Uint8 quit=0;

    dman3_start(&h_in,&h_out);

    /* Allocate and send the message */
    status = MSGQ_alloc (SAMPLE_POOL_ID, &msg, APP_BUFFER_SIZE) ;

    if (status == SYS_OK)
    {

        MSGQ_setMsgId (msg, 121) ;
        MSGQ_setSrcQueue (msg, info->localMsgq) ;

        status = MSGQ_put (info->locatedMsgq, msg) ;
        if (status != SYS_OK)
        {
            // Must free the message
            MSGQ_free (msg) ;
            _printf("MSGQ_put: Failed.\n");
        }
    }
    else
    {
         _printf("MSGQ_alloc: Failed.\n");
    }

    while(quit==0)
    {
        /* Receive a message */
        //status = MSGQ_get(info->localMsgq, (MSGQ_Msg*) &inMsg, SYS_FOREVER);
        status = MSGQ_get(info->localMsgq, (MSGQ_Msg*) &inMsg,0);

        if (status == SYS_OK)
        {
            /* Check if an asynchronous error message is received */
            if (MSGQ_getMsgId ((MSGQ_Msg)inMsg) == MSGQ_ASYNCERRORMSGID)
            {
                /* Must free the message */
                MSGQ_free ((MSGQ_Msg) inMsg) ;
                //status = SYS_EBADIO ;
                _printf("MSGQ_get: Failed.\n");
            }
            else
            {

                //faire le traitement en fonction de l'id message...
                switch (inMsg->openCVFxnIndex)
                {
                case CV_PYR_DOWN:
                {
                	struct PyrDown_msg *msg;
                	msg=(PyrDown_msg*)inMsg->msg_ptr;
                    dspPyrDown(msg->img_src,msg->img_dst,msg->level);
                    break;
                }
                case CV_GOOD_FEATURES_TO_TRACK:
                {
                	struct GoodFeaturesToTrack_msg *msg;
                	msg=(GoodFeaturesToTrack_msg*)inMsg->msg_ptr;
                    dspGoodFeaturesToTrack(msg->img_src,msg->features,msg->total_count,msg->quality_level, msg->min_distance);
                    break;
                }
                case CV_SOBEL:
                {
                    struct Sobel_msg *msg;
                    msg=(Sobel_msg*)inMsg->msg_ptr;
                    dspSobel(msg->img_src,msg->img_dst,msg->dx,msg->dy);
                    break;
                }
                case CV_CVT_COLOR:
                {
                	struct CvtColor_msg *msg;
                	msg=(CvtColor_msg*)inMsg->msg_ptr;
                    dspCvtColor(msg->img_src,msg->img_dst,msg->code);
                    break;
                }
                case CV_THRESHOLD:
                {
                	struct Threshold_msg *msg;
                	msg=(Threshold_msg*)inMsg->msg_ptr;
                    dspThreshold(msg->img_src,msg->img_dst,msg->threshold,msg->max_value,msg->threshold_type);
                    break;
                }
                case CV_CLONE_IMAGE:
                {
                    struct CloneImage_msg *msg;
                    msg=(CloneImage_msg*)inMsg->msg_ptr;
                    dspCloneImage(msg->img_src,msg->img_dst);
                    break;
                }
                case CV_LK:
                {
                	struct CalcOpticalFlowPyrLK_msg *msg;
                	msg=(CalcOpticalFlowPyrLK_msg*)inMsg->msg_ptr;
                    dspCalcOpticalFlowPyrLK(msg->img_A,msg->img_B,msg->pyr_A,msg->pyr_B,
                    		msg->features_A,msg->features_B,msg->count,msg->winSize,
                    		msg->level,msg->feat_status,msg->error,msg->criteria,msg->flags);
                    break;
                }
                case CV_HOUGHLINES2:
                {
                	struct Houghlines2_msg *msg;
                	msg=(Houghlines2_msg*)inMsg->msg_ptr;
                    *(msg->nb_line)=dspHoughLines2(msg->img_src,msg->line_storage,msg->method,msg->rho_step,msg->theta_step,msg->threshold,msg->param1,msg->param2);
                    break;
                }
                case CV_HOUGHLINES2_TEST:
                {
                	struct HoughlinesTracking2_msg *msg;
                	msg=(HoughlinesTracking2_msg*)inMsg->msg_ptr;
					*(msg->nb_line)=dspHoughLines2_test(msg->img_src,msg->line_storage,msg->method,msg->rho,msg->theta_min,msg->theta_max,msg->theta_step,msg->threshold);
					break;
                }
                case CV_HOUGHLINES_TRACKING:
				{
					struct HoughlinesTracking_msg *msg;
					msg=(HoughlinesTracking_msg*)inMsg->msg_ptr;
					*(msg->nb_line)=dspHoughLinesTracking(msg->img_src,msg->line_storage,msg->method,msg->rho_step,msg->theta_moy,msg->delta_theta,msg->theta_step,msg->threshold);
					break;
				}
                case CV_JPEG:
                {
                	struct Jpeg_msg *msg;
					msg=(Jpeg_msg*)inMsg->msg_ptr;
                	dspCvtJpg(msg->img_src,msg->data_img_dst,msg->compression_level,msg->input_format,msg->output_format,msg->output_size);
                    break;
                }
                case CV_QUIT:
                    quit=1;
                    break;
                }

                status=MSGQ_free ((MSGQ_Msg) inMsg) ;

                if (status != SYS_OK)
                {
                     _printf("MSGQ_free: Failed.\n");
                     //while(1)
                     //{
                     //}

                }

                /* Allocate and send the message */
                MSGQ_alloc (SAMPLE_POOL_ID, &msg, APP_BUFFER_SIZE) ;

                MSGQ_setMsgId (msg,DSP_OK) ;

                MSGQ_setSrcQueue (msg, info->localMsgq) ;

                /* Send the message back */
                status = MSGQ_put (info->locatedMsgq, msg) ;
                if (status != SYS_OK)
                {
                    _printf("MSGQ_put: Failed.\n");
                }

            }
        }
        else {
            //SET_FAILURE_REASON (status) ;
        }
    }

    dman3_stop(&h_in,&h_out);

    return status ;
}

/** ============================================================================
 *  @func   TSKMESSAGE_delete
 *
 *  @desc   Delete phase function for the TSKMESSAGE application. It deallocates
 *          all the resources of allocated during create phase of the
 *          application.
 *
 *  @modif  None.
 *  ============================================================================
 */
Int TSKMESSAGE_delete (TSKMESSAGE_TransferInfo * info)
{
    Int     status     = SYS_OK ;
    Int     tmpStatus  = SYS_OK ;
    Bool    freeStatus = FALSE ;

    /* Release the located message queue */
    if (info->locatedMsgq != MSGQ_INVALIDMSGQ) {
        status = MSGQ_release (info->locatedMsgq) ;
        if (status != SYS_OK) _printf("MSGQ_release: Failed.\n");
    }

    /*
     *  Reset the error handler before deleting the MSGQ that receives the error
     *  messages.
     */
    MSGQ_setErrorHandler (MSGQ_INVALIDMSGQ, POOL_INVALIDID) ;

    /* Delete the message queue */
    if (info->localMsgq != MSGQ_INVALIDMSGQ) {
        tmpStatus = MSGQ_close (info->localMsgq) ;
        if ((status == SYS_OK) && (tmpStatus != SYS_OK))
        {
            status = tmpStatus ;
            _printf("MSGQ_close: Failed.\n");
        }
    }

    /* Free the info structure */
    freeStatus = MEM_free (DSPLINK_SEGID,
                           info,
                           sizeof (TSKMESSAGE_TransferInfo)) ;
    if ((status == SYS_OK) && (freeStatus != TRUE))
    {
        status = SYS_EFREE ;
         _printf("MEM_free: Failed.\n");
    }

    return status ;
}


#if defined (__cplusplus)
}
#endif /* defined (__cplusplus) */
