//  created:    2012/01/18
//  filename:   dsp_opencv.c
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 6599
//
//  version:    $Id: $
//
//  purpose:    fonctions tests
//
//
/*********************************************************************/

#include <stdio.h>
#include <stdlib.h>

#if defined OMAPL138 || defined OMAP3530
//faire un header
extern void _printf(const char * format, ...);

#include <std.h>
#include <bcache.h>
#include <idmjpge.h>
#include <ti/sdo/fc/utils/api/alg.h>
#include <ti/sdo/fc/acpy3/acpy3.h>
#include <ti/sdo/fc/dman3/dman3.h>
#endif
/*  ----------------------------------- OpenCV       */
#include "dsp_opencv.h"
#include "opencv_common.h"


#if defined OMAPL138 || defined OMAP3530


/* Input/Output Buffer Descriptor variables */
//pour conversion jpeg, bug en local?
XDM1_BufDesc inputBufDesc, outputBufDesc;

uint32_t convert_formad_id(uint32_t value)
{
    //if(value==CHROMA_NA) return XDM_CHROMA_NA;
    if(value==YUV_420P) return XDM_YUV_420P;
    if(value==YUV_422P) return XDM_YUV_422P;
    if(value==YUV_422IBE) return XDM_YUV_422IBE;
    if(value==YUV_422ILE) return XDM_YUV_422ILE;
    if(value==YUV_444P) return XDM_YUV_444P;
    if(value==YUV_411P) return XDM_YUV_411P;
    if(value==GRAY) return XDM_GRAY;
    if(value==YUV_420SP) return XDM_YUV_420SP;
    if(value==ARGB8888) return XDM_ARGB8888;
    if(value==RGB555) return XDM_RGB555;
    if(value==YUV_444ILE) return XDM_YUV_444ILE;

    _printf("format %i not handled\n",value);
    return 0;
}

//from c64xplus_jpegenc_production exemple
//output size is equal to input size by arm code
void dspCvtJpg(IplImage *img_src,uint8_t *data_img_dst,uint8_t compression_level,uint8_t input_format,uint8_t output_format,uint32_t *output_size)
{
    Int8 *inputData=img_src->imageData;
    Int8 *outputData=(Int8 *)data_img_dst;
    uint32_t width=img_src->width;
    uint32_t height=img_src->height;

    XDAS_Int8 ii;
    IIMGENC1_Fxns            *IIMGENC1fxns;
    IIMGENC1_Status          status;
    IIMGENC1_InArgs          inArgs;
    IIMGENC1_OutArgs         outArgs;
    IIMGENC1_Params          params;
    IIMGENC1_DynamicParams   dynamicParams;

    // Algorithm specific handle
    IALG_Handle handle;


    // Other variables
    XDAS_Int32   retVal ;
    uint32_t	TotalBytes;

_printf("%x %x %x %x %i %i %i %i %i\n",img_src,img_src->imageData,data_img_dst,output_size,input_format,output_format,width,height,img_src->imageSize);
    // Enable Cache Settings
    BCACHE_setMar((Void *)inputData,img_src->imageSize,BCACHE_MAR_ENABLE);
    BCACHE_setMar((Void *)outputData,img_src->imageSize,BCACHE_MAR_ENABLE);

   // Setting the sizes of Base Class Objects
	params.size                    = sizeof(IIMGENC1_Params);
	params.maxHeight               = height;
	params.maxWidth                = width;
	params.maxScans                = 0;
	dynamicParams.inputWidth        =width;
    dynamicParams.inputHeight       =height;
    params.forceChromaFormat        =convert_formad_id(output_format);
    dynamicParams.inputChromaFormat =convert_formad_id(input_format);
    dynamicParams.qValue=compression_level;
    dynamicParams.captureWidth=0;
	params.dataEndianness          = XDM_BYTE;
	dynamicParams.size              = sizeof(IIMGENC1_DynamicParams);
	dynamicParams.numAU             = XDM_DEFAULT;
	dynamicParams.generateHeader    =XDM_ENCODE_AU;

	status.size                     = sizeof(IIMGENC1_Status);

	inArgs.size                     = sizeof(IIMGENC1_InArgs);

	outArgs.size                    = sizeof(IIMGENC1_OutArgs);
	outArgs.extendedError           = 0;
	outArgs.bytesGenerated          = 0;
	outArgs.currentAU               = 0;

	if(params.forceChromaFormat==0 || dynamicParams.inputChromaFormat==0) {
	  return;
	}

    // Create the Algorithm object (instance)
    if ((handle =  (IALG_Handle)ALG_create (
      (IALG_Fxns *) &DMJPGE_TIGEM_IDMJPGE,
      (IALG_Handle) NULL,
      (IALG_Params *) &params)) == NULL)
    {
      _printf("Failed to Create Instance... Exiting for this configuration..\n");
      return;
    }


    // Assigning Algorithm handle fxns field to IIMGdecfxns
    IIMGENC1fxns = (IIMGENC1_Fxns *)handle->fxns ;

    // Activate the Algorithm
   handle->fxns->algActivate(handle);

    // Optional: Set Run time parameters in the Algorithm via control()
    IIMGENC1fxns->control((IIMGENC1_Handle)handle, XDM_SETPARAMS,(IIMGENC1_DynamicParams *)&dynamicParams, (IIMGENC1_Status *)&status);

    // Get Buffer information
    IIMGENC1fxns->control((IIMGENC1_Handle)handle,
                         XDM_GETBUFINFO,
                         (IIMGENC1_DynamicParams *)&dynamicParams,
                         (IIMGENC1_Status *)&status);

    // Fill up the buffers as required by algorithm
    inputBufDesc.numBufs  = status.bufInfo.minNumInBufs ;
    inputBufDesc.descs[0].buf=inputData;
    inputBufDesc.descs[0].bufSize = status.bufInfo.minInBufSize[0];

    for(ii=0; ii< (status.bufInfo.minNumInBufs - 1);ii++ )
    {
      inputBufDesc.descs[ii+1].buf = inputBufDesc.descs[ii].buf +status.bufInfo.minInBufSize[ii];
      inputBufDesc.descs[ii +1].bufSize = status.bufInfo.minInBufSize[ii +1];
    //  _printf("%i %i\n",ii, status.bufInfo.minInBufSize[ii +1]);
    }

    outputBufDesc.descs[0].buf     = outputData;
    outputBufDesc.numBufs     = status.bufInfo.minNumOutBufs ;
    outputBufDesc.descs[0].bufSize = status.bufInfo.minOutBufSize[0];

	if(status.bufInfo.minOutBufSize[0] > img_src->imageSize) {
		_printf("status.bufInfo.minOutBufSize[0] too big\n");
		handle->fxns->algDeactivate(handle);
		ALG_delete (handle);
		return;
	}

	//datasheet says that status.bufInfo.minNumOutBufs=1
	if(status.bufInfo.minNumOutBufs!=1) {
		_printf("status.bufInfo.minNumOutBufs=%i !=1\n",status.bufInfo.minNumOutBufs);
		handle->fxns->algDeactivate(handle);
		ALG_delete (handle);
		return;
	}

	//above is from ti exemple but should not be necessary
	/*
    for(ii=0; ii< (status.bufInfo.minNumOutBufs-1); ii++ )
    {
		if(outputBufDesc.descs[0].bufSize > OUTPUT_BUFFER_SIZE)
		{
		    _printf("outputBufDesc.descs[0].bufSize too big\n");
		    ALG_delete (handle);
			return;
		}

        outputBufDesc.descs[ii+1].buf = outputBufDesc.descs[ii].buf + status.bufInfo.minOutBufSize[ii];
        outputBufDesc.descs[ii+1].bufSize = status.bufInfo.minOutBufSize[ii+1];
    }
*/

    TotalBytes = 0;
   // _printf("1\n");
       // Basic Algorithm process() call
      retVal = IIMGENC1fxns->process((IIMGENC1_Handle)handle,
        (XDM1_BufDesc *)&inputBufDesc,
        (XDM1_BufDesc *)&outputBufDesc,
        (IIMGENC1_InArgs *)&inArgs,
        (IIMGENC1_OutArgs *)&outArgs);

    TotalBytes+=outArgs.bytesGenerated;
    _printf("1\n");
    if(retVal == XDM_EFAIL)
    {
        _printf("IIMGENC1fxns->process failed iteration %i/%i\n",outArgs.currentAU , status.totalAU);
        handle->fxns->algDeactivate(handle);
        ALG_delete (handle);
       return;
    }

      while (outArgs.currentAU < status.totalAU)
      {

	      // Basic Algorithm process() call
	      retVal = IIMGENC1fxns->process((IIMGENC1_Handle)handle,
	        (XDM1_BufDesc *)&inputBufDesc,
	        (XDM1_BufDesc *)&outputBufDesc,
	        (IIMGENC1_InArgs *)&inArgs,
	        (IIMGENC1_OutArgs *)&outArgs);
        TotalBytes+=outArgs.bytesGenerated;

	      if(retVal == XDM_EFAIL)
	      {
	    	  _printf("IIMGENC1fxns->process failed iteration %i/%i\n",outArgs.currentAU , status.totalAU);
	          handle->fxns->algDeactivate(handle);
	          ALG_delete (handle);
	       return;
	      }
	  }
      _printf("2\n");

    *output_size=TotalBytes;
/*
 	 //all this wbinv is not sufficient!
    BCACHE_inv((Void *)img_src,sizeof(IplImage),TRUE);
    BCACHE_inv((Void *)inputData,img_src->imageSize,TRUE);
    BCACHE_wbInv((Void *)outputData,TotalBytes,TRUE);
    BCACHE_inv((Void *)&compression_level,sizeof(uint8_t),TRUE);
    BCACHE_inv((Void *)&input_format,sizeof(uint8_t),TRUE);
    BCACHE_inv((Void *)&output_format,sizeof(uint8_t),TRUE);
    BCACHE_wbInv((Void *)output_size,sizeof(uint32_t),TRUE);
*/
    BCACHE_wbInvAll();
    BCACHE_setMar((Void *)inputData,img_src->imageSize,BCACHE_MAR_DISABLE);
    BCACHE_setMar((Void *)outputData,img_src->imageSize,BCACHE_MAR_DISABLE);

   // DeActivate the Algorithm
   handle->fxns->algDeactivate(handle);

    // Delete the Algorithm instance object specified by handle
   ALG_delete (handle);


}
#endif
