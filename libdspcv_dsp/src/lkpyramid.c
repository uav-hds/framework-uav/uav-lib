//  created:    2012/01/18
//  filename:   lkpyramid.c
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 6599
//
//  version:    $Id: $
//
//  purpose:    Lucas et Kanade pyramidal
//
//
/*********************************************************************/

#include "lkpyramid.h"
#include <stdlib.h>

#if defined OMAPL138 || defined OMAP3530
//#include <xdc/std.h>
#include <std.h>
#include <mem.h>
//#include <ti/xdais/idma3.h>
#include <ti/sdo/fc/acpy3/acpy3.h>
#define _ACPY3_fastConfigure32b(handle,type,addr,id) ACPY3_fastConfigure32b(handle,type,(uint32_t)addr,id)
//faire un header
extern void _printf(const char * format, ...);
#else
#include <string.h>
#include "C64intrins.h"
#include "wrappers.h"
#define _ACPY3_fastConfigure32b(handle,type,addr,id) ACPY3_fastConfigure_ptr(handle,type,(void*)addr,id)
extern pthread_mutex_t mutex;
#endif

#include "opencv_common.h"

#ifdef OMAPL138
extern int32_t DDR;
extern int32_t IRAM;
extern int32_t L1DSRAM;
extern int32_t CACHE_L1D;
#define BUF_SEG_ID IRAM
//#define BUF_SEG_ID L1DSRAM
#define BUF_ALIGN 128
#endif


#ifdef OMAP3530
extern int32_t DDR2;
extern int32_t L1DSRAM;
#define BUF_ALIGN 128
#endif

#define MAX_PYR 5

extern IDMA3_Handle h_in,h_out;

//mettre features_A et B en bcache
void dspCalcOpticalFlowPyrLK(IplImage* restrict img_A,IplImage* restrict img_B,IplImage* restrict pyr_A,IplImage* restrict pyr_B,
        CvPoint* restrict features_A,CvPoint* restrict features_B,int32_t count,CvSize winSize,
        int32_t level,int8_t* restrict feat_status,uint32_t* restrict error,CvTermCriteria criteria,int32_t flags)
{

    ACPY3_Params p;
    int32_t imgB_winx=winSize.width+4;//imgB should be the larger possible
    int32_t imgB_winy=winSize.height+4;//no verfication is done if u+g+v is outside imgB!!

    uint8_t* restrict cache_imageA_1;//=&cache_buffer[0];
    uint8_t* restrict cache_imageB_1;//=&cache_imageA_1[8+(((2*winSize.width+4)*(2*winSize.height+4))>>3)<<3];
    uint8_t* restrict cache_interp_1;//=&cache_imageB_1[8+(((2*imgB_winx+1)*(2*imgB_winy+1))>>3)<<3];
//verifier que ca rentre dans le buffer!! (a priori oui car winx et winy sont petits)
//a faire plus grand, multiple de 8 a cause du traitement conv dxdy
//et a aligner
//a revoir!!! (1 octet en plus devrait suffire...)
    int16_t* restrict cache_dx;//=(int16_t*)(&cache_interp_1[8+(((2*winSize.width+3)*(2*winSize.height+3))>>3)<<3]);
    int16_t* restrict cache_dy;//=(int16_t*)(&cache_dx[8+(((2*winSize.width+1)*(2*winSize.height+1))>>3)<<3]);
    int32_t* restrict cache_dx_dx;//=(int32_t*)(&cache_dy[8+(((2*winSize.width+1)*(2*winSize.height+1))>>3)<<3]);
    int32_t* restrict cache_dy_dy;//=(int32_t*)(&cache_dx_dx[8+(((2*winSize.width+1)*(2*winSize.height+1))>>3)<<3]);
    int32_t* restrict cache_dx_dy;//=(int32_t*)(&cache_dy_dy[8+(((2*winSize.width+1)*(2*winSize.height+1))>>3)<<3]);

    int32_t l;
    uint32_t j,k,kmax=criteria.max_iter;
    int32_t x,y;
    uint8_t flag_lost;
    int32_t start_x,start_x_old,stop_x,stop_x_old,start_y,start_y_old,stop_y,stop_y_old;
    int32_t Gxx,Gyy,Gxy,nx,ny;
    float det_G,inv_Gxx,inv_Gyy,inv_Gxy;
    int32_t ux,uy,gx,gy,vx,vy,bx,by;
    int32_t offset_grad;

    uint8_t *pyr_org_A[MAX_PYR],*pyr_org_B[MAX_PYR];
    int32_t mant_gxx,exp_gxx;
    int32_t mant_gxy,exp_gxy;
    int32_t mant_gyy,exp_gyy;

    int32_t line_width[MAX_PYR];
    int32_t width[MAX_PYR];
    int32_t height[MAX_PYR];

#if !defined(OMAPL138) && !defined(OMAP3530)
    pthread_mutex_lock(&mutex);
#endif
/*
    cache_imageA_1=MEM_alloc(L1DSRAM, (2*winSize.width+4)*(2*winSize.height+4), BUF_ALIGN);
    cache_imageB_1=MEM_alloc(L1DSRAM, (2*imgB_winx+1)*(2*imgB_winy+1), BUF_ALIGN);
    cache_interp_1=MEM_alloc(L1DSRAM, (2*winSize.width+3)*(2*winSize.height+3), BUF_ALIGN);
  */
    cache_imageA_1=MEM_alloc(L1DSRAM, 8+(((2*winSize.width+4)*(2*winSize.height+4))>>3)<<3, BUF_ALIGN);
    cache_imageB_1=MEM_alloc(L1DSRAM, 8+(((2*imgB_winx+1)*(2*imgB_winy+1))>>3)<<3, BUF_ALIGN);
    cache_interp_1=MEM_alloc(L1DSRAM, 8+(((2*winSize.width+3)*(2*winSize.height+3))>>3)<<3, BUF_ALIGN);

    if(cache_imageA_1==NULL || cache_imageB_1==NULL || cache_interp_1==NULL)
    {
        _printf("erreur allocation dspCalcOpticalFlowPyrLK\n");
    }
/*
    cache_dx=MEM_alloc(L1DSRAM, (2*winSize.width+1)*(2*winSize.height+1)*sizeof(int16_t), BUF_ALIGN);
    cache_dy=MEM_alloc(L1DSRAM, (2*winSize.width+1)*(2*winSize.height+1)*sizeof(int16_t), BUF_ALIGN);
    cache_dx_dx=MEM_alloc(L1DSRAM, (2*winSize.width+1)*(2*winSize.height+1)*sizeof(int32_t), BUF_ALIGN);
    cache_dy_dy=MEM_alloc(L1DSRAM, (2*winSize.width+1)*(2*winSize.height+1)*sizeof(int32_t), BUF_ALIGN);
    cache_dx_dy=MEM_alloc(L1DSRAM, (2*winSize.width+1)*(2*winSize.height+1)*sizeof(int32_t), BUF_ALIGN);
  */
    cache_dx=MEM_alloc(L1DSRAM, (8+(((2*winSize.width+1)*(2*winSize.height+1))>>3)<<3)*sizeof(int16_t), BUF_ALIGN);
    cache_dy=MEM_alloc(L1DSRAM, (8+(((2*winSize.width+1)*(2*winSize.height+1))>>3)<<3)*sizeof(int16_t), BUF_ALIGN);
    cache_dx_dx=MEM_alloc(L1DSRAM, (8+(((2*winSize.width+1)*(2*winSize.height+1))>>3)<<3)*sizeof(int32_t), BUF_ALIGN);
    cache_dy_dy=MEM_alloc(L1DSRAM, (8+(((2*winSize.width+1)*(2*winSize.height+1))>>3)<<3)*sizeof(int32_t), BUF_ALIGN);
    cache_dx_dy=MEM_alloc(L1DSRAM, (8+(((2*winSize.width+1)*(2*winSize.height+1))>>3)<<3)*sizeof(int32_t), BUF_ALIGN);

    if(cache_dx==NULL || cache_dy==NULL || cache_dx_dx==NULL || cache_dy_dy==NULL || cache_dx_dy==NULL)
    {
        _printf("erreur allocation dspCalcOpticalFlowPyrLK\n");
    }

    //prevoir erreur si level>taille pyr_org

    //construction d'un array de pointeurs avec les adresses des pyramides
    pyr_org_A[0]=(uint8_t*)img_A->imageData;
    pyr_org_B[0]=(uint8_t*)img_B->imageData;
    pyr_org_A[1]=(uint8_t*)pyr_A->imageData;
    pyr_org_B[1]=(uint8_t*)pyr_B->imageData;
    line_width[0]=img_A->width;
    width[0]=img_A->width;
    height[0]=img_A->height;

    if(img_A->roi!=NULL)
    {
        pyr_org_A[0]+=img_A->roi->xOffset+img_A->roi->yOffset*img_A->width;
        pyr_org_B[0]+=img_B->roi->xOffset+img_B->roi->yOffset*img_B->width;
        width[0]=img_A->roi->width;
        height[0]=img_A->roi->height;
    }

    line_width[1]=width[0]>>1;
    width[1]=width[0]>>1;
    height[1]=height[0]>>1;

    for(l=level;l>1;l--)
    {
        pyr_org_A[l]=pyr_org_A[l-1]+  width[l-1]*height[l-1];
        pyr_org_B[l]=pyr_org_B[l-1]+  width[l-1]*height[l-1];
        line_width[l]=width[0]>>l;
        width[l]=width[0]>>l;
        height[l]=height[0]>>l;
    }

    //configure h_A transfer
    //copy the window around u to cache (from image A)
    //coordinate [ux - ωx - 1, ux + ωx + 2] × [uy - ωy - 1, uy + ωy + 2]
    p.transferType = ACPY3_2D1D;
    p.dstAddr = (void *)cache_imageA_1;
    p.elementSize = 2*winSize.width+4;
    p.numElements = 2*winSize.height+4;
    p.waitId = 0;// waitId of 0 implies wait after the first transfer
    ACPY3_configure(h_in, &p, 0);

    //configure h_B transfer
    //copy the window around u+g to cache (from image B)
    //coordinate [ux+gx - imgB_winx, ux+gx + imgB_winx] × [uy+gy - imgB_winy, uy+gy + imgB_winy]
    p.transferType = ACPY3_2D1D;
    p.dstAddr = (void *)cache_imageB_1;
    p.elementSize =2*imgB_winx+1;
    p.numElements = 2*imgB_winy+1;
    p.waitId = 0;// waitId of 0 implies wait after the first transfer
    ACPY3_configure(h_out, &p, 0);

    //features loop
    for(j=0;j<count;j++)
    {
        //init g and v
        vx=0;vy=0;
        gx=0;gy=0;

        //pyramid level loop
        for(l=level;l>=0;l--)
        {
            //update g
            gx=(gx+vx)<<1;
            gy=(gy+vy)<<1;

            //point u in the current pyr
            //fixed point, 8bits
            ux=(features_A[j].x)<<(8-l);
            uy=(features_A[j].y)<<(8-l);

            //copy the window around int(u) to cache (from image A)
            //coordinate [ux - ωx - 1, ux + ωx + 2] × [uy - ωy - 1, uy + ωy + 2]
            ACPY3_fastConfigure16b(h_in,ACPY3_PARAMFIELD_ELEMENTINDEX_SRC,line_width[l],0);
            _ACPY3_fastConfigure32b(h_in,ACPY3_PARAMFIELD_SRCADDR,(pyr_org_A[l]+line_width[l]*( (uy>>8) -winSize.height-1)+ (ux>>8) -winSize.width-1),0);
            ACPY3_start(h_in);

            //copy the window around int(u+g) to cache (from image B)
            //coordinate [ux+gx - imgB_winx, ux+gx + imgB_winx] × [uy+gy - imgB_winy, uy+gy + imgB_winy]
            ACPY3_fastConfigure16b(h_out,ACPY3_PARAMFIELD_ELEMENTINDEX_SRC,line_width[l],0);
            _ACPY3_fastConfigure32b(h_out,ACPY3_PARAMFIELD_SRCADDR,(pyr_org_B[l]+line_width[l]*( ((uy+gy)>>8) - imgB_winy)+ ((ux+gx)>>8) -imgB_winx),0);
            ACPY3_start(h_out);

            ACPY3_wait(h_in);

            //interpolate pixel value
            bilinear_interp(cache_imageA_1,cache_interp_1,2*winSize.width+4,2*winSize.height+4,ux&0xff,uy&0xff);
            //calculate dx, dy, dx², dy² and dxdy
            conv_dxdy2(cache_interp_1,cache_dx,cache_dy,cache_dx_dx,cache_dy_dy,cache_dx_dy,2*winSize.width+3, 2*winSize.height+3);

            //init v
            vx=0;vy=0;

            ACPY3_wait(h_out);
            //prevoir ici la copie du nouvel element dans cache imageA
            //semble pas necessaire, tellement la copie est rapide...

            start_x_old=2*winSize.width+3;//initialisation, non realistic value to force first computation of G
            flag_lost=0;
            //iterations loop
            for(k=0;k<kmax;k++)
            {
                start_x=-winSize.width;
                stop_x=winSize.width;
                start_y=-winSize.height;
                stop_y=winSize.height;

                //check boundaries for second image
                while(start_x+((ux+gx+vx)>>8) <0) start_x++;
                while(start_y+((uy+gy+vy)>>8) <0) start_y++;
                while(stop_x+((ux+gx+vx)>>8)>width[l]-1) stop_x--;
                while(stop_y+((uy+gy+vy)>>8)>height[l]-1) stop_y--;

                //check boundaries for gradient
                while(start_x+(ux>>8) <1) start_x++;
                while(start_y+(uy>>8) <1) start_y++;
                while(stop_x+(ux>>8)>width[l]-2) stop_x--;
                while(stop_y+(uy>>8)>height[l]-2) stop_y--;

                if( (stop_x-start_x) < 3 || stop_y-start_y<3)
                {
                    flag_lost=1;
                    break;
                }
                calc_image_mismatch(cache_interp_1,cache_imageB_1,imgB_winx,imgB_winy,winSize.width,winSize.height,cache_dx,cache_dy,ux,uy,gx,gy,vx,vy,&bx,&by,start_x,stop_x,start_y,stop_y);

                if(start_x!=start_x_old || stop_x!=stop_x_old || start_y!=start_y_old || stop_y!=stop_y_old)
                {
                    //calculate G
                    Gxx=0;
                    Gyy=0;
                    Gxy=0;

                    offset_grad=(2*winSize.width+1)*winSize.height+winSize.width;
                    for(y=start_y;y<stop_y+1;y++)
                    {
                        for(x=start_x;x<stop_x+1;x++)
                        {
                            Gxx=_sadd(Gxx,cache_dx_dx[offset_grad+x+y*(2*winSize.width+1)]);
                            Gyy=_sadd(Gyy,cache_dy_dy[offset_grad+x+y*(2*winSize.width+1)]);
                            Gxy=_sadd(Gxy,cache_dx_dy[offset_grad+x+y*(2*winSize.width+1)]);
                        }
                    }

                    //verifier les saturations des Gii
                    det_G=(float)Gxx*(float)Gyy-(float)Gxy*(float)Gxy;
                   // if(det_G==0) break;
                    det_G=256/det_G;//256: comes from fixed point
                    inv_Gxx=Gyy*det_G;
                    inv_Gyy=Gxx*det_G;
                    inv_Gxy=-Gxy*det_G;

                    exp_gxx=127-_extu(_ftoi(inv_Gxx),1,24)+7;
                    mant_gxx=0x80+_extu(_ftoi(inv_Gxx),9,25);//on garde 8 chiffres significatifs
                    exp_gyy=127-_extu(_ftoi(inv_Gyy),1,24)+7;
                    mant_gyy=0x80+_extu(_ftoi(inv_Gyy),9,25);//on garde 8 chiffres significatifs
                    exp_gxy=127-_extu(_ftoi(inv_Gxy),1,24)+7;
                    mant_gxy=0x80+_extu(_ftoi(inv_Gxy),9,25);//on garde 8 chiffres significatifs

                    if(inv_Gxx<0) mant_gxx=-mant_gxx;
                    if(inv_Gyy<0) mant_gyy=-mant_gyy;
                    if(inv_Gxy<0) mant_gxy=-mant_gxy;

                    start_x_old=start_x;//a passer dans le if ci dessus
                    stop_x_old=stop_x;
                    start_y_old=start_y ;
                    stop_y_old=stop_y;
                }

                //n=(Ginv*b);
                //convert nx ny to fixed point
                //nx=(int32_t)((inv_Gxx*bx+inv_Gxy*by));
                //ny=(int32_t)((inv_Gxy*bx+inv_Gyy*by));
                #if defined OMAPL138 || defined OMAP3530
                //ce calclu passe pas sur le x86?
                nx=((mant_gxx*bx)>>exp_gxx)+((mant_gxy*by)>>exp_gxy);
                ny=((mant_gxy*bx)>>exp_gxy)+((mant_gyy*by)>>exp_gyy);
                #else
                nx=(int32_t)((inv_Gxx*bx+inv_Gxy*by));
                ny=(int32_t)((inv_Gxy*bx+inv_Gyy*by));
                #endif

                //update v
                vx=_sadd(vx,nx);
                vy=_sadd(vy,ny);

                if(nx<4 && nx>-4 && ny<4 && ny>-4) break;
                //if(nx==0 && ny==0) break;
            }

            if(flag_lost==1) break;

        }

        if(flag_lost==1)
        {
            feat_status[j]=0;
        }
        else
        {
            features_B[j].x=_sadd(gx,vx);//gx+vx;
            features_B[j].y=_sadd(gy,vy);//gy+vy;
            feat_status[j]=1;

            error[j]=calc_feature_error(cache_interp_1,cache_imageB_1,imgB_winx,imgB_winy,winSize.width,winSize.height,ux,uy,gx,gy,vx,vy,start_x,stop_x,start_y,stop_y);

        }

    }


    MEM_free(L1DSRAM, cache_imageA_1,8+(((2*winSize.width+4)*(2*winSize.height+4))>>3)<<3);
    MEM_free(L1DSRAM, cache_imageB_1,8+(((2*imgB_winx+1)*(2*imgB_winy+1))>>3)<<3);
    MEM_free(L1DSRAM, cache_interp_1,8+(((2*winSize.width+3)*(2*winSize.height+3))>>3)<<3);

    MEM_free(L1DSRAM, cache_dx, (8+(((2*winSize.width+1)*(2*winSize.height+1))>>3)<<3)*sizeof(int16_t));
    MEM_free(L1DSRAM, cache_dy, (8+(((2*winSize.width+1)*(2*winSize.height+1))>>3)<<3)*sizeof(int16_t));
    MEM_free(L1DSRAM, cache_dx_dx, (8+(((2*winSize.width+1)*(2*winSize.height+1))>>3)<<3)*sizeof(int32_t));
    MEM_free(L1DSRAM, cache_dy_dy, (8+(((2*winSize.width+1)*(2*winSize.height+1))>>3)<<3)*sizeof(int32_t));
    MEM_free(L1DSRAM, cache_dx_dy, (8+(((2*winSize.width+1)*(2*winSize.height+1))>>3)<<3)*sizeof(int32_t));

#if !defined(OMAPL138) && !defined(OMAP3530)
    pthread_mutex_unlock(&mutex);
#endif
}

//width, height of source; dst is size (width-1)*(height-1)
inline void bilinear_interp(uint8_t* src,uint8_t* dst,uint32_t width,uint32_t height,uint8_t alpha_x,uint8_t alpha_y)
{
    uint32_t i,j,k=0;
    uint32_t result;
    uint32_t k1,k2,k3,k4;

    k1=(0x100-alpha_x)*(0x100-alpha_y);
    k2=alpha_x*(256-alpha_y);
    k3=(256-alpha_x)*alpha_y;
    k4=alpha_x*alpha_y;

    for(j=0;j<height-1;j++)
    {
        for(i=0;i<width-1;i++)
        {
            result=k1*src[k]+k2*src[k+1] +k3*src[k+height]+ k4*src[k+height+1];
            *dst=(uint8_t)(result>>16);
            *dst++;
            k++;
        }
        k++;
    }
}

//calcul les dx²,dy² et dxdy
//pour LK
//on traite un mutiple de 8 pixels
//ceux en trop sont caclulés a partir de données hors de l'image...
//prevoir un buffer de taille conséquente (mutliple de 8)
//on suppose que l'image d'entrée est 9x9 (wx=wy=3)
//a changer pour rendre variable... et a optimiser...

void conv_dxdy2
(
    const uint8_t *restrict line_in,   // Input image data
    int16_t       *restrict out_dx,  // Output image data
    int16_t       *restrict out_dy,  // Output image data
    int32_t       *restrict out_dx_dx,  // Output image data
    int32_t       *restrict out_dy_dy,  // Output image data
    int32_t       *restrict out_dx_dy,  // Output image data
    int16_t cols, int16_t rows              // Image dimensions
)
{
    int32_t i,j,end;

    int32_t mult1   = 0x00fffeff;
    int32_t mult2   = 0x00010201;

    int32_t mult1_b = 0xfffeff00;
    int32_t mult2_b = 0x01020100;
    int32_t w = cols;


//revoir ces conditions!!!

//    _nassert(cols > 3);
//    _nassert(cols % 2 == 0);
//    _nassert(rows > 3);
//    _nassert(cols * (rows - 2) - 2 >= 8);


    // --------------------------------------------------------------------
    //  Iterate over entire image as a single, continuous raster line.
    // --------------------------------------------------------------------

    end        = cols*rows;
    j=0;

    for (i = 0; i <end; i+=9)
    {
        uint64_t in1_d,in1_d2,in2_d1,in2_d2,in3_d,in3_d2;
        uint32_t in1_l,in1_h,in2_l,in2_h,in3_l,in3_h;
        uint32_t in2_l2,in2_h2;

        uint32_t tmp1,tmp3,tmp2,tmp4;
        int32_t H, H3, H5, H7;
        int32_t H2, H4, H6, H8;

        uint32_t b1,b2,b3,b4,b5,b6,b7,b8;
        uint64_t b10,b11,b12,b13,b14,b15;

        // ----------------------------------------------------------------
        //  Read in the required 3x3 region from the input.
        // ----------------------------------------------------------------

        in1_d   = _mem8_const((void*)&line_in[i]);
        in1_d2  = _mem8_const((void*)&line_in[i+2]);
        tmp1    = _loll(in1_d2);
        tmp2    = _hill(in1_d2);
        in1_l   = _loll(in1_d);
        in1_h   = _hill(in1_d);

        in2_d1  = _mem8_const((void*)&line_in[i+w]);
        in2_d2  = _mem8_const((void*)&line_in[i+w+2]);
        in2_l   = _loll(in2_d1);
        in2_l2  = _hill(in2_d1);
        in2_h   = _loll(in2_d2);
        in2_h2  = _hill(in2_d2);

        in3_d   = _mem8_const((void*)&line_in[i+2*w]);
        in3_d2  = _mem8_const((void*)&line_in[i+2*w+2]);
        tmp3    = _loll(in3_d2);
        tmp4    = _hill(in3_d2);
        in3_l   = _loll(in3_d);
        in3_h   = _hill(in3_d);


        // H,H3,H5,H7 comprennent chacun le dy de 2 pixels
        H    = _add2(_pack2(_dotpsu4(mult1_b,in1_l),_dotpsu4(mult1,in1_l)),
                     _pack2(_dotpsu4(mult2_b,in3_l),_dotpsu4(mult2,in3_l)));
        H3   = _add2(_pack2(_dotpsu4(mult1_b,tmp1), _dotpsu4(mult1,tmp1)),
                     _pack2(_dotpsu4(mult2_b,tmp3), _dotpsu4(mult2,tmp3)));
        H5   = _add2(_pack2(_dotpsu4(mult1_b,in1_h),_dotpsu4(mult1,in1_h)),
                     _pack2(_dotpsu4(mult2_b,in3_h),_dotpsu4(mult2,in3_h)));
        H7   = _add2(_pack2(_dotpsu4(mult1_b,tmp2), _dotpsu4(mult1,tmp2)),
                     _pack2(_dotpsu4(mult2_b,tmp4), _dotpsu4(mult2,tmp4)));

        // H2,H4,H6,H8 comprennent chacun le dx de 2 pixels
        b10= _mpyu4ll(in2_l2,0x2020202);
        b11= _mpyu4ll(in1_h, 0x1010101);
        b12= _mpyu4ll(in2_l, 0x2020202);
        b13= _mpyu4ll(in2_h, 0x2020202);
        b14= _mpyu4ll(in2_h2,0x2020202);
        b15= _mpyu4ll(tmp2,  0x1010101);

        b1 = _add2(_add2(_unpklu4(in1_l),_loll(b12)),_unpklu4(in3_l));
        b2 = _add2(_add2(_unpkhu4(in1_l),_hill(b12)),_unpkhu4(in3_l));
        b3 = _add2(_add2(_unpklu4(tmp1), _loll(b13)),_unpklu4(tmp3));
        b4 = _add2(_add2(_unpkhu4(tmp1), _hill(b13)),_unpkhu4(tmp3));
        b5 = _add2(_add2(_loll(b11),_loll(b10)),_unpklu4(in3_h));
        b6 = _add2(_add2(_hill(b11),_hill(b10)),_unpkhu4(in3_h));
        b7 = _add2(_add2(_loll(b15),_loll(b14)),_unpklu4(tmp4));
        b8 = _add2(_add2(_hill(b15),_hill(b14)),_unpkhu4(tmp4));

        H2 = _sub2(b2,b1);
        H4 = _sub2(b4,b3);
        H6 = _sub2(b6,b5);
        H8 = _sub2(b8,b7);
#if defined OMAPL138 || defined OMAP3530
        _mem8((void *)&out_dy_dy[j]) = _mpy2ll(H,H);//effectue le carré et le met dans un double: dy1*dy1 dy2*dy2
        _mem8((void *)&out_dy_dy[j+2]) = _mpy2ll(H3,H3);
        _mem8((void *)&out_dy_dy[j+4]) = _mpy2ll(H5,H5);
        _mem8((void *)&out_dy_dy[j+6]) = _mpy2ll(H7,H7);
        _mem4((void *)&out_dy[j]) = H;
        _mem4((void *)&out_dy[j+2]) = H3;
        _mem4((void *)&out_dy[j+4]) = H5;
        _mem4((void *)&out_dy[j+6]) = H7;


        _mem8((void *)&out_dx_dx[j]) = _mpy2ll(H2,H2);//effectue le carré et le met dans un double: dx1*dx1 dx2*dx2
        _mem8((void *)&out_dx_dx[j+2]) = _mpy2ll(H4,H4);
        _mem8((void *)&out_dx_dx[j+4]) = _mpy2ll(H6,H6);
        _mem8((void *)&out_dx_dx[j+6]) = _mpy2ll(H8,H8);
        _mem4((void *)&out_dx[j]) = H2;
        _mem4((void *)&out_dx[j+2]) = H4;
        _mem4((void *)&out_dx[j+4]) = H6;
        _mem4((void *)&out_dx[j+6]) = H8;//le dernier pixel n'est pas significatif, il sera ecrasé a la prochaine itération

        _mem8((void *)&out_dx_dy[j]) = _mpy2ll(H,H2);//effectue le produit et le met dans un double: dx1*dy1 dx2*dy2
        _mem8((void *)&out_dx_dy[j+2]) = _mpy2ll(H3,H4);
        _mem8((void *)&out_dx_dy[j+4]) = _mpy2ll(H5,H6);
        _mem8((void *)&out_dx_dy[j+6]) = _mpy2ll(H7,H8);
#else
        int64_t toto;
        toto=_mpy2ll(H,H);
        memcpy(&out_dy_dy[j],&toto,8);
        toto=_mpy2ll(H3,H3);
        memcpy(&out_dy_dy[j+2],&toto,8);
        toto=_mpy2ll(H5,H5);
        memcpy(&out_dy_dy[j+4],&toto,8);
        toto=_mpy2ll(H7,H7);
        memcpy(&out_dy_dy[j+6],&toto,8);
        memcpy(&out_dy[j],&H,4);
        memcpy(&out_dy[j+2],&H3,4);
        memcpy(&out_dy[j+4],&H5,4);
        memcpy(&out_dy[j+6],&H7,4);

        toto=_mpy2ll(H2,H2);
        memcpy(&out_dx_dx[j],&toto,8);
        toto=_mpy2ll(H4,H4);
        memcpy(&out_dx_dx[j+2],&toto,8);
        toto=_mpy2ll(H6,H6);
        memcpy(&out_dx_dx[j+4],&toto,8);
        toto=_mpy2ll(H8,H8);
        memcpy(&out_dx_dx[j+6],&toto,8);
        memcpy(&out_dx[j],&H2,4);
        memcpy(&out_dx[j+2],&H4,4);
        memcpy(&out_dx[j+4],&H6,4);
        memcpy(&out_dx[j+6],&H8,4);

        toto=_mpy2ll(H,H2);
        memcpy(&out_dx_dy[j],&toto,8);
        toto=_mpy2ll(H3,H4);
        memcpy(&out_dx_dy[j+2],&toto,8);
        toto=_mpy2ll(H5,H6);
        memcpy(&out_dx_dy[j+4],&toto,8);
        toto=_mpy2ll(H7,H8);
        memcpy(&out_dx_dy[j+6],&toto,8);
#endif
        j+=7;
    }
}


inline void calc_image_mismatch(uint8_t* imgA_interp,uint8_t* imgB,int32_t imgB_winx,int32_t imgB_winy,
                                uint8_t winx,uint8_t winy,
                                int16_t* cache_dx,int16_t* cache_dy,
                                int32_t ux,int32_t uy,int32_t gx,int32_t gy,int32_t vx,int32_t vy,int32_t* bx,int32_t* by,
                                int32_t start_x,int32_t stop_x,int32_t start_y,int32_t stop_y)
{
    int32_t x,y;
    uint32_t result;
    int32_t delta_I;
    int32_t depl_x,depl_y;
    uint8_t alpha_x,alpha_y;
    uint32_t k1,k2,k3,k4;
    int32_t offset_A,offset_B,offset_grad;


    *bx=0;
    *by=0;
    alpha_x=(ux+gx+vx)&0xff;
    alpha_y=(uy+gy+vy)&0xff;

    //deplacement en pixels entiers de u+g+v par rapport à u+g (ref imgB)
    depl_x=((ux+gx+vx)>>8)-((ux+gx)>>8);
    depl_y=((uy+gy+vy)>>8)-((uy+gy)>>8);


    k1=(0x100-alpha_x)*(0x100-alpha_y);
    k2=alpha_x*(256-alpha_y);
    k3=(256-alpha_x)*alpha_y;
    k4=alpha_x*alpha_y;

    offset_A=(2*winx+3)*(winy+1)+winx+1;//imgA is (2*winx+3)(2*winy+3), u centered
    //offset_B=(2*winx+4)*(winy+1)+winx+1+depl_x+depl_y*imgB_width;
    offset_B=(2*imgB_winx+1)*(imgB_winy)+imgB_winx+depl_x+depl_y*(2*imgB_winx+1);//imgB is [ux+gx - imgB_winx, ux+gx + imgB_winx] × [uy+gy - imgB_winy, uy+gy + imgB_winy]
    offset_grad=(2*winx+1)*winy+winx;//grad is (2*winx+1)(2*winy+1), u centered

    for(y=start_y;y<stop_y+1;y++)
    {
        for(x=start_x;x<stop_x+1;x++)
        {
            result=k1*imgB[offset_B+x+y*(2*imgB_winx+1)]+k2*imgB[offset_B+x+1+y*(2*imgB_winx+1)] +k3*imgB[offset_B+x+(y+1)*(2*imgB_winx+1)]+ k4*imgB[offset_B+x+1+(y+1)*(2*imgB_winx+1)];
            delta_I=imgA_interp[offset_A+x+y*(2*winx+3)]-(uint8_t)(result>>16);
            (*bx)+=delta_I*cache_dx[offset_grad+x+y*(2*winx+1)];
            (*by)+=delta_I*cache_dy[offset_grad+x+y*(2*winx+1)];
        }
    }
}



inline uint32_t calc_feature_error(uint8_t* imgA_interp,uint8_t* imgB,int32_t imgB_winx,int32_t imgB_winy,
                                uint8_t winx,uint8_t winy,
                                int32_t ux,int32_t uy,int32_t gx,int32_t gy,int32_t vx,int32_t vy,
                                int32_t start_x,int32_t stop_x,int32_t start_y,int32_t stop_y)
{
    int32_t x,y;
    uint32_t result;
    int32_t delta_I;
    int32_t depl_x,depl_y;
    uint8_t alpha_x,alpha_y;
    uint32_t k1,k2,k3,k4;
    int32_t offset_A,offset_B;
    uint32_t error=0;

    alpha_x=(ux+gx+vx)&0xff;
    alpha_y=(uy+gy+vy)&0xff;

    //deplacement en pixels entiers de u+g+v par rapport à u+g (ref imgB)
    depl_x=((ux+gx+vx)>>8)-((ux+gx)>>8);
    depl_y=((uy+gy+vy)>>8)-((uy+gy)>>8);

    k1=(0x100-alpha_x)*(0x100-alpha_y);
    k2=alpha_x*(256-alpha_y);
    k3=(256-alpha_x)*alpha_y;
    k4=alpha_x*alpha_y;

    offset_A=(2*winx+3)*(winy+1)+winx+1;//imgA is (2*winx+3)(2*winy+3), u centered
    offset_B=(2*imgB_winx+1)*(imgB_winy)+imgB_winx+depl_x+depl_y*(2*imgB_winx+1);//imgB is [ux+gx - imgB_winx, ux+gx + imgB_winx] × [uy+gy - imgB_winy, uy+gy + imgB_winy]

    for(y=start_y;y<stop_y+1;y++)
    {
        for(x=start_x;x<stop_x+1;x++)
        {
            result=k1*imgB[offset_B+x+y*(2*imgB_winx+1)]+k2*imgB[offset_B+x+1+y*(2*imgB_winx+1)] +k3*imgB[offset_B+x+(y+1)*(2*imgB_winx+1)]+ k4*imgB[offset_B+x+1+(y+1)*(2*imgB_winx+1)];
            delta_I=imgA_interp[offset_A+x+y*(2*winx+3)]-(uint8_t)(result>>16);
            error+=delta_I*delta_I;
        }
    }

    return error;
}
