//  created:    2012/01/18
//  filename:   featureselect.c
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 6599
//
//  version:    $Id: $
//
//  purpose:    detection Harris
//
//
/*********************************************************************/

#include "featureselect.h"
#include <stdlib.h>

#if defined OMAPL138 || defined OMAP3530
//#include <xdc/std.h>
#include <std.h>
#include <mem.h>
#include <ti/xdais/idma3.h>
#include <ti/sdo/fc/acpy3/acpy3.h>
#define _ACPY3_fastConfigure32b(handle,type,addr,id) ACPY3_fastConfigure32b(handle,type,(uint32_t)addr,id)
//faire un header
extern void _printf(const char * format, ...);
#else
#include <string.h>
#include "C64intrins.h"
#include "wrappers.h"
#define _ACPY3_fastConfigure32b(handle,type,addr,id) ACPY3_fastConfigure_ptr(handle,type,(void*)addr,id)
extern pthread_mutex_t mutex;
#endif

#include "opencv_common.h"


#ifdef OMAPL138
extern int32_t DDR;
extern int32_t IRAM;
extern int32_t L1DSRAM;
extern int32_t CACHE_L1D;
#define BUF_SEG_ID IRAM
//#define BUF_SEG_ID L1DSRAM
#define BUF_ALIGN 128
#endif


#ifdef OMAP3530
extern int32_t DDR2;
extern int32_t L1DSRAM;
#define BUF_ALIGN 128
#endif

#define BUFF_SIZE (1024*39) //doit etre un multiple de 64

extern IDMA3_Handle h_in,h_out;

void dspGoodFeaturesToTrack(IplImage* img_src,CvPoint* features,uint32_t* count,float quality_level, float min_distance)
{
    ACPY3_Params p;
    uint32_t tmp_count,total_count=0;
    int32_t i,j,feat_wr=0;
    int32_t tmp,tmp2,tmp3;
    uint32_t max=0;
    uint32_t max_count=*count;
    uint8_t* data_src=(uint8_t*)img_src->imageData;

    uint32_t* data_dst;
    uint32_t* tmp_dst;
    uint32_t* data_src_sort;
    uint32_t* data_dst_sort;
    uint32_t min_distance_carre=(uint32_t)(min_distance*min_distance);

    uint32_t* restrict cache_in_dist;
    uint8_t* restrict cache_in;
    int32_t* line_1;
    int32_t* line_2;
    int32_t* line_3;
    int32_t* line_tmp;
    uint32_t* restrict cache_out;
    uint32_t* restrict cache_in_sort;
    uint32_t* restrict cache_in_sort_old;
    uint32_t* cache_tmp;

    uint32_t width,height;

#if !defined(OMAPL138) && !defined(OMAP3530)
    pthread_mutex_lock(&mutex);
#endif

    if(img_src->roi!=NULL) {
        width=img_src->roi->width;
        height=img_src->roi->height;
    } else {
        width=img_src->width;
        height=img_src->height;
    }

    data_dst=(uint32_t*)MEM_alloc(DDR2, width*height*sizeof(uint32_t), BUF_ALIGN);//buffer a cacher avec setmar? et enlever les copies dma...
    tmp_dst=data_dst;
    data_src_sort=data_dst;
    if(data_dst==NULL) {
        _printf("erreur allocation DDR2 dspGoodFeaturesToTrack, taille %lu\n",width*height*sizeof(uint32_t));
    }

//_printf("dst %x %i\n",data_dst,width*height*sizeof(uint32_t));
    cache_in=(uint8_t*)MEM_alloc(L1DSRAM, 3*width, BUF_ALIGN);
    line_1=(int32_t*)MEM_alloc(L1DSRAM, 3*width*sizeof(int32_t), BUF_ALIGN);
    line_2=(int32_t*)MEM_alloc(L1DSRAM, 3*width*sizeof(int32_t), BUF_ALIGN);
    line_3=(int32_t*)MEM_alloc(L1DSRAM, 3*width*sizeof(int32_t), BUF_ALIGN);
    cache_out=(uint32_t*)MEM_alloc(L1DSRAM, width*sizeof(uint32_t), BUF_ALIGN);

    if(cache_in==NULL || line_1==NULL || line_2==NULL || line_3==NULL || cache_out==NULL) {
        _printf("erreur allocation L1DSRAM  dspGoodFeaturesToTrack\n");
    }

    if(img_src->roi!=NULL) {
        data_src+=img_src->roi->xOffset+img_src->roi->yOffset*img_src->width;

        p.transferType = ACPY3_2D1D;
        p.srcAddr = (void *)cache_out;//configure h_out
        p.dstAddr = (void *)cache_in;//configure h_in
        p.elementSize = width;//type uint8_t
        p.srcElementIndex = img_src->width;
        p.dstElementIndex = 1;
        p.numElements = 3;
        p.numFrames = 1;
        p.waitId = 0;// waitId of 0 implies wait after the first transfer
        ACPY3_configure(h_in, &p, 0);


        p.transferType = ACPY3_1D1D;
        p.srcAddr = (void *)cache_out;//configure h_out
        p.dstAddr = (void *)cache_in;//configure h_in
        p.elementSize = sizeof(int32_t)*(width-4);
        p.srcElementIndex = 1;
        p.dstElementIndex = 1;
        p.numElements = 1;
        p.numFrames = 1;
        p.waitId = 0;// waitId of 0 implies wait after the first transfer
        ACPY3_configure(h_out, &p, 0);
    } else {
        p.transferType = ACPY3_1D1D;
        p.srcAddr = (void *)cache_out;//configure h_out
        p.dstAddr = (void *)cache_in;//configure h_in
        p.elementSize = 3*width;//type uint8_t
        p.srcElementIndex = 1;
        p.dstElementIndex = 1;
        p.numElements = 1;
        p.numFrames = 1;
        p.waitId = 0;// waitId of 0 implies wait after the first transfer
        ACPY3_configure(h_in, &p, 0);
        ACPY3_configure(h_out, &p, 0);
        ACPY3_fastConfigure16b(h_out,ACPY3_PARAMFIELD_ELEMENTSIZE,sizeof(int32_t)*(width-4),0);//type int32_t
    }

//boucle a revoir avec + de 1 lignes a chaque fois
//revoir le convdx_dy pour qu'il gere + de 1 ligne
    for(i=0;i<height-1;i++) {
        //on prepare la nouvelle adresse
        _ACPY3_fastConfigure32b(h_in,ACPY3_PARAMFIELD_SRCADDR,data_src,0);

        //if(i>0)
        //le conv dx dy pourrait aussi prendre 3 lignes en entrees
        //sauf si on fait comme le sobel
        conv_dxdy(cache_in,line_3,width,3);

        //on peut de nouveau remplir cache_in
        ACPY3_start(h_in);
        data_src+=img_src->width;

        if(i>2) {//il faut 3 lignes de dx,dy,dxdy pour calculer le gradient
            ACPY3_wait(h_out);
            calc_gradient(line_1,line_2,line_3,cache_out,width,&max);
            _ACPY3_fastConfigure32b(h_out,ACPY3_PARAMFIELD_DSTADDR,data_dst,0);
            ACPY3_start(h_out);
            data_dst+=width-4;
        }

        //permutation circulaire
        line_tmp=line_1;
        line_1=line_2;
        line_2=line_3;
        line_3=line_tmp;

        //attend que les données soient pretes dans cache in
        ACPY3_wait(h_in);
    }

    //derniers transferts
    ACPY3_wait(h_out);

    MEM_free(L1DSRAM, cache_in,3*width);
    MEM_free(L1DSRAM, line_1,3*width*sizeof(int32_t));
    MEM_free(L1DSRAM, line_2,3*width*sizeof(int32_t));
    MEM_free(L1DSRAM, line_3,3*width*sizeof(int32_t));
    MEM_free(L1DSRAM, cache_out,width*sizeof(uint32_t));

    //traitement des valeurs propres
    //on garde que celles superieures à un certain seuil
    cache_in_sort=(uint32_t*)MEM_alloc(L1DSRAM, (width-4)*sizeof(uint32_t), BUF_ALIGN);
    cache_in_sort_old=(uint32_t*)MEM_alloc(L1DSRAM, (width-4)*sizeof(uint32_t), BUF_ALIGN);
    cache_in_dist=(uint32_t*)MEM_alloc(L1DSRAM,BUFF_SIZE-(width-4)*sizeof(uint32_t)*2, BUF_ALIGN);
    data_dst_sort=cache_in_dist;

    if(cache_in_sort==NULL || cache_in_sort_old==NULL || cache_in_dist==NULL)//|| cache_out_sort==NULL || cache_out_sort_old==NULL)
    {
        _printf("erreur allocation dspGoodFeaturesToTrack\n");
    }

    max=max*quality_level;

    if(img_src->roi!=NULL) {
        p.transferType = ACPY3_1D1D;
        p.srcAddr = (void *)cache_out;//configure h_out
        p.dstAddr = (void *)cache_in;//configure h_in
        p.elementSize = sizeof(int32_t)*(width-4);
        p.srcElementIndex = 1;
        p.dstElementIndex = 1;
        p.numElements = 1;
        p.numFrames = 1;
        p.waitId = 0;// waitId of 0 implies wait after the first transfer
        ACPY3_configure(h_in, &p, 0);
    } else {
        ACPY3_fastConfigure16b(h_in,ACPY3_PARAMFIELD_ELEMENTSIZE,sizeof(uint32_t)*(width-4),0);
    }

    //les 2 premieres iterations ne sortent rien (+2)
    //mais 4 lignes ne sont pas traites a cause du gradient: +2-4=-2
    for(i=0;i<height-2;i++)
    {
        //cache_out_old > dst	trait cache_in > cache_out  src > cache_in_old
        if(i<height-4)
        {
            _ACPY3_fastConfigure32b(h_in,ACPY3_PARAMFIELD_SRCADDR,data_src_sort,0);
            _ACPY3_fastConfigure32b(h_in,ACPY3_PARAMFIELD_DSTADDR,cache_in_sort_old,0);
            ACPY3_start(h_in);
            data_src_sort+=(width-4);
        }

        //traitement non necessaire quand i=0 et à la fin
        if((i<height-3) && (i>0))
        {
            //select_high_eig_values(cache_in_sort, cache_out_sort,i-1,&tmp_count,max,width);//changer offset
            select_high_eig_values(cache_in_sort, data_dst_sort,i-1,&tmp_count,max,width);//changer offset
            total_count+=tmp_count;
            data_dst_sort+=2*tmp_count;//rajout
            if(2*(total_count+width)*sizeof(uint32_t)>(BUFF_SIZE-(width-4)*sizeof(uint32_t)*2))
            {
                //_printf("trop de features\n");
                ACPY3_wait(h_in);
                break;
            }
        }

        ACPY3_wait(h_in);

        //permutation circulaire
        cache_tmp=cache_in_sort;
        cache_in_sort=cache_in_sort_old;
        cache_in_sort_old=cache_tmp;
    }

    MEM_free(L1DSRAM, cache_in_sort,(width-4)*sizeof(uint32_t));
    MEM_free(L1DSRAM, cache_in_sort_old,(width-4)*sizeof(uint32_t));

    //revoir cette restriction: et tenter de partager le quicksort si trop de features
    //a faire dans l'etape d'avant: si trop de features, les mettre dans une zone en bcache
    if(sizeof(uint32_t)*total_count*2>BUFF_SIZE) {
        total_count=BUFF_SIZE/(2*sizeof(uint32_t));
        //_printf("encore trop de features\n");
    }

    _quicksort(cache_in_dist,total_count);
    //partie a optimiser, on peut tous laisser dans la cache si ca tient?
    //pas la peine de faire la copie 2d1D par la suite, et tenir compte pour le calcul des distances...
    ACPY3_fastConfigure16b(h_in,ACPY3_PARAMFIELD_ELEMENTSIZE,sizeof(uint32_t)*total_count*2,0);
    _ACPY3_fastConfigure32b(h_in,ACPY3_PARAMFIELD_SRCADDR,cache_in_dist,0);
    _ACPY3_fastConfigure32b(h_in,ACPY3_PARAMFIELD_DSTADDR,tmp_dst,0);
    ACPY3_start(h_in);
    ACPY3_wait(h_in);

    if(total_count!=0)
    {
        //minimum distance
        data_src_sort=tmp_dst;

        if(max_count>MAX_POINTS_TO_TRACK) max_count=MAX_POINTS_TO_TRACK;

        p.transferType = ACPY3_2D1D;//enleve les val propres
        p.srcAddr = (void *)data_src_sort;
        p.elementSize = sizeof(uint32_t);
        p.srcElementIndex = 2*sizeof(uint32_t);
        p.waitId = 0;// waitId of 0 implies wait after the first transfer
        p.dstAddr =(void *)cache_in_dist;
        p.numElements = total_count;
        ACPY3_configure(h_in, &p, 0);

        //cache_in_dist=sort_buffer;

        //ACPY3_fastConfigure32b(h_in,ACPY3_PARAMFIELD_DSTADDR,(uint32_t)cache_in_dist,0);
        //ACPY3_fastConfigure16b(h_in,ACPY3_PARAMFIELD_NUMELEMENTS,total_count,0);

        ACPY3_start(h_in);
        ACPY3_wait(h_in);

        for(i=0;i<total_count;i++)
        {
            tmp=cache_in_dist[i];//tmp =x<<16+y
            if(tmp!=0)
            {
                for(j=i+1;j<total_count;j++)
                {
                    tmp3=cache_in_dist[j];
                    if(tmp3!=0)
                    {
                        tmp2=_sub2(tmp3,tmp);
                        if(_dotp2(tmp2,tmp2)<min_distance_carre) cache_in_dist[j]=0;
                    }
                }
                features[feat_wr].x=_packlh2(0,tmp);
                features[feat_wr].y=_packhl2(0,tmp);
                feat_wr++;
                if(feat_wr==max_count) break;
            }
        }
    }
    *count=feat_wr;

    MEM_free(L1DSRAM, cache_in_dist,BUFF_SIZE-(width-4)*sizeof(uint32_t)*2);
    MEM_free(DDR2,tmp_dst,width*height*sizeof(uint32_t));

#if !defined(OMAPL138) && !defined(OMAP3530)
    pthread_mutex_unlock(&mutex);
#endif
}


//calcul les dx²,dy² et dxdy
void conv_dxdy
(
    const uint8_t *restrict line_in,   // Input image data
    int32_t       *restrict out,  // Output data
    int16_t cols, int16_t rows              // Image dimensions
)
{
    int32_t i,end,final_pass;
    int32_t w = cols;
    int32_t mult2   = 0x00fffeff;
    int32_t mult1   = 0x00010201;

    int32_t mult2_b = 0xfffeff00;
    int32_t mult1_b = 0x01020100;

    _nassert(cols > 3);
    _nassert(cols % 2 == 0);
    _nassert(rows > 3);
    _nassert(cols * (rows - 2) - 2 >= 8);

    // --------------------------------------------------------------------
    //  Iterate over entire image as a single, continuous raster line.
    // --------------------------------------------------------------------

    end        = (cols*(rows-2) - 2);
    final_pass = end - 6;

    for (i = 0; i < end; i+=8)
    {
        uint64_t in1_d,in1_d2,in2_d1,in2_d2,in3_d,in3_d2;
        uint32_t in1_l,in1_h,in2_l,in2_h,in3_l,in3_h;
        uint32_t in2_l2,in2_h2;

        uint32_t tmp1,tmp3,tmp2,tmp4;
        int32_t H, H3, H5, H7;
        int32_t H2, H4, H6, H8;

        uint32_t b1,b2,b3,b4,b5,b6,b7,b8;
        uint64_t b10,b11,b12,b13,b14,b15;

        // ----------------------------------------------------------------
        //  Read in the required 3x3 region from the input.
        // ----------------------------------------------------------------

        in1_d   = _amem8_const((void*)&line_in[i]);
        in1_d2  = _mem8_const((void*)&line_in[i+2]);
        tmp1    = _loll(in1_d2);
        tmp2    = _hill(in1_d2);
        in1_l   = _loll(in1_d);
        in1_h   = _hill(in1_d);

        in2_d1  = _amem8_const((void*)&line_in[i+w]);
        in2_d2  = _mem8_const((void*)&line_in[i+w+2]);
        in2_l   = _loll(in2_d1);
        in2_l2  = _hill(in2_d1);
        in2_h   = _loll(in2_d2);
        in2_h2  = _hill(in2_d2);

        in3_d   = _amem8_const((void*)&line_in[i+2*w]);
        in3_d2  = _mem8_const((void*)&line_in[i+2*w+2]);
        tmp3    = _loll(in3_d2);
        tmp4    = _hill(in3_d2);
        in3_l   = _loll(in3_d);
        in3_h   = _hill(in3_d);


        // H,H3,H5,H7 comprennent chacun le dy de 2 pixels
        H    = _add2(_pack2(_dotpsu4(mult1_b,in1_l),_dotpsu4(mult1,in1_l)),
                     _pack2(_dotpsu4(mult2_b,in3_l),_dotpsu4(mult2,in3_l)));
        H3   = _add2(_pack2(_dotpsu4(mult1_b,tmp1), _dotpsu4(mult1,tmp1)),
                     _pack2(_dotpsu4(mult2_b,tmp3), _dotpsu4(mult2,tmp3)));
        H5   = _add2(_pack2(_dotpsu4(mult1_b,in1_h),_dotpsu4(mult1,in1_h)),
                     _pack2(_dotpsu4(mult2_b,in3_h),_dotpsu4(mult2,in3_h)));
        H7   = _add2(_pack2(_dotpsu4(mult1_b,tmp2), _dotpsu4(mult1,tmp2)),
                     _pack2(_dotpsu4(mult2_b,tmp4), _dotpsu4(mult2,tmp4)));

        // H2,H4,H6,H8 comprennent chacun le dx de 2 pixels

        b10= _mpyu4ll(in2_l2,0x2020202);
        b11= _mpyu4ll(in1_h, 0x1010101);
        b12= _mpyu4ll(in2_l, 0x2020202);
        b13= _mpyu4ll(in2_h, 0x2020202);
        b14= _mpyu4ll(in2_h2,0x2020202);
        b15= _mpyu4ll(tmp2,  0x1010101);

        b1 = _add2(_add2(_unpklu4(in1_l),_loll(b12)),_unpklu4(in3_l));
        b2 = _add2(_add2(_unpkhu4(in1_l),_hill(b12)),_unpkhu4(in3_l));
        b3 = _add2(_add2(_unpklu4(tmp1), _loll(b13)),_unpklu4(tmp3));
        b4 = _add2(_add2(_unpkhu4(tmp1), _hill(b13)),_unpkhu4(tmp3));
        b5 = _add2(_add2(_loll(b11),_loll(b10)),_unpklu4(in3_h));
        b6 = _add2(_add2(_hill(b11),_hill(b10)),_unpkhu4(in3_h));
        b7 = _add2(_add2(_loll(b15),_loll(b14)),_unpklu4(tmp4));
        b8 = _add2(_add2(_hill(b15),_hill(b14)),_unpkhu4(tmp4));

        H2 = _sub2(b2,b1);
        H4 = _sub2(b4,b3);
        H6 = _sub2(b6,b5);
        H8 = _sub2(b8,b7);
#if defined OMAPL138 || defined OMAP3530
        _amem8((void *)&out[w+i]) =  _mpy2ll(H,H);//effectue le carré et le met dans un double: dy1*dy1 dy2*dy2
        _amem8((void *)&out[w+i+2]) = _mpy2ll(H3,H3);
        _amem8((void *)&out[w+i+4]) = _mpy2ll(H5,H5);

        _amem8((void *)&out[i]) = _mpy2ll(H2,H2);//effectue le carré et le met dans un double: dx1*dx1 dx2*dx2
        _amem8((void *)&out[i+2]) = _mpy2ll(H4,H4);
        _amem8((void *)&out[i+4]) = _mpy2ll(H6,H6);

        _amem8((void *)&out[2*w+i]) = _mpy2ll(H,H2);//effectue le produit et le met dans un double: dx1*dy1 dx2*dy2
        _amem8((void *)&out[2*w+i+2]) = _mpy2ll(H3,H4);
        _amem8((void *)&out[2*w+i+4]) = _mpy2ll(H5,H6);
#else
        int64_t toto;
        toto=_mpy2ll(H,H);
        memcpy(&out[w+i],&toto,8);
        toto=_mpy2ll(H3,H3);
        memcpy(&out[w+i+2],&toto,8);
        toto=_mpy2ll(H5,H5);
        memcpy(&out[w+i+4],&toto,8);

        toto=_mpy2ll(H2,H2);
        memcpy(&out[i],&toto,8);
        toto=_mpy2ll(H4,H4);
        memcpy(&out[i+2],&toto,8);
        toto=_mpy2ll(H6,H6);
        memcpy(&out[i+4],&toto,8);

        toto=_mpy2ll(H,H2);
        memcpy(&out[2*w+i],&toto,8);
        toto=_mpy2ll(H3,H4);
        memcpy(&out[2*w+i+2],&toto,8);
        toto=_mpy2ll(H5,H6);
        memcpy(&out[2*w+i+4],&toto,8);
#endif

        if(i!=final_pass) // the last 6 outputs must be stored seperately
        {
#if defined OMAPL138 || defined OMAP3530
          _amem8((void *)&out[w+i+6]) = _mpy2ll(H7,H7);//effectue le carré et le met dans un double: dy1*dy1 dy2*dy2
          _amem8((void *)&out[i+6]) = _mpy2ll(H8,H8);//effectue le carré et le met dans un double: dx1*dx1 dx2*dx2
          _amem8((void *)&out[2*w+i+6]) = _mpy2ll(H7,H8);//effectue le produit et le met dans un double: dx1*dy1 dx2*dy2
#else
        toto=_mpy2ll(H7,H7);
        memcpy(&out[w+i+6],&toto,8);
        toto=_mpy2ll(H8,H8);
        memcpy(&out[i+6],&toto,8);
        toto=_mpy2ll(H7,H8);
        memcpy(&out[2*w+i+6],&toto,8);
#endif
        }
    }

}

void calc_gradient(int32_t* restrict line_1,int32_t* restrict line_2,int32_t* restrict line_3,uint32_t* restrict cache_out,const int32_t width,uint32_t * max)
{
    int32_t i;
    int32_t s_dx[2],s_dy[2],s_dx_dy[2];
    int32_t s_dx_tmp,s_dy_tmp,s_dx_dy_tmp;
    int64_t tmp4,tmp5;
    uint32_t current_max=*max;
    uint32_t result;

    int32_t* cache_dx=line_1;
    int32_t* cache_dy=cache_dx+width;
    int32_t* cache_dx_dy=cache_dy+width;
    int32_t* cache_dx_1=line_2;
    int32_t* cache_dy_1=cache_dx_1+width;
    int32_t* cache_dx_dy_1=cache_dy_1+width;
    int32_t* cache_dx_2=line_3;
    int32_t* cache_dy_2=cache_dx_2+width;
    int32_t* cache_dx_dy_2=cache_dy_2+width;


    //on somme les dx,dy,dx_dy
    //cache_dx,dy,dx_dy sont décallés de 1 à cause de la convolution (premier et dernier pixels ne sont pas significatifs)
    //tester _sadd()
    //initalisation
    //s_dx, s_dy,s_dx_dy sont les sommes verticales
    for(i=0;i<2;i++)
    {
        s_dx[i]=_sadd(_sadd(cache_dx[i+0],cache_dx_1[i+0]),cache_dx_2[i+0]);
        s_dy[i]=_sadd(_sadd(cache_dy[i+0],cache_dy_1[i+0]),cache_dy_2[i+0]);
        s_dx_dy[i]=_sadd(_sadd(cache_dx_dy[i+0],cache_dx_dy_1[i+0]),cache_dx_dy_2[i+0]);
    }


    //les sommes sont indépendantes->optimisable??? (sinon 1 seul s_tmp suffit)
    //_nassert(width % 2 == 0);
    //#pragma MUST_ITERATE(, , 2)
    for(i=0;i<width-4;i+=2)
    {

        s_dx_tmp=_sadd(s_dx[0],s_dx[1]);
        s_dx[0]=_sadd(_sadd(cache_dx[i+2],cache_dx_1[i+2]),cache_dx_2[i+2]);//i+2
        s_dx_tmp+=s_dx[0];

        s_dy_tmp=_sadd(s_dy[0],s_dy[1]);
        s_dy[0]=_sadd(_sadd(cache_dy[i+2],cache_dy_1[i+2]),cache_dy_2[i+2]);//i+2
        s_dy_tmp+=s_dy[0];

        s_dx_dy_tmp=_sadd(s_dx_dy[0],s_dx_dy[1]);
        s_dx_dy[0]=_sadd(_sadd(cache_dx_dy[i+2],cache_dx_dy_1[i+2]),cache_dx_dy_2[i+2]);//i+2
        s_dx_dy_tmp+=s_dx_dy[0];

        s_dx_tmp=(s_dx_tmp>>4);
        s_dy_tmp=(s_dy_tmp>>4);
        s_dx_dy_tmp=(s_dx_dy_tmp>>4);

        tmp4=_mpy32ll(s_dx_tmp,s_dy_tmp);
        tmp5=_mpy32ll(s_dx_dy_tmp,s_dx_dy_tmp);
        tmp4=tmp4-tmp5;//det A
        tmp5=_mpy32ll(_sadd(s_dx_tmp,s_dy_tmp),_sadd(s_dx_tmp,s_dy_tmp));//trace A
        tmp5=(tmp5>>4);
        tmp4=tmp4-tmp5;

        result=(uint32_t)tmp4;
        if(tmp4<0) result=0;
        if(tmp4>0xffffffff) result=0xffffffff;

        cache_out[i]=result;

        if(result>current_max) current_max=result;

        s_dx_tmp=_sadd(s_dx[0],s_dx[1]);
        s_dx[1]=_sadd(_sadd(cache_dx[i+3],cache_dx_1[i+3]),cache_dx_2[i+3]);//i+3
        s_dx_tmp+=s_dx[1];

        s_dy_tmp=_sadd(s_dy[0],s_dy[1]);
        s_dy[1]=_sadd(_sadd(cache_dy[i+3],cache_dy_1[i+3]),cache_dy_2[i+3]);//i+3
        s_dy_tmp+=s_dy[1];

        s_dx_dy_tmp=_sadd(s_dx_dy[0],s_dx_dy[1]);
        s_dx_dy[1]=cache_dx_dy[i+3]+cache_dx_dy_1[i+3]+cache_dx_dy_2[i+3];//i+3
        s_dx_dy_tmp+=s_dx_dy[1];

        s_dx_tmp=(s_dx_tmp>>4);
        s_dy_tmp=(s_dy_tmp>>4);
        s_dx_dy_tmp=(s_dx_dy_tmp>>4);

        tmp4=_mpy32ll(s_dx_tmp,s_dy_tmp);
        tmp5=_mpy32ll(s_dx_dy_tmp,s_dx_dy_tmp);
        tmp4=tmp4-tmp5;
        //tmp5=(s_dx_tmp+s_dy_tmp)*(s_dx_tmp+s_dy_tmp);
        tmp5=_mpy32ll(_sadd(s_dx_tmp,s_dy_tmp),_sadd(s_dx_tmp,s_dy_tmp));
        tmp5=(tmp5>>4);
        tmp4=tmp4-tmp5;

        result=(uint32_t)tmp4;
        if(tmp4<0) result=0;
        if(tmp4>0xffffffff) result=0xffffffff;

        cache_out[i+1]= result;

        if(result>current_max) current_max=result;

    }
    *max=current_max;
}

void select_high_eig_values(uint32_t* restrict in,uint32_t* restrict out,uint32_t offset,uint32_t* count,uint32_t max,int32_t width)
{
    uint32_t i,total_count=0;
    width-=4;

    _nassert(width%4==0);
    for(i=0;i<width;i++)
    {
        if(in[i]>max)
        {
            #if defined OMAPL138 || defined OMAP3530
            _amem8((void *)&out[2*total_count]) = _itoll(in[i],_pack2(i+2,offset+2));
            #else
            int64_t toto=_itoll(in[i],_pack2(i+2,offset+2));
            memcpy(&out[2*total_count],&toto,8);
            #endif
            total_count++;
        }
    }

    *count=total_count;

}

#if defined OMAPL138 || defined OMAP3530
#define SWAP2(list, i, j)               \
{register uint32_t *pi, *pj;            \
register uint64_t tmp;\
     pi=list+2*(i); pj=list+2*(j);      \
                                        \
     tmp=_mem8_const(pi);    \
     _mem8(pi) = _mem8_const(pj);\
   _mem8(pj) = tmp;\
}
#else
#define SWAP2(list, i, j)               \
{ uint32_t *pi, *pj;            \
 uint64_t tmp;\
     pi=list+2*(i); pj=list+2*(j);      \
                                        \
     tmp=_mem8_const(pi);    \
     memcpy(pi,pj,8);\
     memcpy(pj,&tmp,8);\
}
#endif

void _quicksort(uint32_t *pointlist, int32_t n)
{
  uint32_t i, j, ln, rn;

  while (n > 1)
  {
    SWAP2(pointlist, 0, n/2);
    for (i = 0, j = n; ; )
    {
      do
        --j;
      while (pointlist[2*j+1] < pointlist[1]);
      do
        ++i;
      while (i < j && pointlist[2*i+1] > pointlist[1]);
      if (i >= j)
        break;
      SWAP2(pointlist, i, j);
    }
    SWAP2(pointlist, j, 0);
    ln = j;
    rn = n - ++j;
    if (ln < rn)
    {
      _quicksort(pointlist, ln);
      pointlist += 2*j;
      n = rn;
    }
    else
    {
      _quicksort(pointlist + 2*j, rn);
      n = ln;
    }
  }
}
#undef SWAP2
