//  created:    2012/01/18
//  filename:   featureselect.h
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 6599
//
//  version:    $Id: $
//
//  purpose:    detection Harris
//
//
/*********************************************************************/

#ifndef FEATURESELECT_H
#define FEATURESELECT_H

#include "cxtypes.h"
#include <stdint.h>

#if defined OMAPL138 || defined OMAP3530

#else
#include "typedef.h"//pour const et restrict, a deplacer
#endif

void dspGoodFeaturesToTrack(IplImage* img_src,CvPoint* features,uint32_t* count,float quality_level, float min_distance);
//a mettre en inline?
void conv_dxdy
(
    const uint8_t *restrict line_in,   // Input image data
    int32_t       *restrict out,  // Output data
    int16_t cols, int16_t rows              // Image dimensions
);

void calc_gradient(int32_t* restrict line_1,int32_t* restrict line_2,int32_t* restrict line_3,uint32_t* restrict cache_out,const int32_t width,uint32_t* max);
void select_high_eig_values(uint32_t* restrict in, uint32_t* restrict out,uint32_t offset,uint32_t* count,uint32_t max,int32_t width);
void _quicksort(uint32_t *pointlist, int32_t n);


#endif /*FEATURESELECT_H*/
