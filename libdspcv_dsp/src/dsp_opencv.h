//  created:    2012/01/18
//  filename:   dsp_opencv.c
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 6599
//
//  version:    $Id: $
//
//  purpose:    fonctions tests
//
//
/*********************************************************************/

#include <stdint.h>
#include "cxtypes.h"

#if defined OMAPL138 || defined OMAP3530
#else
#define restrict
#endif

void dspCvtJpg(IplImage *img_src,uint8_t *data_img_dst,uint8_t compression_level,uint8_t input_format,uint8_t output_format,uint32_t *output_size);
