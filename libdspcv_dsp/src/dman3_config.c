/*
 * Copyright (c) 2009, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*
 *  ======== dman3_config.c ========
 *  Configuration for DMAN3 Parameters
 */

/*
 *  Guillaume Sanahuja: modifications pour utiliser 2 canaux
 */

#include "dman3_config.h"


static Uns CFG_QDMA_CHANNELS[4] = { 0, 1, 2, 3};
#define DMAN3MAXQDMACHANNELS 8;
#define DMAN3NUMQDMACHANNELS 4;

#ifdef OMAP3530
extern int L1DHEAP; /* Heap Label for L1DRAM memory allocation */
#endif
extern int EXTMEMHEAP; /* Heap Label for external memory allocation */

#ifdef OMAP3530
#define DMAN3HEAPINTERNAL L1DHEAP;
#endif
#define DMAN3HEAPEXTERNAL EXTMEMHEAP;
#define DMAN3PARAMBASEINDEX 78;
#define DMAN3NUMPARAMENTRIES 48;

#ifdef OMAP3530
#define DMAN3TCCALLOCATIONMASKH 0xffffffff;
#define DMAN3TCCALLOCATIONMASKL 0x0;
#endif

#ifdef OMAPL138
#define DMAN3TCCALLOCATIONMASKL 0xffffffff;
/* Keep this H mask 0, as Freon has only 32 Tccs */
#define DMAN3TCCALLOCATIONMASKH 0x0;
#endif

#define DMAN3QDMACHANNELS CFG_QDMA_CHANNELS;

void dman3_start(IDMA3_Handle *h_in,IDMA3_Handle *h_out)
{
    IDMA3_ChannelRec dmaTab[2];
    Uint8 i;
    Int status_dman3 ;
#ifdef OMAP3530
    DMAN3_PARAMS.heapInternal       = DMAN3HEAPINTERNAL;
#endif
    DMAN3_PARAMS.heapExternal       = DMAN3HEAPEXTERNAL;

    DMAN3_PARAMS.paRamBaseIndex     = DMAN3PARAMBASEINDEX;
    DMAN3_PARAMS.numPaRamEntries    = DMAN3NUMPARAMENTRIES;
    DMAN3_PARAMS.tccAllocationMaskH = DMAN3TCCALLOCATIONMASKH;
    DMAN3_PARAMS.tccAllocationMaskL = DMAN3TCCALLOCATIONMASKL;
    DMAN3_PARAMS.qdmaChannels       = CFG_QDMA_CHANNELS;
    DMAN3_PARAMS.maxQdmaChannels    = DMAN3MAXQDMACHANNELS;
    DMAN3_PARAMS.numQdmaChannels    = DMAN3NUMQDMACHANNELS;
    DMAN3_PARAMS.maxTCs             = 2;


    //Initialize DMA manager and ACPY3 library for XDAIS algorithms
    //and grant DMA resources
    DMAN3_init();
    ACPY3_init();

    // Set up the DMAN3 Configurable parameters
    // the heap settings of DMAN3 should be set to valid heap descriptors
    #ifdef OMAP3530
    DMAN3_PARAMS.heapInternal =L1DHEAP;
    #endif
    DMAN3_PARAMS.heapExternal =EXTMEMHEAP;
     #ifdef OMAPL138
    DMAN3_PARAMS.idma3Internal=FALSE;
    #endif

    for(i=0;i<2;i++)
    {
        // Set up the DMA Channel descriptor with the transfer parameters
        dmaTab[i].numTransfers = 1;
        dmaTab[i].numWaits = 1;
        dmaTab[i].priority = IDMA3_PRIORITY_MEDIUM;//IDMA3_PRIORITY_LOW;

        // The ACPY3 transfer protocol will be used as the IDMA3 protocol object
        // This object defines the memory requirements and the initialization and
        // de-initialization functions for the protocol's environment
        dmaTab[i].protocol = &ACPY3_PROTOCOL;
        dmaTab[i].persistent = TRUE;//FALSE;
    }

    // On success this call will return a valid DMA channel handle with the
    // attributes defined above

    status_dman3 = DMAN3_createChannels(0, dmaTab, 2);

    if (status_dman3 == DMAN3_SOK ) {
        *h_in = dmaTab[0].handle;
        *h_out = dmaTab[1].handle;

        // Put the channel in active state
        // Now other ACPY3 APIs can be called on this handle
        ACPY3_activate((IDMA3_Handle)(*h_in));
        ACPY3_activate((IDMA3_Handle)(*h_out));
    }
    else {

    }
}


void dman3_stop(IDMA3_Handle *h_in,IDMA3_Handle *h_out)
{
    // deactivate
    ACPY3_deactivate((IDMA3_Handle)(*h_in));
    ACPY3_deactivate((IDMA3_Handle)(*h_out));

    // Free the channel
    if (( DMAN3_freeChannels(h_in, 1)) != DMAN3_SOK ) {

    }
    if ((DMAN3_freeChannels(h_out, 1)) != DMAN3_SOK ) {

    }

    // module finalization
    DMAN3_exit();
    ACPY3_exit();
}


/*
 *  @(#) ti.sdo.fc.dman3.examples.fastcopy; 1, 0, 0,203; 7-16-2009 16:33:44; /db/atree/library/trees/fc/fc-k07x/src/
 */

