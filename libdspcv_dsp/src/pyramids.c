//  created:    2012/01/18
//  filename:   pyramids.c
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 6599
//
//  version:    $Id: $
//
//  purpose:    construction des pyramides
//
//
/*********************************************************************/

#include "pyramids.h"
#include <stdlib.h>

#if defined OMAPL138 || defined OMAP3530
//#include <xdc/std.h>
#include <std.h>
#include <mem.h>
//#include <ti/xdais/idma3.h>
#include <ti/sdo/fc/acpy3/acpy3.h>
#define _ACPY3_fastConfigure32b(handle,type,addr,id) ACPY3_fastConfigure32b(handle,type,(uint32_t)addr,id)
//faire un header
extern void _printf(const char * format, ...);
#else
#include <string.h>
#include "C64intrins.h"
#include "wrappers.h"
#define _ACPY3_fastConfigure32b(handle,type,addr,id) ACPY3_fastConfigure_ptr(handle,type,(void*)addr,id)
extern pthread_mutex_t mutex;
#endif

#include "opencv_common.h"

#ifdef OMAPL138
extern int32_t DDR;
extern int32_t IRAM;
extern int32_t L1DSRAM;
extern int32_t CACHE_L1D;
#define BUF_SEG_ID IRAM
//#define BUF_SEG_ID L1DSRAM
#define BUF_ALIGN 128
#endif


#ifdef OMAP3530
extern int32_t DDR2;
extern int32_t L1DSRAM;
#define BUF_ALIGN 128
#endif

#define MAX_PYR 5

extern IDMA3_Handle h_in,h_out;

void dspPyrDown(IplImage* img_src,IplImage* img_dst,uint8_t level)
{
    uint32_t i;
    uint32_t offset=0;
    uint8_t* data_src=(uint8_t*)img_src->imageData;
    uint32_t width,height,imageSize;

#if !defined(OMAPL138) && !defined(OMAP3530)
    pthread_mutex_lock(&mutex);
#endif

    if(img_src->roi!=NULL)
    {
        width=img_src->roi->width;
        height=img_src->roi->height;
        data_src+=img_src->roi->xOffset+img_src->roi->yOffset*img_src->width;
    }
    else
    {
        width=img_src->width;
        height=img_src->height;
    }
    imageSize=width*height;

    LevelPyrDown(data_src,(uint8_t*)img_dst->imageData, width,height,img_src->width);
    for(i=1;i<level;i++)
    {
        LevelPyrDown((uint8_t*)img_dst->imageData+offset,(uint8_t*)img_dst->imageData+offset+(imageSize>>(2*i)), width>>i,height>>i,width>>i);
        offset+=(imageSize>>(2*i));
    }

#if !defined(OMAPL138) && !defined(OMAP3530)
    pthread_mutex_unlock(&mutex);
#endif

}

//a revoir pour eviter les recopies inutiles de lignes entrees
//ou faire comme pour sobel, prendre + de 5 lignes d'un coup
//a voir avec l'effet de bord... pas possible a priori
void LevelPyrDown(uint8_t* src,uint8_t* dst, uint32_t width,uint32_t height,uint32_t line_width)
{
    ACPY3_Params p;
    uint32_t i,max_iterations,nb_lines_out,nb_lines_in,size_out;
    uint8_t* cache_in_1;
    uint8_t* cache_in_2;
    uint8_t* cache_out_1;
    uint8_t* cache_out_2;
    uint8_t *data_src;
    uint8_t *data_dst;

    //pour l'appel a PyrDown_5x5, verifier que:
    //...
    //verifier aussi que width*nb_lines_in <65535 !!!

    //calcul de la taille des buffers
    //on met 5 lignes dans les buffer d'entrees, et 1 ligne dans ceux de sorties
    nb_lines_in=5;
    nb_lines_out=1;
    cache_in_1=MEM_alloc(L1DSRAM, nb_lines_in*width, BUF_ALIGN);
    cache_in_2=MEM_alloc(L1DSRAM, nb_lines_in*width, BUF_ALIGN);
    cache_out_1=MEM_alloc(L1DSRAM, nb_lines_out*width, BUF_ALIGN);
    cache_out_2=MEM_alloc(L1DSRAM, nb_lines_out*width, BUF_ALIGN);

    if(cache_in_1==NULL || cache_in_2==NULL || cache_out_1==NULL || cache_out_2==NULL)
    {
        _printf("erreur allocation LevelPyrDown\n");
    }

    max_iterations=(height/nb_lines_out)/2;
    size_out=nb_lines_out*width;


    //premier pas: src > cache_in1
    // calcul de la premiere ligne, cf openCV
    // 1/16[1    4    6    4    1]
    //| l2 | l1 I l0 | l1 | l2 |... ("I" denotes the image boundary)
    data_src=(uint8_t *)(src+2*line_width);//l2
    data_dst=(uint8_t *)dst;
    p.transferType = ACPY3_1D1D;
    p.srcAddr = (void *)data_src;
    p.dstAddr = (void *)cache_in_1;
    p.elementSize = width;
    p.srcElementIndex = 1;
    p.dstElementIndex = 1;
    p.numElements = 1;
    p.numFrames = 1;
    p.waitId = 0;// waitId of 0 implies wait after the first transfer
    ACPY3_configure(h_in, &p, 0);
    ACPY3_start(h_in);//l2
    ACPY3_configure(h_out, &p, 0);
    ACPY3_fastConfigure16b(h_out,ACPY3_PARAMFIELD_ELEMENTSIZE,size_out/2,0);
    data_src-=line_width;//l1
    ACPY3_wait(h_in);

    _ACPY3_fastConfigure32b(h_in,ACPY3_PARAMFIELD_SRCADDR,data_src,0);
    _ACPY3_fastConfigure32b(h_in,ACPY3_PARAMFIELD_DSTADDR,(cache_in_1+width),0);
    ACPY3_start(h_in);//l1
    data_src-=line_width;//l0
    ACPY3_wait(h_in);

    if(line_width!=width)//roi
    {
        p.transferType = ACPY3_2D1D;
        p.srcAddr = data_src;
        p.dstAddr = (cache_in_1+2*width);
        p.elementSize = width;//type uint8_t
        p.srcElementIndex = line_width;
        p.dstElementIndex = 1;
        p.numElements = 3;
        p.numFrames = 1;
        p.waitId = 0;// waitId of 0 implies wait after the first transfer
        ACPY3_configure(h_in, &p, 0);
        ACPY3_start(h_in);//l0 l1 l2
        ACPY3_wait(h_in);

        ACPY3_fastConfigure16b(h_in,ACPY3_PARAMFIELD_NUMELEMENTS,nb_lines_in,0);
    }
    else
    {
        ACPY3_fastConfigure16b(h_in,ACPY3_PARAMFIELD_ELEMENTSIZE,3*width,0);
        _ACPY3_fastConfigure32b(h_in,ACPY3_PARAMFIELD_SRCADDR,data_src,0);
        _ACPY3_fastConfigure32b(h_in,ACPY3_PARAMFIELD_DSTADDR,(cache_in_1+2*width),0);
        ACPY3_start(h_in);//l0 l1 l2
        ACPY3_wait(h_in);

        ACPY3_fastConfigure16b(h_in,ACPY3_PARAMFIELD_ELEMENTSIZE,nb_lines_in*width,0);
    }


    //deuxieme pas: src > cache_in2		trait cache_in1 > cache_out1
    _ACPY3_fastConfigure32b(h_in,ACPY3_PARAMFIELD_SRCADDR,data_src,0);
    _ACPY3_fastConfigure32b(h_in,ACPY3_PARAMFIELD_DSTADDR,cache_in_2,0);
    ACPY3_start(h_in);
    PyrDown_5x5(cache_in_1,cache_out_1,width);
    data_src+=2*nb_lines_out*line_width;
    ACPY3_wait(h_in);

    for(i=0;i<max_iterations;i+=2)
    {
        //cache_out1 > dst	trait cache_in2 > cache_out2	src > cache_in1
        _ACPY3_fastConfigure32b(h_out,ACPY3_PARAMFIELD_SRCADDR,cache_out_1,0);
        _ACPY3_fastConfigure32b(h_out,ACPY3_PARAMFIELD_DSTADDR,data_dst,0);
        ACPY3_start(h_out);

        _ACPY3_fastConfigure32b(h_in,ACPY3_PARAMFIELD_SRCADDR,data_src,0);
        _ACPY3_fastConfigure32b(h_in,ACPY3_PARAMFIELD_DSTADDR,cache_in_1,0);
        ACPY3_start(h_in);

        PyrDown_5x5(cache_in_2,cache_out_2,width);
        data_dst+=size_out/2;
        data_src+=2*nb_lines_out*line_width;
        ACPY3_wait(h_out);
        ACPY3_wait(h_in);

        //src > cache_in2		trait cache_in1 > cache_out1	cache_out2 > dst
        _ACPY3_fastConfigure32b(h_in,ACPY3_PARAMFIELD_SRCADDR,data_src,0);
        _ACPY3_fastConfigure32b(h_in,ACPY3_PARAMFIELD_DSTADDR,cache_in_2,0);
        ACPY3_start(h_in);

        _ACPY3_fastConfigure32b(h_out,ACPY3_PARAMFIELD_SRCADDR,cache_out_2,0);
        _ACPY3_fastConfigure32b(h_out,ACPY3_PARAMFIELD_DSTADDR,data_dst,0);
        ACPY3_start(h_out);

        PyrDown_5x5(cache_in_1,cache_out_1,width);
        data_src+=2*nb_lines_out*line_width;
        data_dst+=size_out/2;
        ACPY3_wait(h_out);
        ACPY3_wait(h_in);

        //la derniere ligne doit etre refaite, cf opencv
        // 1/16[1    4    6    4    1]
        // ...| l0 | l1 | l2 | l3 I l2 | ("I" denotes the image boundary)
        //l0,l1,l2,l3 ont bien été copiées, on rajoute l2 à la place du faux l5
        if(i==max_iterations-4)
        {
            _ACPY3_fastConfigure32b(h_in,ACPY3_PARAMFIELD_SRCADDR,cache_in_2+2*size_out,0);
            _ACPY3_fastConfigure32b(h_in,ACPY3_PARAMFIELD_DSTADDR,(cache_in_2+4*size_out),0);
            ACPY3_fastConfigure16b(h_in,ACPY3_PARAMFIELD_ELEMENTSIZE,width,0);
            ACPY3_fastConfigure16b(h_in,ACPY3_PARAMFIELD_NUMELEMENTS,1,0);
            ACPY3_start(h_in);
            ACPY3_wait(h_in);
        }
    }
    //a priori pas de rebus, car le nombre de lignes est pair, et on traite 2 lignes à chaque itérations de la boucle

    MEM_free(L1DSRAM, cache_in_1,nb_lines_in*width);
    MEM_free(L1DSRAM, cache_in_2,nb_lines_in*width);
    MEM_free(L1DSRAM, cache_out_1,nb_lines_out*width);
    MEM_free(L1DSRAM, cache_out_2,nb_lines_out*width);

}


/*
void PyrDown_3x3(const uint8_t *restrict inptr, uint8_t *restrict outptr, int32_t x_dim)
{
    //----------------------------------------------------------------------
    // count is a variable for the number of output pixels. IN1, IN2 and
    // IN3 are input pointers to three adjacent lines of image data. OUT
    // is a pointer to output data.
    //----------------------------------------------------------------------

    int32_t                      j, count;
    const   uint8_t   *IN1,    *IN2,    *IN3;
    uint32_t            *OUT;

    //----------------------------------------------------------------------
    // maskA_B : A is row no., B is starting column no.
    //----------------------------------------------------------------------

    uint32_t  mask1_0, mask2_0, mask3_0;
    //uint32_t  mask1_1, mask2_1, mask3_1;

    //----------------------------------------------------------------------
    // Other variables.
    //----------------------------------------------------------------------
    double r1_76543210, r2_76543210, r3_76543210;
    uint32_t r1_7654, r1_3210;
    uint32_t r2_7654, r2_3210;
    uint32_t r3_7654, r3_3210;
    uint32_t r1_5432, r2_5432, r3_5432;

    int32_t           sum0, sum1, sum2, sum3;
    uint32_t  sum32, sum10, sum3210;

    //----------------------------------------------------------------------
    //  Read mask values and prepare registers for convolution
    //----------------------------------------------------------------------
    //mask1_0 = 0x00010201;
    //mask2_0 = 0x00020402;
    //mask3_0 = 0x00010201;
    mask1_0 = 0x01020100;
    mask2_0 = 0x02040200;
    mask3_0 = 0x01020100;

    //-----------------------------------------------------------------------
    // Set loop counter for output pixels and three input pointers x_dim
    // apart from the user passed input pointer. Copy output pointer
    //-----------------------------------------------------------------------

    count   =   x_dim >> 3 ;

    IN1     =   inptr;
    IN2     =   IN1+ x_dim;
    IN3     =   IN2+ x_dim;
    OUT     =   (uint32_t *)outptr;

    //----------------------------------------------------------------------
    // In order to minimize data loads, dat re-use is achieved by moves.
    // The data to be used for pix10, pix11 are pre-loaded into pix12 and
    // pix13 and moved within the loop. The process is repeated for rows 2
    // and 3 for pix20, pix21 and pix30 and pix31 respectively.
    //----------------------------------------------------------------------

    r1_76543210 = _memd8_const(IN1);
    r2_76543210 = _memd8_const(IN2);
    r3_76543210 = _memd8_const(IN3);


    r1_3210 = _lo(r1_76543210);
    r2_3210 = _lo(r2_76543210);
    r3_3210 = _lo(r3_76543210);


    sum3 =(_dotpsu4(0x0000202, r1_3210) + _dotpsu4(0x0000404, r2_3210)+ _dotpsu4(0x0000202, r3_3210)+8 ) >> 4;

    for( j = 0; j < count; j++)
    {

        r1_76543210 = _memd8_const(IN1);
        r2_76543210 = _memd8_const(IN2);
        r3_76543210 = _memd8_const(IN3);
        IN1 += 4;
        IN2 += 4;
        IN3 += 4;

        r1_3210 = _lo(r1_76543210);
        r2_3210 = _lo(r2_76543210);
        r3_3210 = _lo(r3_76543210);
        r1_7654 = _hi(r1_76543210);
        r2_7654 = _hi(r2_76543210);
        r3_7654 = _hi(r3_76543210);

        sum0 =(_dotpsu4(mask1_0, r1_3210) + _dotpsu4(mask2_0, r2_3210)+ _dotpsu4(mask3_0, r3_3210)+8 ) >> 4;

        r1_5432 = _packlh2(r1_7654, r1_3210);
        r2_5432 = _packlh2(r2_7654, r2_3210);
        r3_5432 = _packlh2(r3_7654, r3_3210);

        sum1 = (_dotpsu4(mask1_0, r1_5432) + _dotpsu4(mask2_0, r2_5432)+ _dotpsu4(mask3_0, r3_5432)+8 ) >> 4;

        //sum10 = _spack2(sum1, sum0);

        r1_76543210 = _memd8_const(IN1);
        r2_76543210 = _memd8_const(IN2);
        r3_76543210 = _memd8_const(IN3);
        IN1 += 4;
        IN2 += 4;
        IN3 += 4;

        r1_3210 = _lo(r1_76543210);
        r2_3210 = _lo(r2_76543210);
        r3_3210 = _lo(r3_76543210);
        r1_7654 = _hi(r1_76543210);
        r2_7654 = _hi(r2_76543210);
        r3_7654 = _hi(r3_76543210);

        sum2 =(_dotpsu4(mask1_0, r1_3210) + _dotpsu4(mask2_0, r2_3210)+ _dotpsu4(mask3_0, r3_3210)+8 ) >> 4;

        r1_5432 = _packlh2(r1_7654, r1_3210);
        r2_5432 = _packlh2(r2_7654, r2_3210);
        r3_5432 = _packlh2(r3_7654, r3_3210);

        sum10 = _spack2(sum0, sum3);
        sum32 = _spack2(sum2, sum1);

        sum3 =(_dotpsu4(mask1_0, r1_5432) + _dotpsu4(mask2_0, r2_5432)+ _dotpsu4(mask3_0, r3_5432)+8 ) >> 4;


        //sum32 = _spack2(sum3, sum2);


        sum3210 = _spacku4(sum32, sum10);

        _mem4(OUT) = sum3210;
        OUT++;
    }
}

*/


void PyrDown_5x5
(
    const uint8_t *restrict  imgin_ptr,
    uint8_t       *restrict imgout_ptr,
    int16_t                              width
)
{

    int32_t                 i;
    uint32_t                 sum,            sum1;
    uint32_t                 sum3,            sum1_temp;
    uint32_t                 sum3_temp;
    uint32_t                 row_temp2;

    uint32_t                 mask3_0,        mask8_5,        mask13_10;
    uint32_t                 mask18_15,      mask23_20;

    uint32_t                 temp1,          temp5;

    uint64_t              row7_0;
    uint64_t              temp_2;

    uint32_t            mask_4x4,       mask_9x4,       mask_14x4;
    uint32_t            mask_19x4,      mask_24x4;

    const uint8_t	*in,            *in1,           *in2;
    const uint8_t *in3,           *in4;

    // --------------------------------------------------------------------
    //   copy the first 4 mask elemsnts of each row in a separate variable
    // --------------------------------------------------------------------

    mask3_0     =   0x04060401;
    mask8_5     =   0x10181004;
    mask13_10   =   0x18241806;
    mask18_15   =   0x10181004;
    mask23_20   =   0x04060401;

    // --------------------------------------------------------------------
    //   pack the 5th mask element of each row in 2 lower 8 bits of int
    // --------------------------------------------------------------------


    mask_4x4    =   0x00010001;
    mask_9x4    =   0x00040004;
    mask_14x4   =   0x00060006;
    mask_19x4   =   0x00040004;
    mask_24x4   =   0x00010001;

    // --------------------------------------------------------------------
    //       initialise pointers to the starting of each of the 5 rows
    // --------------------------------------------------------------------

    in          =   imgin_ptr;
    in1         =   in  + width;
    in2         =   in1 + width;
    in3         =   in2 + width;
    in4         =   in3 + width;

    // calcul du premier pixel, cf openCV
    // 1/16[1    4    6    4    1]
    //| x2 | x1 I x0 | x1 | x2 |... ("I" denotes the image boundary)
    // ----------------------------------------------------------------
    // Load 4 pixels that can be used to calculate first output pixel
    // from the 1st row
    // ----------------------------------------------------------------
    temp5      =   _mem4_const(( void * )&in[0]);//tester en amem

    // ----------------------------------------------------------------
    // Perform the partial dot-product of input pixels with
    // correponding mask for the 1st output samples
    // ----------------------------------------------------------------
    sum3_temp       =   _dotpsu4(0x00020806 , temp5);

    // ----------------------------------------------------------------
    // Load 4 pixels that can be used to calculate first output pixel
    // from the 2nd row
    // ----------------------------------------------------------------
    temp5      =   _mem4_const(( void *)&in1[0]);//tester en amem

    // ----------------------------------------------------------------
    // Perform the partial dot-product of input pixels with
    // correponding mask for the 1st output samples
    // ----------------------------------------------------------------
    sum3_temp       +=   _dotpsu4(0x00082018,temp5);

    // ----------------------------------------------------------------
    // Load 4 pixels that can be used to calculate first output pixel
    // from the 3rd row
    // ----------------------------------------------------------------
    temp5      =   _mem4_const((void *)&in2[0]);//tester en amem

    // ----------------------------------------------------------------
    // Perform the partial dot-product of input pixels with
    // correponding mask for the 1st and 3rd output samples
    // ----------------------------------------------------------------
    sum3_temp       +=   _dotpsu4(0x000c3024,temp5);

    // ----------------------------------------------------------------
    // Load 4 pixels that can be used to calculate first output pixel
    // from the 4th row
    // ----------------------------------------------------------------
    temp5      =   _mem4_const(( void * ) &in3[0]);//tester en amem

    // ----------------------------------------------------------------
    // Perform the partial dot-product of input pixels with
    // correponding mask for the 1st and 3rd output samples
    // ----------------------------------------------------------------
    sum3_temp       +=   _dotpsu4(0x00082018 , temp5);

    // ----------------------------------------------------------------
    // Load 4 pixels that can be used to calculate first output pixel
    // from the 5th row
    // ----------------------------------------------------------------
    temp5      =   _mem4_const(( void * )&in4[0]);//tester en amem

    // ----------------------------------------------------------------
    // Perform the partial dot-product of input pixels with
    // correponding mask for the 1st and 3rd output samples
    // ----------------------------------------------------------------
    sum3_temp       +=  128+ _dotpsu4(0x00020806 ,temp5);//+128 pour la conversion, cf openCV


    // --------------------------------------------------------------------
    //  Inform the compiler by _nasserts the following:
    //      a) The output array is word aligned
    //      b) Filter Mask array is double word aligned
    //      c) The width is greater than or equal to 6
    //      d) The width is a multiple of 4.
    // --------------------------------------------------------------------

    _nassert((int32_t)imgout_ptr % 4 == 0);    // word aligned
    _nassert(width               >= 4);    // width greater or equal to 4
    _nassert(width           % 4 == 0);    // width is a multiple of 4

    // --------------------------------------------------------------------
    //  Loop is manually unrolled by 4
    // --------------------------------------------------------------------

    #pragma MUST_ITERATE(1, ,1)
    for (i = 0; i < width; i += 4)
    {
        //le dernier pixel est utilisé a la prochaine itération
        sum3        =   (sum3_temp>>8);

        // ----------------------------------------------------------------
        // Load 8 pixels that can be used to calculate 2 output pixels
        // from the 1st row
        // ----------------------------------------------------------------
        row7_0      =   _mem8_const(( void * )&in[i]);
        row_temp2   =   _packlh2((_hill(row7_0)) ,  (_loll(row7_0)));

        // ----------------------------------------------------------------
        // Perform the partial dot-product of input pixels with
        // correponding mask for the 1st and 3rd output samples
        // ----------------------------------------------------------------
        temp1       =   _dotpsu4(mask3_0 , _loll(row7_0));
        temp5       =   _dotpsu4(mask3_0 , row_temp2);
        temp_2      =   _mpysu4ll(mask_4x4, _hill(row7_0));

        // ----------------------------------------------------------------
        //  Calculate sum1, sum2, sum3 and sum4 that holds convolution sums
        //  for four outptu pixels
        // ----------------------------------------------------------------
        sum1_temp   =   temp1 + _ext(_loll(temp_2), 16, 16);
        sum3_temp   =   temp5 + _ext(_hill(temp_2), 16, 16);

        // ----------------------------------------------------------------
        // Load 8 pixels that can be used to calculate 2 output pixels
        // from the 2nd row
        // ----------------------------------------------------------------
        row7_0      =   _mem8_const(( void *)&in1[i]);
        row_temp2   =   _packlh2((_hill(row7_0)) ,  (_loll(row7_0)));

        // ----------------------------------------------------------------
        // Perform the partial dot-product of input pixels with
        // correponding mask for the 1st and 3rd output samples
        // ----------------------------------------------------------------
        temp1       =   _dotpsu4(mask8_5,_loll(row7_0));
        temp5       =   _dotpsu4(mask8_5 , row_temp2);
        temp_2      =   _mpysu4ll(mask_9x4, _hill(row7_0));

        // ----------------------------------------------------------------
        //  Calculate sum1, sum2, sum3 and sum4 that holds convolution sums
        //  for four outptu pixels
        // ----------------------------------------------------------------
        sum1_temp  +=   temp1 + _ext(_loll(temp_2), 16, 16);
        sum3_temp  +=   temp5 + _ext(_hill(temp_2), 16, 16);

        // ----------------------------------------------------------------
        // Load 8 pixels that can be used to calculate 2 output pixels
        // from the 3rd row
        // ----------------------------------------------------------------
        row7_0      =   _mem8_const((void *)&in2[i]);
        row_temp2   =   _packlh2((_hill(row7_0)) ,  (_loll(row7_0)));

        // ----------------------------------------------------------------
        // Perform the partial dot-product of input pixels with
        // correponding mask for the 1st and 3rd output samples
        // ----------------------------------------------------------------
        temp1       =   _dotpsu4(mask13_10,_loll(row7_0));
        temp5       =   _dotpsu4(mask13_10 , row_temp2);
        temp_2      =   _mpysu4ll(mask_14x4, _hill(row7_0));

        // ----------------------------------------------------------------
        //  Calculate sum1, sum2, sum3 and sum4 that holds convolution sums
        //  for four outptu pixels
        // ----------------------------------------------------------------
        sum1_temp  +=   temp1 + _ext(_loll(temp_2), 16, 16);
        sum3_temp  +=   temp5 + _ext(_hill(temp_2), 16, 16);

        // ----------------------------------------------------------------
        // Load 8 pixels that can be used to calculate 2 output pixels
        // from the 4th row
        // ----------------------------------------------------------------
        row7_0      =   _mem8_const(( void * ) &in3[i]);
        row_temp2   =   _packlh2((_hill(row7_0)) ,  (_loll(row7_0)));

        // ----------------------------------------------------------------
        // Perform the partial dot-product of input pixels with
        // correponding mask for the 1st and 3rd output samples
        // ----------------------------------------------------------------
        temp1       =   _dotpsu4(mask18_15 , _loll(row7_0));
        temp5       =   _dotpsu4(mask18_15 , row_temp2);
        temp_2      =   _mpysu4ll(mask_19x4, _hill(row7_0));

        // ----------------------------------------------------------------
        //  Calculate sum1, sum2, sum3 and sum4 that holds convolution sums
        //  for 2 output pixels
        // ----------------------------------------------------------------
        sum1_temp  +=   temp1 + _ext(_loll(temp_2), 16, 16);
        sum3_temp  +=   temp5 + _ext(_hill(temp_2), 16, 16);

        // ----------------------------------------------------------------
        // Load 8 pixels that can be used to calculate 2 output pixels
        // from the 5th row
        // ----------------------------------------------------------------
        row7_0      =   _mem8_const(( void * )&in4[i]);
        row_temp2   =   _packlh2((_hill(row7_0)) ,  (_loll(row7_0)));

        // ----------------------------------------------------------------
        // Perform the partial dot-product of input pixels with
        // correponding mask for the 1st and 3rd output samples
        // ----------------------------------------------------------------
        temp1       =   _dotpsu4(mask23_20 , _loll(row7_0));
        temp5       =   _dotpsu4(mask23_20 , row_temp2);
        temp_2      =   _mpysu4ll(mask_24x4, _hill(row7_0));

        // ----------------------------------------------------------------
        //  Calculate sum1, sum3  that holds convolution sums
        //  for 2 output pixels
        // ----------------------------------------------------------------
        sum1_temp  +=  128+ temp1 + _ext(_loll(temp_2), 16, 16);//+128 pour la conversion, cf openCV
        sum3_temp  +=  128+ temp5 + _ext(_hill(temp_2), 16, 16);

        // ----------------------------------------------------------------
        // Shift the sums to fall in byte range with user defined value
        // ----------------------------------------------------------------
        sum1        =   (sum1_temp >>8);

        // ----------------------------------------------------------------
        // Check for saturation and pack the 4 bytes to store using amem4
        // ----------------------------------------------------------------
        sum         =   _spacku4(_spack2(sum1,sum3) , _spack2(sum1, sum3));
#if defined OMAPL138 || defined OMAP3530
        _mem4((void*)&imgout_ptr[i/2]) = sum;
#else
        memcpy(&imgout_ptr[i/2],&sum,4);
#endif

   }

    //le dernier pixel doit être refait, cf openCV
    // 1/16[1    4    6    4    1]
    // ...| x0 | x1 | x2 | x3 I x2 | ("I" denotes the image boundary)
    // ----------------------------------------------------------------
    // Load 4 pixels that can be used to calculate first output pixel
    // from the 1st row
    // ----------------------------------------------------------------
    i-=4;
    temp5      =   _mem4_const(( void * )&in[i]);

    // ----------------------------------------------------------------
    // Perform the partial dot-product of input pixels with
    // correponding mask for the 1st output samples
    // ----------------------------------------------------------------
    sum1_temp       =   _dotpsu4(0x04070401 , temp5);

    // ----------------------------------------------------------------
    // Load 4 pixels that can be used to calculate first output pixel
    // from the 2nd row
    // ----------------------------------------------------------------
    temp5      =   _mem4_const(( void *)&in1[i]);

    // ----------------------------------------------------------------
    // Perform the partial dot-product of input pixels with
    // correponding mask for the 1st output samples
    // ----------------------------------------------------------------
    sum1_temp       +=   _dotpsu4(0x101c1004,temp5);

    // ----------------------------------------------------------------
    // Load 4 pixels that can be used to calculate first output pixel
    // from the 3rd row
    // ----------------------------------------------------------------
    temp5      =   _mem4_const((void *)&in2[i]);

    // ----------------------------------------------------------------
    // Perform the partial dot-product of input pixels with
    // correponding mask for the 1st and 3rd output samples
    // ----------------------------------------------------------------
    sum1_temp       +=   _dotpsu4(0x182a1806,temp5);

    // ----------------------------------------------------------------
    // Load 4 pixels that can be used to calculate first output pixel
    // from the 4th row
    // ----------------------------------------------------------------
    temp5      =   _mem4_const(( void * ) &in3[i]);

    // ----------------------------------------------------------------
    // Perform the partial dot-product of input pixels with
    // correponding mask for the 1st and 3rd output samples
    // ----------------------------------------------------------------
    sum1_temp       +=   _dotpsu4(0x101c1004 , temp5);

    // ----------------------------------------------------------------
    // Load 4 pixels that can be used to calculate first output pixel
    // from the 5th row
    // ----------------------------------------------------------------
    temp5      =   _mem4_const(( void * )&in4[i]);

    // ----------------------------------------------------------------
    // Perform the partial dot-product of input pixels with
    // correponding mask for the 1st and 3rd output samples
    // ----------------------------------------------------------------
    sum1_temp       +=  128+ _dotpsu4(0x04070401 ,temp5);//+128 pour la conversion, cf openCV

    // ----------------------------------------------------------------
    // Shift the sums to fall in byte range with user defined value
    // ----------------------------------------------------------------
    sum1        =   (sum1_temp >>8);

    // ----------------------------------------------------------------
    // Check for saturation and pack the 4 bytes to store using amem4
    // ----------------------------------------------------------------
    sum         =   _spacku4(_spack2(sum1,sum3) , _spack2(sum1, sum3));
#if defined OMAPL138 || defined OMAP3530
    _mem4((void*)&imgout_ptr[i/2]) = sum;
#else
    memcpy(&imgout_ptr[i/2],&sum,4);
#endif

}
