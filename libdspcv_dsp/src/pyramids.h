//  created:    2012/01/18
//  filename:   pyramids.h
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 6599
//
//  version:    $Id: $
//
//  purpose:    construction des pyramides
//
//
/*********************************************************************/

#ifndef PYRAMIDS_H
#define PYRAMIDS_H

#include "cxtypes.h"
#include <stdint.h>

#if defined OMAPL138 || defined OMAP3530

#else
#include "typedef.h"//pour const et restrict, a deplacer
#endif

void dspPyrDown(IplImage* img_src,IplImage* img_dst,uint8_t level);
void LevelPyrDown(uint8_t* src,uint8_t* dst, uint32_t width,uint32_t height,uint32_t line_width);

//void PyrDown_3x3(const uint8_t *restrict inptr,uint8_t *restrict outptr,int x_dim);
void PyrDown_5x5(const uint8_t *restrict  imgin_ptr, uint8_t *restrict imgout_ptr, int16_t width);

#endif /*PYRAMIDS_H*/
