//  created:    2012/01/18
//  filename:   sobel.c
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 6599
//
//  version:    $Id: $
//
//  purpose:    detecteur de Sobel
//
//
/*********************************************************************/

#include "sobel.h"
#include <stdlib.h>
//#include <IMG_sobel_3x3_8.h>

#if defined OMAPL138 || defined OMAP3530
//#include <xdc/std.h>
#include <std.h>
#include <mem.h>
#include <bcache.h>
//#include <ti/xdais/idma3.h>
#include <ti/sdo/fc/acpy3/acpy3.h>
#define _ACPY3_fastConfigure32b(handle,type,addr,id) ACPY3_fastConfigure32b(handle,type,(uint32_t)addr,id)
//faire un header
extern void _printf(const char * format, ...);
#else
#include <string.h>
#include "C64intrins.h"
#include "wrappers.h"
#define _ACPY3_fastConfigure32b(handle,type,addr,id) ACPY3_fastConfigure_ptr(handle,type,(void*)addr,id)
#define IMG_sobel_3x3_8 IMG_sobel_3x3_8_cn
extern pthread_mutex_t mutex;
#endif

#ifdef OMAPL138
extern int32_t DDR;
extern int32_t IRAM;
extern int32_t L1DSRAM;
extern int32_t CACHE_L1D;
#define BUF_SEG_ID IRAM
//#define BUF_SEG_ID L1DSRAM
#define BUF_ALIGN 128
#endif


#ifdef OMAP3530
extern int32_t DDR2;
extern int32_t L1DSRAM;
#define BUF_ALIGN 128
#endif

//les copies DMA sont plus rapide sur une taille multiple de 8
#define BUFF_SIZE (1024*39) //doit etre un multiple de 64


extern IDMA3_Handle h_in,h_out;

//ajouter les aguments d'entre cf cv, taille x,y etc

//la fonction imglig utilise la valeur absolue du resultat
//la foncton custom sature a 0 les valeurs negatives
//faire comme opencv? fonction qui garde les negatifs sur 16 bits. voir le surcout de temps
//puis traiter avec le threshold pour virer les negatifs
//la fonction xy fait comme imglib
void dspSobel(IplImage* img_src,IplImage* img_dst,int32_t dx, int32_t dy)
{
    ACPY3_Params p;
    uint32_t i,j,max_iterations,nb_lines_out,nb_lines_in,size_out;
    uint8_t* restrict cache_in;
    uint8_t* restrict cache_in_old;
    uint8_t* restrict cache_out;
    uint8_t* restrict cache_out_old;
    uint8_t* cache_tmp;
    uint8_t* restrict data_src=(uint8_t*)img_src->imageData;
    uint8_t* restrict data_dst=(uint8_t*)img_dst->imageData;

#if !defined(OMAPL138) && !defined(OMAP3530)
    pthread_mutex_lock(&mutex);
#endif

    //pour l'appel a Sobel, verifier que:
    // Number of columns in the input image. Must be multiple of 2.
    // Number of rows in the input image. cols * (rows-2) must be multiple of 8.

    //verifier aussi que width*nb_lines_in <65535 !!!

    if(dx>1 || dy>1) {
        _printf("erreur dx=%i, dy=%i non implemente\n",dx,dy);
        return;
    }
    if(dx==0 && dy==1) {
      _printf("erreur dx=0, dy=1 non implemente\n");
      return;
    }

    //calcul de la taille des buffers
    i=0;
    while((2*img_dst->width*i+2*img_dst->width*(i+2))<BUFF_SIZE) i++;
    i--;
    while(img_dst->height%i!=0) i--;

    nb_lines_in=i+2;
    nb_lines_out=nb_lines_in-2;

    cache_in=(uint8_t*)MEM_alloc(L1DSRAM, nb_lines_in*img_dst->width, BUF_ALIGN);
    cache_in_old=(uint8_t*)MEM_alloc(L1DSRAM, nb_lines_in*img_dst->width, BUF_ALIGN);
    cache_out=(uint8_t*)MEM_alloc(L1DSRAM, nb_lines_out*img_dst->width, BUF_ALIGN);
    cache_out_old=(uint8_t*)MEM_alloc(L1DSRAM, nb_lines_out*img_dst->width, BUF_ALIGN);
    max_iterations=(img_dst->height-2)/nb_lines_out;//première et dernière lignes ne sont pas traitées

    size_out=nb_lines_out*img_dst->width;

    if(cache_in==NULL || cache_in_old==NULL || cache_out==NULL || cache_out_old==NULL)
    {
        _printf("erreur allocation dspSobel\n");
    }

    p.transferType = ACPY3_1D1D;
    p.elementSize = nb_lines_in*img_dst->width;
    p.srcElementIndex = 1;
    p.dstElementIndex = 1;
    p.numElements = 1;
    p.numFrames = 1;
    p.waitId = 0;// waitId of 0 implies wait after the first transfer
    ACPY3_configure(h_in, &p, 0);
    ACPY3_configure(h_out, &p, 0);
    ACPY3_fastConfigure16b(h_out,ACPY3_PARAMFIELD_ELEMENTSIZE,nb_lines_out*img_dst->width,0);
    data_dst+=img_dst->width;//se place sur la seconde ligne

    //src > cache_in
    _ACPY3_fastConfigure32b(h_in,ACPY3_PARAMFIELD_SRCADDR,data_src,0);
    _ACPY3_fastConfigure32b(h_in,ACPY3_PARAMFIELD_DSTADDR,cache_in,0);
    ACPY3_start(h_in);
    data_src+=size_out;
    ACPY3_wait(h_in);

    //trait cache_in > cache_out  src > cache_in_old
    _ACPY3_fastConfigure32b(h_in,ACPY3_PARAMFIELD_SRCADDR,data_src,0);
    _ACPY3_fastConfigure32b(h_in,ACPY3_PARAMFIELD_DSTADDR,cache_in_old,0);
    ACPY3_start(h_in);
    data_src+=size_out;
    if(dx==1 && dy==1) IMG_sobel_3x3_8_xy(cache_in,cache_out,img_dst->width,nb_lines_in);
    if(dx==1 && dy==0) IMG_sobel_3x3_8_x(cache_in,cache_out,img_dst->width,nb_lines_in);
    for(j=0;j<nb_lines_out;j++)//met la premiere et la derniere colonne a 0
	{
    	cache_out[j*img_dst->width]=0;
    	cache_out[j*img_dst->width+img_dst->width-1]=0;
	}
    ACPY3_wait(h_in);

    for(i=0;i<max_iterations;i++)
    {
        //src > cache_in
        _ACPY3_fastConfigure32b(h_in,ACPY3_PARAMFIELD_SRCADDR,data_src,0);
        _ACPY3_fastConfigure32b(h_in,ACPY3_PARAMFIELD_DSTADDR,cache_in,0);
        ACPY3_start(h_in);
        data_src+=size_out;

        //cache_out > dst
        _ACPY3_fastConfigure32b(h_out,ACPY3_PARAMFIELD_SRCADDR,cache_out,0);
        _ACPY3_fastConfigure32b(h_out,ACPY3_PARAMFIELD_DSTADDR,data_dst,0);
        ACPY3_start(h_out);
        data_dst+=size_out;

        //trait cache_in_old > cache_out_old
        if(dx==1 && dy==1) IMG_sobel_3x3_8_xy(cache_in_old,cache_out_old,img_dst->width,nb_lines_in);
        if(dx==1 && dy==0) IMG_sobel_3x3_8_x(cache_in_old,cache_out_old,img_dst->width,nb_lines_in);
        for(j=0;j<nb_lines_out;j++)//met la premiere et la derniere colonne a 0
		{
        	cache_out_old[j*img_dst->width]=0;
        	cache_out_old[j*img_dst->width+img_dst->width-1]=0;
		}

        ACPY3_wait(h_out);
        ACPY3_wait(h_in);

        //permutation circulaire
        cache_tmp=cache_in;
        cache_in=cache_in_old;
        cache_in_old=cache_tmp;

        cache_tmp=cache_out;
        cache_out=cache_out_old;
        cache_out_old=cache_tmp;
    }

    //derniere iteration
    //cache_out_old > dst
    _ACPY3_fastConfigure32b(h_out,ACPY3_PARAMFIELD_SRCADDR,cache_out,0);
    _ACPY3_fastConfigure32b(h_out,ACPY3_PARAMFIELD_DSTADDR,data_dst,0);
    ACPY3_fastConfigure16b(h_out,ACPY3_PARAMFIELD_ELEMENTSIZE,((img_dst->height-2)-max_iterations*nb_lines_out)*img_dst->width,0);
    ACPY3_start(h_out);
    ACPY3_wait(h_out);

    BCACHE_setMar((void *)img_dst->imageData,img_dst->imageSize,BCACHE_MAR_ENABLE);
    //remplissage premiere et derniere ligne
    for(i=0;i<img_dst->width;i+=8)
	{
#if defined OMAPL138 || defined OMAP3530
        _amem8((void *)&img_dst->imageData[i])=0;
        _amem8((void *)&img_dst->imageData[i+img_dst->width*(img_dst->height-1)])=0;
#else
        uint64_t toto=0;
        memcpy(&img_dst->imageData[i],&toto,8);
        memcpy(&img_dst->imageData[i+img_dst->width*(img_dst->height-1)],&toto,8);
#endif
	}
    BCACHE_setMar((void *)img_dst->imageData,img_dst->imageSize,BCACHE_MAR_DISABLE);

    BCACHE_wbInv(img_dst->imageData, img_dst->width, TRUE);
    BCACHE_wbInv(img_dst->imageData+img_dst->width*(img_dst->height-1), img_dst->width, TRUE);


    MEM_free(L1DSRAM, cache_in,nb_lines_in*img_dst->width);
    MEM_free(L1DSRAM, cache_in_old,nb_lines_in*img_dst->width);
    MEM_free(L1DSRAM, cache_out,nb_lines_out*img_dst->width);
    MEM_free(L1DSRAM, cache_out_old,nb_lines_out*img_dst->width);

#if !defined(OMAPL138) && !defined(OMAP3530)
    pthread_mutex_unlock(&mutex);
#endif
}

void IMG_sobel_3x3_8_x
(
    const uint8_t *restrict in,   /* Input image data   */
    uint8_t       *restrict out,  /* Output image data  */
    int16_t cols, int16_t rows              /* Image dimensions   */
)
{
    int32_t i,t13,t14,end,final_pass;
    int32_t w = cols;


    _nassert(cols > 3);
    _nassert(cols % 2 == 0);
    _nassert(rows > 3);
    _nassert(cols * (rows - 2) - 2 >= 8);

    /* -------------------------------------------------------------------- */
    /*  Iterate over entire image as a single, continuous raster line.      */
    /* -------------------------------------------------------------------- */

    end        = (cols*(rows-2) - 2);
    final_pass = end - 6;

    for (i = 0; i < end; i+=8)
    {
        uint64_t in1_d,in1_d2,in2_d1,in2_d2,in3_d,in3_d2;
        uint32_t in1_l,in1_h,in2_l,in2_h,in3_l,in3_h;
        uint32_t in2_l2,in2_h2;
        uint32_t tmp1,tmp3,tmp2,tmp4;
        uint32_t t2,t4,t6,t8;
        uint32_t b1,b2,b3,b4,b5,b6,b7,b8;
        uint64_t b10,b11,b12,b13,b14,b15;

        /* ---------------------------------------------------------------- */
        /*  Read in the required 3x3 region from the input.                 */
        /* ---------------------------------------------------------------- */

        in1_d   = _mem8_const((void*)&in[i]);
        in1_d2  = _mem8_const((void*)&in[i+2]);
        tmp1    = _loll(in1_d2);
        tmp2    = _hill(in1_d2);
        in1_l   = _loll(in1_d);
        in1_h   = _hill(in1_d);

        in2_d1  = _mem8_const((void*)&in[i+w]);
        in2_d2  = _mem8_const((void*)&in[i+w+2]);
        in2_l   = _loll(in2_d1);
        in2_l2  = _hill(in2_d1);
        in2_h   = _loll(in2_d2);
        in2_h2  = _hill(in2_d2);

        in3_d   = _mem8_const((void*)&in[i+2*w]);
        in3_d2  = _mem8_const((void*)&in[i+2*w+2]);
        tmp3    = _loll(in3_d2);
        tmp4    = _hill(in3_d2);
        in3_l   = _loll(in3_d);
        in3_h   = _hill(in3_d);

        /* ---------------------------------------------------------------- */
        /*  Apply vertical filter mask.                                       */

        /* ---------------------------------------------------------------- */
        // i00=in[i    ]; i01=in[i    +1]; i02=in[i    +2];
        // i10=in[i+  w];                  i12=in[i+  w+2];
        // i20=in[i+2*w]; i21=in[i+2*w+1]; i22=in[i+2*w+2];
        // V = -i00 +   i02 - 2*i10 + 2*i12 -   i20 + i22;

        /* t2, t4, t6, t8 are the vertical guys, two shorts per word */

        b10= _mpyu4ll(in2_l2,0x2020202);
        b11= _mpyu4ll(in1_h, 0x1010101);
        b12= _mpyu4ll(in2_l, 0x2020202);
        b13= _mpyu4ll(in2_h, 0x2020202);
        b14= _mpyu4ll(in2_h2,0x2020202);
        b15= _mpyu4ll(tmp2,  0x1010101);

        b1 = _add2(_add2(_unpklu4(in1_l),_loll(b12)),_unpklu4(in3_l));
        b2 = _add2(_add2(_unpkhu4(in1_l),_hill(b12)),_unpkhu4(in3_l));
        b3 = _add2(_add2(_unpklu4(tmp1), _loll(b13)),_unpklu4(tmp3));
        b4 = _add2(_add2(_unpkhu4(tmp1), _hill(b13)),_unpkhu4(tmp3));
        b5 = _add2(_add2(_loll(b11),_loll(b10)),_unpklu4(in3_h));
        b6 = _add2(_add2(_hill(b11),_hill(b10)),_unpkhu4(in3_h));
        b7 = _add2(_add2(_loll(b15),_loll(b14)),_unpklu4(tmp4));
        b8 = _add2(_add2(_hill(b15),_hill(b14)),_unpkhu4(tmp4));

        t2 = _max2(_sub2(b2,b1),0);
        t4 = _max2(_sub2(b4,b3),0);
        t6 = _max2(_sub2(b6,b5),0);
        t8 = _max2(_sub2(b8,b7),0);


        t13 = _spacku4(t4,t2);
        t14 = _spacku4(t8,t6);
#if defined OMAPL138 || defined OMAP3530
        if(i!=final_pass) /* the last 6 outputs must be stored seperately */
          _mem8((void *)&out[i+1]) = _itoll(t14,t13);
#else
        if(i!=final_pass)
        {
            int64_t toto=_itoll(t14,t13);
            memcpy(&out[i+1],&toto,8);
        }
#endif
    }
    /* clean up the last 6 "output" guys */
#if defined OMAPL138 || defined OMAP3530
    _mem4((void *)&out[i-7]) = t13;
#else
    memcpy(&out[i-7],&t13,4);
#endif
    out[i-3] = t14 & 0xff;
    out[i-2] = (t14>>8)&0xff;
}

void IMG_sobel_3x3_8_xy
(
    const uint8_t *restrict in,   /* Input image data   */
    uint8_t       *restrict out,  /* Output image data  */
    int16_t cols, int16_t rows              /* Image dimensions   */
)
{
    int32_t i,t13,t14,end,final_pass;
    int32_t w = cols;
    int32_t mult1   = 0x00fffeff;
    int32_t mult2   = 0x00010201;

    int32_t mult1_b = 0xfffeff00;
    int32_t mult2_b = 0x01020100;

    _nassert(cols > 3);
    _nassert(cols % 2 == 0);
    _nassert(rows > 3);
    _nassert(cols * (rows - 2) - 2 >= 8);

    /* -------------------------------------------------------------------- */
    /*  Iterate over entire image as a single, continuous raster line.      */
    /* -------------------------------------------------------------------- */

    end        = (cols*(rows-2) - 2);
    final_pass = end - 6;

    for (i = 0; i < end; i+=8)
    {
        uint64_t in1_d,in1_d2,in2_d1,in2_d2,in3_d,in3_d2;
        uint32_t in1_l,in1_h,in2_l,in2_h,in3_l,in3_h;
        uint32_t in2_l2,in2_h2;
        uint32_t tmp1,tmp3,tmp2,tmp4;
        uint32_t t1,t2,t3,t4,t5,t6,t7,t8,t9,t10,t11,t12;
        int32_t H, H3, H5, H7;
        uint32_t b1,b2,b3,b4,b5,b6,b7,b8;
        uint64_t b10,b11,b12,b13,b14,b15;

        /* ---------------------------------------------------------------- */
        /*  Read in the required 3x3 region from the input.                 */
        /* ---------------------------------------------------------------- */

        in1_d   = _mem8_const((void*)&in[i]);
        in1_d2  = _mem8_const((void*)&in[i+2]);
        tmp1    = _loll(in1_d2);
        tmp2    = _hill(in1_d2);
        in1_l   = _loll(in1_d);
        in1_h   = _hill(in1_d);

        in2_d1  = _mem8_const((void*)&in[i+w]);
        in2_d2  = _mem8_const((void*)&in[i+w+2]);
        in2_l   = _loll(in2_d1);
        in2_l2  = _hill(in2_d1);
        in2_h   = _loll(in2_d2);
        in2_h2  = _hill(in2_d2);

        in3_d   = _mem8_const((void*)&in[i+2*w]);
        in3_d2  = _mem8_const((void*)&in[i+2*w+2]);
        tmp3    = _loll(in3_d2);
        tmp4    = _hill(in3_d2);
        in3_l   = _loll(in3_d);
        in3_h   = _hill(in3_d);

        /* ---------------------------------------------------------------- */
        /*  Apply horizontal and vertical filter masks.  The final filter   */
        /*  output is the sum of the absolute values of these filters.      */
        /* ---------------------------------------------------------------- */
        // i00=in[i    ]; i01=in[i    +1]; i02=in[i    +2];
        // i10=in[i+  w];                  i12=in[i+  w+2];
        // i20=in[i+2*w]; i21=in[i+2*w+1]; i22=in[i+2*w+2];
        // H = -i00 - 2*i01 -   i02 +   i20 + 2*i21 + i22;
        // V = -i00 +   i02 - 2*i10 + 2*i12 -   i20 + i22;

        /* t1, t3, t5, t7 are the horizontal guys, two shorts per word */

        H    = _add2(_pack2(_dotpsu4(mult1_b,in1_l),_dotpsu4(mult1,in1_l)),
                     _pack2(_dotpsu4(mult2_b,in3_l),_dotpsu4(mult2,in3_l)));
        H3   = _add2(_pack2(_dotpsu4(mult1_b,tmp1), _dotpsu4(mult1,tmp1)),
                     _pack2(_dotpsu4(mult2_b,tmp3), _dotpsu4(mult2,tmp3)));
        H5   = _add2(_pack2(_dotpsu4(mult1_b,in1_h),_dotpsu4(mult1,in1_h)),
                     _pack2(_dotpsu4(mult2_b,in3_h),_dotpsu4(mult2,in3_h)));
        H7   = _add2(_pack2(_dotpsu4(mult1_b,tmp2), _dotpsu4(mult1,tmp2)),
                     _pack2(_dotpsu4(mult2_b,tmp4), _dotpsu4(mult2,tmp4)));


        t1 = _abs2(H);
        t3 = _abs2(H3);
        t5 = _abs2(H5);
        t7 = _abs2(H7);

        /* t2, t4, t6, t8 are the vertical guys, two shorts per word */

        b10= _mpyu4ll(in2_l2,0x2020202);
        b11= _mpyu4ll(in1_h, 0x1010101);
        b12= _mpyu4ll(in2_l, 0x2020202);
        b13= _mpyu4ll(in2_h, 0x2020202);
        b14= _mpyu4ll(in2_h2,0x2020202);
        b15= _mpyu4ll(tmp2,  0x1010101);

        b1 = _add2(_add2(_unpklu4(in1_l),_loll(b12)),_unpklu4(in3_l));
        b2 = _add2(_add2(_unpkhu4(in1_l),_hill(b12)),_unpkhu4(in3_l));
        b3 = _add2(_add2(_unpklu4(tmp1), _loll(b13)),_unpklu4(tmp3));
        b4 = _add2(_add2(_unpkhu4(tmp1), _hill(b13)),_unpkhu4(tmp3));
        b5 = _add2(_add2(_loll(b11),_loll(b10)),_unpklu4(in3_h));
        b6 = _add2(_add2(_hill(b11),_hill(b10)),_unpkhu4(in3_h));
        b7 = _add2(_add2(_loll(b15),_loll(b14)),_unpklu4(tmp4));
        b8 = _add2(_add2(_hill(b15),_hill(b14)),_unpkhu4(tmp4));

        t2 = _abs2(_sub2(b2,b1));
        t4 = _abs2(_sub2(b4,b3));
        t6 = _abs2(_sub2(b6,b5));
        t8 = _abs2(_sub2(b8,b7));

        t9 = _add2(t1,t2);
        t10= _add2(t3,t4);
        t11= _add2(t5,t6);
        t12= _add2(t7,t8);

        t13 = _spacku4(t10,t9);
        t14 = _spacku4(t12,t11);

#if defined OMAPL138 || defined OMAP3530
        if(i!=final_pass) /* the last 6 outputs must be stored seperately */
          _mem8((void *)&out[i+1]) = _itoll(t14,t13);
#else
        if(i!=final_pass)
        {
            int64_t toto=_itoll(t14,t13);
            memcpy(&out[i+1],&toto,8);
        }
#endif

    }
    /* clean up the last 6 "output" guys */
#if defined OMAPL138 || defined OMAP3530
    _mem4((void *)&out[i-7]) = t13;
#else
    memcpy(&out[i-7],&t13,4);
#endif
    out[i-3] = t14 & 0xff;
    out[i-2] = (t14>>8)&0xff;
}
