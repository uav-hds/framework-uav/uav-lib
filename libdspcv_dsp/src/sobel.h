//  created:    2012/01/18
//  filename:   sobel.h
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 6599
//
//  version:    $Id: $
//
//  purpose:    detecteur de Sobel
//
//
/*********************************************************************/

#ifndef SOBEL_H
#define SOBEL_H

#include "cxtypes.h"
#include <stdint.h>

#if defined OMAPL138 || defined OMAP3530

#else
#include "typedef.h" //a passer dans le wrapper.h une fois que les typdef seront plus utiles
#endif

void dspSobel(IplImage* img_src,IplImage* img_dst,int32_t dx, int32_t dy);

void IMG_sobel_3x3_8_x
(
    const uint8_t *restrict in,   /* Input image data   */
    uint8_t       *restrict out,  /* Output image data  */
    int16_t cols, int16_t rows              /* Image dimensions   */
);

void IMG_sobel_3x3_8_xy
(
    const uint8_t *restrict in,   /* Input image data   */
    uint8_t       *restrict out,  /* Output image data  */
    int16_t cols, int16_t rows              /* Image dimensions   */
);

#endif /*SOBEL_H*/
