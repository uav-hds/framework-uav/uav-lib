//  created:    2012/01/18
//  filename:   lkpyramid.h
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 6599
//
//  version:    $Id: $
//
//  purpose:    Lucas et Kanade pyramidal
//
//
/*********************************************************************/

#ifndef LKPYRAMID_H
#define LKPYRAMID_H

#include "cxtypes.h"
#include <stdint.h>

#if defined OMAPL138 || defined OMAP3530

#else
#include "typedef.h"//pour const et restrict, a deplacer
#endif

void dspCalcOpticalFlowPyrLK(IplImage* restrict img_A,IplImage* restrict img_B,IplImage* restrict pyr_A,IplImage* restrict pyr_B,
        CvPoint* restrict features_A,CvPoint* restrict features_B,int32_t count,CvSize winSize,
        int32_t level,int8_t* restrict feat_status,uint32_t* restrict error,CvTermCriteria criteria,int32_t flags);

inline void bilinear_interp(uint8_t* src,uint8_t* dst,uint32_t width,uint32_t height,uint8_t alpha_x,uint8_t alpha_y);

void conv_dxdy2
(
    const uint8_t *restrict line_in,   // Input image data
    int16_t       *restrict out_dx,  // Output image data
    int16_t       *restrict out_dy,  // Output image data
    int32_t       *restrict out_dx_dx,  // Output image data
    int32_t       *restrict out_dy_dy,  // Output image data
    int32_t       *restrict out_dx_dy,  // Output image data
    int16_t cols, int16_t rows             // Image dimensions
);

inline void calc_image_mismatch(uint8_t* imgA_interp,uint8_t* imgB,int32_t imgB_winx,int32_t imgB_winy,uint8_t winx,uint8_t winy,int16_t* cache_dx,int16_t* cache_dy,int32_t ux,int32_t uy,int32_t gx,int32_t gy,int32_t vx,int32_t vy,int32_t* bx,int32_t* by,int32_t start_x,int32_t stop_x,int32_t start_y,int32_t stop_y);
inline uint32_t calc_feature_error(uint8_t* imgA_interp,uint8_t* imgB,int32_t imgB_winx,int32_t imgB_winy,
                                uint8_t winx,uint8_t winy,
                                int32_t ux,int32_t uy,int32_t gx,int32_t gy,int32_t vx,int32_t vy,
                                int32_t start_x,int32_t stop_x,int32_t start_y,int32_t stop_y);

#endif /*LKPYRAMID_H*/
