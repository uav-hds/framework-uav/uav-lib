/*!
 * \file MetaDualShock3_impl.h
 * \brief Classe intégrant la manette DualShock3 et les consignes joystick
 * \author Guillaume Sanahuja, Copyright Heudiasyc UMR UTC/CNRS 7253
 * \date 2014/01/14
 * \version 3.4
 */

#ifndef METADUALSHOCK3_IMPL_H
#define METADUALSHOCK3_IMPL_H

#include <IODevice.h>

namespace framework
{
    namespace meta
    {
        class MetaDualShock3;
    }
    namespace filter
    {
        class JoyReference;
    }
}

/*! \class MetaDualShock3_impl
*
* \brief Classe intégrant la manette DualShock3 et les consignes joystick
*/
class MetaDualShock3_impl: public framework::core::IODevice
{
    public:
        MetaDualShock3_impl(framework::meta::MetaDualShock3* self,std::string name);
        ~MetaDualShock3_impl();
        framework::filter::JoyReference *joy_ref;

    private:
        void UpdateFrom(const framework::core::io_data *data);
        framework::meta::MetaDualShock3* self;
        bool joy_init;
        bool wasRollTrimUpButtonPressed,wasRollTrimDownButtonPressed,wasPitchTrimUpButtonPressed,wasPitchTrimDownButtonPressed;
};

#endif // METADUALSHOCK3_IMPL_H
