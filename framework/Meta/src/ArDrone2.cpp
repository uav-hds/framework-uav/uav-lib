//  created:    2014/06/10
//  filename:   ArDrone2.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    Class defining an ardrone2 uav
//
//
/*********************************************************************/

#include "ArDrone2.h"
#include <FrameworkManager.h>
#include <ParrotBldc.h>
#include <ParrotNavBoard.h>
#include <ParrotBatteryMonitor.h>
#include <ParrotCamV.h>
#include <Unix_SerialPort.h>
#include <X4X8Multiplex.h>
#include <AhrsComplementaryFilter.h>
#include <Tab.h>

using std::string;
using namespace framework::core;
using namespace framework::gui;
using namespace framework::sensor;
using namespace framework::filter;
using namespace framework::actuator;

namespace framework { namespace meta {

ArDrone2::ArDrone2(FrameworkManager* parent,string uav_name,filter::UavMultiplex *multiplex) : Uav(parent,uav_name,multiplex) {
    Unix_SerialPort* bldc_port=new Unix_SerialPort(parent,"bldc_port","/dev/ttyO0");
    Unix_SerialPort* nav_port=new Unix_SerialPort(parent,"bldc_port","/dev/ttyO1");

    if(multiplex==NULL) SetMultiplex(new X4X8Multiplex(parent,"x4-multiplex",X4X8Multiplex::X4));

    SetBldc(new ParrotBldc(GetUavMultiplex(),GetUavMultiplex()->GetLayout(),"parrot-bldc",bldc_port));
    navboard=new ParrotNavBoard(parent,"navboard",nav_port,70);
    //ahrs=new AhrsKalman(navboard->GetImu(),"parrot-ahrs");
    SetAhrs(new AhrsComplementaryFilter(navboard->GetImu(),"parrot-ahrs"));

    SetUsRangeFinder(navboard->GetUsRangeFinder());
    Tab* bat_tab=new Tab(parent->GetTabWidget(),"battery");
    SetBatteryMonitor(new ParrotBatteryMonitor(bat_tab->NewRow(),"battery"));

    SetVerticalCamera(new ParrotCamV(parent,"camv",50));
}

ArDrone2::~ArDrone2() {

}

void ArDrone2::StartSensors(void) {
    ((ParrotBatteryMonitor*)GetBatteryMonitor())->Start();
    navboard->Start();
    ((ParrotCamV *)GetVerticalCamera())->Start();
    Uav::StartSensors();
}

} // end namespace meta
} // end namespace framework
