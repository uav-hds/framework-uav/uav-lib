//  created:    2014/01/24
//  filename:   ParrotBatteryMonitor.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    Parrot battery monitor
//
//
/*********************************************************************/

#include "ParrotBatteryMonitor.h"
#include <Layout.h>
#include <LayoutPosition.h>
#include <Unix_I2cPort.h>

//0x49 slave bank
#define GPBR1 0x91

//0x4a slave bank
#define CTRL1 0x00
#define SW1SELECT_LSB 0x06
#define SW1AVERAGE_LSB 0x08
#define CTRL_SW1 0x12
#define GPCH0_LSB 0x37
#define GPCH0_MSB 0x38

using std::string;
using namespace framework::core;

namespace framework
{
namespace sensor
{

ParrotBatteryMonitor::ParrotBatteryMonitor(const gui::LayoutPosition* position,string name) : Thread(position->getLayout(),name,5),BatteryMonitor(position,name)
{
    uint8_t tx[2];

    port=new Unix_I2cPort((BatteryMonitor*)this,"i2c1","/dev/i2c-1");

    //slave 0x49 for clocks
    port->SetSlave(0x49);

    //GPBR1: set MADC_HFCLK_EN and DEFAULT_MADC_CLK_EN
    tx[0]=GPBR1;
    tx[1]=0x90;
    port->Write(tx,2);

    //slave 0x4a for madc
    port->SetSlave(0x4a);

    // Turn on MADC in CTRL1
    tx[0]=CTRL1;
    tx[1]=0x01;
    port->Write(tx,2);

    // Select ADCIN0 for conversion in SW1SELECT_LSB
    tx[0]=SW1SELECT_LSB;
    tx[1]=0x01;
    port->Write(tx,2);

    // Setup SW1AVERAGE_LSB register for averaging
    tx[0]=SW1AVERAGE_LSB;
    tx[1]=0x01;
    port->Write(tx,2);
}

ParrotBatteryMonitor::~ParrotBatteryMonitor()
{
    SafeStop();
    Join();
}

void ParrotBatteryMonitor::Run(void)
{
    unsigned char lsb, msb;
    int raw_voltage;
    uint8_t tx[2];
    uint8_t rx;

    SetPeriodMS(1000);

    while(!ToBeStopped())
    {
        // Start all channel conversion by setting bit 5 to one in CTRL_SW1
        tx[0]=CTRL_SW1;
        tx[1]=0x20;
        port->Write(tx,2);

        WaitPeriod();

        //check end of conversion in CTRL_SW1
        port->Write(tx,1);
        port->Read(&rx,1);
        if((rx&0x02)==0x02)
        {
            tx[0]=GPCH0_LSB;
            port->Write(tx,1);
            port->Read(&lsb,1);

            tx[0]=GPCH0_MSB;
            port->Write(tx,1);
            port->Read(&msb,1);

            raw_voltage = (lsb >> 6) | (msb << 2);
//Printf("bat %i %i %i %f\n",lsb,msb,raw_voltage,raw_voltage*0.013595166);
            //From paparazzi:
            // we know from spec sheet that ADCIN0 has no prescaler
            // so that the max voltage range is 1.5 volt
            // multiply by ten to get decivolts

            //from raw measurement we got quite a lineair response
            //9.0V=662, 9.5V=698, 10.0V=737,10.5V=774, 11.0V=811, 11.5V=848, 12.0V=886, 12.5V=923
            //leading to our 0.13595166 magic number for decivolts conversion
            SetBatteryValue(raw_voltage*0.013595166);
        }

    }
}

} // end namespace sensor
} // end namespace framewor
