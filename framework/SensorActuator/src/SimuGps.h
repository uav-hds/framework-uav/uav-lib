/*!
 * \file SimuGps.h
 * \brief Class for a simulation GPS
 * \author Guillaume Sanahuja, Copyright Heudiasyc UMR UTC/CNRS 7253
 * \date 2013/08/23
 * \version 4.0
 */

#ifndef SIMUGPS_H
#define SIMUGPS_H

#include <Thread.h>
#include <Gps.h>

namespace framework
{
    namespace core
    {
        class FrameworkManager;
    }
}

namespace framework
{
namespace sensor
{
    /*! \class SimuGps
    *
    * \brief Class for a simulation GPS
    */
    class SimuGps : public core::Thread, public Gps
    {
        public:
            /*!
            * \brief Constructor
            *
            * Construct a Novatel.
            *
            * \param parent parent
            * \param name name
            * \param NMEAFlags NMEA sentances to enable
            * \param priority priority of the Thread
            */
            SimuGps(const core::FrameworkManager* parent,std::string name,Gps::NMEAFlags_t NMEAFlags,uint8_t priority);

            /*!
            * \brief Destructor
            *
            */
            ~SimuGps();

        private:
            /*!
            * \brief Update using provided datas
            *
            * Reimplemented from IODevice.
            *
            * \param data data from the parent to process
            */
            void UpdateFrom(const core::io_data *data){};

            /*!
            * \brief Run function
            *
            * Reimplemented from Thread.
            *
            */
            void Run(void);
    };
} // end namespace sensor
} // end namespace framewor
#endif // SIMUGPS_H
