//  created:    2013/12/19
//  filename:   ParrotBldc.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    objet integrant les moteurs ardrone
//
//
/*********************************************************************/

#include "ParrotBldc.h"
#include "ParrotBldc_impl.h"

using std::string;
using namespace framework::core;
using namespace framework::gui;

namespace framework
{
namespace actuator
{

ParrotBldc::ParrotBldc(const IODevice* parent,Layout* layout,string name,SerialPort* serialport) : Bldc(parent,layout,name,4)
{
     pimpl_=new ParrotBldc_impl(this,serialport);
}

ParrotBldc::~ParrotBldc()
{
    delete pimpl_;
}

void ParrotBldc::SetMotors(float* value)
{
    pimpl_->SetMotors(value);
}

} // end namespace actuator
} // end namespace framework
