//  created:    2013/04/03
//  filename:   VrpnObject_impl.h
//
//  author:     César Richard, Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    objet vrpn
//
//
/*********************************************************************/

#ifndef VRPNOBJECT_IMPL_H
#define VRPNOBJECT_IMPL_H

#include <IODevice.h>
#include <string>
#include <stdint.h>
#include <vrpn_Tracker.h>
#include "Quaternion.h"

namespace framework
{
    namespace core
    {
        class cvmatrix;
        class Vector3D;
        class Euler;
    }
    namespace gui
    {
        class TabWidget;
        class Tab;
        class DataPlot1D;
    }
    namespace sensor
    {
        class VrpnClient;
        class VrpnObject;
    }
}

class VrpnObject_impl
{
    friend class VrpnClient_impl;

    public:
        VrpnObject_impl(framework::sensor::VrpnObject* self,const framework::sensor::VrpnClient *parent,std::string name,int id,const framework::gui::TabWidget* tab);
        ~VrpnObject_impl(void);

        void mainloop(void);
        void GetEuler(framework::core::Euler &euler);
        void GetQuaternion(framework::core::Quaternion &quaternion);
        void GetPosition(framework::core::Vector3D &point);
        bool IsTracked(unsigned int timeout_ms);

        framework::gui::Tab* plot_tab;
        framework::gui::DataPlot1D* x_plot;
        framework::gui::DataPlot1D* y_plot;
        framework::gui::DataPlot1D* z_plot;
        framework::core::cvmatrix *output,*state;

        static void	VRPN_CALLBACK handle_pos(void *userdata, const vrpn_TRACKERCB t);

    private:
        framework::sensor::VrpnObject* self;
        const framework::sensor::VrpnClient *parent;
        vrpn_Tracker_Remote* tracker;
        framework::core::Quaternion quaternion;//todo: quaternion should be included in the output to replace euler angles
        void Update(void);
};

#endif // VRPNOBJECT_IMPL_H
