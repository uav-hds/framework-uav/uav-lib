//  created:    2013/04/03
//  filename:   VrpnClient_impl.h
//
//  author:     César Richard, Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    objet se connectant au serveur vrpn
//
//
/*********************************************************************/

#ifndef VRPNCLIENT_IMPL_H
#define VRPNCLIENT_IMPL_H

#include <string>
#include <vector>

namespace framework
{
    namespace core
    {
        class OneAxisRotation;
        class Vector3D;
        class Quaternion;
        class Mutex;
        class SerialPort;
    }
    namespace gui
    {
        class TabWidget;
        class Tab;
        class Layout;
    }
    namespace sensor
    {
        class VrpnClient;
        class VrpnObject;
    }
}

class VrpnObject_impl;
class vrpn_Connection;

class VrpnClient_impl
{
    public:
        VrpnClient_impl(framework::sensor::VrpnClient* self,std::string name,std::string address,uint16_t us_period);
        VrpnClient_impl(framework::sensor::VrpnClient* self,std::string name,framework::core::SerialPort* serialport,uint16_t us_period);
        ~VrpnClient_impl();
        void AddTrackable(framework::sensor::VrpnObject* obj);//normal
        void RemoveTrackable(framework::sensor::VrpnObject* obj);//normal
        void AddTrackable(VrpnObject_impl* obj,uint8_t id);//xbee
        void RemoveTrackable(VrpnObject_impl* obj);//xbee
        void ComputeRotations(framework::core::Vector3D& point);
        void ComputeRotations(framework::core::Quaternion& quat);
        bool UseXbee(void);
        void Run(void);
        framework::gui::Tab* setup_tab;
        framework::gui::TabWidget* tab;
        vrpn_Connection *connection;

    private:
        framework::sensor::VrpnClient* self;
        framework::core::Mutex* mutex;
        uint16_t us_period;
        std::vector<framework::sensor::VrpnObject*> trackables;
        typedef struct xbee_object{
            VrpnObject_impl* vrpnobject;
            uint8_t id;
        } xbee_object;

        std::vector<xbee_object> xbee_objects;
        framework::gui::Tab* main_tab;
        framework::core::OneAxisRotation *rotation_1,*rotation_2;
        framework::core::SerialPort* serialport;
};

#endif // VRPNCLIENT_IMPL_H
