//  created:    2013/12/19
//  filename:   ParrotBldc_impl.h
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    Classe integrant les moteurs ardrone
//
//
/*********************************************************************/


#ifndef PARROTBLDC_IMPL_H
#define PARROTBLDC_IMPL_H

#include <IODevice.h>
#include <stdint.h>

namespace framework
{
    namespace core
    {
        class SerialPort;
    }
    namespace actuator
    {
        class ParrotBldc;
    }
}

class ParrotBldc_impl
{

    public:
        ParrotBldc_impl(framework::actuator::ParrotBldc* self,framework::core::SerialPort* serialport);
        ~ParrotBldc_impl();
        void UpdateFrom(framework::core::io_data *data);
        void SetMotors(float* value);

    private:
        typedef struct gpio_data {
          int pin;
          int value;
        } gpio_data;
        enum gpio_mode {
            GPIO_INPUT = 0, //!< Pin configured for input
            GPIO_OUTPUT_LOW, //!< Pin configured for output with low level
            GPIO_OUTPUT_HIGH, //!< Pin configured for output with high level
        };

        typedef struct gpio_direction {
            int pin;
            enum gpio_mode mode;
        } gpio_direction;

        uint8_t Sat(float value,uint8_t min,uint8_t max);
        void StartTest(void);
        void StopTest(void);
        void reset_bldc(void);
        void gpio_setup_output(uint16_t pin);
        void gpio_setup_input(uint16_t pin);
        void gpio_clear(uint16_t pin);
        void gpio_set(uint16_t pin);
        int cmd(uint8_t cmd, uint8_t *reply, int replylen);
        void set_leds(uint8_t led0, uint8_t led1, uint8_t led2, uint8_t led3);

        framework::actuator::ParrotBldc* self;
        framework::core::SerialPort* serialport;
        bool is_reseted;
        int gpiofp;
};

#endif // PARROTBLDC_IMPL_H
