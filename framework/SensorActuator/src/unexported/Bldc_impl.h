//  created:    2013/11/14
//  filename:   Bldc_impl.h
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    Virtual class for brushless drivers
//
//
/*********************************************************************/

#ifndef BLDC_IMPL_H
#define BLDC_IMPL_H

#include <IODevice.h>
#include <stdint.h>

namespace framework
{
    namespace gui
    {
        class DoubleSpinBox;
        class Layout;
        class Label;
        class DataPlot1D;
        class TabWidget;
        class PushButton;
    }
    namespace actuator
    {
        class Bldc;
    }
}

class Bldc_impl
{
    public:
        Bldc_impl(framework::actuator::Bldc* self,framework::gui::Layout* layout,std::string name,uint8_t motors_count);
        Bldc_impl(framework::actuator::Bldc* self,uint8_t motors_count);//simulation
        ~Bldc_impl();
        void UpdateFrom(const framework::core::io_data *data);
        void LockUserInterface(void) const;
        void UnlockUserInterface(void) const;
        bool are_enabled;
        float* power;
        void UseDefaultPlot(framework::gui::TabWidget* tab);
        uint8_t motors_count;
        framework::gui::Layout* layout;

    private:
        float *values;
        float Sat(float value);
        framework::actuator::Bldc* self;
        framework::gui::DoubleSpinBox *min_value,*max_value,*test_value;
        framework::gui::Label *flight_time;
        framework::gui::DataPlot1D *plots;
        framework::core::Time flight_start_time;
        framework::gui::PushButton **button_test;
        int time_sec;
        bool is_running;
        framework::core::Time test_start_time;
        int tested_motor;//=-1 if no motor is tested
};

#endif // BLDC_IMPL_H
