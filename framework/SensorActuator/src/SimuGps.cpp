//  created:    2014/05/26
//  filename:   SimuGps.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    Class for a simulation GPS
//
//
/*********************************************************************/

#include "SimuGps.h"
#include <FrameworkManager.h>
#include <string.h>
#include <GeoCoordinate.h>

using std::string;
using namespace framework::core;

namespace framework
{
namespace sensor
{

SimuGps::SimuGps(const FrameworkManager* parent,string name,Gps::NMEAFlags_t NMEAFlags,uint8_t priority) : Thread(parent,name,priority), Gps(parent,name,NMEAFlags)
{
}

SimuGps::~SimuGps()
{
    SafeStop();
    Join();
}

void SimuGps::Run(void)
{
    char response[200] = {0};
    int size,result;
//double lat=0;
    SetPeriodMS(500);
    WarnUponSwitches(true);

    while(!ToBeStopped())
    {
        WaitPeriod();
        position->SetCoordinates(49.402313, 2.795463,0);
  //      lat+=.5;
    }


    WarnUponSwitches(false);
}


} // end namespace sensor
} // end namespace framewor
