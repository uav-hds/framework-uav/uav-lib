//  created:    2013/12/19
//  filename:   ParrotBldc_impl.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    Classe integrant les moteurs ardrone
//
//
/*********************************************************************/
#include "ParrotBldc_impl.h"
#include "ParrotBldc.h"
#include <SerialPort.h>
#include <cvmatrix.h>
#include <string.h>

#include <unistd.h>  /* UNIX standard function definitions */
#include <fcntl.h>   /* File control definitions */
#include <sys/ioctl.h>

#define MAX_VALUE 0x1ff

#define MOT_LEDOFF 0
#define MOT_LEDRED 1
#define MOT_LEDGREEN 2
#define MOT_LEDORANGE 3

#define GPIO_PIN_MOTOR1                        171
#define GPIO_PIN_MOTOR2                        172
#define GPIO_PIN_MOTOR3                        173
#define GPIO_PIN_MOTOR4                        174

#define GPIO_PIN_IRQ_FLIPFLOP                   175
#define GPIO_PIN_IRQ_INPUT                      176
#define MB_GREEN_LED    180
#define MB_RED_LED    181

#define GPIO_MAGIC 'p'
#define GPIO_DIRECTION _IOW(GPIO_MAGIC, 0, gpio_direction)
#define GPIO_READ _IOWR(GPIO_MAGIC, 1, gpio_data)
#define GPIO_WRITE _IOW(GPIO_MAGIC, 2, gpio_data)

using namespace framework::core;
using namespace framework::gui;
using namespace framework::actuator;

ParrotBldc_impl::ParrotBldc_impl(ParrotBldc* self,SerialPort* serialport) {
    this->self=self;
    this->serialport=serialport;

    gpiofp=open("/dev/gpio",O_RDWR);
    if (gpiofp <= 0) self->Err("/dev/gpio open error\n");

    gpio_setup_output(MB_GREEN_LED);
    gpio_setup_output(MB_RED_LED);
    gpio_clear(MB_RED_LED);
    gpio_set(MB_GREEN_LED);

    gpio_setup_output(GPIO_PIN_MOTOR1);
    gpio_setup_output(GPIO_PIN_MOTOR2);
    gpio_setup_output(GPIO_PIN_MOTOR3);
    gpio_setup_output(GPIO_PIN_MOTOR4);
    gpio_setup_output(GPIO_PIN_IRQ_FLIPFLOP);
    gpio_setup_input(GPIO_PIN_IRQ_INPUT);

    reset_bldc();
}

ParrotBldc_impl::~ParrotBldc_impl()
{
    gpio_clear(MB_GREEN_LED);
    gpio_set(MB_RED_LED);
    set_leds(MOT_LEDRED,MOT_LEDRED, MOT_LEDRED, MOT_LEDRED);

    close(gpiofp);
}

void ParrotBldc_impl::reset_bldc()
{
    gpio_clear(GPIO_PIN_IRQ_FLIPFLOP);
    usleep(20000);
    gpio_set(GPIO_PIN_IRQ_FLIPFLOP);

    //all select lines inactive
    gpio_clear(GPIO_PIN_MOTOR1);
    gpio_clear(GPIO_PIN_MOTOR2);
    gpio_clear(GPIO_PIN_MOTOR3);
    gpio_clear(GPIO_PIN_MOTOR4);

    //configure motors
    uint8_t reply[121];
    for(int m=0;m<4;m++) {
        gpio_set(GPIO_PIN_MOTOR1 + m);

        cmd(0xe0,reply,2);
        if(reply[1]==0x50) {
            self->Warn("Init motor %i\n",m);
            cmd(0x91,reply,121);
            cmd(0xa1,reply,2);
            cmd(m+1,reply,1);
            usleep(200000);//wait before reconfiguring motor, otherwise it does not reply
            cmd(0xe0,reply,2);
        }
        if(reply[1]!=0x00) {
            self->Err("Init motor error %i\n",m);
        }

        cmd(m+1,reply,1);
        gpio_clear(GPIO_PIN_MOTOR1 + m);
    }

    //all select lines active
    gpio_set(GPIO_PIN_MOTOR1);
    gpio_set(GPIO_PIN_MOTOR2);
    gpio_set(GPIO_PIN_MOTOR3);
    gpio_set(GPIO_PIN_MOTOR4);

    //start multicast
    for(int i=0;i<6;i++) cmd(0xa0,reply,1);

    //reset IRQ flipflop - on error 176 reads 1, this code resets 176 to 0
    gpio_clear(GPIO_PIN_IRQ_FLIPFLOP);
    gpio_set(GPIO_PIN_IRQ_FLIPFLOP);

    usleep(200000);//wait, otherwise leds stay red when motor is stopped
    set_leds(MOT_LEDGREEN,MOT_LEDGREEN, MOT_LEDGREEN, MOT_LEDGREEN);

    is_reseted=true;
}

void ParrotBldc_impl::gpio_setup_input(uint16_t pin)
{
    gpio_direction dir;

    // set the GPIO direction
    dir.pin = pin;
    dir.mode = GPIO_INPUT;
    if(ioctl(gpiofp, GPIO_DIRECTION, &dir)<0) self->Err("ioctl error\n");
}

void ParrotBldc_impl::gpio_setup_output(uint16_t pin)
{
    gpio_direction dir;

    // set the GPIO direction
    dir.pin = pin;
    dir.mode = GPIO_OUTPUT_LOW;
    if(ioctl(gpiofp, GPIO_DIRECTION, &dir)<0) self->Err("ioctl error\n");
}

void ParrotBldc_impl::gpio_set(uint16_t pin) {
    gpio_data data;

    // set the GPIO value
    data.pin = pin;
    data.value = 1;
    if(ioctl(gpiofp, GPIO_WRITE, &data)<0) self->Err("ioctl error\n");
}


void ParrotBldc_impl::gpio_clear(uint16_t pin) {
    gpio_data data;

    // Read the GPIO value
    data.pin = pin;
    data.value = 0;
    if(ioctl(gpiofp, GPIO_WRITE, &data)<0) self->Err("ioctl error\n");
}

void ParrotBldc_impl::SetMotors(float* value) {
    uint8_t cmd[5];
    uint8_t rep[5];
    uint16_t mot_value[4];

    for(int i=0;i<4;i++) mot_value[i]=(uint16_t)(MAX_VALUE*value[i]);
    
//for(int i=0;i<4;i++) Printf("mot %i ",mot_value[i]);
//Printf("\n");
    if(mot_value[0]!=0 || mot_value[1]!=0 || mot_value[2]!=0 || mot_value[3]!=0)
    {

        cmd[0] = 0x20 | ((mot_value[0]&0x1ff)>>4);
        cmd[1] = ((mot_value[0]&0x1ff)<<4) | ((mot_value[1]&0x1ff)>>5);
        cmd[2] = ((mot_value[1]&0x1ff)<<3) | ((mot_value[3]&0x1ff)>>6);
        cmd[3] = ((mot_value[3]&0x1ff)<<2) | ((mot_value[2]&0x1ff)>>7);
        cmd[4] = ((mot_value[2]&0x1ff)<<1);
        ssize_t written=serialport->Write(cmd, sizeof(cmd));
//for(int i=0;i<5;i++) Printf("cmd %x ",cmd[i]);
//Printf("\n");
        if(written<0)
        {
            self->Err("write error (%s)\n",strerror(-written));
        }
        else if (written != sizeof(cmd))
        {
            self->Err("write error %i/%i\n",written,sizeof(cmd));
        }

        ssize_t read=serialport->Read(rep, sizeof(rep));
        if(read<0)
        {
            self->Err("read error (%s)\n",strerror(-read));
        }
        else if (read != sizeof(rep))
        {
            self->Err("read error %i/%i\n",read,sizeof(rep));
        }
        for(int i=0;i<5;i++) {
            if(rep[i]!=cmd[i]) {
                self->Warn("wrong response\n");
                for(int i=0;i<5;i++) Printf("%x %x",cmd[i],rep[i]);
                Printf("\n");
            }
        }
//for(int i=0;i<5;i++) Printf("rep %x ",rep[i]);
//Printf("\n");
        is_reseted=false;

    }
    else
    {
        if(is_reseted==false) reset_bldc();
    }
}

uint8_t ParrotBldc_impl::Sat(float value,uint8_t min,uint8_t max)
{
    uint8_t sat_value=(uint8_t)value;

    if(value>((float)sat_value+0.5)) sat_value++;

    if(value<(float)min) sat_value=min;
    if(value>(float)max) sat_value=max;

    return sat_value;
}

int ParrotBldc_impl::cmd(uint8_t cmd, uint8_t *reply, int replylen) {
    ssize_t written=serialport->Write(&cmd, 1);
    if(written<0) {
        self->Err("write error (%s)\n",strerror(-written));
    } else if (written != 1) {
        self->Err("write error %i/1\n",written);
    }

    return serialport->Read(reply, replylen);
}

void ParrotBldc_impl::set_leds(uint8_t led0, uint8_t led1, uint8_t led2, uint8_t led3) {
    uint8_t cmd[2];

    led0 &= 0x03;
    led1 &= 0x03;
    led2 &= 0x03;
    led3 &= 0x03;

    //printf("LEDS: %d %d %d %d \n", led0, led1, led2, led3);

    cmd[0]=0x60 | ((led0&1)<<4) | ((led1&1)<<3) | ((led2&1)<<2) | ((led3&1) <<1);
    cmd[1]=((led0&2)<<3) | ((led1&2)<<2) | ((led2&2)<<1) | ((led3&2)<<0);

    ssize_t written=serialport->Write(cmd, sizeof(cmd));
    if(written<0) {
        self->Err("write error (%s)\n",strerror(-written));
    } else
        if (written != sizeof(cmd)) {
            self->Err("write error %i/%i\n",written,sizeof(cmd));
        }

    serialport->Read(cmd, sizeof(cmd));//flush
}
