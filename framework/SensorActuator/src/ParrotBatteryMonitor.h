/*!
 * \file ParrotBatteryMonitor.h
 * \brief Parrot battery monitor
 * \author Guillaume Sanahuja, Copyright Heudiasyc UMR UTC/CNRS 7253
 * \date 2014/01/24
 * \version 4.0
 */

#ifndef PARROTBATTERYMONITOR_H
#define PARROTBATTERYMONITOR_H

#include <BatteryMonitor.h>
#include <Thread.h>

namespace framework
{
namespace gui
{
    class Layout;
}
namespace core
{
    class Unix_I2cPort;
}

namespace sensor
{
    /*! \class ParrotBatteryMonitor
    *
    * \brief Parrot battery monitor
    */
    class ParrotBatteryMonitor : public core::Thread,public BatteryMonitor
    {
        public:
            /*!
            * \brief Constructor
            *
            * Construct a ParrotBatteryMonitor at given position.
            *
            * \param position position
            * \param name name
            */
            ParrotBatteryMonitor(const gui::LayoutPosition* position,std::string name);

            /*!
            * \brief Destructor
            *
            */
            ~ParrotBatteryMonitor();

        private:
            /*!
            * \brief Run function
            *
            * Reimplemented from Thread.
            *
            */
            void Run(void);

            core::Unix_I2cPort* port;
    };
}; // end namespace sensor
}; // end namespace framework
#endif // PARROTBATTERYMONITOR_H
