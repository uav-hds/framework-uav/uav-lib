//  created:    2012/01/19
//  filename:   Ps3Eye.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    objet integrant la camera Ps3Eye
//
//
/*********************************************************************/

#include "Ps3Eye.h"
#include <GroupBox.h>
#include <DoubleSpinBox.h>
#include <CheckBox.h>
#include <GridLayout.h>
#include <cvimage.h>
#include <FrameworkManager.h>
#include <linux/videodev2.h>
#include <dspcv_gpp.h>
#include <stdio.h>

using std::string;
using namespace framework::core;
using namespace framework::gui;

namespace framework
{
namespace sensor
{

Ps3Eye::Ps3Eye(const FrameworkManager* parent,string name,int camera_index,uint8_t priority)
    : V4LCamera(parent,name,camera_index,320,240,cvimage::Type::Format::YUYV,priority) {

}

Ps3Eye::~Ps3Eye() {
}

} // end namespace sensor
} // end namespace framework
