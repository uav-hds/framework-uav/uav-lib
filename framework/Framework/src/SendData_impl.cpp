//  created:    2012/03/07
//  filename:   SendData.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    Base class for sending datas to ground station
//
//
/*********************************************************************/

#include "SendData_impl.h"

SendData_impl::SendData_impl()
{
}

SendData_impl::~SendData_impl()
{
}
