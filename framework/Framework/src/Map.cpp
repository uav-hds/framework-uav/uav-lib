//  created:    2013/07/23
//  filename:   Map.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    Class displaying a GPS map on the ground station
//
//
/*********************************************************************/

#include "Map.h"
#include "LayoutPosition.h"
#include "Layout.h"
#include "GeoCoordinate.h"
#include "FrameworkManager.h"
#include <string.h>
#include <sstream>

using std::string;
using std::ostringstream;

namespace framework
{
namespace gui
{

using namespace core;

Map::Map(const LayoutPosition* position,string name):SendData(position,name,"Map",1000) {
    size_t i=0;
    while(1) {
        double latitude,longitude;
        double altitude=0;
        ostringstream lat_prop,long_prop,alt_prop;
        lat_prop << "lat" << i;
        long_prop << "long" << i;
        alt_prop << "alt" << i;
        if(GetPersistentXmlProp(lat_prop.str(),latitude) && GetPersistentXmlProp(long_prop.str(),longitude)) {
            SetVolatileXmlProp(lat_prop.str(),latitude);
            SetVolatileXmlProp(long_prop.str(),longitude);
            if(GetPersistentXmlProp(alt_prop.str(),altitude)) SetVolatileXmlProp(alt_prop.str(),altitude);
            GeoCoordinate *checkpoint=new GeoCoordinate(this,"checkpoint",latitude,longitude,altitude);
            checkpoints.push_back(checkpoint);
        } else {
            break;
        }
        i++;
    }
    for(size_t i=0;i<checkpoints.size();i++) {
        double latitude,longitude,altitude;
        checkpoints.at(i)->GetCoordinates(&latitude,&longitude,&altitude);
        printf("%i %f %f\n",i,latitude,longitude);
    }

    SendXml();
/*
    //update value from xml file
    XmlEvent(XmlFileNode());
    if(checkpoints_node.size()!=0) SendXml();//pour les checkpoints

    //on enleve les checkpoints du xml
    for(size_t i=0;i<checkpoints_node.size();i++)
    {
        xmlUnlinkNode(checkpoints_node.at(i));
        xmlFreeNode(checkpoints_node.at(i));
    }*/
}

Map::~Map() {

}

void Map::ExtraXmlEvent(void) {

//attention pas rt safe (creation checkpoint)
    size_t i=0;
    while(1) {
        printf("test %i\n",i);
        double latitude,longitude;
        double altitude=0;
        ostringstream lat_prop,long_prop,alt_prop;
        lat_prop << "lat" << i;
        long_prop << "long" << i;
        alt_prop << "alt" << i;
        if(GetPersistentXmlProp(lat_prop.str(),latitude) && GetPersistentXmlProp(long_prop.str(),longitude)) {
            GetPersistentXmlProp(alt_prop.str(),altitude);
            if(i>=checkpoints.size()) {
                GeoCoordinate *checkpoint=new GeoCoordinate(this,"checkpoint",latitude,longitude,altitude);
                checkpoints.push_back(checkpoint);
                printf("add %i\n",i);
            } else {
                checkpoints.at(i)->SetCoordinates(latitude,longitude,altitude);
            }
        } else {
            if(i==checkpoints.size()) break;
        }
        i++;
    }

    for(size_t i=0;i<checkpoints.size();i++) {
        double latitude,longitude,altitude;
        checkpoints.at(i)->GetCoordinates(&latitude,&longitude,&altitude);
        printf("%i %f %f\n",i,latitude,longitude);
    }
}

void Map::AddPoint(const GeoCoordinate* point,string name) {
    SetVolatileXmlProp("point",name);
    SendXml();

    to_draw.push_back(point);
    SetSendSize(to_draw.size()*3*sizeof(double));//lat,long,alt
}

void Map::CopyDatas(char* buf) const {
    for(size_t i=0;i<to_draw.size();i++) {
        double latitude,longitude,altitude;
        to_draw.at(i)->GetCoordinates(&latitude,&longitude,&altitude);
        memcpy(buf+i*3*sizeof(double),&latitude,sizeof(double));
        memcpy(buf+sizeof(double)+i*3*sizeof(double),&longitude,sizeof(double));
        memcpy(buf+2*sizeof(double)+i*3*sizeof(double),&altitude,sizeof(double));
    }
}

} // end namespace gui
} // end namespace framework
