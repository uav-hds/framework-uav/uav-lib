/*!
 * \file OneAxisRotation_impl.h
 * \brief Classe définissant une rotation autour d'un axe
 * \author Guillaume Sanahuja, Copyright Heudiasyc UMR UTC/CNRS 7253
 * \date 2013/04/17
 * \version 4.0
 */

#ifndef ONEAXISROTATION_IMPL_H
#define ONEAXISROTATION_IMPL_H

namespace framework
{
    namespace core
    {
        class Vector3D;
        class Euler;
        class Quaternion;
        class RotationMatrix;
    }
    namespace gui
    {
        class GroupBox;
        class ComboBox;
        class DoubleSpinBox;
    }
}

/*! \class OneAxisRotation_impl
* \brief Classe définissant une rotation autour d'un axe
*
*/
class OneAxisRotation_impl
{
    public:
        OneAxisRotation_impl(framework::gui::GroupBox* box);
        ~OneAxisRotation_impl();
        void ComputeRotation(framework::core::Vector3D& point) const;
        void ComputeRotation(framework::core::Quaternion& quat) const;
        void ComputeRotation(framework::core::RotationMatrix& matrix) const;
        void ComputeRotation(framework::core::Euler& euler) const;

    private:
        framework::gui::ComboBox *rot_axe;
        framework::gui::DoubleSpinBox* rot_value;
};

#endif // ONEAXISROTATION_IMPL_H
