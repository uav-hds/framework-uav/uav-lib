//  created:    2013/05/07
//  filename:   Object.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    classe pour object_impl
//
//
/*********************************************************************/

#ifndef OBJECT_IMPL_H
#define OBJECT_IMPL_H

class Object_impl
{
    public:
        Object_impl(const framework::core::Object* self,const framework::core::Object* parent=NULL,std::string name="",std::string type="");
        ~Object_impl();
        std::string name,type;
        std::vector<const framework::core::Object*> childs;
        std::vector<const framework::core::Object*> type_childs;
        void AddChild(const framework::core::Object* child);
        void RemoveChild(const framework::core::Object* child);
        bool ErrorOccured(bool recursive);
        bool error_occured;
        const framework::core::Object* parent;

    private:
        const framework::core::Object* self;
};

#endif // OBJECT_IMPL_H
