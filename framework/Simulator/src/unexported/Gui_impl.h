//  created:    2013/03/27
//  filename:   Gui_impl.h
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    classe definissant une Gui
//
/*********************************************************************/

#ifndef GUI_IMPL_H
#define GUI_IMPL_H

#include <string>
#include <vector>
#include <kernel/io_hdfile.h>
#include <kernel/err.h>
#include <EDriverTypes.h>
#include <vector3d.h>

namespace irr
{
    class IrrlichtDevice;
    namespace scene
    {
        class ISceneManager;
        class IMeshSceneNode;
        class ITriangleSelector;
    }
    namespace video
    {
        class IVideoDriver;
    }
    namespace gui
    {
        class IGUIFont;
        class IGUIEnvironment;
    }
}

namespace framework
{
    namespace core
    {
        class Object;
    }
    namespace simulator
    {
        class Model;
        class GenericObject;
        class Gui;
    }
}

class MyEventReceiver;

class Gui_impl
{
    public:
        Gui_impl(framework::simulator::Gui* self,int app_width, int app_height,int scene_width, int scene_height,std::string media_path,irr::video::E_DRIVER_TYPE driver_type=irr::video::EDT_OPENGL);
        ~Gui_impl();
        void RunGui(std::vector<framework::simulator::Model*> modeles,std::vector<framework::simulator::GenericObject*> objects);
        void setMesh(std::string file,irr::core::vector3df position = irr::core::vector3df(0,0,0),irr::core::vector3df rotation= irr::core::vector3df(0,0,0),irr::core::vector3df scale= irr::core::vector3df(1,1,1));

        irr::scene::IMeshSceneNode* node;
        irr::IrrlichtDevice *device;
        std::string media_path;
        irr::video::IVideoDriver* driver;
        irr::scene::ISceneManager* smgr;
        int scene_width,scene_height;

    private:
        MyEventReceiver *receiver;
        irr::scene::ITriangleSelector* selector;
        irr::gui::IGUIFont* font;
        irr::gui::IGUIEnvironment* env;
        void setWindowCaption(framework::core::Object* object, int fps);
        void takeScreenshot(void);
        hdfile_t *dbtFile_r,*dbtFile_w;
        size_t dbtSize(std::vector<framework::simulator::Model*> modeles);
        char* dbtbuf;
        framework::simulator::Gui *self;
};

#endif // GUI_IMPL_H
