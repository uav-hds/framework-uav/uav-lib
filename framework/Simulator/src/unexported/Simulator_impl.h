//  created:    2013/03/25
//  filename:   Simulator_impl.h
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    classe de base du simulateur
//
/*********************************************************************/

#ifndef SIMULATOR_IMPL_H
#define SIMULATOR_IMPL_H

#include <vrpn_Connection.h>
#include <Thread.h>


namespace framework
{
    namespace simulator
    {
        class Simulator;
        class Model;
        class GenericObject;
    }
}

class Simulator_impl: public vrpn_Connection_IP, private framework::core::Thread
{
    friend class framework::simulator::Model;
    friend class framework::simulator::GenericObject;

    public:
        Simulator_impl(framework::simulator::Simulator* self,int optitrack_mstime=10,float yaw_deg=30);
        ~Simulator_impl();

        void RunSimu(void);
        float yaw_rad;

    private:
        void Run(void);
        framework::simulator::Simulator* self;
        std::vector<framework::simulator::Model*> models;
        std::vector<framework::simulator::GenericObject*> objects;
        int optitrack_mstime;
};

#endif // SIMULATOR_IMPL_H
