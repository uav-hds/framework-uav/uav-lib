//  created:    2012/08/21
//  filename:   AnimPoursuite.h
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    classe definissant une animation poursuite pour camera
//
/*********************************************************************/

#ifndef ANIMPOURSUITE_H
#define ANIMPOURSUITE_H

#include <ISceneNodeAnimator.h>
#include <position2d.h>
#include <vector3d.h>

namespace framework
{
namespace simulator
{

    class AnimPoursuite : public irr::scene::ISceneNodeAnimator
    {
        public:
            AnimPoursuite(const irr::scene::ISceneNode* parent,float rotateSpeed = -500.0f, float zoomSpeed = 4.0f);
            ~AnimPoursuite();

            void animateNode(irr::scene::ISceneNode* node, irr::u32 timeMs);
            ISceneNodeAnimator* createClone(irr::scene::ISceneNode* node,irr::scene::ISceneManager* newManager=0);
            bool MouseMoved(const irr::SEvent& event,irr::core::position2df MousePos);
            void setPositionOffset(irr::core::vector3df newpos);
            void setTargetOffset(irr::core::vector3df newpos);

        private:
            virtual bool isEventReceiverEnabled(void) const
            {
                return true;
            }

            irr::core::vector3df pos_offset,target_offset;
            bool LMouseKey;
            irr::core::position2df RotateStart;
            irr::core::position2df MousePos;
            const irr::scene::ISceneNode* parent;
            bool Rotating;
            float RotY,RotZ;
            float rotateSpeed;
            float zoomSpeed;
            float currentZoom;
            float sat(float value);
    };

} // end namespace simulator
} // end namespace framework

#endif // ANIMPOURSUITE_H
