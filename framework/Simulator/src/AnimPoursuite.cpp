//  created:    2012/08/21
//  filename:   AnimPoursuite.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    classe definissant une animation poursuite pour camera
//
/*********************************************************************/
#ifdef GL

#include "AnimPoursuite.h"
#include "Simulator.h"
#include "Model.h"
#include "Model_impl.h"
#include <ICursorControl.h>
#include <ICameraSceneNode.h>

using namespace irr;
using namespace gui;
using namespace core;
using namespace scene;

namespace framework
{
namespace simulator
{

AnimPoursuite::AnimPoursuite(const ISceneNode* parent,float rotateSpeed , float zoomSpeed )
{
    this->parent=parent;
    this->zoomSpeed=zoomSpeed;
    this->rotateSpeed=rotateSpeed;
    currentZoom=100;
    RotY=20;
    RotZ=0;
    Rotating=false;
    LMouseKey= false;
}

AnimPoursuite::~AnimPoursuite()
{

}

void AnimPoursuite::setPositionOffset(vector3df newpos)
{
    pos_offset=newpos;
}

void AnimPoursuite::setTargetOffset(vector3df newpos)
{
    target_offset=newpos;
}

float AnimPoursuite::sat(float value)
{
    if(value>89) value=89;
    if(value<-89) value=-89;
    return value;
}

void AnimPoursuite::animateNode(ISceneNode* node, u32 timeMs)
{
    ICameraSceneNode* camera = static_cast<ICameraSceneNode*>(node);
    vector3df pos;

    float nRotY = RotY;
    float nRotZ = RotZ;

    if (LMouseKey==true)
    {
        if (!Rotating)
        {
            RotateStart = MousePos;
            Rotating = true;
            nRotY = RotY;
            nRotZ = RotZ;
        }
        else
        {
            nRotY += (RotateStart.Y - MousePos.Y) * rotateSpeed;
            nRotZ += (RotateStart.X - MousePos.X) * rotateSpeed;
            nRotY=sat(nRotY);
        }
    }
    else if (Rotating)
    {
        RotY += (RotateStart.Y - MousePos.Y) * rotateSpeed;
        RotZ += (RotateStart.X - MousePos.X) * rotateSpeed;
        RotY=sat(RotY);
        nRotY = RotY;
        nRotZ = RotZ;
        Rotating = false;
    }

    pos.X = -currentZoom;
    pos.Y=0;
    pos.Z=0;

    pos.rotateXZBy(-nRotY);
    pos.rotateXYBy(getSimulator()->Yaw()+nRotZ+parent->getRotation().Z);

    camera->setPosition(parent->getPosition()+pos+pos_offset);
    camera->setTarget(parent->getPosition()+target_offset);
}

ISceneNodeAnimator* AnimPoursuite::createClone(ISceneNode* node,
            ISceneManager* newManager)
{
    return NULL;
}

bool AnimPoursuite::MouseMoved(const SEvent& event,irr::core::position2df MousePos)
{
    if (event.EventType != EET_MOUSE_INPUT_EVENT)
        return false;

    switch(event.MouseInput.Event)
    {

    case EMIE_MOUSE_WHEEL:
        currentZoom -= event.MouseInput.Wheel * zoomSpeed;
        if(currentZoom<=0) currentZoom=zoomSpeed;
        break;
    case EMIE_LMOUSE_PRESSED_DOWN:
        LMouseKey = true;
        break;
    case EMIE_LMOUSE_LEFT_UP:
        LMouseKey= false;
        break;
    case EMIE_MOUSE_MOVED:
        this->MousePos = MousePos;
        break;
    default:
        return false;
        break;
    }

    return true;
}

} // end namespace simulator
} // end namespace framework

#endif //GL
