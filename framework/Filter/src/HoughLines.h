/*!
 * \file HoughLines.h
 * \brief HoughLines algorithm
 * \author Guillaume Sanahuja, Copyright Heudiasyc UMR UTC/CNRS 7253
 * \date 2015/10/07
 * \version 4.0
 */


#ifndef HOUGHLINES_H
#define HOUGHLINES_H

#include <IODevice.h>
#include <cv.h>

namespace framework {
    namespace core {
        class cvimage;
        class cvmatrix;
    }
    namespace gui {
        class LayoutPosition;
        class SpinBox;
        class DoubleSpinBox;
    }
}

namespace framework { namespace filter {
    /*! \class HoughLines
    *
    * \brief HoughLines algorithm
    *
    * HoughLines is done using the DSP of the DM3730.
    */
    class HoughLines : public core::IODevice {
        public:
            /*!
            * \brief Constructor
            *
            * Construct an OpticalFlow filter at given position. \n
            * After calling this function, position will be deleted as it is no longer usefull. \n
            *
            * \param parent parent
            * \param position position
            * \param name name
            */
            HoughLines(const core::IODevice* parent,const gui::LayoutPosition* position,std::string name);

            /*!
            * \brief Destructor
            *
            */
            ~HoughLines();

            bool isLineDetected(void) const;
            float GetOrientation(void) const;
            float GetDistance(void) const;
            core::cvmatrix *Output(void) const;

        private:
            void UpdateFrom(const core::io_data *data);
            gui::SpinBox *fullRhoStep,*trackingRhoStep,*nbPoints;
            gui::DoubleSpinBox *fullThetaStep,*trackingThetaStep,*trackingDeltaTheta;
            bool isTracking,lostLine,initLine,noLine;
            int nbLines;
            float yawMean,prevYawMean;
            float prevRhoMean,rhoMean;
            float prevThetaMean,thetaMean;
            float distance,orientation;
            CvMat* linesStorage;
            core::cvmatrix *output;
    };
} // end namespace filter
} // end namespace framework
#endif // HOUGHLINES_H
