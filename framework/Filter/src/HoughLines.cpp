//  created:    2015/10/07
//  filename:   HoughLines.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    HoughLines
//
//
/*********************************************************************/

#include "HoughLines.h"
#include <cvimage.h>
#include <cvmatrix.h>
#include <Layout.h>
#include <GroupBox.h>
#include <SpinBox.h>
#include <DoubleSpinBox.h>
#include <dspcv_gpp.h>
#include <typeinfo>

#define MAX_LINES 100

using std::string;
using namespace framework::core;
using namespace framework::gui;

namespace framework { namespace filter {

HoughLines::HoughLines(const IODevice* parent,const LayoutPosition* position,string name) : IODevice(parent,name) {
    GroupBox* reglages_groupbox=new GroupBox(position,name);
    fullRhoStep=new SpinBox(reglages_groupbox->NewRow(),"full rho step:","pixels",0,255,1,1);
    fullThetaStep=new DoubleSpinBox(reglages_groupbox->LastRowLastCol(),"full theta step:","degrees",0,90,1,1);
    trackingRhoStep=new SpinBox(reglages_groupbox->NewRow(),"tracking rho step:","pixels",0,255,1,1);
    trackingThetaStep=new DoubleSpinBox(reglages_groupbox->LastRowLastCol(),"tracking theta step:","degrees",0,90,1,1);
    trackingDeltaTheta=new DoubleSpinBox(reglages_groupbox->LastRowLastCol(),"tracking delta theta:","degrees",0,90,1,1);
    nbPoints=new SpinBox(reglages_groupbox->NewRow(),"nb points:",0,10000,10,100);

    isTracking=false;
    lostLine=false;
    initLine=false;
    linesStorage = cvCreateMat(MAX_LINES, 1, CV_32FC2);

    //init output matrix of same size as init
    cvmatrix_descriptor* desc=new cvmatrix_descriptor(4,1);
    desc->SetElementName(0,0,"distance");
    desc->SetElementName(1,0,"orientation rad");
    desc->SetElementName(2,0,"orientation deg");
    desc->SetElementName(3,0,"line_detected");
    output=new cvmatrix(this,desc,floatType,name);

    try{
        cvimage::Type const &imageType=dynamic_cast<cvimage::Type const &>(parent->GetOutputDataType());
        if(imageType.GetFormat()!=cvimage::Type::Format::GRAY) {
            Err("input image is not grey\n");
        }
    } catch(std::bad_cast& bc) {
        Err("io type mismatch\n");
    }
}

HoughLines::~HoughLines(void) {
    cvReleaseMat(&linesStorage);
}

void HoughLines::UpdateFrom(const io_data *data) {
    cvimage *cvImage=(cvimage*)data;
    IplImage *gimg=cvImage->img;

    data->GetMutex();
    if(!isTracking) {
        nbLines=dspHoughLines2(gimg,linesStorage,CV_HOUGH_STANDARD,
                                fullRhoStep->Value(),fullThetaStep->Value()*CV_PI/180,
                                nbPoints->Value());
    } else {
        nbLines=dspHoughLines2_test(gimg,linesStorage,CV_HOUGH_STANDARD,
                                     trackingRhoStep->Value(),
                                     yawMean-trackingDeltaTheta->Value()*CV_PI/180,yawMean+trackingDeltaTheta->Value()*CV_PI/180,
                                     trackingThetaStep->Value()*CV_PI/180,
                                     nbPoints->Value());
    }
    data->ReleaseMutex();

    //saturation sur le nb max de ligne, au cas ou le DSP n'aurait pas la meme valeur
    if(nbLines>MAX_LINES) {
        Warn("erreur nb lines %i>%i\n",nbLines,MAX_LINES);
        nbLines=MAX_LINES;
    }

    //printf("Lines = %i %f %f\n", nbLines,linesStorage->data.fl[0],linesStorage->data.fl[1]);

    prevYawMean = yawMean;
    yawMean = 0;

    prevRhoMean= rhoMean;
    rhoMean = 0;

    prevThetaMean= thetaMean;
    thetaMean = 0;
//enlever le yaw[i] pour utiliser yaw_mean

//if(flag_First == 1) printf("yaw mean %f rho mean %f\n",prevYawMean*180/CV_PI,prevRhoMean);

    int nb_good_lines=0;
    for(int i=0;i<nbLines;i++) {
        float yaw,rho;

        //pour ne pas fausser les moyennes:
        if(linesStorage->data.fl[2*i+1]>CV_PI/2) {//rho en 2*i, angles en 2*i+1
            //les angles sont entre 0 et 180° en sortie de Hough, donc si angle>90° on le remplace par angle-180°
            // au final les angles sont entre -90 et +90°
            linesStorage->data.fl[2*i+1]=linesStorage->data.fl[2*i+1]-CV_PI;
            linesStorage->data.fl[2*i]=-linesStorage->data.fl[2*i];
        }

        yaw = linesStorage->data.fl[2*i+1];
        rho = linesStorage->data.fl[2*i];
        if(yaw>=CV_PI/2) yaw-=CV_PI; //ne devrait pas se produire

        if(isTracking) {//pas la 1ère image
            // on ne garde que les lignes à moins de 30°
            if( fabs(yaw-prevYawMean) > 30*CV_PI/180 ) {
                // déjà fait dans dspHoughLines2_test
                //yaw  = prevYawMean;
                //linesStorage->data.fl[2*i]  = prevRhoMean;
                //linesStorage->data.fl[2*i+1] = theta_Mean_1;
                //printf("enleve yaw %i/%i\n",i,nbLines);
                continue;
            }
            if( fabs(rho-prevRhoMean) > 100) {
                //probablement: supprime les lignes trop éloignées
                //printf("enleve rho %i/%i\n",i,nbLines);
                //printf("yaw %f rho %f\n",yaw*180/CV_PI,rho);
                //yaw = prevYawMean;
                //linesStorage->data.fl[2*i]  = prevRhoMean;
                //linesStorage->data.fl[2*i+1] = theta_Mean_1;
                continue;
            }
            yawMean += yaw;
            rhoMean += linesStorage->data.fl[2*i];
            thetaMean += linesStorage->data.fl[2*i+1];
            nb_good_lines++;
        } else {
            // toutes les lignes elles sont bonnes
            yawMean += yaw;
            rhoMean += linesStorage->data.fl[2*i];
            thetaMean += linesStorage->data.fl[2*i+1];
            nb_good_lines++;
        }
    }

    if(nb_good_lines!=0) {
        CvPoint pt1, pt2;
        float m,p,xCentre;

        yawMean /= nb_good_lines;
        rhoMean /= nb_good_lines;
        thetaMean /= nb_good_lines;

        float a   = cos(thetaMean);
        float b   = sin(thetaMean);
        if( fabs(b) < 0.001 ) {
            pt1.x = pt2.x = cvRound(rhoMean);
            pt1.y = 0;
            pt2.y = gimg->height;
        } else if( fabs(a) < 0.001 ) {//ne devrait pas se produire pour 2 raisons: Sobel se fait seulement sur x et le Hough ne cherche que des droites à +/- 10°
            pt1.y = pt2.y = cvRound(rhoMean);
            pt1.x = 0;
            pt2.x = gimg->width;
        } else {
            pt1.x = 0;
            pt1.y = cvRound(rhoMean/b);
            pt2.x = cvRound(rhoMean/a);
            pt2.y = 0;
        }

        if(pt1.x!=pt2.x) {
            m=((float)pt1.y-(float)pt2.y)/((float)pt1.x-(float)pt2.x);
            p=(float)pt1.y-m*(float)pt1.x;
            //x_c=((float)img->height/2.-p)/m;
            if(m!=0) {
                xCentre=(gimg->height/2-p)/m;//mettre une variable,
            }
        } else {
            m=1;//pour aller dans le bon if suivant
            xCentre=pt1.x;
        }

        if(m!=0) {
            float y_pos=(xCentre-gimg->width/2)/271.76;
            if(!lostLine) {
                distance=y_pos;
                orientation=-yawMean;
                noLine=false;
                initLine=true;
                isTracking = true;
            } else {
                //printf("perdu cur %f new %f %f\n",distance,y_pos,-yawMean);
                //cette partie necessite le flux optique pour savoir qu'on retrouve la bonne ligne
                //mais le flux optique n'est pas accessible dans ce objet
                //if(fabs(distance-y_pos)<0.3 && fabs(orientation-(-yawMean))<20*CV_PI/180) {
                    //Printf("reaccorche ligne err:%f\n",fabs(distance-y_pos));
                    distance=y_pos;
                    orientation=-yawMean;
                    noLine=false;
                    lostLine=false;
                    isTracking = true;/*
                } else {
                    isTracking=false;
                    yawMean=0;
                    //distance+=of.of.vy/271.76;
                    noLine=true;
                }*/
            }
        } else {
            Warn("droite horizontale\n");
            /*
            if(flag_First==1)
            {
                for(int i=0;i<nbLines;i++)
                {
                    printf("%i theta = %f , rho = %f \n", i,linesStorage->data.fl[2*i+1]*180/PI, linesStorage->data.fl[2*i]);
                }
            }
            */
            isTracking=false;
            yawMean=0;
            //distance+=of.of.vy/271.76;
            noLine=false;
        }
    } else {
        //Printf("pas de ligne visible\n");
        isTracking=false;
        yawMean=0;
        if(initLine==true && !lostLine) {
            printf("perdu a %f %f\n",distance,orientation);

            lostLine=true;
        }
        //distance+=of.of.vy/271.76;
        noLine=true;
        //printf("ligne ko\n");
    }

    output->GetMutex();
    output->SetValueNoMutex(0,0,distance);
    output->SetValueNoMutex(1,0,orientation);
    output->SetValueNoMutex(2,0,orientation*180/CV_PI);
    if(noLine) {
        output->SetValueNoMutex(3,0,0);
    } else {
        output->SetValueNoMutex(3,0,1);
    }
    output->ReleaseMutex();

    output->SetDataTime(data->DataTime());
    ProcessUpdate(output);
}

bool HoughLines::isLineDetected() const {
    if(output->Value(3,0)==1) {
        return true;
    } else {
        return false;
    }
}

float HoughLines::GetOrientation(void) const {
    return output->Value(1,0);
}

float HoughLines::GetDistance(void) const {
    return output->Value(0,0);
}

cvmatrix *HoughLines::Output(void) const {
    return output;
}

} // end namespace filter
} // end namespace framework
