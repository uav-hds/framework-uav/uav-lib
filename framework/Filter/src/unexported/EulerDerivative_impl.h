/*!
 * \file EulerDerivative.h
 * \brief Classe permettant le calcul d'une derivee d'Euler
 * \author Guillaume Sanahuja, Copyright Heudiasyc UMR UTC/CNRS 7253
 * \date 2011/05/01
 * \version 4.0
 */

#ifndef EULERDERIVATIVE_IMPL_H
#define EULERDERIVATIVE_IMPL_H

#include <IODevice.h>

namespace framework
{
    namespace core
    {
        class cvmatrix;
    }
    namespace gui
    {
        class LayoutPosition;
        class DoubleSpinBox;
    }
    namespace filter
    {
        class EulerDerivative;
    }
}

/*! \class EulerDerivative
* \brief Classe permettant le calcul d'une derivee d'Euler
*/

class EulerDerivative_impl
{
    public:
        EulerDerivative_impl(framework::filter::EulerDerivative* self,const framework::gui::LayoutPosition* position,std::string name,const framework::core::cvmatrix* init_value=NULL);
        ~EulerDerivative_impl();
        void UpdateFrom(const framework::core::io_data *data);
        framework::core::cvmatrix *output;

    private:
        framework::gui::DoubleSpinBox* T;
        framework::core::Time previous_time;
        bool first_update;
        framework::core::cvmatrix* prev_value;

};

#endif // EULERDERIVATIVE_IMPL_H
