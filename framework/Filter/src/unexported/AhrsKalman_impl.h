/*!
 * \file AhrsKalman_impl.h
 * \brief Class defining an Ahrs Kalman filter
 * \author Guillaume Sanahuja, Copyright Heudiasyc UMR UTC/CNRS 7253
 * \date 2014/01/15
 * \version 4.0
 */

#ifndef AHRSKALMAN_IMPL_H
#define AHRSKALMAN_IMPL_H

#include <Ahrs.h>
#include <Euler.h>

namespace framework {
    namespace core {
        class AhrsData;
    }
    namespace gui {
        class Layout;
        class DoubleSpinBox;
    }
}

/*! \class AhrsKalman_impl
* \brief Class defining an Ahrs Kalman filter
*/

class AhrsKalman_impl {
    public:
        AhrsKalman_impl(const framework::gui::Layout *layout,framework::core::AhrsData *ahrsData);
        ~AhrsKalman_impl();
        void UpdateFrom(const framework::core::io_data *data);

    private:
        framework::core::AhrsData *ahrsData;
        typedef struct ars_Gyro1DKalman
        {
            /* These variables represent our state matrix x */
            double x_angle,
                  x_bias;

            /* Our error covariance matrix */
            double P_00,
                  P_01,
                  P_10,
                  P_11;

            /*
             * Q is a 2x2 matrix of the covariance. Because we
             * assume the gyro and accelero noise to be independend
             * of eachother, the covariances on the / diagonal are 0.
             *
             * Covariance Q, the process noise, from the assumption
             *    x = F x + B u + w
             * with w having a normal distribution with covariance Q.
             * (covariance = E[ (X - E[X])*(X - E[X])' ]
             * We assume is linair with dt
             */
            double Q_angle, Q_gyro;
            /*
             * Covariance R, our observation noise (from the accelerometer)
             * Also assumed to be linair with dt
             */
            double R_angle;
        } ars_Gyro1DKalman;

        // Initializing the struct
        void ars_Init(ars_Gyro1DKalman *filterdata, double Q_angle, double Q_gyro, double R_angle);
        // Kalman predict
        void ars_predict(ars_Gyro1DKalman *filterdata, const double gyro, const double dt);
        // Kalman update
        void ars_update(ars_Gyro1DKalman *filterdata, const double angle_m);

        ars_Gyro1DKalman ars_roll;
        ars_Gyro1DKalman ars_pitch;
        framework::core::Time previous_time;
        bool is_init;
        framework::gui::DoubleSpinBox *Q_angle,*Q_gyro,*R_angle;
        framework::core::Euler euler;
};

#endif // AHRSKALMAN_IMPL_H
