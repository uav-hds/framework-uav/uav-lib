/*!
 * \file LowPassFilter_impl.h
 * \brief Classe permettant le calcul d'un filtre passe bas
 * \author Guillaume Sanahuja, Copyright Heudiasyc UMR UTC/CNRS 7253
 * \date 2011/05/01
 * \version 4.0
 */

#ifndef LOWPASSFILTER_FILTER_IMPL_H
#define LOWPASSFILTER_FILTER_IMPL_H

#include <IODevice.h>

namespace framework
{
    namespace core
    {
        class cvmatrix;
    }
    namespace gui
    {
        class LayoutPosition;
        class SpinBox;
        class DoubleSpinBox;
    }
    namespace filter
    {
        class LowPassFilter;
    }
}

class LowPassFilter_impl
{

    public:
        LowPassFilter_impl(const framework::filter::LowPassFilter* self,const framework::gui::LayoutPosition* position,std::string name,const framework::core::cvmatrix* init_value=NULL);
        ~LowPassFilter_impl();
        void UpdateFrom(const framework::core::io_data *data);
        framework::core::cvmatrix *output;

    private:
        framework::core::Time previous_time;
        bool first_update;
        framework::core::cvmatrix* prev_value;
        framework::gui::DoubleSpinBox *freq,*T;
};

#endif // LOWPASSFILTER_FILTER_IMPL_H
