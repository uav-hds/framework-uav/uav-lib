//  created:    2012/08/29
//  filename:   JoyReference_impl.h
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    generation de consignes a partir joystick
//
//
/*********************************************************************/

#ifndef JOYREFERENCE_IMPL_H
#define JOYREFERENCE_IMPL_H

#include <stdint.h>
#include <Object.h>
#include <Quaternion.h>

namespace framework {
    namespace core {
        class cvmatrix;
        class io_data;
        class AhrsData;
    }
    namespace gui {
        class LayoutPosition;
        class GroupBox;
        class DoubleSpinBox;
        class SpinBox;
        class Label;
        class PushButton;
    }
    namespace filter {
        class JoyReference;
    }
}

class JoyReference_impl {

    public:
        JoyReference_impl(framework::filter::JoyReference *self,const framework::gui::LayoutPosition* position,std::string name);
        ~JoyReference_impl();
        void SetRollAxis(float value);
        void SetPitchAxis(float value);
        void SetYawAxis(float value);
        void SetAltitudeAxis(float value);
        float ZRef(void) const;
        float dZRef(void) const;
        float RollTrim(void) const;
        float PitchTrim(void) const;
        void SetYawRef(float value);
        void SetZRef(float value);
        void RollTrimUp(void);
        void RollTrimDown(void);
        void PitchTrimUp(void);
        void PitchTrimDown(void);
        void Update(framework::core::Time time);
        void UpdateFrom(const framework::core::io_data *data);
        framework::core::cvmatrix *output;
        framework::core::AhrsData *ahrsData;

    private:
        framework::core::cvmatrix *input;

        framework::gui::GroupBox *reglages_groupbox;
        framework::gui::DoubleSpinBox *deb_roll,*deb_pitch,*deb_wz,*deb_dz;
        framework::gui::DoubleSpinBox *trim;
        framework::gui::Label *label_roll,*label_pitch;
        framework::gui::PushButton *button_roll,*button_pitch;

        float z_ref;
        framework::core::Quaternion q_z=framework::core::Quaternion(1,0,0,0);
        float trim_roll,trim_pitch;
        framework::core::Time previous_time;

        framework::filter::JoyReference *self;
};

#endif // JOYREFERENCE_IMPL_H
