/*!
 * \file Ahrs_impl.h
 * \brief Virtual class for Ahrs_impl
 * \author Guillaume Sanahuja, Copyright Heudiasyc UMR UTC/CNRS 7253
 * \date 2014/01/14
 * \version 4.0
 */

#ifndef AHRS_IMPL_H
#define AHRS_IMPL_H

#include <DataPlot.h>

namespace framework {
    namespace core {
        class AhrsData;
    }
    namespace gui {
        class Tab;
        class DataPlot1D;
    }
    namespace filter {
        class Ahrs;
    }
}

/*! \class Ahrs_impl
* \brief Abstract class for Ahrs_impl
*
* Use this class to define a custom Ahrs_impl. \n
*
*/

class Ahrs_impl {
    public:
        Ahrs_impl(framework::filter::Ahrs* self);
        ~Ahrs_impl();
        void UseDefaultPlot(void);
        void AddPlot(const framework::core::AhrsData *ahrsData,framework::gui::DataPlot::Color_t color);
        framework::gui::DataPlot1D *rollPlot,*pitchPlot,*yawPlot;
        framework::gui::DataPlot1D *wXPlot,*wYPlot,*wZPlot;
        framework::gui::DataPlot1D *q0Plot,*q1Plot,*q2Plot,*q3Plot;
        framework::core::AhrsData *ahrsData;

    private:
        framework::gui::Tab *eulerTab,*quaternionTab;
        framework::filter::Ahrs* self;
};

#endif // AHRS_IMPL_H
