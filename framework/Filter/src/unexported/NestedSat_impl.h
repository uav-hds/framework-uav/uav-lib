/*!
 * \file NestedSat.h
 * \brief Classe permettant le calcul d'un Pid avec saturations
 * \author Guillaume Sanahuja, Copyright Heudiasyc UMR UTC/CNRS 7253
 * \date 2013/04/15
 * \version 4.0
 */

#ifndef NESTEDSAT_IMPL_H
#define NESTEDSAT_IMPL_H

#include <Object.h>

namespace framework
{
    namespace core
    {
        class cvmatrix;
        class io_data;
    }
    namespace gui
    {
        class Layout;
        class LayoutPosition;
        class DoubleSpinBox;
        class DataPlot1D;
    }
    namespace filter
    {
        class NestedSat;
    }
}

/*! \class NestedSat
* \brief Classe permettant le calcul d'un Pid avec saturations
*/
class NestedSat_impl
{
    public:
        NestedSat_impl(framework::filter::NestedSat* self,const framework::gui::LayoutPosition* position,std::string name);
        ~NestedSat_impl();
        float Value(void);
        void UseDefaultPlot(const framework::gui::Layout* layout,int row,int col);
        void UpdateFrom(const framework::core::io_data *data);
        void ConvertSatFromDegToRad(void);
        float k;

    private:
        framework::filter::NestedSat* self;
        framework::gui::DoubleSpinBox *kp,*kd,*sat,*dsat,*usat;
        framework::gui::DataPlot1D *plot;
        float Sat(float value,float borne);
};


#endif // NESTEDSAT_IMPL_H
