/*!
 * \file PidThrust_impl.h
 * \brief Classe permettant le calcul d'un Pid
 * \author Guillaume Sanahuja, Copyright Heudiasyc UMR UTC/CNRS 7253
 * \date 2014/11/07
 * \version 4.0
 */

#ifndef PIDTHRUST_IMPL_H
#define PIDTHRUST_IMPL_H

#include <Object.h>

namespace framework
{
    namespace core
    {
        class cvmatrix;
        class io_data;
    }
    namespace gui
    {
        class LayoutPosition;
        class DoubleSpinBox;
    }
    namespace filter
    {
        class PidThrust;
    }
}

/*! \class PidThrust_impl
* \brief Class defining a PID
*/

class PidThrust_impl
{
    public:
        PidThrust_impl(framework::filter::PidThrust* self,const framework::gui::LayoutPosition* position,std::string name);
        ~PidThrust_impl();
        void UseDefaultPlot(const framework::gui::LayoutPosition* position);
        void UpdateFrom(const framework::core::io_data *data);
        float i,offset_g;
        framework::gui::DoubleSpinBox *offset,*pas_offset;

    private:
        framework::filter::PidThrust* self;
        framework::core::Time previous_time;
        bool first_update;

        //matrix
        framework::core::cvmatrix *state;

        framework::gui::DoubleSpinBox *T,*kp,*ki,*kd,*sat,*sati;
};

#endif // PIDTHRUST_IMPL_H
