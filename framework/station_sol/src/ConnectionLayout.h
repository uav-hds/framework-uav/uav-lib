#ifndef CONNECTIONLAYOUT_H
#define CONNECTIONLAYOUT_H

#include "Layout.h"

class UdtSocket;
class DataRemote;

class ConnectionLayout:  public Layout
{
    Q_OBJECT

    public:
        ConnectionLayout(UdtSocket* socket,QString name);
        ~ConnectionLayout();
        void XmlToSend(QDomDocument doc);
        void addDataRemote(DataRemote* data);
        void removeDataRemote(DataRemote* data);
        void LoadXml(QDomDocument to_parse);
        QString getRemoteName();

    private:
        static int uncompressBuffer(char *in, ssize_t in_size,char **out,ssize_t *out_size);
        void handleUncompressedFrame(char *buf,ssize_t size);
        void drawDatas(char* buf,int buf_size,uint16_t period,bool big_endian=false);
        bool isRemoteNameDefined;
        QString remoteName;
        UdtSocket* socket;
        QList<DataRemote*> dataremotes;

    private slots:
        void receive(char* buf,int  size);

    signals:
        void setRemoteName(QString name);
};

#endif // CONNECTIONLAYOUT_H
