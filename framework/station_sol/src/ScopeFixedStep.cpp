#include "ScopeFixedStep.h"
#include "Scrollbar.h"
#include <qwt_plot_curve.h>

ScopeFixedStep::ScopeFixedStep(QString title,float ymin,float ymax,float step_s,float view_size_s,unsigned int refresh_rate_ms,unsigned int history_size): Scope(title,ymin,ymax,view_size_s,refresh_rate_ms,history_size) {
    this->step_s=step_s;
}

ScopeFixedStep::~ScopeFixedStep() {

}

void ScopeFixedStep::plot(double* value) {
    elapsed_time_s+=step_s;
    for(int i=0;i<curves.count();i++) {
        Curve* curve=curves.at(i);

        curve->data_x[curve->index]=elapsed_time_s;
        curve->data_y[curve->index]=value[i];
        curve->index++;

        updateCurve(curve);
    }
}
