#ifndef GROUPBOX_H
#define GROUPBOX_H

#include "Layout.h"

class QGroupBox;

class GroupBox: public Layout
{

    public:
        GroupBox(Layout* parent,int row, int col,QString name);
        ~GroupBox();

    private:
        QGroupBox* box;
};

#endif // GROUPBOX_H
