#ifndef _SCROLLBAR_H
#define _SCROLLBAR_H 1

#include <qscrollbar.h>

class ScrollBar: public QScrollBar
{
    Q_OBJECT

public:
    ScrollBar(QWidget *parent = NULL);
    ScrollBar(Qt::Orientation, QWidget *parent = NULL);
    ScrollBar(float minBase, float maxBase,
        Qt::Orientation o, QWidget *parent = NULL);

    void setInverted(bool);
    bool isInverted(void) const;

    float minBaseValue(void) const;
    float maxBaseValue(void) const;

    float minSliderValue(void) const;
    float maxSliderValue(void) const;

    int extent(void) const;

Q_SIGNALS:
    void sliderMoved(Qt::Orientation, float, float);
    void valueChanged(Qt::Orientation, float, float);

public Q_SLOTS:
    virtual void setBase(float min, float max);
    virtual void moveSlider(float min, float max);

protected:
    void sliderRange(int value, float &min, float &max) const;
    int mapToTick(float) const;
    float mapFromTick(int) const;

private Q_SLOTS:
    void catchValueChanged(int value);
    void catchSliderMoved(int value);

private:
    void init(void);

    bool d_inverted;
    float d_minBase;
    float d_maxBase;
    int d_baseTicks;
};

#endif
