#ifndef SCOPE_H_INCLUDED
#define SCOPE_H_INCLUDED

#include <qwt_plot.h>
#include <QPen>

class ScrollBar;
class QwtPlotCurve;
class QwtPlotItem;
class QMouseEvent;

class Scope: public QwtPlot {
    Q_OBJECT

    public:
        Scope(QString title,float ymin, float ymax,float view_size_s=20,unsigned int refresh_rate_ms=40,unsigned int history_size=20000);
        ~Scope();
        int addCurve(QPen pen,QString legend);
        void resetXView(void);
        void resetYView(void);

    protected:
        struct Curve {
            QwtPlotCurve* plot;
            double* data_x;
            double* data_y;
            unsigned int index;
            unsigned int min_index;
            unsigned int max_index;
        };
        QList<Curve*> curves;
        float elapsed_time_s;

        void updateCurve(Curve* curve);
        bool eventFilter(QObject *, QEvent *);

    private:
        float ymin,ymax;
        float view_size_s,orig_view_size_s;
        unsigned int history_size;
        ScrollBar* scrollbar;
        bool scrolling;
        float min_scroll,max_scroll;

        void alignScales(void);
        void showCurve(QwtPlotItem *item, bool on);
        void computeMinIndex(Curve* curve,float displayed_min_time);
        void computeMaxIndex(Curve* curve,float displayed_max_time);
        void changeViewSize(float new_view_size_s);

    private slots:
        void legendChecked(const QVariant &itemInfo, bool on );
        void scrollBarMoved(Qt::Orientation o, float min, float max);
};


#endif // SCOPE_H_INCLUDED
