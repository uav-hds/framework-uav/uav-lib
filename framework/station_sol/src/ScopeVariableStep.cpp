#include "ScopeVariableStep.h"
#include "Scrollbar.h"
#include <QTime>
#include <qwt_plot_curve.h>

ScopeVariableStep::ScopeVariableStep(QString title,float ymin,float ymax,float view_size_s,unsigned int refresh_rate_ms,unsigned int history_size): Scope(title,ymin,ymax,view_size_s,refresh_rate_ms,history_size) {

}

ScopeVariableStep::~ScopeVariableStep() {

}

void ScopeVariableStep::plot(double value,QTime time,unsigned int curve_index) {
    static QTime start_time=QTime::currentTime();
    Curve* curve=curves.at(curve_index);

    elapsed_time_s=start_time.msecsTo(time)/1000.;
    curve->data_x[curve->index]=elapsed_time_s;
    curve->data_y[curve->index]=value;
    curve->index++;

    updateCurve(curve);
}
