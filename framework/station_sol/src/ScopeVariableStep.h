#ifndef SCOPEVARIABLESTEP_H_INCLUDED
#define SCOPEVARIABLESTEP_H_INCLUDED

#include <Scope.h>

class ScopeVariableStep: public Scope {
    public:
        ScopeVariableStep(QString title,float ymin, float ymax,float view_size_s=20,unsigned int refresh_rate_ms=40,unsigned int history_size=20000);
        ~ScopeVariableStep();
        void plot(double value,QTime time,unsigned int curve_index=0);

    private:

};


#endif // SCOPEVARIABLESTEP_H_INCLUDED
