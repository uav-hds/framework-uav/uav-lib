#ifndef SCOPEFIXEDSTEP_H_INCLUDED
#define SCOPEFIXEDSTEP_H_INCLUDED

#include <Scope.h>


class ScopeFixedStep: public Scope {
    public:
        ScopeFixedStep(QString title,float ymin, float ymax,float step_s,float view_size_s=20,unsigned int refresh_rate_ms=40,unsigned int history_size=20000);
        ~ScopeFixedStep();
        void plot(double* value);

    protected:
        float step_s;
};


#endif // SCOPEFIXEDSTEP_H_INCLUDED
