//  created:    2015/06/09
//  filename:   XbeeCommon.h
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    xbee common defines and functions
//
//
/*********************************************************************/

#include <stdint.h>
#include <stdlib.h>
#include <string>

#define START_DELIMITER 0x7e
#define TX_REQUEST 0x01
#define TX_STATUS 0x89
#define RX_PACKET 0x81
#define OPTION 0x00
#define LAST_FRAME 0x80
#define FRAME_ID_MASK 0x7f
#define MAX_FRAME_SIZE 98

uint8_t calcChecksum(char* buf,ssize_t size);
uint8_t getType(char data);
std::string getTypeDesc(char data);
bool isLastFrame(char data);
