//  created:    2015/06/08
//  filename:   Xbee.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    xbee modem
//
//
/*********************************************************************/

#ifndef XBEE_H
#define XBEE_H

#include <string>
#include <stdint.h>
#include <sys/types.h>
#include <pthread.h>
#include <queue>
#include "Message.h"
#include "XbeeTxMessage.h"
#include "XbeeRxMessage.h"
#include "CommunicationChannel.h"

class Xbee: public CommunicationChannel {
    public:
        Xbee(std::string dev_name,uint32_t baudrate,uint16_t destination_address);
        ~Xbee();
        void handleMessage(Message* message);
        Message* getMessage(void);
        void addToEpoll(int eid);
        bool isFdEqualsTo(int fd);

    private:
        int openAndConfigureSerialPort(std::string dev_name,uint32_t baudrate);
        static void* threadFunc(void * arg);
        static int rateToConstant(uint32_t baudrate);
        void run(void);
        ssize_t write(XbeeTxMessage* message,int id_buf);
        int lockMutex(pthread_mutex_t *mutex);
        int releaseMutex(pthread_mutex_t *mutex);
        bool readFrame(void);
        ssize_t read(char* buf,ssize_t size);
        void handleTxStatus(uint8_t frame_id,uint8_t status);
        void handleRxPacket(char* buf,ssize_t size);
        void mutexInit(pthread_mutex_t *mutex);

        pthread_t thread;
        pthread_mutex_t currentXbeeTxMessageMutex,messagesMutex;
        bool is_running;
        int serial_fd;
        uint16_t destination_address;
        std::queue<XbeeTxMessage*> txMessages;
        std::queue<XbeeRxMessage*> rxMessages;
        XbeeTxMessage* currentXbeeTxMessage;
        XbeeTxMessage* nextUdpXbeeTxMessage;
        XbeeRxMessage* currentXbeeRxMessage;
        int pipefd[2];
};

#endif // XBEE_H
