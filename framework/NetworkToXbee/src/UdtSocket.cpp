//  created:    2015/06/04
//  filename:   UdtSocket.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    udt socket
//
//
/*********************************************************************/

#include "UdtSocket.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

using namespace std;

UdtSocket::UdtSocket() {
    UDT::startup();

    file_socket=0;
    com_socket=0;
}

UdtSocket::~UdtSocket() {
    UDT::close(file_socket);
    UDT::close(com_socket);
    UDT::cleanup();
}

void UdtSocket::listenAndAccept(int port) {
    UDTSOCKET serv = createSocket();

    sockaddr_in my_addr;
    my_addr.sin_family = AF_INET;
    my_addr.sin_port = htons(port);
    my_addr.sin_addr.s_addr = INADDR_ANY;
    memset(&(my_addr.sin_zero), '\0', 8);

    if (UDT::ERROR == UDT::bind(serv, (sockaddr*)&my_addr, sizeof(my_addr))) {
        printf("bind, %s\n",UDT::getlasterror().getErrorMessage());
    }

    printf("UdtSocket: listenning on port %i\n",port);
    UDT::listen(serv, 1);

    //socket file_socket, doit être créé en premier, cf station sol
    file_socket=accept(serv);
    com_socket=accept(serv);
    int status=UDT::close(serv);
    if(status!=0) printf("Error udt::close %s",UDT::getlasterror().getErrorMessage());

    printf("UdtSocket: connected on port %i\n",port);
}

UDTSOCKET UdtSocket::accept(UDTSOCKET serv)
{
    sockaddr_in their_addr;
    int namelen = sizeof(their_addr);
    UDTSOCKET socket_fd;

    if (UDT::INVALID_SOCK == (socket_fd = UDT::accept(serv, (sockaddr*)&their_addr, &namelen)))
    {
        printf("accept: %s, code %i\n",UDT::getlasterror().getErrorMessage(),UDT::getlasterror().getErrorCode());
    }

    return socket_fd;
}

void UdtSocket::connect(string address,int port) {
    printf("UdtSocket: connecting to %s:%i\n",address.c_str(),port);
    while(1) {
        if(!file_socket) file_socket=tryConnect(address,port);
        if(file_socket && !com_socket) com_socket=tryConnect(address,port);
        if(com_socket) break;
    }

    printf("UdtSocket: connected to %s:%i\n",address.c_str(),port);
}

UDTSOCKET UdtSocket::tryConnect(string address,int port) {
    UDTSOCKET new_fd=createSocket();
    UDT::TRACEINFO perf;

    sockaddr_in serv_addr;
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(short(port));

    if (inet_pton(AF_INET, address.c_str(), &serv_addr.sin_addr) <= 0) {
        printf("incorrect network address\n");
    }
    memset(&(serv_addr.sin_zero), '\0', 8);

    if (UDT::ERROR == UDT::connect(new_fd, (sockaddr*)&serv_addr, sizeof(serv_addr))) {
        UDT::close(new_fd);
        return 0;
    }

    //get buf size, as UDT_RCVDATA does not work...
    //we suppose there is nothing in the buffer
    UDT::perfmon( new_fd, &perf);
    sock_buf_size=perf.byteAvailRcvBuf;

    return new_fd;
}

UDTSOCKET UdtSocket::createSocket(void) {
    UDTSOCKET sock = UDT::socket(AF_INET, SOCK_DGRAM, 0);

    bool blocking = true;
    UDT::setsockopt(sock, 0, UDT_SNDSYN, &blocking, sizeof(bool));
    UDT::setsockopt(sock, 0, UDT_RCVSYN, &blocking, sizeof(bool));
    bool reuse = true;
    UDT::setsockopt(sock, 0, UDT_REUSEADDR, &reuse, sizeof(bool));

    return sock;
}

void UdtSocket::addToEpoll(int eid) {
    int events = UDT_EPOLL_IN;
    UDT::epoll_add_usock(eid,file_socket,&events);
    UDT::epoll_add_usock(eid,com_socket,&events);

}

Message* UdtSocket::getMessage(UDTSOCKET socket) {
    Message* message=NULL;

    //get approximate of data to be received
    UDT::TRACEINFO perf;
    UDT::perfmon(socket,&perf);

    char* buf=(char*)malloc((sock_buf_size-perf.byteAvailRcvBuf)*sizeof(char));
    int bytesRead = UDT::recvmsg(com_socket,buf, sock_buf_size-perf.byteAvailRcvBuf);

    if(bytesRead>0) {
        message=new Message(buf,bytesRead,Message::Udt);
    }

    free(buf);

    return message;
}

void UdtSocket::handleMessage(Message* message) {
    //message->print();
    write(message);
}

ssize_t UdtSocket::write(Message* message) {
    int bytesSent=UDT::sendmsg(com_socket,message->getBuf(),message->getSize(), -1, true);
    if(bytesSent!=message->getSize()) {
        printf("erreur envoi: %s\n",UDT::getlasterror().getErrorMessage());
    }
    return bytesSent;
}
