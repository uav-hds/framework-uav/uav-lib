//  created:    2015/06/05
//  filename:   UdpSocket.h
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    udp socket
//
//
/*********************************************************************/

#ifndef UDPSOCKET_H
#define UDPSOCKET_H

#include "Message.h"
#include <string>
#include <arpa/inet.h>

class UdpSocket {
    public:
        UdpSocket();
        ~UdpSocket();
        void handleMessage(Message* message);
        void addToEpoll(int eid);
        Message* getMessage(void);
        void bind(int port);
        void connect(std::string address,int port);
        bool isFdEqualsTo(int fd);

    private:
        ssize_t write(Message* message);
        int socket;
        struct sockaddr_in sock_in;
};

#endif // UDPSOCKET_H
