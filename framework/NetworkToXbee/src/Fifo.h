//  created:    2015/06/04
//  filename:   Fifo.h
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    fifo to simulate xbee
//
//
/*********************************************************************/

#ifndef FIFO_H
#define FIFO_H

#include <string>
#include <sys/types.h>
#include "Message.h"
#include "CommunicationChannel.h"

class Fifo: public CommunicationChannel {
    public:
        Fifo(std::string name,bool uav_side);
        ~Fifo();
        void handleMessage(Message* message);
        Message* getMessage(void);
        void addToEpoll(int eid);
        bool isFdEqualsTo(int fd);

    private:
        ssize_t write(Message* message);

        std::string read_name,write_name;
        bool needs_to_unlink_read_fd,needs_to_unlink_write_fd;
        int read_fd,write_fd;
};

#endif // FIFO_H
