//  created:    2015/06/05
//  filename:   Message.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    define message
//
//
/*********************************************************************/

#include "Message.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

Message::Message(char* buf,ssize_t size,MessageType_t messageType) {
    this->buf=(char*)malloc(size);
    this->size=size;
    this->messageType=messageType;

    memcpy(this->buf,buf,size);
}

Message::~Message() {
    free(buf);
}

char* Message::getBuf(void) {
    return buf;
}

ssize_t Message::getSize(void) {
    return size;
}

Message::MessageType_t Message::getMessagetype(void) {
    return messageType;
}

void Message::print(void) {
    if(buf[0]=='<') {
        printf("xml %i %i\n",size,messageType);
        for(int i=0;i<size;i++) {
            printf("%c",buf[i]);
        }
        printf("\n");
    }  else {
        for(int i=0;i<size;i++) {
            printf("%x ",buf[i]);
        }
        printf("\n");
    }
}
