//  created:    2015/06/04
//  filename:   Fifo.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    fifo to simulate xbee
//
//
/*********************************************************************/

#include "Fifo.h"
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <udt.h>

using namespace std;

Fifo::Fifo(string name,bool uav_side) {
    needs_to_unlink_read_fd=false;
    needs_to_unlink_write_fd=false;

    if(uav_side) {
        read_name=name+"_to_uav";
        write_name=name+"_from_uav";
    } else {
        read_name=name+"_from_uav";
        write_name=name+"_to_uav";
    }

    read_fd = open(read_name.c_str(), O_RDWR);
    if(read_fd<0) {
        mkfifo(read_name.c_str(), 0666);
        read_fd = open(read_name.c_str(), O_RDWR);
        needs_to_unlink_read_fd=true;
    }

    write_fd = open(write_name.c_str(), O_RDWR);
    if(write_fd<0) {
        mkfifo(write_name.c_str(), 0666);
        write_fd = open(write_name.c_str(), O_RDWR);
        needs_to_unlink_write_fd=true;
    }
}

Fifo::~Fifo() {
    close(read_fd);
    close(write_fd);

    if(needs_to_unlink_read_fd) unlink(read_name.c_str());
    if(needs_to_unlink_write_fd) unlink(write_name.c_str());
}

bool Fifo::isFdEqualsTo(int fd) {
    if(fd==read_fd) {
        return true;
    } else {
        return false;
    }
}

void Fifo::addToEpoll(int eid) {
    int events = UDT_EPOLL_IN;
    UDT::epoll_add_ssock(eid,read_fd,&events);
}

void Fifo::handleMessage(Message* message) {
    //message->print();
    write(message);
}

ssize_t Fifo::write(Message* message) {
    int msgid=message->getMessagetype();
    ::write(write_fd,&msgid,sizeof(msgid));

    ssize_t size=message->getSize();
    ::write(write_fd,&size,sizeof(ssize_t));

    return ::write(write_fd,message->getBuf(),message->getSize());
}

Message* Fifo::getMessage(void) {
    int msgid;
    ::read(read_fd,&msgid,sizeof(msgid));

    ssize_t size;
    ::read(read_fd,&size,sizeof(size));

    char* buf=(char*)malloc(size);
    size=::read(read_fd,buf,size);
    Message* message=new Message(buf,size,(Message::MessageType_t)msgid);
    free(buf);

    return message;
}
