//  created:    2015/06/09
//  filename:   XbeeCommon.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    xbee common defines and functions
//
//
/*********************************************************************/

#include "XbeeCommon.h"
#include "Message.h"

uint8_t calcChecksum(char* buf,ssize_t size) {
    uint8_t checksum=0;
    for(ssize_t i=0;i<size;i++) checksum+=(uint8_t)buf[i];

    return (uint8_t)(0xff-checksum);
}

uint8_t getType(char data) {
    return (uint8_t)(data&FRAME_ID_MASK);
}

std::string getTypeDesc(char data) {
  switch(data&FRAME_ID_MASK) {
    case Message::Udt:
      return "udt";
    case Message::Udp:
      return "udp";
    case Message::Tcp:
      return "tcp";
    default:
      return "unknown";
  }
}

bool isLastFrame(char data) {
    if((data&LAST_FRAME)==LAST_FRAME) {
        return true;
    } else {
        return false;
    }
}
