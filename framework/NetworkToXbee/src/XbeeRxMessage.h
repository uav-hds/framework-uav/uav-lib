//  created:    2015/06/09
//  filename:   XbeeRxMessage.h
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    define xbee rx message
//
//
/*********************************************************************/

#ifndef XBEERXMESSAGE_H
#define XBEERXMESSAGE_H

#include "Message.h"
#include <vector>
#include <stdint.h>

class XbeeRxMessage {
    public:
        XbeeRxMessage(Message::MessageType_t type);
        ~XbeeRxMessage();
        void addFrame(char* buf,ssize_t size);
        bool isComplete(void);
        void print(void);
        Message* getMessage(void);

    private:
        Message::MessageType_t type;
        int currentFrameId;
        char* buf;
        ssize_t buf_size;
        int frame_count,number_of_frames;
};

#endif // XBEERXMESSAGE_H
