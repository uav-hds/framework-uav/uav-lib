//  created:    2015/06/04
//  filename:   main.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    network to xbee communication
//
//
/*********************************************************************/

#include <stdio.h>
#include <sys/resource.h>
#include <tclap/CmdLine.h>
#include "UdtSocket.h"
#include "TcpSocket.h"
#include "UdpSocket.h"
#include "Fifo.h"
#include "Xbee.h"
#include "CommunicationChannel.h"

using namespace TCLAP;
using namespace std;

//#define LOGMESSAGES

#ifdef LOGMESSAGES
#include "Log.h"
Log *log;
#endif // LOGMESSAGES

string mode;
string com_port;
int udt_port;
int tcp_port;
int xbee_destination;
UdtSocket* udtsocket;
TcpSocket* tcpsocket;
UdpSocket* udpsocket;
CommunicationChannel* comChannel;
void parseOptions(int argc, char** argv);
void process(void);
void handleUdtSet(set<UDTSOCKET>* udt_readfds);
void handleSysSet(set<SYSSOCKET>* sys_readfds);
void handleComChannelMessage(void);
void handleTcpMessage(void);
void handleUdpMessage(void);

#define USE_XBEE

#define STACK_SIZE 1024*1024

int main(int argc, char* argv[]) {

    struct rlimit rl;
    int result = getrlimit(RLIMIT_STACK, &rl);
    if (result == 0) {
      printf("%i %i\n",rl.rlim_cur,rl.rlim_max);
      rl.rlim_cur =STACK_SIZE;
      rl.rlim_max =STACK_SIZE;
      result = setrlimit(RLIMIT_STACK, &rl);
      if (result != 0) {
          fprintf(stderr, "setrlimit returned result = %d\n", result);
      }
    }

    result = getrlimit(RLIMIT_STACK, &rl);
    if (result == 0) {
      printf("%i %i\n",rl.rlim_cur,rl.rlim_max);
    }
    parseOptions(argc,argv);
#ifdef USE_XBEE
    comChannel=new Xbee(com_port,111111,xbee_destination);
#endif
    udtsocket=new UdtSocket();
    tcpsocket=new TcpSocket();
    udpsocket=new UdpSocket();

    if(mode=="uav") {
        printf("Uav side\n");
        udtsocket->listenAndAccept(udt_port);
        tcpsocket->connect("127.0.0.1",tcp_port);
        udpsocket->connect("127.0.0.1",tcp_port+1);
#ifndef USE_XBEE
        comChannel=new Fifo("/tmp/myfifo",true);
#endif
    } else if (mode=="station"){
        printf("Ground Station side\n");
        udtsocket->connect("127.0.0.1",udt_port);
        tcpsocket->listenAndAccept(tcp_port);
        udpsocket->bind(tcp_port+1);
#ifndef USE_XBEE
        comChannel=new Fifo("/tmp/myfifo",false);
#endif
    } else {
        printf("error, unsupported mode\n");
        delete udtsocket;
        delete tcpsocket;
        delete udpsocket;
        delete comChannel;
        exit(0);
    }
#ifdef LOGMESSAGES
    printf("Creating log file\n");
    log=new Log("./main_");
#endif // LOGMESSAGES

    printf("Entering process loop\n");
    process();

#ifdef LOGMESSAGES
    delete log;
#endif // LOGMESSAGES

    delete udtsocket;
    delete tcpsocket;
    delete udpsocket;
    delete comChannel;
}

void process(void) {
    while(1) {

        int eid = UDT::epoll_create();
        udtsocket->addToEpoll(eid);
        tcpsocket->addToEpoll(eid);
        udpsocket->addToEpoll(eid);
        comChannel->addToEpoll(eid);

        set<UDTSOCKET> udt_readfds;
        set<SYSSOCKET> sys_readfds;

        int rv=UDT::epoll_wait(eid, &udt_readfds, NULL, 10,&sys_readfds) ;

        if(rv == -1) {
            if(UDT::getlasterror().getErrorCode()!=6003) printf("prob %i\n",UDT::getlasterror().getErrorCode());
        } else if(rv == 0) {
            printf("timeout\n"); // a timeout occured
        } else {
            handleUdtSet(&udt_readfds);
            handleSysSet(&sys_readfds);
        }

        UDT::epoll_release(eid);
    }

    pthread_exit(0);
}

void handleUdtSet(set<UDTSOCKET>* udt_readfds) {
    for (set<UDTSOCKET>::iterator i = udt_readfds->begin(); i != udt_readfds->end(); i++) {
        Message* message=udtsocket->getMessage(*i);
        if(message!=NULL) {
#ifdef LOGMESSAGES
        log->logMessage(message,"Udt->Xbee");
#endif // LOGMESSAGES
            comChannel->handleMessage(message);
            delete message;
        }
    }
}

void handleSysSet(set<SYSSOCKET>* sys_readfds) {
    for (set<SYSSOCKET>::iterator i = sys_readfds->begin(); i != sys_readfds->end(); i++) {
        if(comChannel->isFdEqualsTo(*i)) handleComChannelMessage();
        if(tcpsocket->isFdEqualsTo(*i)) handleTcpMessage();
        if(udpsocket->isFdEqualsTo(*i)) handleUdpMessage();
    }
}

void handleComChannelMessage(void) {
    Message* message=comChannel->getMessage();
    if(message!=NULL) {
        if(message->getMessagetype()==Message::Udt) {
#ifdef LOGMESSAGES
            log->logMessage(message,"Xbee->Udt");
#endif // LOGMESSAGES
            udtsocket->handleMessage(message);
        }
        if(message->getMessagetype()==Message::Tcp) {
#ifdef LOGMESSAGES
            log->logMessage(message,"Xbee->Tcp");
#endif // LOGMESSAGES
            tcpsocket->handleMessage(message);
        }
        if(message->getMessagetype()==Message::Udp) {
#ifdef LOGMESSAGES
            log->logMessage(message,"Xbee->Udp");
#endif // LOGMESSAGES
            udpsocket->handleMessage(message);
        }
        delete message;
    }
}

void handleTcpMessage(void) {
    Message* message=tcpsocket->getMessage();
    if(message!=NULL) {
#ifdef LOGMESSAGES
        log->logMessage(message,"Tcp->Xbee");
#endif // LOGMESSAGES
        //printf("send tcp\n");
        comChannel->handleMessage(message);
        delete message;
    }
}

void handleUdpMessage(void) {
    Message* message=udpsocket->getMessage();
    if(message!=NULL) {
#ifdef LOGMESSAGES
        log->logMessage(message,"Udp->Xbee");
#endif // LOGMESSAGES
        //printf("send udp\n");
        comChannel->handleMessage(message);
        delete message;
    }
}

void parseOptions(int argc, char** argv) {
    try {
        CmdLine cmd("Command description message", ' ', "0.1");

        ValueArg<string> modeArg("m","mode","mode (uav or station)",true,"uav","string");
        cmd.add(modeArg);

        ValueArg<string> comportArg("s","serial","serial com port",true,"/dev/ttyUSB0","string");
        cmd.add(comportArg);

        ValueArg<int> xbeedestinationArg("d","destination","xbee destination address",true,1,"int");
        cmd.add(xbeedestinationArg);

        ValueArg<int> udtportArg("u","udt","udt port",true,9000,"int");
        cmd.add(udtportArg);

        ValueArg<int> tcpportArg("t","tcp","tcp port",true,9000,"int");
        cmd.add(tcpportArg);

        cmd.parse(argc, argv);

        // Get the value parsed by each arg.
        mode=modeArg.getValue();
        com_port=comportArg.getValue();
        udt_port=udtportArg.getValue();
        tcp_port=tcpportArg.getValue();
        xbee_destination=xbeedestinationArg.getValue();

    } catch (ArgException &e) {   // catch any exceptions
        cerr << "error: " << e.error() << " for arg " << e.argId() << endl;
    }
}
