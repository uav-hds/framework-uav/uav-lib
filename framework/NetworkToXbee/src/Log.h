#ifndef LOG_H
#define LOG_H

#include "Message.h"
#include <string.h>
#include <iostream>
#include <fstream>

class Log {
public:
    Log(const char* prefixString);
    ~Log();
    void logMessage(Message* message,const char *transmissionDirection);
    void logBuffer(char *buf,size_t size,const char *transmissionDirection);
private:
    void formatedDate(char *dateString,size_t size,const char* format);
    std::ofstream logFile;
};


#endif // LOG_H
