//  created:    2015/06/09
//  filename:   CommunicationChannel.h
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    abstract calls for communication channel
//
//
/*********************************************************************/

#ifndef COMMUNICATIONCHANNEL_H
#define COMMUNICATIONCHANNEL_H


#include "Message.h"

class CommunicationChannel {
    public:
        CommunicationChannel(){};
        ~CommunicationChannel(){};
        virtual void handleMessage(Message* message)=0;
        virtual Message* getMessage(void)=0;
        virtual void addToEpoll(int eid)=0;
        virtual bool isFdEqualsTo(int fd)=0;
};

#endif // COMMUNICATIONCHANNEL_H
