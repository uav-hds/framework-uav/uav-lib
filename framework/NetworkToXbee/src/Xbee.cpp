//  created:    2015/06/08
//  filename:   Xbee.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    xbee modem
//
//
/*********************************************************************/

#include "Xbee.h"
#include "XbeeCommon.h"
#include <errno.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <termio.h>
#include <linux/serial.h>
#include <string.h>
#include <udt.h>

//#define LOGMESSAGES

#ifdef LOGMESSAGES
#include "Log.h"
Log *logXbee;
#endif // LOGMESSAGES

#define STACK_SIZE 1024*100

using namespace std;

Xbee::Xbee(string dev_name,uint32_t baudrate,uint16_t destination_address) {
    this->destination_address=destination_address;

    if(openAndConfigureSerialPort(dev_name,baudrate)!=0) {
        exit(0);
    }

    currentXbeeTxMessage=NULL;
    currentXbeeRxMessage=NULL;
    nextUdpXbeeTxMessage=NULL;

    mutexInit(&currentXbeeTxMessageMutex);
    mutexInit(&messagesMutex);

    if(pipe(pipefd)!=0) {
        printf("error creating pipe %s\n",strerror(errno));
    }

    // Initialize thread creation attributes
    pthread_attr_t attr;
    if(pthread_attr_init(&attr) != 0) {
        printf("Error pthread_attr_init\n");
    }

    if(pthread_attr_setstacksize(&attr, STACK_SIZE) != 0) {
        printf("Error pthread_attr_setstacksize\n");
    }

    if(pthread_create(&thread, &attr, threadFunc, (void*)this) != 0) {
        printf("pthread_create error\n");
    }

    if(pthread_attr_destroy(&attr) != 0) {
      printf("Error pthread_attr_destroy\n");
    }

    if(pthread_getattr_np(thread, &attr)!= 0) {
        printf("Error pthread_getattr_np\n");
    }
size_t taille;
    if(pthread_attr_getstacksize(&attr,&taille)!= 0) {
        printf("Error pthread_attr_getstacksize\n");
    }
    printf("taille %i\n",taille);
#ifdef LOGMESSAGES
    printf("Creating log file\n");
    logXbee=new Log("./xbee_");
#endif // LOGMESSAGES
}

Xbee::~Xbee() {
    is_running=false;
    int status=pthread_join(thread,NULL);
    if(status!=0) printf("Xbee error %s\n",strerror(status));

    close(serial_fd);
    close(pipefd[0]);
    close(pipefd[1]);
#ifdef LOGMESSAGES
    delete logXbee;
#endif // LOGMESSAGES
}

void* Xbee::threadFunc(void * arg) {
    Xbee *caller = (Xbee*)arg;
    caller->run();
    pthread_exit(0);
}

void Xbee::run(void) {
    is_running=true;
    while(is_running) {
        readFrame();
    }
}

void Xbee::handleTxStatus(uint8_t frame_id,uint8_t status) {
    if(status!=0) {
        if(getType(currentXbeeTxMessage->getBuf(0)[8])!=Message::Udp) {
            //printf("Xbee: err envoi frame id %i, status %i type %i\n",frame_id,status,getType(currentXbeeTxMessage->getBuf(0)[8]));
            printf("Xbee: err envoi frame id %i, status %i type %s\n",frame_id,status,getTypeDesc(currentXbeeTxMessage->getBuf(0)[8]).c_str());
            //lockMutex();//mutex not necessary, as currentXbeeTxMessage!=NULL
            write(currentXbeeTxMessage,currentXbeeTxMessage->getIndex(frame_id));
            //releaseMutex();
            return;
        }
    } //else {
        currentXbeeTxMessage->setAck(frame_id);
        //if(getType(currentXbeeTxMessage->getBuf(0)[8])==Message::Udt) printf("status ok %i\n",frame_id);
        if(!currentXbeeTxMessage->areAllFramesAck()) {
            write(currentXbeeTxMessage,currentXbeeTxMessage->getIndex(frame_id)+1);
        } else {
            XbeeTxMessage* nextXbeeTxMessage;
            lockMutex(&messagesMutex);
            if(txMessages.size()!=0) {
                //printf("next\n");
                nextXbeeTxMessage=txMessages.front();
                txMessages.pop();
            } else if(nextUdpXbeeTxMessage!=NULL) {
                nextXbeeTxMessage=nextUdpXbeeTxMessage;
                nextUdpXbeeTxMessage=NULL;
            } else{
                //printf("no more to send\n");
                nextXbeeTxMessage=NULL;
            }
            releaseMutex(&messagesMutex);

            lockMutex(&currentXbeeTxMessageMutex);
            delete currentXbeeTxMessage;
            currentXbeeTxMessage=nextXbeeTxMessage;
            releaseMutex(&currentXbeeTxMessageMutex);

            if(currentXbeeTxMessage!=NULL) write(currentXbeeTxMessage,0);
        }
   // }
}

void Xbee::handleRxPacket(char* buf,ssize_t size) {
    if(currentXbeeRxMessage==NULL) {
        currentXbeeRxMessage=new XbeeRxMessage((Message::MessageType_t)getType(buf[0]));
    }
    currentXbeeRxMessage->addFrame(buf,size);
    if(currentXbeeRxMessage->isComplete()) {
        lockMutex(&messagesMutex);
        rxMessages.push(currentXbeeRxMessage);
        releaseMutex(&messagesMutex);
        currentXbeeRxMessage=NULL;
        printf("+");
        fflush(stdout);
        if(::write(pipefd[1],"1",1)!=1) printf("Xbee::handleRxPacket error writting to pipe\n");
    }
}

bool Xbee::readFrame(void) {
    char header[3];
    //char buf[100];
//printf("read\n");
    ssize_t nb_read=read(header, 3);
    if(nb_read!=3) {
        printf("Xbee::readFrame header read error %i/3\n",nb_read);
        for(int i=0;i<nb_read;i++) printf("%x ",(uint8_t)header[i]);
        printf("\n");
        return false;
    }
    int size=(uint8_t)header[1]*255+(uint8_t)header[2]+1;
    //printf("%i %i %x %x %x ",(uint8_t)size,nb_read,header[0],(uint8_t)header[1],(uint8_t)header[2]);
    if(header[0]!=START_DELIMITER) {
        printf("Xbee::readFrame header error\n");
        printf("%i %i %x %x %x\n",(uint8_t)size,nb_read,(uint8_t)header[0],(uint8_t)header[1],(uint8_t)header[2]);
        return false;
    }
    char* buf=(char*)malloc(size);

    //if(size>sizeof(buf)) printf("buf size to small %i/%i\n",size,sizeof(buf));

    ssize_t bytesToRead=size;
    while (bytesToRead!=0) {
        nb_read=read(buf+size-bytesToRead,bytesToRead);
        if (nb_read<0) {
            printf("Xbee::readFrame message read error");
            free(buf);
            return false;
        }
        bytesToRead-=nb_read;
    }

/*
    if(nb_read!=size) {
        printf("Xbee::readFrame message read error %i/%i\n",nb_read,size);
        for(int i=0;i<nb_read;i++) printf("%x ",(uint8_t)buf[i]);
        printf("\n");
        free(buf);
        return false;
    }
    */
    /*
    if(getType(buf[5])==Message::Udp) {
        for(int i=0;i<size;i++) printf("%x ",(uint8_t)buf[i]);
        printf("\n");
        //printf("\n%i/%i\n",nb_read,size);
    }
*/
    if(calcChecksum(buf,size-1)!=(uint8_t)buf[size-1]) {
        printf("Xbee::readFrame checksum error %x %x\n",calcChecksum(buf,size-1),(uint8_t)buf[size-1]);
        for(int i=0;i<size;i++) printf("%x ",(uint8_t)buf[i]);
        printf("\n");
        free(buf);
        return false;
    }

    if((uint8_t)buf[0]==TX_STATUS) {
        handleTxStatus(buf[1],buf[2]);
    }

    if((uint8_t)buf[0]==RX_PACKET) {
        handleRxPacket(&buf[5],size-6);
    }

    free(buf);
//printf("read ok\n");
    return true;
}

bool Xbee::isFdEqualsTo(int fd) {
    if(fd==pipefd[0]) {
        return true;
    } else {
        return false;
    }
}

void Xbee::addToEpoll(int eid) {
    int events = UDT_EPOLL_IN;
    UDT::epoll_add_ssock(eid,pipefd[0],&events);
}

void Xbee::handleMessage(Message* message) {
    //message->print();
    XbeeTxMessage* xbeemessage=new XbeeTxMessage(message,destination_address);
    //xbeemessage->print();

    if(currentXbeeTxMessage==NULL) {//first check if a message is waiting for ack
        lockMutex(&currentXbeeTxMessageMutex);
        currentXbeeTxMessage=xbeemessage;
        //printf("direct\n");
        write(currentXbeeTxMessage,0);
        releaseMutex(&currentXbeeTxMessageMutex);
    } else {
        lockMutex(&messagesMutex);
        if(message->getMessagetype()==Message::Udp) {
            nextUdpXbeeTxMessage=xbeemessage;//we keep only last udp message
        } else {
            txMessages.push(xbeemessage);
        }
        //printf("indirect %i\n",txMessages.size());
        releaseMutex(&messagesMutex);
    }
};

ssize_t Xbee::write(XbeeTxMessage* message,int index) {/*
    if(getType(message->getBuf(0)[8])==Message::Udp) {
        for(int i=10;i<message->getSize(index)-1;i++) printf("%x ",message->getBuf(index)[i]);
        printf("\n");
    }
*/
    return ::write(serial_fd,message->getBuf(index),message->getSize(index));
}

Message* Xbee::getMessage(void) {
  char dummy;
  if(::read(pipefd[0],&dummy,1)!=1) printf("Xbee::getMessage error read pipe\n");
  printf("-");
  fflush(stdout);
  lockMutex(&messagesMutex);
  Message* message=rxMessages.front()->getMessage();
  delete rxMessages.front();
  rxMessages.pop();
  releaseMutex(&messagesMutex);

  //message->print();
  return message;
}

int Xbee::lockMutex(pthread_mutex_t *mutex) {
    int status=pthread_mutex_lock(mutex);
    if(status!=0) printf("error (%s)\n",strerror(status));
    return status;
}

int Xbee::releaseMutex(pthread_mutex_t *mutex) {
    int status=pthread_mutex_unlock(mutex);
    if(status!=0) printf("error (%s)\n",strerror(status));
    return status;
}

ssize_t Xbee::read(char* buf,ssize_t size) {
    ssize_t bytesRead=::read(serial_fd,buf, size);
#ifdef LOGMESSAGES
    logXbee->logBuffer(buf,bytesRead,"Xbee<-");
#endif // LOGMESSAGES
    return bytesRead;
}

int Xbee::rateToConstant(uint32_t baudrate) {
#define B(x) case x: return B##x
	switch(baudrate) {
		B(50);     B(75);     B(110);    B(134);    B(150);
		B(200);    B(300);    B(600);    B(1200);   B(1800);
		B(2400);   B(4800);   B(9600);   B(19200);  B(38400);
		B(57600);  B(115200); B(230400); B(460800); B(500000);
		B(576000); B(921600); B(1000000);B(1152000);B(1500000);
	default: return 0;
	}
#undef B
}

int Xbee::openAndConfigureSerialPort(string dev_name,uint32_t baudrate) {
    printf("Configuring %s for %i\n",dev_name.c_str(),baudrate);

    serial_fd=open(dev_name.c_str(), O_RDWR | O_NOCTTY);

    if (serial_fd<0){
        printf("Unable to open com port %s\n",dev_name.c_str());
        return -1;
    }

    int speed = 0;
    speed = rateToConstant(baudrate);

    if (speed == 0) {
		// Custom divisor
		struct serial_struct serinfo;
		serinfo.reserved_char[0] = 0;
		if (ioctl(serial_fd, TIOCGSERIAL, &serinfo) < 0){
            printf("TIOCGSERIAL error\n");
            return -1;
        }
		serinfo.flags &= ~ASYNC_SPD_MASK;
		serinfo.flags |= ASYNC_SPD_CUST;
		serinfo.custom_divisor = (serinfo.baud_base + (baudrate / 2)) / baudrate;
		if (serinfo.custom_divisor < 1) serinfo.custom_divisor = 1;

		if (ioctl(serial_fd, TIOCSSERIAL, &serinfo) < 0){
            printf("TIOCSSERIAL error\n");
            return -1;
        }
		if (ioctl(serial_fd, TIOCGSERIAL, &serinfo) < 0){
            printf("TIOCGSERIAL error\n");
            return -1;
        }
		if (serinfo.custom_divisor * baudrate != serinfo.baud_base) {
			printf("actual baudrate is %d / %d = %f\n",
			      serinfo.baud_base, serinfo.custom_divisor,
			      (float)serinfo.baud_base / serinfo.custom_divisor);
		}
	}

    //Get the current options for the port...
    struct termios options;
    tcgetattr(serial_fd, &options);

    //set the baud rate
    cfsetospeed(&options, speed ?: B38400);
    cfsetispeed(&options, speed ?: B38400);

    //set the number of data bits.
    options.c_cflag &= ~CSIZE;  // Mask the character size bits
    options.c_cflag |= CS8;

    //set the number of stop bits to 1
    options.c_cflag &= ~CSTOPB;

    //Set parity to None
    options.c_cflag &=~PARENB;

    //set for non-canonical (raw processing, no echo, etc.)
    options.c_iflag = IGNPAR|IGNBRK; // ignore parity check
    options.c_oflag = 0; // raw output
    options.c_lflag = 0; // raw input

    //Time-Outs -- won't work with NDELAY option in the call to open
    options.c_cc[VMIN]  = 200 ;  // block reading until RX x characers. If x = 0, it is non-blocking.
    options.c_cc[VTIME] = 5;   // Inter-Character Timer -- i.e. timeout= x*.1 s

    //Set local mode and enable the receiver
    options.c_cflag |= (CLOCAL | CREAD);

    //Set the new options for the port...
    if (tcsetattr(serial_fd, TCSANOW, &options) != 0){
        printf("Configuring com port failed\n");
        return -1;
    }

    if (tcflush(serial_fd,TCIOFLUSH)==-1) {
        printf("flush failed\n");
        return -1;
    }

    return 0;
}

void Xbee::mutexInit(pthread_mutex_t *mutex) {
    if(pthread_mutex_init(mutex, NULL)!=0) {
        printf("pthread_mutex_init error\n");
    }
}
