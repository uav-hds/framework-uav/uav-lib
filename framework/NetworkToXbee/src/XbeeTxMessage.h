//  created:    2015/06/07
//  filename:   XbeeTxMessage.h
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    define xbee tx message
//
//
/*********************************************************************/

#ifndef XBEETXMESSAGE_H
#define XBEETXMESSAGE_H

#include "Message.h"
#include <vector>
#include <stdint.h>

class XbeeTxMessage {
    public:
        XbeeTxMessage(Message* message,uint16_t destination);
        ~XbeeTxMessage();
        void print(void);
        char* getBuf(int index);
        ssize_t getSize(int index);
        ssize_t getNbBufs(void);
        int getIndex(int frame_id);
        void setAck(int frame_id);
        bool areAllFramesAck(void);

    private:
        std::vector<char*> bufs;
        std::vector<ssize_t> sizes;
        std::vector<bool> acks;
};

#endif // XBEETXMESSAGE_H
