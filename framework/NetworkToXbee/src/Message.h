//  created:    2015/06/05
//  filename:   Message.h
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    define message
//
//
/*********************************************************************/

#ifndef MESSAGE_H
#define MESSAGE_H

#include <sys/types.h>

class Message {
    public:
        typedef enum {
            Udt,
            Udp,
            Tcp,
        } MessageType_t;

        Message(char* buf,ssize_t size,MessageType_t messageType);
        ~Message();
        void print(void);
        char* getBuf(void);
        ssize_t getSize(void);
        MessageType_t getMessagetype(void);

    private:
        char* buf;
        ssize_t size;
        MessageType_t messageType;
};

#endif // MESSAGE_H
