//  created:    2015/06/05
//  filename:   UdpSocket.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    udp socket
//
//
/*********************************************************************/

#include "UdpSocket.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <udt.h>
#include <netdb.h>

using namespace std;

UdpSocket::UdpSocket() {

}

UdpSocket::~UdpSocket() {
    close(socket);
}

bool UdpSocket::isFdEqualsTo(int fd) {
    if(fd==socket) {
        return true;
    } else {
        return false;
    }
}

void UdpSocket::bind(int port) {
    socket = ::socket(AF_INET, SOCK_DGRAM, 0);

    sockaddr_in sin = { 0 };
    sin.sin_addr.s_addr=INADDR_ANY;
    sin.sin_port = htons(port);
    sin.sin_family = AF_INET;
    if(::bind(socket,(sockaddr *) &sin, sizeof sin) == -1) {
        printf("UdpSocket bind: %s\n",strerror(errno));
    }
}


void UdpSocket::connect(string address,int port) {
    socket = ::socket(AF_INET, SOCK_DGRAM, 0); //UDP

    struct hostent *hostinfo;
    hostinfo = gethostbyname(address.c_str());
    if (hostinfo == NULL) {
       printf("UdpSocket error gethostbyname\n");
    }

    sock_in.sin_addr=*(in_addr *) hostinfo->h_addr;
    sock_in.sin_port = htons(port);
    sock_in.sin_family = AF_INET;
}


void UdpSocket::addToEpoll(int eid) {
    int events = UDT_EPOLL_IN;
    UDT::epoll_add_ssock(eid,socket,&events);
}

Message* UdpSocket::getMessage(void) {
    Message* message=NULL;

    char* buf=(char*)malloc(1024*sizeof(char));
    socklen_t sinsize = sizeof(sock_in);
    ssize_t bytesRead=recvfrom(socket, buf,1024*sizeof(char), 0, (sockaddr *) &sock_in, &sinsize);
    if(bytesRead>0) {
        message=new Message(buf,bytesRead,Message::Udp);
    }

    free(buf);

    return message;
}

void UdpSocket::handleMessage(Message* message) {
    //message->print();
    write(message);
}

ssize_t UdpSocket::write(Message* message) {
    ssize_t bytesSent=sendto(socket,message->getBuf(),message->getSize(), 0, (struct sockaddr *) &(sock_in), sizeof(sock_in));
    if(bytesSent!=message->getSize()) {
        printf("erreur envoi %i/%i\n",bytesSent,message->getSize());
    }
    return bytesSent;
}
