//  created:    2015/06/04
//  filename:   UdtSocket.h
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    udt socket
//
//
/*********************************************************************/

#ifndef UDTSOCKET_H
#define UDTSOCKET_H

#include <udt.h>
#include <arpa/inet.h>
#include "Message.h"

class UdtSocket {
    public:
        UdtSocket();
        ~UdtSocket();
        void handleMessage(Message* message);
        void addToEpoll(int eid);
        Message* getMessage(UDTSOCKET socket);
        void listenAndAccept(int port);
        void connect(std::string address,int port);

    private:
        UDTSOCKET file_socket,com_socket;
        int32_t sock_buf_size;

        UDTSOCKET tryConnect(std::string address,int port);
        UDTSOCKET createSocket(void);
        UDTSOCKET accept(UDTSOCKET serv);
        ssize_t write(Message* message);
};

#endif // UDTSOCKET_H
