//  created:    2015/06/05
//  filename:   TcpSocket.h
//
//  author:     Gildas Bayard
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    tcp socket
//
//
/*********************************************************************/

#ifndef TCPSOCKET_H
#define TCPSOCKET_H

#include "Message.h"
#include <string>

class TcpSocket {
    public:
        TcpSocket();
        ~TcpSocket();
        void handleMessage(Message* message);
        void addToEpoll(int eid);
        Message* getMessage(void);
        void listenAndAccept(int port);
        void connect(std::string address,int port);
        bool isFdEqualsTo(int fd);

    private:
        int accept(int serv);
        ssize_t write(Message* message);
        int socket;
};

#endif // TCPSOCKET_H
