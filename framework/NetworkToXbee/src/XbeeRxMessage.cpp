//  created:    2015/06/09
//  filename:   XbeeRxMessage.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    define xbee rx message
//
//
/*********************************************************************/

#include "XbeeRxMessage.h"
#include "XbeeCommon.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

//constantes XBee
#define START_DELIMITER 0x7e
#define TX_REQUEST 0x01
#define TX_STATUS 0x89
#define RX_PACKET 0x81
#define OPTION 0x00
#define LAST_TRAME 0xff
#define XBEE_MAX_SIZE 98

XbeeRxMessage::XbeeRxMessage(Message::MessageType_t type) {
    this->type=type;
    currentFrameId=1;
    buf=NULL;
    buf_size=0;
    frame_count=0;
    number_of_frames=-1;
}

XbeeRxMessage::~XbeeRxMessage() {
    free(buf);
}

void XbeeRxMessage::addFrame(char* buf,ssize_t size) {
    if(getType(buf[0])!=type) {
        printf("type mismatch, expected %i, got %i\n",type,buf[0]);
        printf("todo: a rmettre dans la pile de message\n");//on a recu le message d'une trame suivante alors
        //qu'on a pas fini la trame actuelle
        for(int i=0;i<size;i++) printf("%x ",buf[i]);
        printf("\n");
        return;
    }
    uint8_t frame_id=(uint8_t)buf[1];
/*
     if(getType(buf[0])==Message::Udt) {
        printf("%x %i\n",(uint8_t)buf[0],buf[1]);
        for(int i=2;i<size;i++) printf("%c",buf[i]);
        printf("\n");
     }*/
    ssize_t new_size=(frame_id-1)*MAX_FRAME_SIZE+size-2;
    if(new_size>buf_size) {
        this->buf=(char*)realloc(this->buf,new_size);
        buf_size=new_size;
    }
    memcpy(this->buf+(frame_id-1)*MAX_FRAME_SIZE,buf+2,size-2);

    frame_count++;
    if(isLastFrame(buf[0])) number_of_frames=frame_id;
}

bool XbeeRxMessage::isComplete(void) {
    if(number_of_frames==frame_count) {
        //printf("complete\n");
        return true;
    }else{
        return false;
    }
}

Message* XbeeRxMessage::getMessage(void) {
    if(isComplete()) {
        return new Message(buf,buf_size,type);
    } else {
        return NULL;
    }

}

void XbeeRxMessage::print(void) {
    for(int i=0;i<buf_size;i++) printf("%c",buf[i]);
    printf("\n");
}
