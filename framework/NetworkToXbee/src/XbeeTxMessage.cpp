//  created:    2015/06/07
//  filename:   XbeeTxMessage.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    define xbee tx message
//
//
/*********************************************************************/

#include "XbeeTxMessage.h"
#include "XbeeCommon.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

XbeeTxMessage::XbeeTxMessage(Message* message,uint16_t destination) {
    char* ptr=message->getBuf();
    ssize_t remaining_datas=message->getSize();
    uint8_t id_trame=0;

    while(remaining_datas!=0) {
        ssize_t frame_size,payload_size;
        if(remaining_datas>MAX_FRAME_SIZE) {
            payload_size=MAX_FRAME_SIZE;
        }else{
            payload_size=remaining_datas;
        }
        remaining_datas-=payload_size;
        frame_size=payload_size+11;//header and checksum
        //printf("frame %i payload %i remaining %i\n",frame_size,payload_size,remaining_datas);
        char* buf=(char*)malloc(frame_size);

        id_trame++;
        if(id_trame==0) {
            printf("error message is too long!");
            //how to handle this?
        }

        buf[0]=START_DELIMITER;
        buf[1]=0;//MSB, size 0 as payload max=100 bytes
        buf[2]=frame_size-4;//LSB size=api specific strcuture length
        //checksum begins here
        buf[3]=TX_REQUEST;
        buf[4]=id_trame;
        buf[5]=destination>>8;
        buf[6]=destination&0xff;
        buf[7]=OPTION;
        buf[8]=message->getMessagetype();
        if(remaining_datas==0) buf[8]|=LAST_FRAME;
        buf[9]=id_trame;

        memcpy(&buf[10],ptr,payload_size);
        ptr+=payload_size;

        buf[10+payload_size]=calcChecksum(&buf[3],payload_size+7);
        bufs.push_back(buf);
        sizes.push_back(frame_size);
        acks.push_back(false);
    }
}

XbeeTxMessage::~XbeeTxMessage() {
    for(int i=0;i<bufs.size();i++) {
        free(bufs.at(i));
    }
}

void XbeeTxMessage::setAck(int frame_id) {
    acks.at(getIndex(frame_id))=true;
}

bool XbeeTxMessage::areAllFramesAck(void) {
    for(int i=0;i<acks.size();i++) {
        if(!acks.at(i)) return false;
    }
    return true;
}

char* XbeeTxMessage::getBuf(int index) {
    return bufs.at(index);
}

ssize_t XbeeTxMessage::getSize(int index) {
    return sizes.at(index);
}

int XbeeTxMessage::getIndex(int frame_id) {
    return frame_id-1;
}

ssize_t XbeeTxMessage::getNbBufs(void) {
    return bufs.size();
}
void XbeeTxMessage::print(void) {
    for(int i=0;i<bufs.size();i++) {
        printf("trame %i/%i %i\n",i,bufs.size(),sizes.at(i));

        for(int j=0;j<sizes.at(i);j++) {
            printf("%x ",(uint8_t)bufs.at(i)[j]);
        }
        printf("\n");
    }
}
