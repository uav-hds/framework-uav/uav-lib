//  created:    2015/06/05
//  filename:   TcpSocket.cpp
//
//  author:     Gildas Bayard
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    tcp socket
//
//
/*********************************************************************/

#include "TcpSocket.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <udt.h>
#include <fcntl.h>

using namespace std;

TcpSocket::TcpSocket() {

}

TcpSocket::~TcpSocket() {
    close(socket);
}

bool TcpSocket::isFdEqualsTo(int fd) {
    if(fd==socket) {
        return true;
    } else {
        return false;
    }
}

void TcpSocket::listenAndAccept(int port) {
    int serv=::socket(AF_INET,SOCK_STREAM,0);

    sockaddr_in my_addr;
    my_addr.sin_family = AF_INET;
    my_addr.sin_port = htons(port);
    my_addr.sin_addr.s_addr=INADDR_ANY;

    memset(&(my_addr.sin_zero), '\0', 8);

    if (bind(serv,(sockaddr*)&my_addr,sizeof(my_addr))<0) {
        printf("TcpSocket bind: %s\n",strerror(errno));
    }

    printf("TcpSocket: listenning on port %i\n",port);
    ::listen(serv,1);

    socket=accept(serv);
    printf("TcpSocket: connected on port %i\n",port);
}

int TcpSocket::accept(int serv) {
    sockaddr_in their_addr;
    socklen_t namelen = sizeof(their_addr);
    int socket;

    if ((socket=::accept(serv,(sockaddr*)&their_addr, &namelen))<0) {
        printf("TCP accept: %s\n",strerror(errno));
    }

    return socket;
}

void TcpSocket::connect(string address,int port) {
    bool success=false;
    int timeout=100;
    int flags=0;

    printf("TcpSocket: connecting to %s:%i\n",address.c_str(),port);
    while(!success) {
        success=true;
        socket=::socket(AF_INET,SOCK_STREAM,0);

        sockaddr_in serv_addr;
        serv_addr.sin_family = AF_INET;
        serv_addr.sin_port = htons(short(port));
        if (inet_pton(AF_INET, address.c_str(), &serv_addr.sin_addr) <= 0) {
            printf("TcpSocket: incorrect network address.");
            success=false;
        }
        memset(&(serv_addr.sin_zero), '\0', 8);

        //go non blocking
        fcntl(socket, F_SETFL, flags | O_NONBLOCK);
        if (::connect(socket, (sockaddr*)&serv_addr, sizeof(serv_addr))==-1) {
            if (errno != EINPROGRESS) {
                printf("TcpSocket: connect %s\n",strerror(errno));
                success=false;
            } else {
                //now block with a timeout
                struct timeval tv;
                tv.tv_sec=timeout/1000; //timeout is in ms
                tv.tv_usec=(timeout%1000)*1000;
                fd_set rset;
                FD_ZERO(&rset);
                FD_SET(socket, &rset);
                fd_set wset = rset;
                int ret=select(socket + 1, NULL, &wset, NULL,(timeout==0)?NULL:&tv); //man 2 connect EINPROGRESS
                if (ret<0) {
                    printf("TcpSocket: select %s\n",strerror(errno));
                    success=false;
                } else {
                    if (ret==0) {
                        //timeout reached
                        printf("TcpSocket: timeout reached\n");
                        success=false;
                    } else {
                        //something happened on our socket. Check if an error occured
                        int error;
                        socklen_t len = sizeof(error);
                        if (getsockopt(socket, SOL_SOCKET, SO_ERROR, &error, &len)!=0) {
                            printf("TcpSocket: getsockopt %s\n",strerror(errno));
                            success=false;
                        }
                        if (error!=0) {
                            //printf("TcpSocket: socket error: %d(%s)\n",error,strerror(error));
                            success=false;
                        }
                    }
                }
            }
        }
        if (!success) {
            close(socket);
        }
    }
    //switch back to blocking mode (default)
    fcntl(socket, F_SETFL, flags);

    printf("TcpSocket: connected to %s:%i\n",address.c_str(),port);
}


void TcpSocket::addToEpoll(int eid) {
    int events = UDT_EPOLL_IN;
    UDT::epoll_add_ssock(eid,socket,&events);
}

Message* TcpSocket::getMessage(void) {
    Message* message=NULL;

    char* buf=(char*)malloc(1024*sizeof(char));
    ssize_t bytesRead=recv(socket,buf,1024*sizeof(char),MSG_DONTWAIT);

    if(bytesRead>0) {
        message=new Message(buf,bytesRead,Message::Tcp);
    }

    free(buf);

    return message;
}

void TcpSocket::handleMessage(Message* message) {
    //message->print();
    write(message);
}

ssize_t TcpSocket::write(Message* message) {
    ssize_t bytesSent=send(socket, message->getBuf(), message->getSize(), MSG_DONTWAIT);
    if(bytesSent!=message->getSize()) {
        printf("erreur envoi %i/%i\n",bytesSent,message->getSize());
    }
    return bytesSent;
}
