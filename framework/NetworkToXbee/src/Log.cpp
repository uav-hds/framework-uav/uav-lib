#include "Log.h"
#include <sys/time.h>
#include <stdint.h>

using namespace std;

Log::Log(const char *prefixString) {
    char dateString[128];
    formatedDate(dateString,128,"%d-%m-%Y_%I:%M:%S");
    char pathString[128];
//    strncpy(pathString,"/var/log/network2xbee_",128);
    strncpy(pathString,prefixString,128);
    strncat(pathString,dateString,128);
    printf("Log file created\n");
    logFile.open(pathString);
}

Log::~Log() {
    logFile.close();
}


void Log::formatedDate(char *dateString,size_t size,const char* format) {
    time_t rawtime;
    struct tm* timeinfo;
    time(&rawtime);
    timeinfo=localtime(&rawtime);
    strftime(dateString,size,format,timeinfo);
}

void Log::logBuffer(char *buf,size_t size,const char *transmissionDirection) {
    //write the message received and the timestamp
    struct timeval tv;
    gettimeofday(&tv,NULL);
    char epochString[128];
    snprintf(epochString,128,"%d %d",tv.tv_sec,tv.tv_usec);
    logFile << epochString << " " << transmissionDirection;
    logFile << "|";
    for (int i=0; i<size;i++) {
        unsigned char c=buf[i];
        logFile << hex << (unsigned int)c << " ";
    }
    logFile << endl;
}

void Log::logMessage(Message* message, const char *transmissionDirection) {
    logBuffer(message->getBuf(),message->getSize(),transmissionDirection);
}
